{-# LANGUAGE OverloadedStrings #-}
module OrdersSpec (spec) where


import           Data.Function ((&))
import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec = do

    it "addOrder should append orders" $
        (emptyOrders
                & addOrder OrderSurface
                & addOrder (OrderTorpedo (point 5 12))
                & addOrder (OrderTriggerMine (point 3 8))
                & renderOrders) `shouldBe` "SURFACE | TORPEDO 5 12 | TRIGGER 3 8"


    it "orders should be able to render" $
        let
            render items
                = items
                & ordersWith
                & renderOrders
        in do
            render [OrderMove North Torpedo] `shouldBe` "MOVE N TORPEDO"
            render [OrderMove East Silence] `shouldBe` "MOVE E SILENCE"
            render [OrderMove South Mine] `shouldBe` "MOVE S MINE"
            render [OrderMove West Sonar] `shouldBe` "MOVE W SONAR"

            render [OrderSilence South 3] `shouldBe` "SILENCE S 3"
            render [OrderTorpedo (point 4 7)] `shouldBe` "TORPEDO 4 7"
            render [OrderSonar (Sector 4)] `shouldBe` "SONAR 4"
            render [OrderSurface] `shouldBe` "SURFACE"
            render [OrderDropMine West] `shouldBe` "MINE W"
            render [OrderTriggerMine (point 3 8)] `shouldBe` "TRIGGER 3 8"
            render [OrderMessage "sector clear"] `shouldBe` "MSG sector clear"
            render [OrderSonar (Sector 4), OrderSurface] `shouldBe` "SONAR 4 | SURFACE"

    it "orders should be parsed" $ do
        parseOrders "MSG Hunting | MOVE N TORPEDO" `shouldBe` ordersWith [
            OrderMessage "Hunting", OrderMove North Torpedo]

        parseOrders "MOVE E SILENCE | SONAR 4 | TRIGGER 4 6" `shouldBe` ordersWith [
            OrderMove East Silence, OrderSonar (Sector 4), OrderTriggerMine (point 4 6)]

        parseOrders "MOVE S MINE | SURFACE | MINE W" `shouldBe` ordersWith [
            OrderMove South Mine, OrderSurface, OrderDropMine West]

        parseOrders "MOVE W SONAR | SILENCE N 2" `shouldBe` ordersWith [
            OrderMove West Sonar, OrderSilence North 2]

        parseOrders "MSG Hunting | unknown" `shouldBe` emptyOrders


    it "orders render/parser should be opposite operation" $
        let text = "MSG Hunting | MOVE W TORPEDO | SURFACE | SILENCE E 3 | TORPEDO 3 10 | MINE N | TRIGGER 3 10 | SONAR 2"
        in (text & parseOrders & renderOrders) `shouldBe` text


    it "explosionsInOrders should find all explosions in orders" $
        let
            check_explosion_positions items expected
                = explosionsInOrders (ordersWith items) `shouldMatchList` expected
        in do

            check_explosion_positions
                [OrderSurface, OrderTorpedo (point 5 12), OrderTriggerMine (point 3 8)]
                [point 5 12, point 3 8]

            check_explosion_positions
                [OrderSonar (Sector 4), OrderSilence South 3]
                []


    it "sonarInOrders should find sonar order" $
        let
            check_sonar items expected
                = sonarInOrders (ordersWith items) `shouldBe` expected
        in do

            check_sonar
                [OrderSurface, OrderTorpedo (point 5 12), OrderTriggerMine (point 3 8)]
                Nothing

            check_sonar
                [OrderSonar (Sector 4), OrderSilence South 3]
                (Just (Sector 4))


    it "surfaceInOrders should find surface order" $
        let
            check_surface items expected
                = surfaceInOrders (ordersWith items) `shouldBe` expected
        in do

            check_surface
                [OrderSurface, OrderTorpedo (point 5 12), OrderTriggerMine (point 3 8)]
                True

            check_surface
                [OrderSonar (Sector 4), OrderSilence South 3]
                False
