# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from itertools import chain
from typing import Callable, FrozenSet, Iterator, List, Optional, Set, Text

from constants import ExplosionDelta
from decorators import memoize_method
from direction import Direction
from geometry import Position, Rect
from pathfinder import astar_search, search_distance


@dataclass(frozen=True)
class Targets:
    safe: FrozenSet[Position]
    danger: FrozenSet[Position]

    def all(self):
        return self.safe | self.danger


class WorldMap:
    def __init__(self, width: int, height: int):
        self.area: Rect = Rect.to(Position(width, height))
        self.islands: Set[Position] = set()
        self.water: Set[Position] = set(self.area)

    def walk(self) -> Iterator[Position]:
        return iter(self.area)

    def set_island(self, position: Position) -> None:
        self.islands.add(position)
        self.water.remove(position)

    def to_sector(self, position: Position) -> int:
        return (position.x // 5 + (position.y // 5) * (self.area.width() // 5)) + 1

    def is_water(self, position: Position) -> bool:
        return position in self.water

    @memoize_method
    def all_water(self) -> Set[Position]:
        return self.water

    @memoize_method
    def neighbors(self, position: Position) -> List[Position]:
        return [p for p in [d.move(position) for d in Direction] if self.is_water(p)]

    @memoize_method
    def find_biggest_water_area(self) -> Rect:
        result = [self.area]
        for island in self.islands:
            result = list(chain.from_iterable(r.split(island) for r in result))
        return sorted(result, key=lambda r: r.area(), reverse=True)[0]

    @memoize_method
    def distance(self, start: Position, goal: Position) -> int:
        found, _, distance = astar_search(start, goal, self.neighbors, Position.distance)
        if found:
            return distance
        return -1

    @memoize_method
    def possible_targets(self, position: Position) -> Targets:
        costs = search_distance(position, 4, self.neighbors)
        explosion = self.explosion_range(position)
        return Targets(
            frozenset(target for target in costs if target not in explosion),
            frozenset(target for target in costs if target in explosion))

    @memoize_method
    def explosion_range(self, position: Position) -> Set[Position]:
        result = set()
        for delta in ExplosionDelta:
            result.add(position + delta)
        return result & self.all_water()

    def render(self, symbols: Callable[[Position], Optional[Text]] = None) -> Text:
        def display(position: Position) -> Text:
            if symbols:
                return (symbols(position) or "•") + " "
            return "• "

        topline = "┏" + "┳".join(["━━━━━━━━━━━"] * (self.area.width() // 5)) + "┓\n"
        middleline = "┣" + "╋".join(["━━━━━━━━━━━"] * (self.area.width() // 5)) + "┫\n"
        bottomline = "┗" + "┻".join(["━━━━━━━━━━━"] * (self.area.width() // 5)) + "┛\n"

        result = topline
        for y in range(self.area.height()):
            if y % 5 == 0 and y != 0:
                result += middleline
            for x in range(self.area.width()):
                position = Position(x, y)
                if x % 5 == 0:
                    result += "┃ "
                if not self.is_water(position):
                    result += "▲ "
                else:
                    result += display(position)
            result += "┃\n"
        return result + bottomline
