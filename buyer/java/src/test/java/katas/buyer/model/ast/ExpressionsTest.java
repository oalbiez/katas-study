package katas.buyer.model.ast;

import katas.buyer.model.*;
import org.junit.Test;

import java.math.BigDecimal;

import static katas.buyer.model.ast.Describe.describe;
import static katas.buyer.model.ast.Expressions.*;
import static org.junit.Assert.assertEquals;

public class ExpressionsTest {
    @Test
    public void playing_with_describing_expression() {
        assertEquals(
                "<offer price> * <offer quantity> / <requested quantity>",
                describe(div(mul(offerPrice(), offerQuantity()), requestedQuantity())));
    }

    @Test
    public void evaluate() {
        assertEquals(
                new BigDecimal("4.00"),
                div(mul(offerPrice(), offerQuantity()), requestedQuantity()).accept(new Evaluate(PurchaseRequest.of(Quantity.of(10)), Offer.of(Vendor.of("Anonymous"), Quantity.of(20), Price.of("2.00")))));
    }
}