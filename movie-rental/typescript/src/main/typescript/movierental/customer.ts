import { Movie } from "./movie";
import { add, Price } from "./price";
import { Rental } from "./rental";

export class Customer {
    private name: string;
    private rentals: Rental[] = [];

    public constructor(name: string) {
        this.name = name;
    }

    public addRental(arg: Rental) {
        this.rentals.push(arg);
    }

    public getName(): string {
        return this.name;
    }

    public totalAmount(): Price {
        return this.rentals
            .map((each) => each.charge())
            .reduce(add, Price.euro(0));
    }

    public frequentRenterPoints(): number {
        return this.rentals
            .map((each) => each.frequentRenterPoints())
            .reduce((left, right) => left + right, 0);
    }

    public statement(): string {
        let result = "Rental Record for " + this.getName() + "\n";
        for (const each of this.rentals) {
            result += "\t" + each.getTitle() + "\t" + each.charge().render() + "\n";
        }
        result += "Amount owed is " + this.totalAmount().render() + "\n";
        result += "You earned " + this.frequentRenterPoints() + " frequent renter points";
        return result;
    }
}
