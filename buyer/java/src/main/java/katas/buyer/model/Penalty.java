package katas.buyer.model;

public final class Penalty {
    private final Price penalty;

    public static Penalty of(final double amount) {
        return new Penalty(Price.of(amount));
    }

    public static Penalty of(final Price penalty) {
        return new Penalty(penalty);
    }

    private Penalty(final Price penalty) {
        this.penalty = penalty;
    }

    public TimedPenalty after(final Delay limit) {
        return new TimedPenalty(penalty, limit);
    }
}
