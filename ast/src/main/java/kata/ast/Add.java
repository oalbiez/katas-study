package kata.ast;

public class Add implements AST {
    private final AST left;
    private final AST right;

    public Add(AST left, AST right) {
        this.left = left;
        this.right = right;
    }

    public <T> T accept(ASTVisitor<T> visitor) {
        return visitor.visitAdd(this);
    }

    public AST left() {
        return left;
    }

    public AST right() {
        return right;
    }
}
