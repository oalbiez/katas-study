#!/usr/bin/python
"""Jeu du pendu.

Usage:
  hangman_text [options]

Options:
  -h --help       Affiche l'aide.
  --errors=<max>  Nombre d'erreur max avant de perdre [default: 3].
  --style=<impl>  Selectionne le style de programmation utilisé [default: oop]
"""
from docopt import docopt


if __name__ == "__main__":
    arguments = docopt(__doc__, version='Hangman 1.0')
    if arguments["--style"] == "oop":
        from console.functional import run
        print("Using object oriented implementation")
    if arguments["--style"] == "fp":
        from console.functional import run
        print("Using functional implementation")
    run(int(arguments["--errors"]))
