package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.srp.ast.Transactions;
import katas.transactions.value.Value;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static java.util.Arrays.stream;
import static katas.transactions.srp.ast.Transactions.amount;
import static katas.transactions.srp.statistics.Statistics.*;
import static katas.transactions.value.Values.value;
import static org.junit.Assert.assertEquals;

public class GroupTest {
    @Test
    public void should_group_by_client__two_transaction_one_client() {
        assertEquals(value(2), forClient("SG", transaction("SG"), transaction("SG")));
    }

    @Test
    public void should_group_by_client__two_transaction_two_client() {
        assertEquals(value(1), forClient("SG", transaction("SG"), transaction("BNP")));
    }

    @Test
    public void should_group_by_client__client_missing() {
        Assert.assertEquals(value(0), forClient("BNP", transaction("SG")));
    }

    @Test
    public void should_describe() {
        assertEquals("transaction count by client", group(Transactions.byClient(), count()).describe());
        assertEquals("sum of amount by client", group(Transactions.byClient(), sum(amount())).describe());
    }

    private Transaction transaction(final String client) {
        return Transaction.transaction(client, "A", 100, LocalDate.of(2017, Month.APRIL, 17).atStartOfDay());
    }

    private Value forClient(final String client, final Transaction... transactions) {
        final Group stat = group(Transactions.byClient(), count());
        stream(transactions).forEach(stat::updateWith);
        return stat.statisticFor(client).value();
    }
}
