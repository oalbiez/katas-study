package katas.birthdaygreetings.business;

import com.google.common.base.Objects;

import static java.lang.Character.isUpperCase;

public final class Name {
    private final String value;

    public static Name name(final String value) {
        if (!isUpperCase(value.codePointAt(0))) {
            throw new IllegalArgumentException(value + " is not a valid name");
        }
        return new Name(value);
    }

    private Name(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        return Objects.equal(value, ((Name) other).value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }
}
