# -*- coding: utf-8 -*-

from direction import Direction
from geometry import Position
from orders import Orders, Powers
from orders import DropMine, Message, Move, Silence, Sonar, Surface, Torpedo, TriggerMine


def orders(*commands):
    return Orders.of(*commands)


def test_order_should_be_able_to_render():
    def render(*commands):
        return orders(*commands).render()

    assert render(Move(Direction.North, Powers.Torpedo)) == "MOVE N TORPEDO"
    assert render(Move(Direction.East, Powers.Silence)) == "MOVE E SILENCE"
    assert render(Silence(Direction.South, 3)) == "SILENCE S 3"
    assert render(Torpedo(Position(4, 7))) == "TORPEDO 4 7"
    assert render(Sonar(4)) == "SONAR 4"
    assert render(Surface()) == "SURFACE"
    assert render(DropMine(Direction.West)) == "MINE W"
    assert render(TriggerMine(Position(3, 8))) == "TRIGGER 3 8"
    assert render(Message("sector clear")) == "MSG sector clear"
    assert render(Sonar(4), Surface()) == "SONAR 4|SURFACE"


def test_orders_should_find_all_explosion_positions():
    def check_explosion_positions(given_orders, expected):
        assert given_orders.explosion_positions() == set(expected)

    check_explosion_positions(
        orders(Torpedo(Position(5, 12)), TriggerMine(Position(3, 8))),
        (Position(5, 12), Position(3, 8)))

    check_explosion_positions(
        orders(Silence(Direction.South, 0), Sonar(2)),
        ())


def test_orders_should_find_sonar():
    assert orders(Silence(Direction.South, 0), Sonar(2)).sonar() == Sonar(2)
    assert orders(Surface()).sonar() is None


def test_representation_of_order_should_be_python():
    # pylint: disable=eval-used
    value = orders(
        Move(Direction.North, Powers.Torpedo),
        Silence(Direction.South, 0),
        Torpedo(Position(5, 12)),
        Sonar(2),
        Surface(),
        DropMine(Direction.West),
        TriggerMine(Position(3, 8)),
        Message("Hunting"))
    assert eval(repr(value)) == value


def test_orders_should_be_ordered():
    assert Orders.empty().add(Sonar(4)).add(Surface()) == orders(Sonar(4), Surface())


def test_orders_should_be_iterable():
    assert len(list(orders(Sonar(4), Surface()))) == 2
