from getpass import getpass
from hangman.errors import WordTooSmall, WordWithInvalidLetter
from hangman import functional as hm


def input_word_to_guess():
    while True:
        try:
            return hm.normalize_word_to_guess(getpass("Entrez le mot à deviner:"))
        except WordTooSmall:
            print("Le mot à deviner doit contenir au moins 4 lettres")
        except WordWithInvalidLetter:
            print("Le mot à deviner ne doit contenir que des lettres et sans accents")


def display_status(state):
    print('\nMot à trouver: %(puzzle)s, vous avez fait  %(errors)i erreure(s)' % {
        'errors': hm.errors(state),
        'puzzle': hm.puzzle(state)})


def display_final(state):
    if hm.is_won(state):
        print('\nGagné avec %(errors)i erreure(s). Le mot était %(puzzle)s.' % {
            'errors': hm.errors(state),
            'puzzle': state.word_to_guess})
    if hm.is_lost(state):
        print('\nPerdu. Le mot était %(puzzle)s.' % {
            'puzzle': state.word_to_guess})


def user_guess(state):
    value = input("Entrez une lettre ou un mot: ")
    if len(value) == 1:
        return hm.test_a_letter(state, value)
    return hm.test_a_word(state, value)


def run(max_errors):
    hangman = hm.new_state(input_word_to_guess(), max_errors)
    while not (hm.is_won(hangman) or hm.is_lost(hangman)):
        display_status(hangman)
        hangman = user_guess(hangman)
    display_final(hangman)


__all__ = ["run"]
