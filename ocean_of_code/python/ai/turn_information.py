# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from textwrap import dedent
from typing import Text

from geometry import Position
from intelligence_report import IntelligenceReport
from sonar_result import SonarResult
from subsystem import Subsystem


@dataclass(frozen=True)
class TurnInformations:
    my_position: Position
    my_life: int
    my_subsystem: Subsystem
    my_sonar: SonarResult
    opponent_life: int
    intelligence: IntelligenceReport

    def __repr__(self) -> Text:
        return dedent(f"""
        TurnInformations(
            my_position={repr(self.my_position)},
            my_life={repr(self.my_life)},
            my_subsystem={repr(self.my_subsystem)},
            my_sonar={repr(self.my_sonar)},
            opponent_life={repr(self.opponent_life)},
            intelligence={repr(self.intelligence)}
        )
            """)
