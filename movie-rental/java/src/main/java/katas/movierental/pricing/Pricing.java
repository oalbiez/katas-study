package katas.movierental.pricing;

import com.google.common.collect.Lists;
import katas.movierental.Duration;
import katas.movierental.Price;

import java.util.List;

import static java.util.stream.Collectors.joining;
import static katas.movierental.Price.euro;

public class Pricing {
    private static final ValueDriver PRICE_DRIVER = new ValueDriver() {
        public Value zero() {
            return new PriceValue(euro(0));
        }

        public Value add(Value left, Value right) {
            return new PriceValue(fromValue(left).add(fromValue(right)));
        }

        public Value mul(Value left, double right) {
            return new PriceValue(fromValue(left).mul(right));
        }
    };


    private List<Rule> rules = Lists.newArrayList();

    private Pricing(Rule rule) {
        rules.add(rule);
    }

    public static Pricing pricing(Rule rule) {
        return new Pricing(rule);
    }

    public static Rule cost(Price price) {
        return new FlatRule(new PriceValue(price));
    }

    public Pricing then(Rule rule) {
        rules.add(rule);
        return this;
    }

    public Price chargeFor(Duration duration) {
        return fromValue(new PriceCalculator(rules, duration, PRICE_DRIVER).total());
    }

    public String describe() {
        return rules.stream().map(Rule::describe).collect(joining(" then "));
    }

    public String explain(Duration duration) {
        return new PriceCalculator(rules, duration, PRICE_DRIVER).audit();
    }

    private static class PriceValue implements Value {
        private final Price price;

        PriceValue(Price price) {
            this.price = price;
        }

        public Price price() {
            return price;
        }

        @Override
        public String toString() {
            return price.toString();
        }
    }

    private static Price fromValue(final Value value) {
        return ((PriceValue) value).price();
    }
}


