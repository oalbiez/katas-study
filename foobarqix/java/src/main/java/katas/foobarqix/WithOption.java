package katas.foobarqix;

import io.vavr.control.Option;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static io.vavr.control.Option.none;
import static java.lang.String.valueOf;

/**
 */
public class WithOption {

    private static Option<String> foo(final int value) {
        return value % 3 == 0 ? Option.of("Foo") : none();
    }

    private static Option<String> bar(final int value) {
        return value % 5 == 0 ? Option.of("Bar") : none();
    }

    private static Option<String> qix(final int value) {
        return value % 7 == 0 ? Option.of("Qix") : none();
    }

    @SuppressWarnings("unchecked")
    public static String fooBarQix(final int input) {
        List<Option<String>> rules = newArrayList(foo(input), bar(input), qix(input));
        String result = rules.stream().filter(Option::isDefined).map(Option::get).collect(Collectors.joining());
        return result.isEmpty() ? valueOf(input) : result;
    }
}
