# Bottle challenge

Trois bouteilles de capacité différentes: 3L, 5L et 8L.
Les deux premières sont pleines, le dernière est vide.
Quel ordre de transvasement (sans perte de liquide) il faut réaliser pour obtenir 4L dans une bouteille ?
