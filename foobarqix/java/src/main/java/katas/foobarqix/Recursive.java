package katas.foobarqix;

import com.google.common.collect.Iterables;

import java.util.List;

import static java.lang.String.valueOf;
import static katas.foobarqix.Recursive.Pair.pair;

/**
 * Recursive implementation.
 */
public class Recursive {

    private static final List<Pair<Integer, String>> RULES = List.of(new Pair<>(3, "Foo"), new Pair<>(5, "Bar"), new Pair<>(7, "Qix"));

    public static String fooBarQix(final int input) {
        Pair<String, Boolean> pair = recursive(input, RULES);
        return pair.second() ? pair.first() : valueOf(input);
    }

    private static Pair<String, Boolean> recursive(final int value, final List<Pair<Integer, String>> rules) {
        if (rules.isEmpty()) {
            return pair("", false);
        }
        Pair<String, Boolean> current = applyRule(value, head(rules));
        Pair<String, Boolean> next = recursive(value, tail(rules));
        return pair(current.first() + next.first(), current.second() || next.second());
    }

    private static Pair<String, Boolean> applyRule(final int value, final Pair<Integer, String> rule) {
        if (isDivisibleBy(value, rule.first())) {
            return pair(rule.second(), true);
        }
        return pair("", false);
    }

    private static List<Pair<Integer, String>> tail(List<Pair<Integer, String>> rules) {
        return rules.subList(1, rules.size());
    }

    private static Pair<Integer, String> head(List<Pair<Integer, String>> rules) {
        return Iterables.get(rules, 0);
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }

    static final class Pair<first, second> {
        private first first;
        private second second;

        static <first, second> Pair<first, second> pair(first first, second second) {
            return new Pair<>(first, second);
        }

        private Pair(first first, second second) {
            this.first = first;
            this.second = second;
        }

        first first() {
            return this.first;
        }

        second second() {
            return this.second;
        }
    }
}
