package katas.wallet;

import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static katas.wallet.Currency.EUR;
import static katas.wallet.FixedRateProvider.fixed;
import static katas.wallet.FixedRateProvider.rates;
import static katas.wallet.Price.price;
import static org.junit.Assert.assertEquals;


public class WalletGherkinTest {
    private Wallet wallet = wallet();
    private RateProvider rateProvider = fixed();
    private Price price = price(EUR, 0);

    @Test
    public void empty_wallet_should_value_zero_in_euro() {
        given_an_empty_wallet();
        when_I_evaluate_in_EUR();
        then_the_price_should_be("EUR 0.00");
    }

    @Test
    public void wallet_with_stock_in_euros_should_sum_of_values_in_euro() {
        given_a_wallet_with("EUR 100", "EUR 200");
        when_I_evaluate_in_EUR();
        then_the_price_should_be("EUR 300.00");
    }

    @Test
    public void wallet_with_stock_in_different_currency_should_use_change() {
        given_a_wallet_with("USD 200");
        and_exchange_rates("USD/EUR 0.5");
        when_I_evaluate_in_EUR();
        then_the_price_should_be("EUR 100.00");
    }

    @Test
    public void wallet_with_stock_in_different_currencies_should_use_change() {
        given_a_wallet_with("EUR 10", "DZD 50", "KRW 200", "XBT 0.00002354");
        and_exchange_rates("DZD/EUR 0.01", "KRW/EUR 0.001", "XBT/EUR 4000");
        when_I_evaluate_in_EUR();
        then_the_price_should_be("EUR 10.79");
    }

    @Test
    public void wallet_should_round_the_whole_sum() {
        given_a_wallet_with("KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1");
        and_exchange_rates("KRW/EUR 0.001");
        when_I_evaluate_in_EUR();
        then_the_price_should_be("EUR 0.01");
    }

    private void given_an_empty_wallet() {
    }

    private void given_a_wallet_with(String... wallet) {
        this.wallet = wallet(wallet);
    }

    private void and_exchange_rates(String... rates) {
        rateProvider = rates(rates);
    }

    private void when_I_evaluate_in_EUR() {
        price = wallet.valuateIn(EUR, rateProvider);
    }

    private void then_the_price_should_be(final String price) {
        assertEquals(price, this.price.toString());
    }

    private static Wallet wallet(final String... stocks) {
        return Wallet.wallet(Stream.of(stocks).map(Price::price).collect(Collectors.toList()));
    }
}
