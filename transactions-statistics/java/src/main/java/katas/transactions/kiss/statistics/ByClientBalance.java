package katas.transactions.kiss.statistics;

import katas.transactions.Transaction;

import java.util.Collection;
import java.util.Map;

public final class ByClientBalance implements Statistic {
    private final ByClient<Balance> balance = new ByClient<>(Balance::new);

    public void updateWith(final Transaction transaction) {
        balance.updateWith(transaction);
    }

    public Balance statisticFor(final String client) {
        return balance.statisticFor(client);
    }

    public Map<String, Balance> getStatistics() {
        return balance.getStatistics();
    }

    public Collection<String> clients() {
        return balance.clients();
    }
}
