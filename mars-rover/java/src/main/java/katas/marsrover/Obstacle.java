package katas.marsrover;


import com.google.common.base.Objects;

public class Obstacle {
    private final Position position;
    private final String description;

    public static Obstacle obstacle(Position position, String description) {
        return new Obstacle(position, description);
    }

    public boolean isLocatedAt(Position position) {
        return Objects.equal(this.position, position);
    }

    private Obstacle(Position position, String description) {
        this.position = position;
        this.description = description;
    }
}
