package katas.foobarqix;

public class WithEnumFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return WithEnum.fooBarQix(input);
    }
}
