package katas.marsrover;

public interface Move {
    Position finalPosition();

    Move move(final Command command);
}
