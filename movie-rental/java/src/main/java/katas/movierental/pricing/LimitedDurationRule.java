package katas.movierental.pricing;

import katas.movierental.Duration;

final class LimitedDurationRule implements Rule {
    private final Duration duration;
    private final Rule inner;

    LimitedDurationRule(Duration duration, Rule inner) {
        this.duration = duration;
        this.inner = inner;
    }

    public Rule perDay() {
        throw new MalformedPricing("cannot apply perDay after a for_ rule");
    }

    public Rule for_(Duration duration) {
        throw new MalformedPricing("cannot apply for_ more than one on a rule");
    }

    public Value priceFor(Duration duration, ValueDriver driver) {
        return inner.priceFor(Duration.min(duration, this.duration), driver);
    }

    public Duration consume(Duration remaining) {
        return Duration.sub(remaining, duration);
    }

    public String describe() {
        return inner.describe() + " for " + duration.value() + " day(s)";
    }
}
