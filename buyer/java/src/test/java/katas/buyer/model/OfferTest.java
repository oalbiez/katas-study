package katas.buyer.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OfferTest {
    @Test
    public void offer_should_evaluate_normalizing_price_by_quantities() {
        assertEquals("EUR 3.00",
                evaluate(
                        offer(Quantity.of(10), Price.of(3)),
                        Quantity.of(10), Delay.of("10 days"), Penalty.of(100)));

        assertEquals("EUR 6.00",
                evaluate(
                        offer(Quantity.of(20), Price.of(3)),
                        Quantity.of(10), Delay.of("10 days"), Penalty.of(100)));
    }

    @Test
    public void offer_should_evaluate_with_cost_of_delay() {
        assertEquals("EUR 103.00",
                evaluate(
                        offer(Quantity.of(10), Price.of(3), Delay.of("30 days")),
                        Quantity.of(10), Delay.of("20 days"), Penalty.of(100)));
    }


    private Offer offer(final Quantity quantity, final Price price) {
        return offer(quantity, price, Delay.none());
    }

    private Offer offer(final Quantity quantity, final Price price, final Delay delay) {
        return Offer.of(Vendor.of("Anonymous"), quantity, price, delay);
    }

    private static String evaluate(final Offer offer, final Quantity quantity, final Delay delay, final Penalty penalty) {
        return offer.evaluateFor(quantity, penalty.after(delay)).toString();
    }
}