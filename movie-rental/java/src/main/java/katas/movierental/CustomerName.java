package katas.movierental;

public class CustomerName {
    private final String value;

    public static CustomerName name(String value) {
        return new CustomerName(value);
    }

    private CustomerName(String value) {
        this.value = value;
    }

    public String render() {
        return value;
    }
}
