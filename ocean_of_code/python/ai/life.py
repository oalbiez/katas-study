# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Text


@dataclass(frozen=True)
class LifeStatus:
    previous: int
    current: int

    def amount_lost(self) -> int:
        return self.previous - self.current

    def update(self, life: int) -> LifeStatus:
        return LifeStatus(previous=self.current, current=life)

    def __repr__(self) -> Text:
        return f"LifeStatus({self.previous}, {self.current})"
