from collections import namedtuple
from hangman import word
from .errors import WordTooSmall, WordWithInvalidLetter

VALID_LETTERS = "abcdefghijklmnopqrstuvwxyz"


HangmanState = namedtuple('HangmanState', 'word_to_guess words letters max_errors')


def normalize_word_to_guess(word_to_guess):
    if len(word_to_guess) < 4:
        raise WordTooSmall("Word too small")
    invalid_letters = [repr(l) for l in word_to_guess.lower() if l not in VALID_LETTERS]
    if len(invalid_letters) > 0:
        raise WordWithInvalidLetter(invalid_letters)
    return word_to_guess.lower()


def new_state(word_to_guess, max_errors=3):
    return HangmanState(normalize_word_to_guess(word_to_guess), set(), set(), max_errors)


def test_a_letter(state, letter_to_test):
    return HangmanState(state.word_to_guess, state.words, state.letters | set([letter_to_test]), state.max_errors)


def test_a_word(state, word_to_test):
    return HangmanState(state.word_to_guess, state.words | set([word_to_test]), state.letters, state.max_errors)


def errors(state):
    word_errors = len(state.words - set([state.word_to_guess]))
    letter_errors = len(state.letters - set(word.middle(state.word_to_guess)))
    return word_errors + letter_errors


def puzzle(state):
    def render_letter(letter_to_guess):
        if letter_to_guess in state.letters:
            return letter_to_guess
        return '_'

    initial = word.initial(state.word_to_guess)
    middle = word.middle(state.word_to_guess)
    final = word.final(state.word_to_guess)
    return initial + "".join(render_letter(l) for l in middle) + final


def is_won(state):
    return state.word_to_guess in state.words or all(l in state.letters for l in word.middle(state.word_to_guess))


def is_lost(state):
    return errors(state) >= state.max_errors
