package katas.bottle;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.String.valueOf;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static katas.bottle.Bottle.delta;

public final class Solution {
    private final Bottle[] bottles;
    private final String action;
    private final Solution previous;

    static Solution solution(Bottle... bottles) {
        return new Solution(bottles, "", null);
    }

    private Solution(Bottle[] bottles, String action, Solution previous) {
        this.previous = previous;
        this.bottles = bottles;
        this.action = action;
    }

    public List<Integer> fromIndexes() {
        return allIndexes().filter(i -> bottles[i].quantity() > 0).collect(toList());
    }

    public List<Integer> toIndexes() {
        return allIndexes().filter(i -> bottles[i].remaining() > 0).collect(toList());
    }

    public Solution transfer(final int from, final int to) {
        final int delta = delta(bottles[from], bottles[to]);
        final Bottle[] newBottles = Arrays.copyOf(bottles, bottles.length);
        newBottles[from] = newBottles[from].updateWith(-delta);
        newBottles[to] = newBottles[to].updateWith(delta);
        return new Solution(newBottles, action(from, to), this);
    }

    public boolean match(final Predicate<Bottle> predicate) {
        return Arrays.stream(bottles).anyMatch(predicate);
    }

    public String hash() {
        return stream(bottles).map(Bottle::quantity).map(String::valueOf).collect(joining());
    }

    public String history() {
        StringBuilder builder = new StringBuilder();
        if (previous != null) {
            builder.append(previous.history());
            builder.append(action).append(" ").append(Arrays.toString(bottles)).append("\n");
        } else {
            builder.append("Initial: ").append(Arrays.toString(bottles)).append("\n");
        }
        return builder.toString();
    }

    private String action(final int from, final int to) {
        return valueOf(from + 1) + " -> " + valueOf(to + 1) + "; ";
    }

    private Stream<Integer> allIndexes() {
        return IntStream.range(0, bottles.length).boxed();
    }
}
