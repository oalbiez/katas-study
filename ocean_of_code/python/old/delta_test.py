# -*- coding: utf-8 -*-

from hypothesis import given
from hypothesis.strategies import integers

from ai import Delta
from helpers.strategies import deltas


@given(deltas())
def test_delta_should_have_neutral_element_on_addition(delta):
    assert Delta.zero() + delta == delta + Delta.zero() == delta


@given(deltas(), deltas())
def test_delta_should_be_addable(left, right):
    assert left + right == Delta(left.dx + right.dx, left.dy + right.dy)


@given(deltas(), deltas())
def test_delta_should_be_subable(left, right):
    assert left - right == Delta(left.dx - right.dx, left.dy - right.dy)


@given(deltas(), deltas(), deltas())
def test_delta_should_be_assosiative_on_addition(x, y, z):
    assert (x + y) + z == x + (y + z)


@given(deltas(), integers())
def test_delta_should_be_scaled_with_factor(delta, factor):
    assert delta * factor == Delta(delta.dx * factor, delta.dy * factor)


@given(deltas())
def test_delta_length_should_be_positive(delta):
    assert delta.length() >= 0


def test_delta_length_samples():
    assert Delta(3, 4).length() == 5
