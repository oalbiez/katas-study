package kata.ast;

public interface ASTVisitor<T> {
    T visitNumber(Number number);

    T visitAdd(Add add);

    T visitSub(Sub sub);
}
