# -*- coding: utf-8 -*-

from geometry import Rect, Position


def test_area_construction_should_reorganize_position():
    assert Rect.to(Position(5, 5)).topleft == Position.zero()
    assert Rect.to(Position(-5, 5)).topleft == Position(-5, 0)
    assert Rect.between(Position(-5, -5), Position(5, 5)).topleft == Position(-5, -5)


def test_area_should_contains_topleft():
    assert Position(0, 0) in Rect.between(Position(0, 0), Position(5, 5))


def test_area_should_not_contains_bottomright():
    assert Position(5, 5) not in Rect.between(Position(0, 0), Position(5, 5))


def test_area_contains_should_reject_all_external_position():
    area = Rect.between(Position(0, 0), Position(5, 5))
    assert Position(-1, 0) not in area
    assert Position(0, -1) not in area
    assert Position(6, 0) not in area
    assert Position(0, 6) not in area
    assert Position(-1, 6) not in Rect.between(Position(0, 0), Position(15, 15))


def test_area_should_be_iterable():
    assert list(Rect.between(Position(0, 0), Position(2, 1))) == [Position(x=0, y=0), Position(x=1, y=0)]


def test_area_should_have_area():
    assert Rect.between(Position(0, 0), Position(5, 5)).area() == 25


def test_area_should_have_width():
    assert Rect.between(Position(0, 0), Position(5, 5)).width() == 5


def test_area_should_have_height():
    assert Rect.between(Position(0, 0), Position(5, 6)).height() == 6


def test_area_should_have_center():
    assert Rect.between(Position(0, 0), Position(5, 5)).center() == Position(2, 2)


def test_area_should_be_splitted_in_four_with_point_in_middle():
    assert Rect.between(Position(0, 0), Position(5, 5)).split(Position(2, 2)) == [
        Rect(Position(0, 0), Position(2, 5)),
        Rect(Position(3, 0), Position(5, 5)),
        Rect(Position(0, 0), Position(5, 2)),
        Rect(Position(0, 3), Position(5, 5))]


def test_area_should_not_be_splitted_with_outside_point():
    assert Rect.between(Position(0, 0), Position(5, 5)).split(Position(10, 2)) == [
        Rect(Position(0, 0), Position(5, 5))]


def test_area_should_be_splitted_without_empty_rect():
    assert Rect.between(Position(0, 0), Position(5, 5)).split(Position(4, 4)) == [
        Rect(Position(0, 0), Position(4, 5)),
        Rect(Position(0, 0), Position(5, 4))]
