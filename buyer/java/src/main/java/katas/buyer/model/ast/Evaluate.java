package katas.buyer.model.ast;

import katas.buyer.model.Offer;
import katas.buyer.model.PurchaseRequest;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;

public class Evaluate implements ExpressionVisitor<BigDecimal> {
    private final PurchaseRequest request;
    private final Offer offer;

    public Evaluate(final PurchaseRequest request, final Offer offer) {
        this.request = request;
        this.offer = offer;
    }

    public BigDecimal acceptAdd(final Expression left, final Expression right) {
        return left.accept(this).add(right.accept(this));
    }

    public BigDecimal acceptSub(final Expression left, final Expression right) {
        return left.accept(this).subtract(right.accept(this));
    }

    public BigDecimal acceptMul(final Expression left, final Expression right) {
        return left.accept(this).multiply(right.accept(this));
    }

    public BigDecimal acceptDiv(final Expression left, final Expression right) {
        return left.accept(this).divide(right.accept(this), HALF_UP);
    }

    public BigDecimal acceptOfferPrice() {
        return offer.price().value();
    }

    public BigDecimal acceptOfferQuantity() {
        return offer.quantity().value();
    }

    public BigDecimal acceptRequestedQuantity() {
        return request.quantity().value();
    }
}
