# -*- coding: utf-8 -*-

from direction import Direction
from geometry import Position
from helpers.mapdsl import parse
from tracking import Tracking
from tracking import CanMove, DropMine, InPositions, InSector, MineTrigged, NotInPositions, NotInSector, Reset


def expect(expected, symbols="◎"):
    def expected_positions():
        _, positions = parse(expected)
        expected_positions = set()
        for symbol in symbols:
            expected_positions.update(positions[symbol] or [])
        return expected_positions

    def action(tracking):
        assert tracking.solutions() == expected_positions()
        assert tracking.count() == len(expected_positions())
        assert len(list(tracking)) >= len(expected_positions())
        return tracking

    return action


def expect_mines(expected, symbols="☠"):
    def expected_mines():
        _, positions = parse(expected)
        expected_positions = set()
        for symbol in symbols:
            expected_positions.update(positions[symbol] or [])
        return expected_positions

    def action(tracking):
        assert tracking.mines() == expected_mines()
        return tracking

    return action


def constraint(*constraints):
    def action(tracking):
        print("constraints:", repr(constraints))
        tracking = tracking.update(list(constraints))
        print(tracking.statistics())
        print(tracking.render_solutions())
        if tracking.mines():
            print(tracking.render_mines())
        print("")
        return tracking

    return action


def scenario(worldmap_definition, *actions):
    worldmap, _ = parse(worldmap_definition)
    tracking = Tracking.empty(worldmap)
    for action in actions:
        tracking = action(tracking)


def test_tracking_should_consider_all_positions_without_information():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_single_move_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(CanMove.to(Direction.North)),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ • ┃ • • ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_multiples_moves_constraints():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ • • ┃
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_silence_constraints():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.silence()),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_silence_constraints_when_blocked():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.silence()),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • ◎ ◎ ┃ ◎ • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • ◎ • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • ◎ • ┃
            ┃ • ◎ ◎ • ▲ ┃ ▲ ▲ • ◎ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • ◎ ◎ • ▲ ┃ ▲ ▲ • ◎ • ┃
            ┃ • • • • • ┃ • • • ◎ • ┃
            ┃ • ▲ ▲ ▲ • ┃ • ◎ ◎ ◎ • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • ◎ ◎ ◎ • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_in_position_in_zone_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InSector.of(4)),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ • • • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_single_in_position_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InPositions.single(Position(2, 2))),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ ◎ • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_in_position_in_torpedo_range_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InPositions.torpedo(Position(2, 2))),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ • • • • ┃
            ┃ ▲ ▲ x ◎ ◎ ┃ ◎ ◎ • • • ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • ◎ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """, symbols="◎x"))


def test_tracking_with_in_position_in_explosion_range_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InPositions.explosion(Position(2, 2))),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃
            ┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃
            ┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_not_in_position_in_zone_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(NotInSector.of(1)),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_single_not_in_position_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(NotInPositions.single(Position(2, 2))),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_not_in_position_in_explosion_range_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(NotInPositions.explosion(Position(2, 2))),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_surface_and_silence():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.North)),
        constraint(InSector.of(4), Reset()),
        constraint(CanMove.silence()),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • ◎ • ┃
            ┃ ▲ ▲ • • • ┃ • • • ◎ • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ • • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • ◎ ◎ ◎ • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_drop_mine_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InPositions.explosion(Position(2, 2))),
        constraint(DropMine()),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃
            ┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃
            ┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """),
        expect_mines("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • ☠ ☠ • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ☠ ☠ ☠ ┃ • • • • • ┃
            ┃ ▲ ▲ ☠ ☠ ☠ ┃ • • • • • ┃
            ┃ ☠ ☠ ☠ ☠ ▲ ┃ ▲ ▲ • • • ┃
            ┃ • ☠ ☠ ☠ ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_with_mine_trigged_constraint():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InPositions.of(Position(2, 2), Position(3, 2))),
        constraint(DropMine()),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(DropMine()),
        constraint(MineTrigged.at(Position(2, 1))),
        expect_mines("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • ☠ • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • ☠ • ☠ ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • ☠ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_long_scenario():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(InSector.of(7), Reset()),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(NotInSector.of(8)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.silence()),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.East)),
        constraint(InSector.of(5)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        constraint(InSector.of(4)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.silence()),
        constraint(InPositions.torpedo(Position(x=3, y=1))),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.North)),
        constraint(InPositions.torpedo(Position(x=1, y=1))),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.silence()),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(InPositions.torpedo(Position(x=4, y=4))),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.silence()),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.North)),
        constraint(NotInSector.of(1)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        constraint(InSector.of(1), Reset()),
        constraint(InPositions.torpedo(Position(x=0, y=0))),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.silence()),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.East)),
        constraint(InSector.of(1)),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.silence()),
        constraint(CanMove.to(Direction.East)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.South)),
        constraint(InSector.of(2)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.South)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.silence()),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ • • • • • ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • • ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))
