package katas.movierental.statement;

import katas.movierental.CustomerName;
import katas.movierental.FrequentRenterPoint;
import katas.movierental.Price;

import java.util.List;

public class Statement {
    final private CustomerName customer;
    final private Price total;
    final private FrequentRenterPoint frequentRenterPoints;
    final private List<Line> lines;

    public Statement(CustomerName customer, Price total, FrequentRenterPoint frequentRenterPoints, List<Line> lines) {
        this.customer = customer;
        this.total = total;
        this.frequentRenterPoints = frequentRenterPoints;
        this.lines = lines;
    }

    public String renderText() {
        StringBuilder result = new StringBuilder("Rental Record for " + customer.render() + "\n");
        for (final Line line : lines) {
            result.append("\t").append(line.renderText()).append("\n");
        }
        result.append("Amount owed is ").append(total.render()).append("\n");
        result.append("You earned ").append(frequentRenterPoints.render()).append(" frequent renter points");
        return result.toString();

    }
}
