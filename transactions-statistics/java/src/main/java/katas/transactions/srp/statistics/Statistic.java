package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.value.Value;

public interface Statistic {
    void updateWith(Transaction transaction);

    void accept(StatisticVisitor visitor);

    Statistic fork();

    String describe();

    Value value();
}
