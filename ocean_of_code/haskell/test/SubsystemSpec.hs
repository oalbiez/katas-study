{-# LANGUAGE OverloadedStrings #-}
module SubsystemSpec (spec) where


import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec = do


    it "Subsystem should be available" $
        let available = Subsystem 0 1 0 3
            disabled = Subsystem (-1) (-1) (-1) (-1)
        in do
            isAvailable Mine available `shouldBe` True
            isAvailable Silence available `shouldBe` True
            isAvailable Sonar available `shouldBe` True
            isAvailable Torpedo available `shouldBe` True

            isAvailable Mine disabled `shouldBe` False
            isAvailable Silence disabled `shouldBe` False
            isAvailable Sonar disabled `shouldBe` False
            isAvailable Torpedo disabled `shouldBe` True


    it "Subsystem should be ready when counter reach 0" $
        let ready = Subsystem 0 0 0 0
            cooldown = Subsystem 2 1 3 1
        in do
            isReady Mine ready `shouldBe` True
            isReady Silence ready `shouldBe` True
            isReady Sonar ready `shouldBe` True
            isReady Torpedo ready `shouldBe` True

            isReady Mine cooldown `shouldBe` False
            isReady Silence cooldown `shouldBe` False
            isReady Sonar cooldown `shouldBe` False
            isReady Torpedo cooldown `shouldBe` False

