package katas.foobarqix;

public class FooBarQixWithMapShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return FooBarQixWithMap.fooBarQix(input);
    }
}
