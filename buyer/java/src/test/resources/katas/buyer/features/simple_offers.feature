Feature: Decide a purchase on simple offers


  Scenario: We should accept the only offer

    Given a purchase request of 10 cups
    When we have a single offer of 10 cups for EUR 4.00 by Carrouf
    Then we should accept the offer 10 cups for EUR 4.00 by Carrouf


  Scenario: We should accept the best offer

    Given a purchase request of 10 cups
    When we receive following offers
      | Quantity | Unitary Price | Vendor  |
      | 10       | 5.00          | Carrouf |
      | 10       | 4.00          | Carrouf |
    Then we should accept the offer 10 cups for EUR 4.00 by Carrouf


  Scenario: We should accept the best offer for the requested quantity

    # Scenario invalid tel quel, il manque vision du marché et la peremption
    Given a purchase request of 10 cups
    When we receive following offers
      | Quantity | Unitary Price | Vendor  |
      | 10       | 2.50          | Carrouf |
      | 20       | 2.00          | Carrouf |
    Then we should accept the offer 10 cups for EUR 2.50 by Carrouf


  Scenario: We should accept the best offer taking penalty in account


    Given the penalty is EUR 10.00 per day
    And a purchase request of 10 cups in 7 days
    When we receive following offers
      | Quantity | Unitary Price | Delay | Vendor  |
      | 10       | 2.50          | 7     | Carrouf |
      | 10       | 2.00          | 30    | Carrouf |
    Then we should accept the offer 10 cups for EUR 2.50 in 7 days by Carrouf


  Scenario: We should accept the best offer taking penalty and quantity in account

    # Scenario invalid tel quel, il manque vision du marché et la peremption
    Given the penalty is EUR 10.00 per day
    And a purchase request of 10 cups in 7 days
    When we receive following offers
      | Quantity | Unitary Price | Delay | Vendor  |
      | 10       | 2.50          | 7     | Carrouf |
      | 15       | 1.00          | 10    | Carrouf |
    Then we should accept the offer 10 cups for EUR 2.50 in 7 days by Carrouf


    Given the penalty is EUR 10.00 per day
    And a purchase request of 10 cups in 7 days
    When we receive following offers
      | Quantity | Unitary Price | Delay | Vendor  |
      | 10       | 2.50          | 7     | Carrouf |
      | 12       | 1.00          | 8    | Carrouf |
    Then we should accept the offer 12 cups for EUR 1.00 in 8 days by Carrouf


#  Scenario: Strange
#
#    Given the penalty is EUR 10.00 per day
#    And usage is
#    And a purchase request of 10 cups in 7 days
#    When we have an offer of 10 cups for EUR 2.50 in 7 days by Carrouf
#    And we have an offer of 15 cups for EUR 1.00 in 8 days by Carrouf
#    Then we should accept the offer 10 cups for EUR 2.50 in 7 days by Carrouf
