package katas.buyer.model.ast;

public interface Expression {
    <T> T accept(ExpressionVisitor<T> visitor);
}
