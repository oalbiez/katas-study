﻿using NUnit.Framework;
using static NUnit.Framework.Assert;
using static Wallet.Currency;
using static Wallet.FixedRateProvider;

namespace Wallet.Tests
{
    [TestFixture]
    public class MoneyTest
    {
        [Test]
        public void money_should_equal()
        {
            AreEqual(EUR.Amount(0), EUR.Amount(0));
            AreNotEqual(USD.Amount(0), EUR.Amount(0));
            AreNotEqual(EUR.Amount(10), EUR.Amount(2));
        }

        [Test]
        public void money_should_render()
        {
            AreEqual("EUR 0", EUR.Amount(0).ToString());
        }

        [Test]
        public void money_should_add_when_same_currency()
        {
            AreEqual(EUR.Amount(10), EUR.Amount(7) + EUR.Amount(3));
        }

        [Test]
        public void money_should_not_add_when_different_currencies()
        {
            Catch(typeof(MismatchCurrency), () => Unused(USD.Amount(7) + EUR.Amount(5)));
        }

        [Test]
        public void money_should_change()
        {
            AreEqual(EUR.Amount(10), EUR.Amount(10).Change(EUR, Fixed()));
            AreEqual(EUR.Amount(10), USD.Amount(20).Change(EUR, Fixed().Rate(USD, EUR, 0.5m)));
        }

        private static void Unused(object value)
        {
        }
    }
}