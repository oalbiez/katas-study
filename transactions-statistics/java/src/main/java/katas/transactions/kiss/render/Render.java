package katas.transactions.kiss.render;

import katas.transactions.kiss.statistics.AllStatistics;
import katas.transactions.kiss.statistics.Balance;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static katas.transactions.kiss.statistics.Statistics.ratio;

public class Render {
    public static List<ByClientSummary> renderByClientSummary(final AllStatistics statistics) {
        final List<ByClientSummary> collector = newArrayList();
        for (final String client : statistics.byClient().clients()) {
            final Balance balance = statistics.byClient().statisticFor(client);
            collector.add(new ByClientSummary(
                    client,
                    balance.count(),
                    balance.credit(),
                    balance.debt(),
                    balance.sold(),
                    balance.volume(),
                    ratio(balance.volume(), statistics.balance().volume())));
        }
        return collector;
    }
}
