import { Duration } from "../duration";
import { add, mul, Price } from "../price";

// tslint:disable:max-classes-per-file

export class PriceDuration {
  public static of(duration: Duration): PriceDuration {
    return new PriceDuration(Price.euro(0), duration);
  }

  private accumulator: Price;
  private duration: Duration;

  private constructor(accumulator: Price, duration: Duration) {
      this.accumulator = accumulator;
      this.duration = duration;
  }

  public isExhausted(): boolean {
    return this.duration.days() === 0;
  }

  public total(): Price {
    return this.accumulator;
  }

  public consumeAll(on: (consumed: Duration) => Price): PriceDuration {
    return new PriceDuration(add(this.accumulator, on(this.duration)), Duration.days(0));
  }

  public consume(duration: Duration, on: (consumed: Duration) => Price): PriceDuration {
    const consumed = Duration.min(this.duration, duration);
    const remaining = Duration.sub(this.duration, duration);
    return new PriceDuration(add(this.accumulator, on(consumed)), remaining);
  }
}

export interface Rule {
    priceFor(input: PriceDuration): PriceDuration;
}

class FlatRule implements Rule {
    private price: Price;

    public constructor(price: Value) {
        this.price = price;
    }

    public priceFor(input: PriceDuration): PriceDuration {
        return input.consumeAll((_) => this.price);
    }
}

class LimitedDurationRule implements Rule {
    private duration: Duration;
    private inner: Rule;

    public constructor(duration: Duration, inner: Rule) {
        this.duration = duration;
        this.inner = inner;
    }

    public priceFor(input: PriceDuration): PriceDuration {
      return input.consume(
        this.duration,
        (consumed) => this.inner.priceFor(PriceDuration.of(consumed)).total());
    }
}

class ProportionalRule implements Rule {
    private price: Price;

    public constructor(price: Value) {
        this.price = price;
    }

    public priceFor(input: PriceDuration): PriceDuration {
        return input.consumeAll((consumed) => mul(this.price, consumed.days()));
    }
}

export class Rules {
  public static flat(price: Value): Rule {
    return new FlatRule(price);
  }

  public static proportional(price: Value): Rule {
    return new ProportionalRule(price);
  }

  public static limited(duration: Duration, rule: Rule): Rule {
    return new LimitedDurationRule(duration, rule);
  }
}

export class Pricing {
    public static of(rule: Rule): Pricing {
        return new Pricing([rule]);
    }

    private rules: Rule[];

    private constructor(rules: Rule[]) {
        this.rules = rules;
    }

    public then(rule: Rule): Pricing {
        return new Pricing([...this.rules, rule]);
    }

    public chargeFor(duration: Duration): Price {
      let remaining = PriceDuration.of(duration);
      for (const rule of this.rules) {
          remaining = rule.priceFor(remaining);
          if (remaining.isExhausted()) {
              break;
          }
      }
      return remaining.total();
    }
}
