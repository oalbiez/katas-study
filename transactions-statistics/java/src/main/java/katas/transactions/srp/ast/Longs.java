package katas.transactions.srp.ast;

import katas.transactions.Transaction;

public final class Longs {
    public static ValueProvider<Long> literal(final long value) {
        return new ValueProvider<Long>() {
            public Long get(final Transaction transaction) {
                return value;
            }

            public String describe() {
                return String.valueOf(value);
            }
        };
    }

    public static ValueProvider<Long> add(final ValueProvider<Long> left, final ValueProvider<Long> right) {
        return new ValueProvider<Long>() {
            public Long get(final Transaction transaction) {
                return left.get(transaction) + right.get(transaction);
            }

            public String describe() {
                return left.describe() + " + " + right.describe();
            }
        };
    }

    public static ValueProvider<Boolean> gt(final ValueProvider<Long> left, final ValueProvider<Long> right) {
        return new ValueProvider<Boolean>() {
            public Boolean get(final Transaction transaction) {
                return left.get(transaction) > right.get(transaction);
            }

            public String describe() {
                return left.describe() + " > " + right.describe();
            }
        };
    }


}
