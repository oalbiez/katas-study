# -*- coding: utf-8 -*-

from ai import Navigation, Trace, Position
from helpers.mapdsl import parse


def check_navigable(definition):
    worldmap, symbols = parse(definition)
    navigation = Navigation(worldmap)
    assert navigation.avoid == set(symbols['x'])


def test_remove_isolate_water():
    check_navigable("""
        ┏━━━━━━━━━━━┓
        ┃ • • ▲ x ▲ ┃
        ┃ • • • ▲ ▲ ┃
        ┃ • • • ▲ ▲ ┃
        ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┛
    """)


def test_remove_dead_channels():
    check_navigable("""
        ┏━━━━━━━━━━━┓
        ┃ x ▲ • • • ┃
        ┃ x ▲ • • • ┃
        ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┛
    """)


def test_find_path():
    worldmap, symbols = parse("""
        ┏━━━━━━━━━━━┓
        ┃ • ▲ • g • ┃
        ┃ • ▲ • • • ┃
        ┃ s • ▲ • ▲ ┃
        ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┛
    """)
    navigation = Navigation(worldmap)
    trace = Trace.on(symbols['s'][0])
    assert navigation.path(trace, symbols['g'][0]) == [
        Position(x=0, y=2),
        Position(x=1, y=2),
        Position(x=1, y=3),
        Position(x=2, y=3),
        Position(x=3, y=3),
        Position(x=3, y=2),
        Position(x=3, y=1),
        Position(x=3, y=0)]
