# -*- coding: utf-8 -*-

import pytest

from hypothesis import assume, given
from hypothesis.strategies import integers

from ai import Direction, Position
from helpers.strategies import positions


def test_direction_should_be_parsable():
    assert Direction.parse("N") == Direction.North
    assert Direction.parse("S") == Direction.South
    assert Direction.parse("E") == Direction.East
    assert Direction.parse("W") == Direction.West
    with pytest.raises(ValueError):
        assert Direction.parse("BAD")


@given(positions())
def test_direction_should_be_applicable_to_position(position):
    assert Direction.North.move(Direction.South.move(position)) == position
    assert Direction.East.move(Direction.West.move(position)) == position


@given(positions())
def test_direction_should_be_reverted(position):
    assert Direction.North.revert(Direction.North.move(position)) == position
    assert Direction.South.revert(Direction.South.move(position)) == position
    assert Direction.East.revert(Direction.East.move(position)) == position
    assert Direction.West.revert(Direction.West.move(position)) == position


@given(positions(), integers())
def test_direction_should_scaled_move(position, scale):
    assert Direction.North.scaled_move(Direction.South.scaled_move(position, scale), scale) == position
    assert Direction.East.scaled_move(Direction.West.scaled_move(position, scale), scale) == position


@given(positions(), integers())
def test_direction_should_scaled_move_really(position, scale):
    assume(scale != 0 and position != Position.zero())
    assert Direction.North.scaled_move(position, scale) != position
    assert Direction.South.scaled_move(position, scale) != position
    assert Direction.East.scaled_move(position, scale) != position
    assert Direction.West.scaled_move(position, scale) != position


def test_direction_should_be_representable():
    assert repr(Direction.North) == "Direction.North"
    assert repr(Direction.South) == "Direction.South"
    assert repr(Direction.East) == "Direction.East"
    assert repr(Direction.West) == "Direction.West"
