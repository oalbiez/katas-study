package katas.transactions.kiss.statistics;

import katas.transactions.Transaction;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

import static com.google.common.collect.Maps.newEnumMap;
import static java.time.Duration.between;
import static java.util.Collections.unmodifiableMap;
import static katas.transactions.kiss.statistics.Periods.findPeriod;

public final class AllStatistics implements Statistic {
    private final LocalDate today;
    private final Balance balance = new Balance();
    private final ByClientBalance byClient = new ByClientBalance();
    private final Map<Periods, ByClientBalance> byPeriods = newEnumMap(Periods.class);

    public AllStatistics(final LocalDate today) {
        this.today = today;
        for (final Periods period : Periods.values()) {
            byPeriods.put(period, new ByClientBalance());
        }
    }

    public void updateWith(final Transaction transaction) {
        balance.updateWith(transaction);
        byClient.updateWith(transaction);
        byPeriods.get(findPeriod(ageOf(transaction))).updateWith(transaction);
    }

    public Collection<String> clients() {
        return byClient.clients();
    }

    public Balance balance() {
        return balance;
    }

    public ByClientBalance byClient() {
        return byClient;
    }

    public Map<Periods, ByClientBalance> byPeriods() {
        return unmodifiableMap(byPeriods);
    }

    private long ageOf(final Transaction transaction) {
        return between(transaction.creation().toLocalDate().atStartOfDay(), today.atStartOfDay()).toDays();
    }
}
