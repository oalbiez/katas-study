package katas.bottle;

import com.google.common.base.Objects;

public final class Bottle {
    private final int capacity;
    private final int quantity;

    static Bottle bottle(final int capacity, final int quantity) {
        return new Bottle(capacity, Math.min(capacity, quantity));
    }

    static int delta(final Bottle from, final Bottle to) {
        return Math.min(from.quantity, to.remaining());
    }

    private Bottle(int capacity, int quantity) {
        this.capacity = capacity;
        this.quantity = quantity;
    }

    public Bottle updateWith(int delta) {
        return bottle(capacity, quantity + delta);
    }

    public int remaining() {
        return capacity - quantity;
    }

    public int quantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bottle bottle = (Bottle) o;
        return capacity == bottle.capacity &&
                quantity == bottle.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(capacity, quantity);
    }

    @Override
    public String toString() {
        return String.valueOf(quantity) + "L/" + String.valueOf(capacity) + "L";
    }
}
