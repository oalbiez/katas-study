{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE OverloadedLabels      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE ViewPatterns          #-}
module SituationAwarnessMySpec (spec) where


import           Named
import           OceanOfCode
import           Test.Hspec


constraints ::
    "orders" :? String ->
    "report" :? String ->
    "sector" :? Sector ->
    "life" :? LifeStatus
    -> [Constraint]
constraints
    (argDef #orders "" -> orders)
    (argDef #report "" -> report)
    (argDef #sector (Sector 1) -> sector)
    (argDef #life (LifeStatus 6 6) -> life)
    = myConstraints life (parseOrders orders) (parseReport report) sector


spec :: Spec
spec = do

    it "should add no contraint with no orders or sonar orders" $ do
        constraints ! #orders "NA"
                    ! defaults
            `shouldMatchList` []

        constraints ! #orders "SONAR 4"
                    ! defaults
            `shouldMatchList` []


    it "should add sector constraint with sonar report" $ do
        constraints ! #report "SONAR 4"
                    ! #sector (Sector 4)
                    ! defaults
            `shouldMatchList` [inSector (Sector 4)]

        constraints ! #report "SONAR 2"
                    ! #sector (Sector 5)
                    ! defaults
            `shouldMatchList` [notInSector (Sector 2)]


    it "should add movement contraint with move" $ do
        constraints ! #orders "MOVE N TORPEDO"
                    ! defaults
            `shouldMatchList` [canMoveTo North]

        constraints ! #orders "MOVE S MINE"
                    ! defaults
            `shouldMatchList` [canMoveTo South]

        constraints ! #orders "MOVE E SONAR"
                    ! defaults
            `shouldMatchList` [canMoveTo East]

        constraints ! #orders "MOVE W SILENCE"
                    ! defaults
            `shouldMatchList` [canMoveTo West]


    it "should add movement contraint with silence" $
        constraints ! #orders "SILENCE N 3"
                    ! defaults
            `shouldMatchList` [canMoveWithSilence]


    it "should add sector contraint with surface" $
        constraints ! #orders "SURFACE"
                    ! #sector (Sector 5)
                    ! defaults
            `shouldMatchList` [inSector (Sector 5), Reset]


    it "should add mine dropped contraint with mine drop" $
        constraints ! #orders "MINE N"
                    ! defaults
            `shouldMatchList` [mineDropped]


    describe "should add in mine trigged contraint with single mine trigger" $ do

        it "without life lost" $
            constraints ! #orders "TRIGGER 4 4"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [mineTrigged (point 4 4), notInExplosion (point 4 4)]

        it "with one life lost" $
            constraints ! #orders "TRIGGER 4 4"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [mineTrigged (point 4 4), inBlastExplosion (point 4 4)]

        it "with two lifes lost" $
            constraints ! #orders "TRIGGER 4 4"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [mineTrigged (point 4 4), inPosition (point 4 4)]


    describe "should add in torpedo contraint with single torpedo" $ do

        it "without life lost" $
            constraints ! #orders "TORPEDO 4 4"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 4 4),
                    notInExplosion (point 4 4)]

        it "with one life lost" $
            constraints ! #orders "TORPEDO 4 4"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 4 4),
                    inBlastExplosion (point 4 4)]

        it "with two lifes lost" $
            constraints ! #orders "TORPEDO 4 4"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 4 4),
                    inPosition (point 4 4)]


    describe "should add constraints regarding explosions and life lost" $ do

        it "opponent single explosion without life lost" $ do
            constraints ! #report "TORPEDO 4 4"
                        ! #orders "NA"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [notInExplosion (point 4 4)]

            constraints ! #report "TRIGGER 4 4"
                        ! #orders "NA"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [notInExplosion (point 4 4)]

        it "opponent single explosion with one life lost" $ do
            constraints ! #report "TORPEDO 4 4"
                        ! #orders "NA"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [inBlastExplosion (point 4 4)]

            constraints ! #report "TRIGGER 4 4"
                        ! #orders "NA"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [inBlastExplosion (point 4 4)]

        it "opponent single explosion with two lifes lost" $ do
            constraints ! #report "TORPEDO 4 4"
                        ! #orders "NA"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [inPosition (point 4 4)]

            constraints ! #report "TRIGGER 4 4"
                        ! #orders "NA"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [inPosition (point 4 4)]

        it "lots of explosions without life lost" $
            constraints ! #report "TORPEDO 4 4 | TRIGGER 8 7"
                        ! #orders "TORPEDO 3 7 | TRIGGER 8 4"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 3 7),
                    mineTrigged (point 8 4),
                    notInExplosion (point 4 4),
                    notInExplosion (point 8 7),
                    notInExplosion (point 3 7),
                    notInExplosion (point 8 4)]

        it "lots of explosions with one life lost" $
            constraints ! #report "TORPEDO 4 4 | TRIGGER 8 7"
                        ! #orders "TORPEDO 3 7 | TRIGGER 8 4"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 3 7),
                    mineTrigged (point 8 4),
                    notInPosition (point 4 4),
                    notInPosition (point 8 7),
                    notInPosition (point 3 7),
                    notInPosition (point 8 4)]

        it "lots of explosions with two lifes lost" $
            constraints ! #report "TORPEDO 4 4 | TRIGGER 8 7"
                        ! #orders "TORPEDO 3 7 | TRIGGER 8 4"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 3 7),
                    mineTrigged (point 8 4)]
