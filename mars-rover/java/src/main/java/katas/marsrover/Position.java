package katas.marsrover;

import com.google.common.base.Objects;

import static java.lang.Math.floorMod;

public class Position {
    private final long x;
    private final long y;

    public static Position position(long x, long y) {
        return new Position(x, y);
    }

    private Position(long x, long y) {
        this.x = x;
        this.y = y;
    }

    public long x() {
        return x;
    }

    public long y() {
        return y;
    }

    public Position add(final Distance distance) {
        return position(x + distance.dx(), y + distance.dy());
    }

    public Position wrap(final long width, final long height) {
        return position(floorMod(x, width), floorMod(y, height));
    }

    @Override
    public String toString() {
        return "Position{x=" + x + ", y=" + y + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(x, y);
    }
}
