# -*- coding: utf-8 -*-

from hypothesis import given

from geometry import Position
from helpers.strategies import deltas, positions


@given(positions(), deltas())
def test_position_should_be_added_with_delta(position, delta):
    assert position + delta == Position(position.x + delta.dx, position.y + delta.dy)


@given(positions(), deltas())
def test_position_should_be_subed_with_delta(position, delta):
    assert position - delta == Position(position.x - delta.dx, position.y - delta.dy)


@given(positions(), positions())
def test_difference_of_two_positions_is_a_delta(left, right):
    # pylint: disable=arguments-out-of-order
    assert left + Position.delta(right, left) == right


def test_position_should_be_parsable():
    assert Position.parse("3 6") == Position(3, 6)


def test_position_min_should_get_minium_for_each_axis():
    assert Position.min(Position(3, 6), Position(5, 2)) == Position(3, 2)


def test_position_max_should_get_maximum_for_each_axis():
    assert Position.max(Position(3, 6), Position(5, 2)) == Position(5, 6)


@given(positions(), positions())
def test_distance_between_two_points(left, right):
    assert Position.distance(left, right) == Position.delta(left, right).length()
