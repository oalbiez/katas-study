package katas.buyer.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PriceTest {
    @Test
    public void price_should_be_able_to_multiply_by_quantity() {
        assertEquals(Price.of(6.00), Price.of(2.00).mul(Quantity.of(3)));
    }

    @Test
    public void price_should_be_able_to_divide_by_quantity() {
        assertEquals(Price.of(2.00), Price.of(12.0).div(Quantity.of(6)));
    }
}