package katas.movierental.pricing;

import org.junit.Test;

import static katas.movierental.Duration.days;
import static katas.movierental.Price.euro;
import static katas.movierental.pricing.Pricing.cost;
import static katas.movierental.pricing.Pricing.pricing;
import static org.junit.Assert.assertEquals;

public class PricingTest {
    @Test
    public void pricing_should_price_constant_price() {
        assertEquals(euro(2), pricing(cost(euro(2))).chargeFor(days(3)));
    }

    @Test
    public void pricing_should_price_constant_price_with_limited_duration() {
        assertEquals(euro(2), pricing(cost(euro(2)).for_(days(3))).chargeFor(days(1)));
        assertEquals(euro(2), pricing(cost(euro(2)).for_(days(3))).chargeFor(days(2)));
        assertEquals(euro(2), pricing(cost(euro(2)).for_(days(3))).chargeFor(days(3)));
    }

    @Test
    public void pricing_should_price_per_day_price() {
        assertEquals(euro(6), pricing(cost(euro(2)).perDay()).chargeFor(days(3)));
    }

    @Test
    public void pricing_should_price_per_day_price_with_limited_duration() {
        assertEquals(euro(2), pricing(cost(euro(2)).perDay().for_(days(3))).chargeFor(days(1)));
        assertEquals(euro(4), pricing(cost(euro(2)).perDay().for_(days(3))).chargeFor(days(2)));
        assertEquals(euro(6), pricing(cost(euro(2)).perDay().for_(days(3))).chargeFor(days(3)));
    }

    @Test
    public void pricing_should_price_with_multiples_rules() {
        assertEquals(euro(8),
                pricing(cost(euro(2)).for_(days(3))).then(cost(euro(3)).perDay())
                        .chargeFor(days(5)));
        assertEquals(euro(7),
                pricing(cost(euro(2)).perDay().for_(days(2))).then(cost(euro(3)))
                        .chargeFor(days(10)));
        assertEquals(euro(2),
                pricing(cost(euro(2)).for_(days(2))).then(cost(euro(3)))
                        .chargeFor(days(1)));
    }

    @Test(expected = Overflow.class)
    public void pricing_should_manage_overflow() {
        pricing(cost(euro(2)).for_(days(3))).chargeFor(days(5));
    }

    @Test(expected = MalformedPricing.class)
    public void pricing_should_error_when_more_than_one_per_day_applyed_on_rule() {
        cost(euro(2)).perDay().perDay();
    }

    @Test(expected = MalformedPricing.class)
    public void pricing_should_error_when_more_than_one_for_applyed_on_rule() {
        cost(euro(2)).for_(days(3)).for_(days(7));
    }

    @Test(expected = MalformedPricing.class)
    public void pricing_should_error_when_par_day_applyed_after_for() {
        cost(euro(2)).for_(days(3)).perDay();
    }

    @Test
    public void pricing_should_self_describe() {
        assertEquals("cost €2.0", pricing(cost(euro(2))).describe());
        assertEquals("cost €2.0 per day", pricing(cost(euro(2)).perDay()).describe());
        assertEquals("cost €2.0 for 5 day(s)", pricing(cost(euro(2)).for_(days(5))).describe());
        assertEquals("cost €2.0 for 5 day(s) then cost €3.0 per day",
                pricing(cost(euro(2)).for_(days(5))).then(cost(euro(3)).perDay()).describe());
    }

    @Test
    public void pricing_should_explain() {
        assertEquals(
                "rule [cost €2.0]: price=€2.0, total=€2.0, remaining=0",
                pricing(cost(euro(2))).explain(days(4)));
        assertEquals(
                "rule [cost €2.0 for 4 day(s)]: price=€2.0, total=€2.0, remaining=6; " +
                        "rule [cost €3.0 per day]: price=€18.0, total=€20.0, remaining=0",
                pricing(cost(euro(2)).for_(days(4))).then(cost(euro(3)).perDay()).explain(days(10)));
        assertEquals(
                "rule [cost €2.0 for 4 day(s)]: price=€2.0, total=€2.0, remaining=6; " +
                        "overflow of 6",
                pricing(cost(euro(2)).for_(days(4))).explain(days(10)));
    }
}