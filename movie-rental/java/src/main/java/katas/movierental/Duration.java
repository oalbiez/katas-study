package katas.movierental;

public final class Duration implements Comparable<Duration> {
    private static final int INFINITE_VALUE = -1;

    private int value;

    public static Duration infinite() {
        return new Duration(INFINITE_VALUE);
    }

    public static Duration zero() {
        return new Duration(0);
    }

    public static Duration days(int value) {
        return new Duration(value);
    }

    public static Duration add(Duration left, Duration right) {
        if (left.isInfinite() || right.isInfinite()) {
            return infinite();
        }
        return days(left.value + right.value);
    }

    public static Duration sub(Duration left, Duration right) {
        if (left.isInfinite()) {
            return right.isInfinite() ? zero() : infinite();
        }
        if (right.isInfinite()) {
            return zero();
        }
        return days(Math.max(left.value - right.value, 0));
    }

    public static Duration max(Duration left, Duration right) {
        if (left.isInfinite() || right.isInfinite()) {
            return infinite();
        }
        return days(Math.max(left.value, right.value));
    }

    public static Duration min(Duration left, Duration right) {
        if (left.isInfinite()) {
            return right;
        }
        if (right.isInfinite()) {
            return left;
        }
        return days(Math.min(left.value, right.value));
    }

    public Duration(int value) {
        this.value = value;
    }

    public boolean isInfinite() {
        return value == INFINITE_VALUE;
    }

    public boolean IsNotInfinite() {
        return value != INFINITE_VALUE;
    }

    public boolean isZero() {
        return value == 0;
    }

    public boolean isNotZero() {
        return value != 0;
    }

    @Override
    public String toString() {
        return isInfinite() ? "Duration: infinite" : "Duration: " + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return value == ((Duration) o).value;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(value);
    }

    @Override
    public int compareTo(Duration o) {
        if (isInfinite()) {
            return o.isInfinite() ? 0 : 1;
        }

        if (o.isInfinite()) {
            return -1;
        }
        return Integer.compare(value, o.value);
    }

    public int value() {
        return value;
    }
}
