# -*- coding: utf-8 -*-

from ai import TrackingInformations, TrackingByTrace, Position, Direction
from ai import TrackMove, TrackSurface, TrackTorpedo, TrackSonar, TrackSilence, SonarResult
from helpers.mapdsl import parse


class ScenarioOn:
    def __init__(self, definition):
        self.worldmap, _ = parse(definition)
        self.tracking = TrackingByTrace(self.worldmap)

    def spy(self, *informations):
        print("informations:", repr(informations))
        self.tracking.track(TrackingInformations(informations))
        print(self.tracking.statistics())
        print("")
        return self

    def expected_positions(self, expected, symbols="◎"):
        _, positions = parse(expected)

        expected_positions = set()
        for symbol in symbols:
            expected_positions.update(positions[symbol] or [])

        print(self.tracking.render())
        assert self.tracking.solutions() == expected_positions
        assert self.tracking.count() == len(expected_positions)
        return self


def test_tracking_should_consider_all_positions_without_information():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_single_move():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackMove(Direction.North))
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ • ┃ • • ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_multiples_moves():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.West))
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ • • ┃
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_surface():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackSurface(4))
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ • • • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_torpedo():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackTorpedo(Position(2, 2)))
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ • • • • ┃
            ┃ ▲ ▲ x ◎ ◎ ┃ ◎ ◎ • • • ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • ◎ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """, symbols="◎x"))


def test_tracking_after_sonar_hit():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackSonar(1, SonarResult.HIT))
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ • • • • • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ • • • • • ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_sonar_miss():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackSonar(1, SonarResult.MISS))
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_silence():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackSilence())
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ▲ ▲ • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_silence_when_blocked():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.North))
        .spy(TrackSilence())
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • ◎ ◎ ┃ ◎ • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • ◎ • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • ◎ • ┃
            ┃ • ◎ ◎ • ▲ ┃ ▲ ▲ • ◎ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • ◎ ◎ • ▲ ┃ ▲ ▲ • ◎ • ┃
            ┃ • • • • • ┃ • • • ◎ • ┃
            ┃ • ▲ ▲ ▲ • ┃ • ◎ ◎ ◎ • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • ◎ ◎ ◎ • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_after_surface_and_silence():
    (ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """)
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.North))
        .spy(TrackSurface(4))
        .spy(TrackSilence())
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • ◎ • ┃
            ┃ ▲ ▲ • • • ┃ • • • ◎ • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃
            ┃ • • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • ◎ ◎ ◎ • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))


def test_tracking_long_scenario():
    scenario = (
        ScenarioOn("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))
    (scenario
        .spy(TrackSurface(sector=7))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackSonar(sector=8, result=SonarResult.MISS))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.North))
        .spy(TrackSilence())
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.East))
        .spy(TrackSonar(sector=5, result=SonarResult.HIT))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.West)))
    (scenario
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.West))
        .spy(TrackSonar(sector=4, result=SonarResult.HIT))
        .spy(TrackMove(Direction.North))
        .spy(TrackSilence())
        .spy(TrackTorpedo(target=Position(x=3, y=1)))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.North))
        .spy(TrackTorpedo(target=Position(x=1, y=1)))
        .spy(TrackMove(Direction.East))
        .spy(TrackSilence())
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East)))
    (scenario
        .spy(TrackMove(Direction.East))
        .spy(TrackTorpedo(target=Position(x=4, y=4)))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.West))
        .spy(TrackSilence())
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.North))
        .spy(TrackSonar(sector=1, result=SonarResult.MISS))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.West))
        .spy(TrackSurface(sector=1))
        .spy(TrackTorpedo(target=Position(x=0, y=0)))
        .spy(TrackMove(Direction.West)))
    (scenario
        .spy(TrackSilence())
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.East))
        .spy(TrackSonar(sector=1, result=SonarResult.HIT))
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.North))
        .spy(TrackSilence())
        .spy(TrackMove(Direction.East))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.South))
        .spy(TrackSonar(sector=2, result=SonarResult.HIT))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.South))
        .spy(TrackMove(Direction.West))
        .spy(TrackMove(Direction.North))
        .spy(TrackMove(Direction.West))
        .spy(TrackSilence()))
    (scenario
        .expected_positions("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • ▲ ▲ • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))
