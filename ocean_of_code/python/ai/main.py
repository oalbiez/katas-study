# -*- coding: utf-8 -*-
from ai import Game, Interaction


if __name__ == "__main__":
    GAME = Game(Interaction())
    GAME.choose_starting_position()
    while True:
        GAME.turn()
