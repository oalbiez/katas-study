
export function unindent(content: string): string {
  return content
    .split(/\r?\n/g)
    .filter((line) => line.match(/^[ \t]*\|/g))
    .map((line) => line.replace(/^[ \t]*\|/g, ""))
    .join("\n") + "\n";
}
