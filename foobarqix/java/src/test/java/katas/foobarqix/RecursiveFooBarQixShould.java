package katas.foobarqix;

public class RecursiveFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return Recursive.fooBarQix(input);
    }
}
