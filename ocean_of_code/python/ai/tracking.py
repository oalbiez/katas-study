# -*- coding: utf-8 -*-
from __future__ import annotations

from collections import Counter
from dataclasses import dataclass
from itertools import islice
from typing import FrozenSet, Iterable, Iterator, Optional, Set, Text, Tuple, Union
from textwrap import dedent
from functools import reduce

from decorators import memoize_method
from direction import Direction
from geometry import Position, Rect
from ship_trail import ShipTrail, Mine
from world_map import WorldMap
from constants import ExplosionDelta, ExplosionBlastDelta, TorpedoDeltas


@dataclass(frozen=True)
class Path:
    directions: Tuple[Direction, ...]

    @staticmethod
    def empty() -> Path:
        return Path(tuple())

    @staticmethod
    def of(*directions: Direction) -> Path:
        return Path(directions)

    def apply(self, worldmap: WorldMap, trail: ShipTrail) -> Optional[ShipTrail]:
        for direction in self.directions:
            next_position = direction.move(trail.last())
            if next_position in trail or not worldmap.is_water(next_position):
                return None
            trail = trail.move(next_position)
        return trail


@dataclass(frozen=True)
class CanMove:
    paths: FrozenSet[Path]

    @staticmethod
    def to(direction: Direction) -> CanMove:
        return CanMove(frozenset([Path.of(direction)]))

    @staticmethod
    def silence() -> CanMove:
        return CanMove(SilencePaths)

    def process(self, worldmap: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        if len(self.paths) == 1:
            for trail in trails:
                for path in self.paths:
                    new_trail = path.apply(worldmap, trail)
                    if new_trail:
                        yield new_trail
        else:
            for trail in trails:
                for path in self.paths:
                    new_trail = path.apply(worldmap, trail)
                    if new_trail:
                        yield new_trail.clear()


@dataclass(frozen=True)
class DropMine:
    # pylint: disable=no-self-use
    def process(self, worldmap: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        for trail in trails:
            yield trail.mine(Mine.entanglement(worldmap.neighbors(trail.current)))


@dataclass(frozen=True)
class MineTrigged:
    position: Position

    @staticmethod
    def at(target: Position) -> MineTrigged:
        return MineTrigged(target)

    def process(self, _: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        return (trail.remove_mine(self.position) for trail in trails if trail.has_mine(self.position))


@dataclass(frozen=True)
class InPositions:
    positions: FrozenSet[Position]

    @staticmethod
    def of(*positions: Position) -> InPositions:
        return InPositions(frozenset(positions))

    @staticmethod
    def single(position: Position) -> InPositions:
        return InPositions(frozenset([position]))

    @staticmethod
    def torpedo(target: Position) -> InPositions:
        return InPositions(frozenset(target + delta for delta in TorpedoDeltas))

    @staticmethod
    def explosion(target: Position) -> InPositions:
        return InPositions(frozenset(target + delta for delta in ExplosionDelta))

    @staticmethod
    def explosion_blast(target: Position) -> InPositions:
        return InPositions(frozenset(target + delta for delta in ExplosionBlastDelta))

    def process(self, _: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        return (trail for trail in trails if trail.last() in self.positions)


@dataclass(frozen=True)
class NotInPositions:
    positions: FrozenSet[Position]

    @staticmethod
    def single(position: Position) -> NotInPositions:
        return NotInPositions(frozenset([position]))

    @staticmethod
    def explosion(target: Position) -> NotInPositions:
        return NotInPositions(frozenset(target + delta for delta in ExplosionDelta))

    def process(self, _: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        return (trail for trail in trails if trail.last() not in self.positions)


@dataclass(frozen=True)
class InSector:
    sector: int

    @staticmethod
    def of(sector: int) -> InSector:
        return InSector(sector)

    def process(self, worldmap: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        return (trail for trail in trails if worldmap.to_sector(trail.last()) == self.sector)


@dataclass(frozen=True)
class NotInSector:
    sector: int

    @staticmethod
    def of(sector: int) -> NotInSector:
        return NotInSector(sector)

    def process(self, worldmap: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        return (trail for trail in trails if worldmap.to_sector(trail.last()) != self.sector)


@dataclass(frozen=True)
class Reset:
    # pylint: disable=no-self-use
    def process(self, _: WorldMap, trails: Iterable[ShipTrail]) -> Iterable[ShipTrail]:
        return (ShipTrail.on(trail.last()) for trail in trails)


TrackingConstraint = Union[
    CanMove,
    DropMine,
    InPositions,
    InSector,
    MineTrigged,
    NotInPositions,
    NotInSector,
    Reset]


@dataclass(frozen=True)
class PositionEstimation:
    position: Position
    estimation: float

    def __str__(self):
        return f"{self.position}, {self.estimation * 100:.2f}%"


@dataclass
class Tracking:
    worldmap: WorldMap
    trails: Set[ShipTrail]

    @staticmethod
    def empty(worldmap: WorldMap) -> Tracking:
        return Tracking(worldmap, _compact(ShipTrail.on(p) for p in worldmap.all_water()))

    def __iter__(self) -> Iterator[ShipTrail]:
        return iter(self.trails)

    def statistics(self):
        trails_length = [len(trail) for trail in self.trails]
        return dedent(f"""
            - solutions: {self.count()} {", ".join(str(p) for p in (islice(self.solutions(), 0, 10)))}
            - trails: {len(self.trails)}
            - by_sectors: {repr(self.by_sector())}
            - trails length: {min(trails_length)} - {max(trails_length)}
            - area: {self.area().area()}
            - best estimation: {str(self.best_solution())}
            """)

    def update(self, constraints: Iterable[TrackingConstraint]) -> Tracking:
        if not constraints:
            return self
        new_trails: Iterable[ShipTrail] = self.trails
        new_trails = reduce(
            lambda trails, constraint: constraint.process(self.worldmap, trails),
            constraints,
            new_trails)
        return Tracking(self.worldmap, _compact(new_trails))

    @memoize_method
    def area(self):
        return Rect.containing(list(self.solutions()))

    @memoize_method
    def best_solution(self) -> Optional[PositionEstimation]:
        counters = Counter(trail.last() for trail in self.trails)
        solutions = counters.most_common(1)
        if solutions:
            total = sum(counters.values())
            (position, count) = solutions[0]
            return PositionEstimation(position, count / total)
        return None

    @memoize_method
    def solutions(self) -> FrozenSet[Position]:
        return frozenset(trail.last() for trail in self.trails)

    @memoize_method
    def count(self) -> int:
        return len(self.solutions())

    @memoize_method
    def mines(self) -> FrozenSet[Position]:
        result: Set[Position] = set()
        for trail in self.trails:
            result |= trail.all_mines()
        return frozenset(result)

    @memoize_method
    def by_sector(self):
        return Counter(self.worldmap.to_sector(trail.last()) for trail in self.trails)

    def render_solutions(self) -> Text:
        solutions = self.solutions()

        def display(position: Position) -> Optional[Text]:
            if position in solutions:
                return "◎"
            return None
        return self.worldmap.render(display)

    def render_mines(self) -> Text:
        mines = self.mines()

        def display(position: Position) -> Optional[Text]:
            if position in mines:
                return "☠"
            return None
        return self.worldmap.render(display)

    def render_trails(self) -> Iterable[Text]:
        # pylint: disable=cell-var-from-loop
        for trail in self.trails:
            def display(position: Position) -> Optional[Text]:
                if position == trail.current:
                    return "◎"
                if position in trail.visited:
                    return "#"
                return None
            yield self.worldmap.render(display)


def _compact(trails: Iterable[ShipTrail]) -> Set[ShipTrail]:
    return set(trails)


def _build_silence_paths() -> FrozenSet[Path]:
    paths = set([Path.empty()])
    for distance in range(1, 5):
        paths.add(Path((Direction.North,) * distance))
        paths.add(Path((Direction.South,) * distance))
        paths.add(Path((Direction.East,) * distance))
        paths.add(Path((Direction.West,) * distance))
    return frozenset(paths)


SilencePaths = _build_silence_paths()
