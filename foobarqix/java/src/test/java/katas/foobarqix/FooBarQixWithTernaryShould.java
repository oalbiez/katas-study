package katas.foobarqix;

public class FooBarQixWithTernaryShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return FooBarQixWithTernary.fooBarQix(input);
    }
}
