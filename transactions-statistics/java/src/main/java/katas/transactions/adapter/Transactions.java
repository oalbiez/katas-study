package katas.transactions.adapter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Transactions {

    public static List<Transaction> translateSuspended(final List<SuspendedTransaction> transactions) {
        return transactions.stream().map(Transactions::suspendedTransaction).collect(Collectors.toList());
    }

    public static List<Transaction> translateSettled(final List<SettledTransaction> transactions) {
        return transactions.stream().map(Transactions::settledTransaction).collect(Collectors.toList());
    }

    public static Transaction suspendedTransaction(final SuspendedTransaction transaction) {
        return new Transaction() {
            public String client() {
                return transaction.counterpart;
            }

            public long amount() {
                return transaction.amount;
            }

            public LocalDate date() {
                return transaction.suspended;
            }

            public boolean isSuspended() {
                return true;
            }
        };
    }

    public static Transaction settledTransaction(final SettledTransaction transaction) {
        return new Transaction() {
            public String client() {
                return transaction.client;
            }

            public long amount() {
                return transaction.amount;
            }

            public LocalDate date() {
                return transaction.settlementdate;
            }

            public boolean isSuspended() {
                return false;
            }
        };
    }
}
