package kata.ast;

public class Number implements AST {
    private final int value;

    public Number(int value) {
        this.value = value;
    }

    public <T> T accept(ASTVisitor<T> visitor) {
        return visitor.visitNumber(this);
    }

    public int value() {
        return value;
    }
}
