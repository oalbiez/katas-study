# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass

from powers import Powers


@dataclass(frozen=True)
class Subsystem:
    torpedo: int
    sonar: int
    silence: int
    mine: int

    def is_available(self, power: Powers) -> bool:
        if power == Powers.Torpedo:
            return self.torpedo >= 0
        if power == Powers.Sonar:
            return self.sonar >= 0
        if power == Powers.Silence:
            return self.silence >= 0
        if power == Powers.Mine:
            return self.mine >= 0
        return False

    def is_mine_ready(self) -> bool:
        return self.mine == 0

    def is_silence_ready(self) -> bool:
        return self.silence == 0

    def is_sonar_ready(self) -> bool:
        return self.sonar == 0

    def is_torpedo_ready(self) -> bool:
        return self.torpedo == 0
