
import { statementText, statementHtml } from 'rental';
import expect from 'expect';

describe('statement', () => {

  it('should be able to rent regular movies', () => {
    const movies = {
      'F001': { 'title': 'Regular movie', 'code': 'regular' },
    };

    const customer = {
      'name': 'Jhon Doe',
      'rentals': [
        { 'movieID': 'F001', 'days': 2 },
        { 'movieID': 'F001', 'days': 5 },
      ],
    };
    expect(statementText(customer, movies)).toEqual('Rental Record for Jhon Doe\n\tRegular movie\t2\n\tRegular movie\t6.5\nAmount owed is 8.5\nYou earned 2 frequent renter points\n');
  });

  it('should be able to rent childrens movies', () => {
    const movies = {
      'F001': { 'title': 'Childrens movie', 'code': 'childrens' },
    };

    const customer = {
      'name': 'Jhon Doe',
      'rentals': [
        { 'movieID': 'F001', 'days': 2 },
        { 'movieID': 'F001', 'days': 5 },
      ],
    };
    expect(statementText(customer, movies)).toEqual('Rental Record for Jhon Doe\n\tChildrens movie\t1.5\n\tChildrens movie\t4.5\nAmount owed is 6\nYou earned 2 frequent renter points\n');
  });

  it('should be able to rent new movies', () => {
    const movies = {
      'F001': { 'title': 'New movie', 'code': 'new' },
    };

    const customer = {
      'name': 'Jhon Doe',
      'rentals': [
        { 'movieID': 'F001', 'days': 2 },
        { 'movieID': 'F001', 'days': 5 },
      ],
    };
    expect(statementText(customer, movies)).toEqual('Rental Record for Jhon Doe\n\tNew movie\t6\n\tNew movie\t15\nAmount owed is 21\nYou earned 3 frequent renter points\n');
  });

});

describe('statementHtml', () => {

  it('should be able render without rental', () => {
    const movies = {};
    const customer = {
      'name': 'Jhon Doe',
      'rentals': [],
    };
    expect(statementHtml(customer, movies)).toEqual(cleanup(`
      |<h1>Rental Record for <em>Jhon Doe</em></h1>
      |<table>
      |</table>
      |<p>Amount owed is <em>0</em></p>
      |<p>You earned <em>0</em> frequent renter points</p>`));
  });

  it('should be able render with some rentals', () => {
    const movies = {
      'F001': { 'title': 'Star Wars', 'code': 'regular' },
      'F002': { 'title': 'Bambi', 'code': 'childrens' },
      'F003': { 'title': 'Assassin\'s Creed', 'code': 'new' },
    };
    const customer = {
      'name': 'Jhon Doe',
      'rentals': [
        { 'movieID': 'F001', 'days': 2 },
        { 'movieID': 'F002', 'days': 5 },
        { 'movieID': 'F003', 'days': 3 },
      ],
    };
    expect(statementHtml(customer, movies)).toEqual(cleanup(`
      |<h1>Rental Record for <em>Jhon Doe</em></h1>
      |<table>
      |<tr><td>Star Wars</td><td>2</td></tr>
      |<tr><td>Bambi</td><td>4.5</td></tr>
      |<tr><td>Assassin's Creed</td><td>9</td></tr>
      |</table>
      |<p>Amount owed is <em>15.5</em></p>
      |<p>You earned <em>4</em> frequent renter points</p>`));
  });

});


function cleanup(text) {
  let result = '';
  for (const line of text.split('\n')) {
    let space = true;
    for (let i = 0; i < line.length; i++) {
      if (line[i] === ' ' || line[i] === '\t') {
        //
      }
      else if (line[i] === '|') {
        if (space) {
          result += line.substring(i + 1, line.length);
          result += '\n';
        }
      }
      else {
        space = false;
      }
    }
  }
  return result;
}

/*
func CleanupString(data string) string {
	var output bytes.Buffer
	data = strings.TrimPrefix(data, "\n")
	data = strings.TrimSuffix(data, "\n")
	for _, line := range strings.SplitAfter(data, "\n") {
		space := true
		for i, c := range line {
			switch c {
			case '\t', ' ':
				continue
			case '|':
				if space {
					output.WriteString(line[i+1:])
					break
				}
			case '#':
				if space {
					break
				}
			default:
				space = false
			}
		}
	}
	return output.String()
}


*/
