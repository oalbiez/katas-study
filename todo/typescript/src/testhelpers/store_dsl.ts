import { parseTodoLists, renderTodoLists } from "../models/todo_dsl";
import { TodoListMemoryStore } from "../models/todo_list_memory_store";
import { TodoListStore } from "../models/todo_list_store";

export function storeWith(definition: string): TodoListStore {
  return new TodoListMemoryStore(parseTodoLists(definition));
}

export function renderStore(store: TodoListStore): string {
  return renderTodoLists(store.all());
}
