package katas.transactions.srp.statistics;

import katas.transactions.srp.ast.ValueProvider;

import java.util.List;

import static java.util.Arrays.asList;

public final class Statistics {
    private Statistics() {
    }

    public static SumOf sum(final ValueProvider<Long> provider) {
        return new SumOf(provider);
    }

    public static Count count() {
        return new Count();
    }

    public static All all(final Statistic inner) {
        return new All(inner);
    }

    public static Group group(final ValueProvider<String> key, final Statistic inner) {
        return new Group(key, inner);
    }

    public static Select select(final ValueProvider<Boolean> predicate, final Statistic statistic) {
        return new Select(predicate, statistic);
    }

    public static Statistic composite(final Statistic... inner) {
        return composite(asList(inner));
    }

    public static Statistic composite(final List<Statistic> inner) {
        return new Composite(inner);
    }
}
