# -*- coding: utf-8 -*-
from __future__ import annotations

from enum import Enum
from typing import Text


class Powers(Enum):
    Torpedo = "TORPEDO"
    Sonar = "SONAR"
    Silence = "SILENCE"
    Mine = "MINE"

    def render(self) -> Text:
        return self.value

    def __repr__(self) -> Text:
        return f"Powers.{self.name}"

    @classmethod
    def parse(cls, value: Text) -> Powers:
        for power in cls:
            if value == power.value:
                return power
        raise ValueError("unknown power " + repr(value))
