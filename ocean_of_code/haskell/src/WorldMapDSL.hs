module WorldMapDSL where


import qualified Data.Char     as Char
import           Data.Function ((&))
import qualified Data.List     as List
import qualified Data.Map      as Map
import qualified Data.Maybe    as Maybe
import           OceanOfCode


cleanup :: String -> [String]
cleanup definition = lines definition
                   & filter (notElem '━')
                   & map (filter (/= '┃'))
                   & map (filter (not . Char.isSpace))
                   & filter (not . null)

toCells :: [String] -> [(Char, Point)]
toCells byLines = map number byLines
              & number
              & toPoints
    where
        number = zip (iterate (1 +) 0)
        toPoints = concatMap
                (\(lnum, line) -> map
                    (\(cnum, cell) -> (cell, point cnum lnum)) line)

guessSize :: [String] -> Point
guessSize byLines = point (length $ head byLines) (length byLines)

parse :: String -> (WorldMap, Map.Map Char [Point])
parse definition =
    let byLines = cleanup definition
        mapSize = guessSize byLines
        symbols = toCells byLines
                & map (\(s, p) -> (s, [p]))
                & Map.fromListWith (++)
        islands = Map.findWithDefault [] '▲' symbols
    in (
        worldmap (rectTo mapSize) islands,
        symbols & Map.delete '▲' & Map.delete '•'
    )


type RenderOption = Point -> Maybe String

none :: RenderOption
none = const Nothing

symbols :: String -> [Point] -> RenderOption -> RenderOption
symbols value positions inner position =
    if position `elem` positions then Just value else inner position

cricleFor :: [Point] -> RenderOption
cricleFor positions = symbols "◎" positions none

deathFor :: [Point] -> RenderOption
deathFor positions = symbols "☠" positions none


render :: RenderOption -> WorldMap -> String
render symbol (WorldMap zone points) =
    let part = List.replicate (width zone `div` 5) "━━━━━━━━━━━"
        topline = "┏" ++ List.intercalate "┳" part ++ "┓\n"
        middleline = "┣" ++ List.intercalate "╋" part ++ "┫\n"
        bottomline = "┗" ++ List.intercalate "┻" part ++ "┛\n"
        display p =
            if p `notElem` points
                then "▲"
                else symbol p & Maybe.fromMaybe "•"
        rlines y = if y < height zone
            then (if y `rem` 5 == 0 && y /= 0 then middleline else "") ++
                List.intersperse ' ' (rcells y 0) ++ "\n" ++ rlines (y+1)
            else ""
        rcells y x = if x < width zone
            then (if x `rem` 5 == 0 then "┃" else "") ++ display (point x y) ++ rcells y (x + 1)
            else "┃"
    in topline ++ rlines 0 ++ bottomline
