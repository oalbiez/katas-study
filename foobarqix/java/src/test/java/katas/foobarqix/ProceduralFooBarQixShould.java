package katas.foobarqix;

public class ProceduralFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return Procedural.fooBarQix(input);
    }
}
