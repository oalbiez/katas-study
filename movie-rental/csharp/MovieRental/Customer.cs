﻿using System.Collections.Generic;
using System.Linq;

namespace MovieRental
{
    public class Customer
    {
        public readonly List<Rental> Rentals = new List<Rental>();

        public Customer(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public void AddRental(Rental rental)
        {
            Rentals.Add(rental);
        }

        public string Statement()
        {
            var result = "Rental Record for " + Name + "\n";
            foreach (var rental in Rentals)
            {
                // show figures for this rental
                result += "\t" + rental.Title + "\t" + rental.Charge().Amount + "\n";
            }

            result += "Amount owed is " + TotalAmount().Amount + "\n";
            result += "You earned " + FrequentRenterPoints() + " frequent renter points";
            return result;
        }

        private int FrequentRenterPoints() => Rentals.Sum(rental => rental.FrequentRenterPoints());

        private Price TotalAmount() => Rentals.Aggregate(Price.Euro(0), (price, rental) => price + rental.Charge());
    }
}