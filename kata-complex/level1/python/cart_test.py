import unittest

from cart import Cart, CartPricerUseCase, Item, Article


class MemoryArticleRepository:
    def __init__(self, *articles):
        self.articles = dict(((article.identifier, article) for article in articles))

    def get_article(self, identifier):
        return self.articles[identifier]


class MemoryCartRepository:
    def __init__(self, *carts):
        self.carts = dict(((cart.identifier, cart) for cart in carts))

    def save(self, cart):
        pass

    def get_cart(self, identifier):
        return self.carts[identifier]


class CartPricerUseCaseUnitTest(unittest.TestCase):

    def test_cart_total_price_should_be_0_when_cart_is_empty(self):
        self.assertEqual(0, self.compute_total(Cart('C01', items=[])))

    def test_cart_total_price_should_be_sum_of_articles_price(self):
        self.assertEqual(100*4 + 5*450, self.compute_total(
            cart=Cart('C01', items=[Item('A01', 4), Item('A02', 5)]),
            articles=[Article('A01', "Lait", 100), Article('A02', "Chocolat", 450)]))

    def compute_total(self, cart, articles=None):
        carts = MemoryCartRepository(cart)
        articles = MemoryArticleRepository(*(articles or []))
        CartPricerUseCase(articles, carts).compute([cart.identifier])
        return carts.get_cart(cart.identifier).total
