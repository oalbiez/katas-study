class WordTooSmall(ValueError):
    pass


class WordWithInvalidLetter(ValueError):
    def __init__(self, invalid_letters):
        super().__init__()
        self.invalid_letters = invalid_letters
