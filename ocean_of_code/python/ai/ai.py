# -*- coding: utf-8 -*-
from __future__ import annotations

import random
import sys

from textwrap import indent
from typing import Optional

from direction import Direction
from geometry import Position
from intelligence_report import IntelligenceReport
from life import LifeStatus
from navigation import Navigation
from orders import DropMine, Message, Move, Silence, Sonar, Surface, Torpedo, TriggerMine, Orders
from performance import Performance
from powers import Powers
from ship_trail import ShipTrail, Mine
from situation_awarness import SituationAwarness, TurnSituation
from sonar_result import SonarResult
from subsystem import Subsystem
from turn_information import TurnInformations
from world_map import WorldMap


def debug(*args):
    print(*args, file=sys.stderr, flush=True)


def replay(text):
    debug(indent(text, '#>', lambda line: True))


class OpponentSubmarine:
    # pylint: disable=too-few-public-methods

    def __init__(self):
        self.life: LifeStatus = LifeStatus(6, 6)

    def update_life(self, life: int) -> OpponentSubmarine:
        self.life = self.life.update(life)
        return self


class Submarine:
    def __init__(self, initial: Position):
        self.trail: ShipTrail = ShipTrail.on(initial)
        self.subsystem = Subsystem(-1, -1, -1, -1)
        self.life: LifeStatus = LifeStatus(6, 6)
        self.orders: Orders = Orders.empty()

    def position(self) -> Position:
        return self.trail.last()

    def update_position(self, position: Position) -> Submarine:
        self.trail = self.trail.move(position)
        return self

    def add_mine(self, position: Position) -> Submarine:
        self.trail = self.trail.mine(Mine.on(position))
        return self

    def trigger_mine(self, position: Position) -> Submarine:
        self.trail = self.trail.remove_mine(position)
        return self

    def surface(self) -> None:
        self.trail = self.trail.clear()

    def can_move(self, position: Position) -> bool:
        return position not in self.trail

    def update_subsystem(self, subsystem: Subsystem) -> Submarine:
        self.subsystem = subsystem
        return self

    def update_life(self, life: int) -> Submarine:
        self.life = self.life.update(life)
        return self

    def update_orders(self, orders: Orders) -> Submarine:
        self.orders = orders
        return self

    def has_power(self, power: Powers) -> bool:
        return self.subsystem.is_available(power)

    def is_torpedo_ready(self) -> bool:
        return self.subsystem.is_torpedo_ready()

    def is_sonar_ready(self) -> bool:
        return self.subsystem.is_sonar_ready()

    def is_silence_ready(self) -> bool:
        return self.subsystem.is_silence_ready()

    def is_mine_ready(self) -> bool:
        return self.subsystem.is_mine_ready()


def select_starting_point(worldmap: WorldMap) -> Position:
    return worldmap.find_biggest_water_area().center()


class Game:
    def __init__(self, interaction: Interaction):
        self.interaction = interaction
        self.worldmap: WorldMap = interaction.parse_map()
        self.navigation = Navigation(self.worldmap)
        self.my_ship: Submarine = Submarine(select_starting_point(self.worldmap))
        self.opponent = OpponentSubmarine()
        self.situation = SituationAwarness.on(self.worldmap)
        debug("# Initial state")
        replay(f"""
            scenario(
                '''
                {self.worldmap.render()}
                ''',
            """)

    def choose_starting_position(self) -> None:
        self.interaction.send_starting_position(self.my_ship.position())

    def turn(self) -> None:
        debug("## Waiting")
        wait_duration = Performance()
        turn = self.interaction.parse_turn()
        debug("wait duration:", wait_duration.elapsed())
        perf = Performance()

        debug("## Update status")
        self.my_ship.update_position(turn.my_position).update_life(turn.my_life).update_subsystem(turn.my_subsystem)
        self.opponent.update_life(turn.opponent_life)
        debug("duration:", perf.elapsed())

        debug("## Spying")
        turn_situation = TurnSituation(
            turn.my_position,
            self.my_ship.life,
            turn.my_sonar,
            self.my_ship.orders,
            self.opponent.life,
            turn.intelligence
        )
        replay(f"turn({repr(turn_situation)}),")
        self.situation = self.situation.update(turn_situation)
        debug(self.situation.statistics())
        debug("duration:", perf.elapsed())

        debug("## Thinking")
        orders = self.think(turn)
        debug("orders:", repr(orders))
        self.my_ship.update_orders(orders)
        self.interaction.send_orders(self.my_ship.orders)
        debug("duration:", perf.elapsed())
        debug("")

    def think(self, turn) -> Orders:
        return self.minefield(turn)
        # best_position = self.situation.best_opponent_position()
        # if best_position:
        #     debug("ATTACKING", best_position)
        #     return self.attacking(turn, best_position)
        # debug("HUNTING")
        # return self.hunting(turn)

    def attacking(self, _, position):
        def power():
            if not self.my_ship.is_torpedo_ready():
                return Powers.Torpedo
            if self.my_ship.has_power(Powers.Sonar):
                if self.situation.should_sonar() and not self.my_ship.is_sonar_ready():
                    return Powers.Sonar
            return Powers.Silence

        def goto(target):
            path = self.navigation.path(self.my_ship.trail, target)
            if len(path) > 3:
                next_step = path[1]
                for direction in Direction:
                    if direction.move(self.my_ship.position()) == next_step:
                        return direction
            return None

        def move_command():
            move = goto(position)
            if not move:
                move = self.possible_moves()
            if not move:
                self.my_ship.surface()
                return Surface()
            return Move(move, power())

        commands = Orders.of(Message(f"Attacking {position}"))
        if self.my_ship.is_torpedo_ready():
            target = self.situation.should_torpedo(self.my_ship.position())
            if target:
                commands = commands.add(Torpedo(target))
        if self.my_ship.is_silence_ready():
            commands = commands.add(Silence(Direction.North, 0))
        return commands.add(move_command())

    def hunting(self, _):
        def move_command():
            move = self.possible_moves()
            if not move:
                self.my_ship.surface()
                return Surface()
            return Move(move, power())

        def power():
            if self.my_ship.has_power(Powers.Sonar):
                if self.situation.should_sonar() and not self.my_ship.is_sonar_ready():
                    return Powers.Sonar
            if not self.my_ship.is_torpedo_ready():
                return Powers.Torpedo
            return Powers.Silence

        commands = Orders.of(Message("Hunting"))
        if self.my_ship.is_sonar_ready():
            sonar = self.situation.should_sonar()
            if sonar:
                commands = commands.add(Sonar(sonar))
        if self.my_ship.is_silence_ready():
            commands = commands.add(Silence(Direction.North, 0))
        else:
            commands = commands.add(move_command())
        return commands

    def minefield(self, _):
        def move_command():
            move = self.possible_moves()
            if not move:
                self.my_ship.surface()
                return Surface()
            return Move(move, power())

        def power():
            if self.my_ship.has_power(Powers.Sonar):
                if self.situation.should_sonar() and not self.my_ship.is_sonar_ready():
                    return Powers.Sonar
            if not self.my_ship.is_torpedo_ready():
                return Powers.Torpedo
            if self.my_ship.has_power(Powers.Mine) and not self.my_ship.is_mine_ready():
                return Powers.Mine
            return Powers.Silence

        commands = Orders.empty()
        mine = self.situation.should_trigger_mine(self.my_ship.trail)
        debug("mine to trigger: ", mine)
        if mine:
            commands = commands.add(TriggerMine(mine))
            self.my_ship.trigger_mine(mine)
        if self.my_ship.is_sonar_ready():
            sonar = self.situation.should_sonar()
            if sonar:
                commands = commands.add(Sonar(sonar))
        if self.my_ship.is_mine_ready():
            direction = self.situation.should_drop_mine(self.my_ship.trail)
            if direction:
                commands = commands.add(DropMine(direction))
                self.my_ship.add_mine(direction.move(self.my_ship.position()))
        if self.my_ship.is_torpedo_ready():
            target = self.situation.should_torpedo(self.my_ship.position())
            if target:
                commands = commands.add(Torpedo(target))
        if self.my_ship.is_silence_ready():
            commands = commands.add(Silence(Direction.North, 0))

        commands = commands.add(move_command())
        return commands

    def possible_moves(self) -> Optional[Direction]:
        def is_valid(position):
            return self.my_ship.can_move(position) and self.navigation.is_navigable(position)
        moves = []
        for direction in Direction:
            next_position = direction.move(self.my_ship.position())
            if is_valid(next_position):
                close_index = len([p for p in self.navigation.neighbors(next_position) if not self.my_ship.can_move(p)])
                moves.append((close_index, direction))
        if moves:
            best = max(move[0] for move in moves)
            return random.choice([move[1] for move in moves if move[0] == best])
        return None


class Interaction:
    # pylint: disable=no-self-use

    def __init__(self):
        pass

    def parse_map(self):
        # pylint: disable=unused-variable
        width, height, my_id = self.__parse_integer()
        data = [input() for i in range(height)]
        worldmap = WorldMap(width, height)
        for position in worldmap.walk():
            if data[position.y][position.x] == "x":
                worldmap.set_island(position)
        return worldmap

    def send_starting_position(self, position):
        print(position.x, position.y)

    def send_orders(self, orders: Orders):
        print(orders.render())

    def parse_turn(self) -> TurnInformations:
        (
            x,
            y,
            my_life,
            opp_life,
            torpedo_cooldown,
            sonar_cooldown,
            silence_cooldown,
            mine_cooldown) = self.__parse_integer()
        sonar = SonarResult.parse(input())
        intelligence = IntelligenceReport.parse(input())
        return TurnInformations(
            Position(x, y),
            my_life,
            Subsystem(torpedo_cooldown, sonar_cooldown, silence_cooldown, mine_cooldown),
            sonar,
            opp_life,
            intelligence)

    def __parse_integer(self):
        return [int(i) for i in input().split()]
