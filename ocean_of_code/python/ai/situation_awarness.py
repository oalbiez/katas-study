# -*- coding: utf-8 -*-
from __future__ import annotations

import random

from dataclasses import dataclass
from textwrap import dedent
from typing import Iterable, List, Optional, Text

from geometry import Position
from intelligence_report import IntelligenceReport
import intelligence_report as intelligence
from life import LifeStatus
from orders import Orders
import orders
from sonar_result import SonarResult
from tracking import Tracking, TrackingConstraint
from tracking import CanMove, DropMine, InPositions, InSector, MineTrigged, NotInPositions, NotInSector, Reset
from world_map import WorldMap
from ship_trail import ShipTrail
from direction import Direction


@dataclass(frozen=True)
class TurnSituation:
    my_position: Position
    my_life: LifeStatus
    my_sonar_echo: SonarResult
    my_orders: Orders
    opponent_life: LifeStatus
    intelligence: IntelligenceReport

    def my_constraints(self, worldmap: WorldMap) -> Iterable[TrackingConstraint]:
        constraints: List[TrackingConstraint] = []
        sonar = self.intelligence.sonar()
        if sonar:
            my_sector = worldmap.to_sector(self.my_position)
            if my_sector == sonar.sector:
                constraints.append(InSector.of(sonar.sector))
            else:
                constraints.append(NotInSector.of(sonar.sector))
        life_lost = self.__my_unexplained_life_lost()
        for target in self.intelligence.explosion_positions():
            constraints.extend(self.__on_explosion(target, life_lost))
        for command in self.my_orders:
            if isinstance(command, orders.DropMine):
                constraints.append(DropMine())
            elif isinstance(command, orders.Move):
                constraints.append(CanMove.to(command.direction))
            elif isinstance(command, orders.Silence):
                constraints.append(CanMove.silence())
            elif isinstance(command, orders.Surface):
                constraints.append(InSector.of(worldmap.to_sector(self.my_position)))
                constraints.append(Reset())
            elif isinstance(command, orders.Torpedo):
                constraints.append(InPositions.torpedo(command.target))
                constraints.extend(self.__on_explosion(command.target, life_lost))
            elif isinstance(command, orders.TriggerMine):
                constraints.append(MineTrigged.at(command.target))
                constraints.extend(self.__on_explosion(command.target, life_lost))
        return constraints

    def opponent_constraints(self) -> Iterable[TrackingConstraint]:
        constraints: List[TrackingConstraint] = []
        sonar = self.my_orders.sonar()
        if sonar:
            if self.my_sonar_echo == SonarResult.HIT:
                constraints.append(InSector.of(sonar.sector))
            elif self.my_sonar_echo == SonarResult.MISS:
                constraints.append(NotInSector.of(sonar.sector))
        life_lost = self.__opponent_unexplained_life_lost()
        for target in self.my_orders.explosion_positions():
            constraints.extend(self.__on_explosion(target, life_lost))
        for data in self.intelligence:
            if isinstance(data, intelligence.MineDroped):
                constraints.append(DropMine())
            elif isinstance(data, intelligence.MineTrigged):
                constraints.append(MineTrigged.at(data.target))
                constraints.extend(self.__on_explosion(data.target, life_lost))
            elif isinstance(data, intelligence.Movement):
                constraints.append(CanMove.to(data.direction))
            elif isinstance(data, intelligence.Silence):
                constraints.append(CanMove.silence())
            elif isinstance(data, intelligence.Surface):
                constraints.append(InSector.of(data.sector))
                constraints.append(Reset())
            elif isinstance(data, intelligence.Torpedo):
                constraints.append(InPositions.torpedo(data.target))
                constraints.extend(self.__on_explosion(data.target, life_lost))
        return constraints

    def __on_explosion(self, target: Position, life_lost: int) -> List[TrackingConstraint]:
        explosion_count = len(self.my_orders.explosion_positions() | self.intelligence.explosion_positions())
        constraints: List[TrackingConstraint] = []
        if life_lost == 0:
            constraints.append(NotInPositions.explosion(target))
        if life_lost == 1:
            if explosion_count == 1:
                constraints.append(InPositions.explosion_blast(target))
            else:
                constraints.append(NotInPositions.single(target))
        if life_lost == 2:
            if explosion_count == 1:
                constraints.append(InPositions.single(target))
        return constraints

    def __my_unexplained_life_lost(self):
        result = self.my_life.amount_lost()
        if self.my_orders.surface() is not None:
            result -= 1
        return result

    def __opponent_unexplained_life_lost(self):
        result = self.opponent_life.amount_lost()
        if self.intelligence.surface() is not None:
            result -= 1
        return result

    def __repr__(self) -> Text:
        return dedent(f"""TurnSituation(
    my_position={repr(self.my_position)},
    my_life={repr(self.my_life)},
    my_sonar_echo={repr(self.my_sonar_echo)},
    my_orders={repr(self.my_orders)},
    opponent_life={repr(self.opponent_life)},
    intelligence={repr(self.intelligence)})""")


@dataclass
class SituationAwarness:
    worldmap: WorldMap
    opponent_tracking: Tracking
    my_tracking: Tracking

    @staticmethod
    def on(worldmap: WorldMap) -> SituationAwarness:
        return SituationAwarness(
            worldmap,
            Tracking.empty(worldmap),
            Tracking.empty(worldmap))

    def update(self, turn: TurnSituation) -> SituationAwarness:
        # self.my_tracking = self.my_tracking.update(turn.my_constraints(self.worldmap))
        return SituationAwarness(
            self.worldmap,
            self.opponent_tracking.update(turn.opponent_constraints()),
            self.my_tracking)

    def best_opponent_position(self) -> Optional[Position]:
        best_solution = self.opponent_tracking.best_solution()
        if best_solution and best_solution.estimation > 0.1:
            return best_solution.position
        return None

    def should_sonar(self) -> Optional[int]:
        by_sector = self.opponent_tracking.by_sector()
        if len(by_sector) == 1:
            return None
        best_sector = None
        best_score = 0
        for sector, score in by_sector.items():
            if score > best_score:
                best_score = score
                best_sector = sector
        return best_sector

    def should_torpedo(self, my_position: Position) -> Optional[Position]:
        target = self.best_opponent_position()
        if target:
            targetable = self.worldmap.possible_targets(my_position)
            if target in targetable.safe or target in targetable.danger:
                return target
        return None

    def should_drop_mine(self, trail: ShipTrail) -> Optional[Direction]:
        result: List[Direction] = []
        for direction in Direction:
            target = direction.move(trail.current)
            if self.worldmap.is_water(target) and not trail.has_mine(target):
                result.append(direction)
        if result:
            return random.choice(result)
        return None

    def should_trigger_mine(self, trail: ShipTrail) -> Optional[Position]:
        opponent_positions = self.opponent_tracking.solutions()
        for mine in trail.all_mines():
            blast_zone = self.worldmap.explosion_range(mine)
            if mine in opponent_positions and trail.current not in blast_zone:
                return mine
        for mine in trail.all_mines():
            blast_zone = self.worldmap.explosion_range(mine)
            if blast_zone & opponent_positions and trail.current not in blast_zone:
                return mine
        return None

    def statistics(self) -> Text:
        return dedent(f"""
opponent: {self.opponent_tracking.statistics()}
best_position: {self.best_opponent_position()}
should_sonar: {self.should_sonar()}
""")

    def render_opponent(self) -> Text:
        return self.opponent_tracking.render_solutions()
