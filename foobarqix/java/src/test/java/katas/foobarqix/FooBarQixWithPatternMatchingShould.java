package katas.foobarqix;

public class FooBarQixWithPatternMatchingShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return FooBarQixWithPatternMatching.fooBarQix(input);
    }
}
