package katas.foobarqix;

import static katas.foobarqix.FooBarQixWithRules.rule;
import static katas.foobarqix.FooBarQixWithRules.rules;

public class FooBarQixWithRulesShould extends FooBarQixShould {
    private final static FooBarQixWithRules RULES = rules(
            rule(3, "Foo"),
            rule(5, "Bar"),
            rule(7, "Qix"));

    String fooBarQix(final int input) {
        return RULES.compute(input);
    }
}
