package katas.transactions.transaction;

public enum HoldingReason {
    PTYH("Party Hold"),
    CSDH("CSD Hold");

    private final String description;

    HoldingReason(final String description) {
        this.description = description;
    }

    public String describe() {
        return description;
    }
}
