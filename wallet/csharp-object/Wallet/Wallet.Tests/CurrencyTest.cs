﻿using NUnit.Framework;
using static NUnit.Framework.Assert;
using static Wallet.Currency;

namespace Wallet.Tests
{
    [TestFixture]
    public class CurrencyTest
    {
        [Test]
        public void currency_should_equal()
        {
            AreEqual(EUR, EUR);
            AreNotEqual(USD, EUR);
        }

        [Test]
        public void currency_should_render()
        {
            AreEqual("EUR", EUR.ToString());
        }

        [Test]
        public void precision_should_round_to_nearest_value()
        {
            AreEqual(EUR.Amount(1.23m), EUR.Amount(1.234m));
            AreEqual(EUR.Amount(1.24m), EUR.Amount(1.235m));
            AreEqual(EUR.Amount(1.24m), EUR.Amount(1.236m));
        }

        [Test]
        public void euro_should_have_2_digits_precision()
        {
            AreEqual(EUR.Amount(0.01m), EUR.Amount(0.01m));
            AreEqual(EUR.Amount(0), EUR.Amount(0.001m));
        }
    }
}