package katas.birthdaygreetings.business;

import org.junit.Test;

import java.time.LocalDate;

import static java.time.Month.*;
import static katas.birthdaygreetings.business.Birthdate.birthdate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BirthdateUnitTest {
    @Test
    public void is_birthday_should_true_when_same_month_and_day_of_month() {
        assertTrue(birthdate(2000, JANUARY, 1).isBirthday(LocalDate.of(2018, JANUARY, 1)));
    }

    @Test
    public void is_birthday_should_true_when_born_on_29_february_and_today_is_28_february_on_non_leap_years() {
        assertTrue(birthdate(2004, FEBRUARY, 29).isBirthday(LocalDate.of(2015, FEBRUARY, 28)));
    }

    @Test
    public void is_birthday_should_false_otherwise() {
        assertFalse(birthdate(2000, JANUARY, 1).isBirthday(LocalDate.of(2018, JULY, 1)));
        assertFalse(birthdate(2004, FEBRUARY, 29).isBirthday(LocalDate.of(2016, FEBRUARY, 28)));
    }
}