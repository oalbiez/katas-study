package katas.buyer.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import katas.buyer.model.*;
import org.assertj.core.api.SoftAssertions;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static katas.buyer.model.Decider.decider;

public class PurchaseRequestStepDefinitions {
    private PurchaseRequest request;
    private final List<Offer> offers = newArrayList();
    private Decider decider = decider();

    @Given("the penalty is EUR {price} per day")
    public void thePenaltyIsEURPerDay(final Price price) throws Throwable {
        decider = decider(Penalty.of(price));
    }

    @Given("a purchase request of {quantity} cups")
    public void aPurchaseRequestOfCupsForEUR(final Quantity quantity) {
        request = PurchaseRequest.of(quantity, Delay.none());
    }

    @Given("a purchase request of {quantity} cups in {delay}")
    public void aPurchaseRequestOfCupsForEUR(final Quantity quantity, final Delay delay) {
        request = PurchaseRequest.of(quantity, delay);
    }

    @When("we have a single offer of {quantity} cups for EUR {price} by {vendor}")
    public void weHaveASingleOfferOfCupsForEURBy(final Quantity quantity, final Price price, final Vendor vendor) {
        offers.add(Offer.of(vendor, quantity, price));
    }

    @Then("we should accept the offer {quantity} cups for EUR {price} by {vendor}")
    public void weShouldAcceptTheOfferCupsForEURBy(final Quantity quantity, final Price price, final Vendor vendor) {
        final Decision decision = decider.decisionFor(request, offers);
        final SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(decision.vendor()).isEqualToComparingFieldByField(vendor);
        assertions.assertThat(decision.quantity()).isEqualToComparingFieldByField(quantity);
        assertions.assertThat(decision.price()).isEqualToComparingFieldByField(price);
        assertions.assertAll();
    }

    @Then("we should accept the offer {quantity} cups for EUR {price} in {delay} by {vendor}")
    public void weShouldAcceptTheOfferCupsForEURBy(final Quantity quantity, final Price price, final Delay delay, final Vendor vendor) {
        final Decision decision = decider.decisionFor(request, offers);
        final SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(decision.vendor()).isEqualToComparingFieldByField(vendor);
        assertions.assertThat(decision.quantity()).isEqualToComparingFieldByField(quantity);
        assertions.assertThat(decision.price()).isEqualToComparingFieldByField(price);
        assertions.assertAll();
    }

    @When("we receive following offers")
    public void coco(List<Offer> offers) {
        this.offers.addAll(offers);
    }
}
