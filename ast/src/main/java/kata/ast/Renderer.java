package kata.ast;

public class Renderer {
    public static String renderInfix(AST ast) {
        return ast.accept(new ASTVisitor<String>() {
            public String visitNumber(Number number) {
                return String.valueOf(number.value());
            }

            public String visitAdd(Add add) {
                return add.left().accept(this) + " + " + add.right().accept(this);
            }

            public String visitSub(Sub sub) {
                return sub.left().accept(this) + " - " + sub.right().accept(this);
            }
        });
    }


    public static String renderRpn(AST ast) {
        if (ast instanceof Number) {
            Number number = (Number) ast;
            return String.valueOf(number.value());
        }
        if (ast instanceof Add) {
            Add add = (Add) ast;
            return renderRpn(add.left()) + " " + renderRpn(add.right()) + " +";
        }
        return null;
    }
}
