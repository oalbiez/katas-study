package katas.transactions.value;

import com.google.common.base.Objects;

public final class SingleValue implements Value {
    private final String content;

    SingleValue(final String content) {
        this.content = content;
    }

    public String get() {
        return content;
    }

    public <T> T accept(final ValueVisitor<T> visitor) {
        return visitor.visitSingle(this);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        final SingleValue value = (SingleValue) other;
        return Objects.equal(content, value.content);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(content);
    }

    @Override
    public String toString() {
        return content;
    }
}
