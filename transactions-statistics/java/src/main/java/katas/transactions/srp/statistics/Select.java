package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.srp.ast.ValueProvider;
import katas.transactions.value.Value;

public final class Select implements Statistic {
    private final ValueProvider<Boolean> predicate;
    private final Statistic statistic;

    Select(final ValueProvider<Boolean> predicate, final Statistic statistic) {
        this.predicate = predicate;
        this.statistic = statistic;
    }

    public void updateWith(final Transaction transaction) {
        if (predicate.get(transaction)) {
            statistic.updateWith(transaction);
        }
    }

    public void accept(final StatisticVisitor visitor) {
        visitor.visitSelect(this);
    }

    public Statistic fork() {
        return new Select(predicate, statistic.fork());
    }

    public String describe() {
        return statistic.describe() + " when " + predicate.describe();
    }

    public Value value() {
        return statistic.value();
    }

    public Statistic statistic() {
        return statistic;
    }

    public ValueProvider<Boolean> predicate() {
        return predicate;
    }
}
