package katas.buyer.model.ast;

public class Describe implements ExpressionVisitor<String> {

    public static String describe(final Expression expression) {
        return expression.accept(new Describe());
    }

    public String acceptAdd(final Expression left, final Expression right) {
        return left.accept(this) + " + " + right.accept(this);
    }

    public String acceptSub(final Expression left, final Expression right) {
        return left.accept(this) + " - " + right.accept(this);
    }

    public String acceptMul(final Expression left, final Expression right) {
        return left.accept(this) + " * " + right.accept(this);
    }

    public String acceptDiv(final Expression left, final Expression right) {
        return left.accept(this) + " / " + right.accept(this);
    }

    public String acceptOfferPrice() {
        return "<offer price>";
    }

    public String acceptOfferQuantity() {
        return "<offer quantity>";
    }

    public String acceptRequestedQuantity() {
        return "<requested quantity>";
    }
}
