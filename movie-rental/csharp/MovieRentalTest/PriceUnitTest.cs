﻿using MovieRental;
using NUnit.Framework;
using static MovieRental.Price;
using static NUnit.Framework.Assert;

namespace MovieRentalTest
{
    [TestFixture]
    public class PriceUnitTest
    {
        [Test]
        public void price_should_equals()
        {
            AreEqual(Euro(0), Euro(0));
        }

        [Test]
        public void price_should_compare()
        {
            True(Euro(4) != Euro(3));
        }

        [Test]
        public void price_should_render()
        {
            AreEqual("3€", Euro(3).ToString());
        }

        [Test]
        public void price_should_add()
        {
            AreEqual(Euro(7), Euro(3) + Euro(4));
        }

        [Test]
        public void price_should_mul()
        {
            AreEqual(Euro(8), Euro(4) * 2);
        }
    }
}