package katas.birthdaygreetings.business;

import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;

import static java.time.Month.FEBRUARY;
import static java.time.format.DateTimeFormatter.ISO_DATE;

public final class Birthdate {
    private final static MonthDay FEBRUARY_29 = MonthDay.of(FEBRUARY, 29);
    private final static MonthDay FEBRUARY_28 = MonthDay.of(FEBRUARY, 28);
    private final LocalDate value;

    public static Birthdate birthdate(final int year, final int month, final int day) {
        return birthdate(LocalDate.of(year, month, day));
    }

    public static Birthdate birthdate(final int year, final Month month, final int day) {
        return birthdate(LocalDate.of(year, month, day));
    }

    public static Birthdate birthdate(final String date) {
        return birthdate(LocalDate.parse(date, ISO_DATE));
    }

    public static Birthdate birthdate(final LocalDate date) {
        return new Birthdate(date);
    }

    private Birthdate(final LocalDate value) {
        this.value = value;
    }

    public boolean isBirthday(final LocalDate today) {
        if (!today.isLeapYear() && birthday().equals(FEBRUARY_29) && MonthDay.from(today).equals(FEBRUARY_28)) {
            return true;
        }
        return birthday().equals(MonthDay.from(today));
    }

    private MonthDay birthday() {
        return MonthDay.from(value);
    }

    @Override
    public String toString() {
        return value.format(ISO_DATE);
    }
}
