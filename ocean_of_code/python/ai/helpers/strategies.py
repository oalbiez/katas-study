from hypothesis.strategies import integers, tuples

from geometry import Position, Delta


def deltas():
    return tuples(integers(), integers()).map(lambda t: Delta(*t))


def positions():
    return tuples(integers(), integers()).map(lambda t: Position(*t))
