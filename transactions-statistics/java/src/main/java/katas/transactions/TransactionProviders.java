package katas.transactions;

import java.time.LocalDate;

public final class TransactionProviders {
    public static TransactionProvider random(final LocalDate today) {
        return new RandomTransactionProvider(today);
    }
}
