﻿using System;
using System.Collections.Generic;

namespace MovieRental
{
    public struct Price : IEquatable<Price>
    {
        private Price(double amount)
        {
            Amount = amount;
        }

        public double Amount { get; }

        public bool Equals(Price other)
        {
            return Amount.Equals(other.Amount);
        }

        public static Price Euro(double value) => new Price(value);

        public static Price operator +(Price x, Price y) => new Price(x.Amount + y.Amount);
        public static Price operator *(Price x, double factor) => new Price(x.Amount * factor);
        public static bool operator ==(Price left, Price right) => Equals(left, right);
        public static bool operator !=(Price left, Price right) => !Equals(left, right);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (@obj is Price)
            {
                return Equals((Price) obj);
            }

            return false;
        }

        public override int GetHashCode() => Amount.GetHashCode();

        public override string ToString() => $"{Amount}€";
    }
}