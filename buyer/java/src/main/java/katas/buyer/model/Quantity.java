package katas.buyer.model;

import java.math.BigDecimal;

public final class Quantity {
    private final long value;

    public static Quantity of(final String value) {
        return of(Long.valueOf(value));
    }

    public static Quantity of(final long value) {
        return new Quantity(value);
    }

    private Quantity(final long value) {
        this.value = value;
    }

    public BigDecimal value() {
        return BigDecimal.valueOf(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
