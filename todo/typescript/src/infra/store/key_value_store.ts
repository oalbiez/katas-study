import { List } from "immutable";
import { Maybe } from "tsmonad";

export type Key string;
export type Value string;

export interface KeyValueStore {
    save(key: Key, value: Value);
    load(key: Key): Maybe<Value>;
    keys(): List<Key>;
    values(): List<Value>;
    remove(key: Key);
}
