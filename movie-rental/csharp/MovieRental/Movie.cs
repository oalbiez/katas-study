﻿namespace MovieRental
{
    public class Movie
    {
        public Movie(string title, IPriceCode priceCode)
        {
            Title = title;
            PriceCode = priceCode;
        }

        public string Title { get; }
        public IPriceCode PriceCode { get; }

        public Price ChargeFor(Duration days) => PriceCode.Chargefor(days);

        public int FrequentRenterPointsFor(Duration days) => PriceCode.FrequentRenterPointsFor(days);
    }
}