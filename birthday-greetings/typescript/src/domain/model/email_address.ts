
const validation = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;

export class EmailAddress {
    public static fromString(value: string): EmailAddress {
        if (validation.test(value)) {
            return new EmailAddress(value);
        }
        throw new TypeError(`${value} is not a valid email`);
    }

    private constructor(public readonly value: string) {
    }
}
