package katas.marsrover;

public class Map {
    private final MapSize size;
    private final Obstacles obstacles;

    public static Map map(MapSize size, Obstacles obstacles) {
        return new Map(size, obstacles);
    }

    private Map(MapSize size, Obstacles obstacles) {
        this.size = size;
        this.obstacles = obstacles;
    }

    public Move move(final Position position, Distance distance) {
        Position nextPosition = map(position.add(distance));
        return obstacles.at(nextPosition).<Move>map(o -> new MoveAborted(o, position)).orElseGet(() -> new MoveCompleted(nextPosition));
    }

    private Position map(final Position position) {
        return position.wrap(size.width(), size.height());
    }

}
