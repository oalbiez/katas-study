package katas.bottle;

import org.junit.Test;

import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.joining;
import static katas.bottle.Bottle.bottle;
import static katas.bottle.Solution.solution;
import static org.junit.Assert.*;

public class SolutionTest {
    @Test
    public void solution_should_list_bottles_available_as_source() {
        assertEquals("0", render(solution(bottle(4, 4)).fromIndexes()));
        assertEquals("0", render(solution(bottle(4, 3)).fromIndexes()));
        assertEquals("", render(solution(bottle(4, 0)).fromIndexes()));
    }

    @Test
    public void solution_should_list_bottles_available_as_destination() {
        assertEquals("", render(solution(bottle(4, 4)).toIndexes()));
        assertEquals("0", render(solution(bottle(4, 3)).toIndexes()));
        assertEquals("0", render(solution(bottle(4, 0)).toIndexes()));
    }

    @Test
    public void solution_should_match_predicate_when_any_bottle_match() {
        assertTrue(solution(bottle(4, 4)).match(b -> b.quantity() == 4));
        assertFalse(solution(bottle(4, 3)).match(b -> b.quantity() == 4));
    }

    @Test
    public void solution_should_transfer_one_bottle_into_another() {
        Solution solution = solution(bottle(3, 3), bottle(5, 0));
        assertTrue(solution.transfer(0, 1).match(contains(bottle(3, 0))));
        assertTrue(solution.transfer(0, 1).match(contains(bottle(5, 3))));
    }

    @Test
    public void solution_should_transfer_making_a_copy() {
        Solution solution = solution(bottle(3, 3), bottle(5, 0));
        solution.transfer(0, 1);
        assertTrue(solution.match(contains(bottle(3, 3))));
        assertTrue(solution.match(contains(bottle(5, 0))));
    }

    private static String render(final List<Integer> indexes) {
        return indexes.stream().map(String::valueOf).collect(joining(", "));
    }

    private static Predicate<Bottle> contains(Bottle bottle) {
        return bottle::equals;
    }
}
