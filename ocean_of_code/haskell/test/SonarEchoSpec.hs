{-# LANGUAGE OverloadedStrings #-}
module SonarEchoSpec (spec) where


import           Data.Function ((&))
import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec = do

    it "SonarEcho render" $ do
        renderSonarEcho NA `shouldBe` "NA"
        renderSonarEcho Hit `shouldBe` "Y"
        renderSonarEcho Miss `shouldBe` "N"


    it "SonarEcho render/parser should be opposite operation" $ do
        ("NA" & parseSonarEcho & renderSonarEcho) `shouldBe` "NA"
        ("Y" & parseSonarEcho & renderSonarEcho) `shouldBe` "Y"
        ("N" & parseSonarEcho & renderSonarEcho) `shouldBe` "N"

