package katas.buyer.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DelayTest {

    @Test
    public void delay_should_be_subtractable() {
        assertEquals(Delay.of("2"), Delay.of("3").sub(Delay.of("1")));
    }

    @Test
    public void delay_should_be_clampable_to_zero() {
        assertEquals(Delay.of("0"), Delay.of("-3").clampToZero());
        assertEquals(Delay.of("0"), Delay.of("0").clampToZero());
        assertEquals(Delay.of("3"), Delay.of("3").clampToZero());
    }
}