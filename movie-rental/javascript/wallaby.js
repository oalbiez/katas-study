module.exports = () => {
  return {
    files: [
      'src/**/*.js',
    ],
    tests: [
      'src/**/__tests__/*-test.js',
    ],
    debug: true,
  };
};
