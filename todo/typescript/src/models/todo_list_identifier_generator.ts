import { TodoListIdentifier } from "./todo_list_identifier";

export interface TodoListIdentifierGenerator {
  generate(): TodoListIdentifier;
}
