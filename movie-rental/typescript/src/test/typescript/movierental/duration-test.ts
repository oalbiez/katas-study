import { Duration } from "../../../main/typescript/movierental/duration";

describe("Duration should", () => {

    it("have min", () => {
        expect(Duration.min(Duration.days(4), Duration.days(6))).toEqual(Duration.days(4));
    });
});
