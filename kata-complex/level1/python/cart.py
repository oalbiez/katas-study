

class Article:
    def __init__(self, identifier, name, price):
        self.identifier = identifier
        self.name = name
        self.price = price


class ArticleRepository:
    def get_article(self, identifier):
        pass


class CartRepository:
    def save(self, cart):
        pass

    def get_cart(self, identifier):
        pass


class Cart:
    def __init__(self, identifier, items, total=0):
        self.identifier = identifier
        self.items = items or []
        self.total = total

class Item:
    def __init__(self, article_id, quantity):
        self.article_id = article_id
        self.quantity = quantity


class CartPricerUseCase:
    def __init__(self, article_repository, cart_repository):
        self.article_repository = article_repository
        self.cart_repository = cart_repository

    def compute(self, cart_identifiers):
        for cart_identifier in cart_identifiers:
            self.compute_cart_price(self.cart(cart_identifier))

    def compute_cart_price(self, cart):
        cart.total = sum((self.article(item.article_id).price * item.quantity for item in cart.items))
        self.cart_repository.save(cart)

    def cart(self, cart_identifier):
        return self.cart_repository.get_cart(cart_identifier)

    def article(self, article_identifier):
        return self.article_repository.get_article(article_identifier)
