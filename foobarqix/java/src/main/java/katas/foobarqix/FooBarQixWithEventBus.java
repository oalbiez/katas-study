package katas.foobarqix;

import java.util.List;
import java.util.function.Consumer;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.valueOf;

/**
 */
public class FooBarQixWithEventBus {

    interface Event {
    }

    static class EventBus {
        private final List<Consumer<Event>> subscribers = newArrayList();

        void notifyEvent(final Event event) {
            subscribers.forEach(subscriber -> subscriber.accept(event));
        }

        @SuppressWarnings("unchecked")
        <T extends Event> void subscribe(Class<T> type, final Consumer<T> consumer) {
            subscribers.add(event -> {
                if (type.isInstance(event)) {
                    consumer.accept((T) event);
                }
            });
        }
    }

    static class Input implements Event {
        final int value;

        Input(int value) {
            this.value = value;
        }
    }

    static class FooSymbolic implements Event {
        final String symbol;
        final int value;

        FooSymbolic(String symbol, int value) {
            this.symbol = symbol;
            this.value = value;
        }
    }

    static class FooNumeric implements Event {
        final int value;

        FooNumeric(int value) {
            this.value = value;
        }
    }

    static class BarSymbolic implements Event {
        final String symbol;
        final int value;

        BarSymbolic(String symbol, int value) {
            this.symbol = symbol;
            this.value = value;
        }
    }

    static class BarNumeric implements Event {
        final int value;

        BarNumeric(int value) {
            this.value = value;
        }
    }

    static class Result implements Event {
        final String value;

        Result(String value) {
            this.value = value;
        }
    }

    private String result;

    public String fooBarQix(final int input) {
        final EventBus eventBus = new EventBus();

        eventBus.subscribe(Input.class, event -> {
            if (isDivisibleBy(event.value, 3)) {
                eventBus.notifyEvent(new FooSymbolic("Foo", event.value));
            } else {
                eventBus.notifyEvent(new FooNumeric(event.value));
            }

        });

        eventBus.subscribe(FooNumeric.class, event -> {
            if (isDivisibleBy(event.value, 5)) {
                eventBus.notifyEvent(new BarSymbolic("Bar", event.value));
            } else {
                eventBus.notifyEvent(new BarNumeric(event.value));
            }
        });
        eventBus.subscribe(FooSymbolic.class, event -> {
            if (isDivisibleBy(event.value, 5)) {
                eventBus.notifyEvent(new BarSymbolic(event.symbol + "Bar", event.value));
            } else {
                eventBus.notifyEvent(new BarSymbolic(event.symbol, event.value));
            }
        });

        eventBus.subscribe(BarNumeric.class, event -> {
            if (isDivisibleBy(event.value, 7)) {
                eventBus.notifyEvent(new Result("Qix"));
            } else {
                eventBus.notifyEvent(new Result(valueOf(event.value)));
            }
        });
        eventBus.subscribe(BarSymbolic.class, event -> {
            if (isDivisibleBy(event.value, 7)) {
                eventBus.notifyEvent(new Result(event.symbol + "Qix"));
            } else {
                eventBus.notifyEvent(new Result(event.symbol));
            }
        });

        eventBus.subscribe(Result.class, event -> result = event.value);

        eventBus.notifyEvent(new Input(input));
        return result;
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
