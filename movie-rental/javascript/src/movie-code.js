'use strict';


export const MovieCode = {
  'childrens': Childrens,
  'new': New,
  'regular': Regular,
};


const Childrens = {
  charge(days) {
    let thisAmount = 1.5;
    if (days > 3) {
      thisAmount += (days - 3) * 1.5;
    }
    return thisAmount;
  },
  frequentRenterPoints() {
    return 1;
  },
};


const Regular = {
  charge(days) {
    let thisAmount = 2;
    if (days > 2) {
      thisAmount += (days - 2) * 1.5;
    }
    return thisAmount;
  },
  frequentRenterPoints() {
    return 1;
  },
};


const New = {
  charge(days) {
    return days * 3;
  },
  frequentRenterPoints(days) {
    if (days > 2) {
      return 2;
    }
    return 1;
  },
};
