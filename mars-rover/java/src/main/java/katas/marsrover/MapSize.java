package katas.marsrover;

public class MapSize {
    private final long width;
    private final long height;

    public MapSize(long width, long height) {
        this.width = width;
        this.height = height;
    }

    public long width() {
        return width;
    }

    public long height() {
        return height;
    }
}
