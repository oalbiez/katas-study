package katas.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (final Item item : items) {
            final boolean isAgedBrie = item.name.equals("Aged Brie");
            final boolean isBackstage = item.name.equals("Backstage passes to a TAFKAL80ETC concert");
            final boolean isSulfuras = item.name.equals("Sulfuras, Hand of Ragnaros");

            if (isAgedBrie) {
                if (item.quality < 50) {
                    item.quality = item.quality + 1;

                }

                item.sellIn = item.sellIn - 1;

                if (item.sellIn < 0) {
                    if (item.quality < 50) {
                        item.quality = item.quality + 1;
                    }
                }
                return ;
            } else if (isSulfuras) {
                return ;

            } if (isBackstage) {
                if (item.quality < 50) {
                    item.quality = item.quality + 1;
                    if (item.sellIn < 11) {
                            if (item.quality < 50) {
                                item.quality = item.quality + 1;
                            }
                        }

                    if (item.sellIn < 6) {
                            if (item.quality < 50) {
                                item.quality = item.quality + 1;
                            }
                        }
                }

                item.sellIn = item.sellIn - 1;

                if (item.sellIn < 0) {
                    item.quality = 0;
                }
                return ;
            } else {
                if (item.quality > 0) {
                    item.quality = item.quality - 1;
                }

                item.sellIn = item.sellIn - 1;

                if (item.sellIn < 0) {
                    if (item.quality > 0) {
                        item.quality = item.quality - 1;
                    }
                }

            }
        }
    }

}
