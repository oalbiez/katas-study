package katas.wallet;

public interface RateProvider {
    Rate rateFor(Currency from, Currency to);
}
