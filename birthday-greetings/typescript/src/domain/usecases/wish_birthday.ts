import { User } from "domain/model/user";

export interface WishBirthday {
    wishFor(user: User): void;
}
