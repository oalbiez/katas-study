package katas.transactions.kiss.poi;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

public class StyleBuilder {
    private final CellStyle style;
    private final Workbook workbook;

    public static StyleBuilder style(final Workbook workbook) {
        return new StyleBuilder(workbook);
    }

    private StyleBuilder(final Workbook workbook) {
        this.workbook = workbook;
        style = this.workbook.createCellStyle();
    }

    public StyleBuilder align(final HorizontalAlignment alignment) {
        style.setAlignment(alignment);
        return this;
    }

    public StyleBuilder format(final int format) {
        style.setDataFormat((short) format);
        return this;
    }

    public StyleBuilder format(final String format) {
        style.setDataFormat(workbook.createDataFormat().getFormat(format));
        return this;
    }

    public StyleBuilder font(final Font font) {
        style.setFont(font);
        return this;
    }

    public CellStyle get() {
        return style;
    }
}
