package katas.movierental.statement;

import katas.movierental.Price;

public class Line {
    final private String description;
    final private Price price;

    public Line(String description, Price price) {
        this.description = description;
        this.price = price;
    }

    public String renderText() {
        return description + "\t" + price.render();
    }
}
