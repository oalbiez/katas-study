import { expect } from "chai";
import * as path from "path";

import { User } from "domain/model/user";
import { CSVUserProvider } from "infra/spi/csv_user_provider";

describe("CSVUserProvider", () => {

    it("load users from CSV file", () => {
        expect(loadFrom("users.csv")).to.be.equals("john.doe@foobar.com, mary.ann@foobar.com");
    });

    function loadFrom(filepath: string): string {
        const users: User[] = new CSVUserProvider(path.resolve(__dirname, filepath)).all();
        return users.map((u) => u.email.value).join(", ");
    }

});
