package katas.buyer.features;

import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;
import io.cucumber.cucumberexpressions.ParameterType;
import io.cucumber.cucumberexpressions.Transformer;
import io.cucumber.datatable.DataTableType;
import io.cucumber.datatable.TableEntryTransformer;
import katas.buyer.model.*;

import java.util.Locale;

import static java.util.Locale.ENGLISH;

@SuppressWarnings("unused")
public class TypeRegistryConfiguration implements TypeRegistryConfigurer {

    public Locale locale() {
        return ENGLISH;
    }

    public void configureTypeRegistry(final TypeRegistry typeRegistry) {
        typeRegistry.defineParameterType(new ParameterType<>("quantity", "\\d+", Quantity.class, (Transformer<Quantity>) Quantity::of));
        typeRegistry.defineParameterType(new ParameterType<>("price", "\\d+.\\d{2}", Price.class, (Transformer<Price>) Price::of));
        typeRegistry.defineParameterType(new ParameterType<>("vendor", ".+", Vendor.class, Vendor::of));
        typeRegistry.defineParameterType(new ParameterType<>("delay", "\\d+ days?", Delay.class, Delay::of));
        typeRegistry.defineDataTableType(new DataTableType(Offer.class, rowToOffer()));
    }

    private TableEntryTransformer<Offer> rowToOffer() {
        return entry ->
                Offer.of(
                        Vendor.of(entry.get("Vendor")),
                        Quantity.of(entry.get("Quantity")),
                        Price.of(entry.get("Unitary Price")),
                        Delay.of(entry.get("Delay"))
                );
    }
}
