# -*- coding: utf-8 -*-

from hypothesis import given

from geometry import Position
from ship_trail import ShipTrail
from helpers.strategies import positions


@given(positions())
def test_trail_should_contains_initial_position(position):
    assert position in ShipTrail.on(position)


def test_trail_should_contains_positions():
    trail = trail_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert Position(1, 0) in trail
    assert Position(2, 0) in trail
    assert Position(3, 0) not in trail


def test_trail_should_keep_history_of_position():
    trail = trail_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert Position(0, 0) in trail
    assert Position(1, 0) in trail
    assert Position(2, 0) in trail


def test_trail_should_update_last_position():
    trail = trail_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert trail.last() == Position(2, 0)


def test_trail_should_clean_all_but_last_position():
    trail = trail_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert Position(0, 0) not in trail.clear()
    assert Position(1, 0) not in trail.clear()
    assert Position(2, 0) in trail.clear()


def trail_with(initial, *items):
    trail = ShipTrail.on(initial)
    for position in items:
        trail = trail.move(position)
    return trail
