package katas.transactions.value;

public interface ValueVisitor<T> {
    T visitSingle(SingleValue value);

    T visitList(ListValue value);

    T visitMap(MapValue value);
}
