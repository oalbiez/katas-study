package katas.transactions.pic;

import katas.transactions.Transaction;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TransactionAgregate {


    Double solde = 0.0;
    List<Agregate> agregates = new ArrayList<>();

    static class Agregate {

         Long montant;
         LocalDateTime instant;

        public Agregate(Long montant, LocalDateTime instant) {
            this.montant = montant;
            this.instant = instant;
        }
    }


    public void update(Transaction transaction) {
        Agregate a = new Agregate(transaction.amount(), transaction.creation());
        int index = agregates.size();
        if (index != 0 && agregates.get(index - 1).instant.equals(transaction.creation())) {
            agregates.get(index - 1).montant += transaction.amount();
            return;
        }
        agregates.add(a);
    }
}
