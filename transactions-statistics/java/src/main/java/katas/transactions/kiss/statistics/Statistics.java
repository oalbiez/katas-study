package katas.transactions.kiss.statistics;

public final class Statistics {
    public static double ratio(final long left, final long right) {
        return Math.round((double) left / (double) right * 1000d) / 1000d;
    }
}
