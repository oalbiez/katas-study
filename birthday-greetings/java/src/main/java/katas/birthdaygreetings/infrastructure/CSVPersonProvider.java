package katas.birthdaygreetings.infrastructure;

import katas.birthdaygreetings.business.Person;
import katas.birthdaygreetings.business.PersonProvider;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Lists.newArrayList;
import static java.nio.file.Files.newBufferedReader;
import static katas.birthdaygreetings.business.Birthdate.birthdate;
import static katas.birthdaygreetings.business.EmailAddress.emailAddress;
import static katas.birthdaygreetings.business.Name.name;
import static katas.birthdaygreetings.business.Person.person;
import static org.apache.commons.csv.CSVFormat.DEFAULT;

public class CSVPersonProvider implements PersonProvider {
    private final Path file;

    public CSVPersonProvider(final Path file) {
        this.file = file;
    }

    public List<Person> getPersons() {
        try {
            final List<Person> result = newArrayList();
            for (final CSVRecord record : new CSVParser(newBufferedReader(file), DEFAULT)) {
                result.add(person(name(record.get(0)), emailAddress(record.get(1)), birthdate(record.get(2))));
            }
            return result;
        } catch (final IOException e) {
            throw propagate(e);
        }
    }
}
