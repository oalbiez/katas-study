import { Duration } from "./duration";
import { Price } from "./price";
import { Chidlrens, NewRelease, PriceCode, Regular } from "./pricecode";

export class Movie {
    public static CHILDRENS = new Chidlrens();
    public static NEW_RELEASE = new NewRelease();
    public static REGULAR = new Regular();

    private title: string;
    private priceCode: PriceCode;

    public constructor(title: string, priceCode: PriceCode) {
        this.title = title;
        this.priceCode = priceCode;
    }

    public getTitle(): string {
        return this.title;
    }

    public chargeFor(duration: Duration): Price {
        return this.priceCode.chargeFor(duration);
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return this.priceCode.frequentRenterPointsFor(duration);
    }
}
