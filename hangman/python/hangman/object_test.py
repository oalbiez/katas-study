from hangman import object as hm


# pylint: disable=duplicate-code


def test_hangman_puzzle_should_hide_unknown_letters():
    assert puzzle_for(word_to_guess="word", inputs=[]) == "w__d"
    assert puzzle_for(word_to_guess="another", inputs=[]) == "a_____r"
    assert puzzle_for(word_to_guess="another", inputs=[letter('n')]) == "an____r"
    assert puzzle_for(word_to_guess="another", inputs=[letter('w')]) == "a_____r"


def test_hangman_is_won_when_all_letters_where_found():
    assert is_won(word_to_guess="little", inputs=[letter("i"), letter("t"), letter("l")]) is True
    assert is_won(word_to_guess="little", inputs=[letter("w"), letter("i"), letter("t"), letter("l")]) is True


def test_hangman_is_won_when_the_word_is_found():
    assert is_won(word_to_guess="little", inputs=[word("little")]) is True
    assert is_won(word_to_guess="little", inputs=[word("bad"), word("little")]) is True


def test_hangman_is_not_won_when_the_word_is_not_completyl_guessed():
    assert is_won(word_to_guess="little", inputs=[letter("t")]) is False
    assert is_won(word_to_guess="little", inputs=[word("big")]) is False


def test_hangman_is_lost_when_more_than_3_errors_by_default():
    assert is_lost(word_to_guess="little", inputs=[word("one"), word("two"), word("three")]) is True
    assert is_lost(word_to_guess="little", inputs=[letter("z"), letter("y"), letter("x")]) is True


def test_hangman_is_not_lost_when_less_than_3_errors_by_default():
    assert is_lost(word_to_guess="little", inputs=[letter("i"), letter("t"), letter("l")]) is False
    assert is_lost(word_to_guess="little", inputs=[letter("t")]) is False


def test_hangman_errors_whould_report_the_number_of_errors():
    assert errors(word_to_guess="little", inputs=[]) == 0
    assert errors(word_to_guess="little", inputs=[letter("i")]) == 0
    assert errors(word_to_guess="little", inputs=[letter("i"), letter("t"), letter("l")]) == 0
    assert errors(word_to_guess="little", inputs=[letter("w")]) == 1
    assert errors(word_to_guess="little", inputs=[letter("w"), letter("w")]) == 1
    assert errors(word_to_guess="little", inputs=[letter("w"), letter('h')]) == 2
    assert errors(word_to_guess="little", inputs=[word("bad")]) == 1
    assert errors(word_to_guess="little", inputs=[word("bad"), word("bad")]) == 1


# pylint: enable=duplicate-code


def is_lost(word_to_guess, inputs):
    return hangman_with(word_to_guess, inputs).is_lost()


def errors(word_to_guess, inputs):
    return hangman_with(word_to_guess, inputs).errors()


def puzzle_for(word_to_guess, inputs):
    return hangman_with(word_to_guess, inputs).puzzle()


def is_won(word_to_guess, inputs):
    return hangman_with(word_to_guess, inputs).is_won()


def word(value):
    return lambda hangman: hangman.test_a_word(value)


def letter(value):
    return lambda hangman: hangman.test_a_letter(value)


def hangman_with(word_to_guess, inputs):
    hangman = hm.Hangman(hm.WordToGuess(word_to_guess))
    for i in inputs:
        i(hangman)
    return hangman
