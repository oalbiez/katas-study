#!/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import annotations

import cProfile

from geometry import Position
from intelligence_report import IntelligenceReport
from life import LifeStatus
from orders import Orders
from situation_awarness import SituationAwarness, TurnSituation
from sonar_result import SonarResult
from helpers.mapdsl import parse
from performance import Performance


def turn(data):
    def action(situation):
        print(repr(data))
        perf = Performance()
        situation = situation.update(data)
        print("duration:", perf.elapsed())
        print(situation.statistics())
        print(situation.render_opponent())
        print("")
        # for trail in situation.opponent_tracking.render_trails():
        #     print(trail)
        return situation

    return action


def monitor(inner):
    def action(situation):
        session = cProfile.Profile()
        session.enable()
        situation = inner(situation)
        session.disable()
        session.print_stats()
        return situation

    return action


def scenario(worldmap_definition, *actions):
    worldmap, _ = parse(worldmap_definition)
    situation = SituationAwarness.on(worldmap)
    for action in actions:
        situation = action(situation)


scenario(
    '''
    ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
    ┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • • • • • ┃
    ┃ • • • • ▲ ┃ ▲ • ▲ ▲ • ┃ • • • • • ┃
    ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
    ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
    ┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃
    ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
    ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃ • • • • ▲ ┃
    ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃ • • • • ▲ ┃
    ┃ • • • ▲ ▲ ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
    ┃ • • • ▲ ▲ ┃ • • • • • ┃ • • • • • ┃
    ┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • • • • • ┃
    ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
    ┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • • • • • ┃
    ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
    ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
    ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
    ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
    ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
    ''',
    turn(TurnSituation(
        my_position=Position(11, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse(''),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('NA'))),
    turn(TurnSituation(
        my_position=Position(11, 9),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(10, 9),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(10, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(10, 7),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(11, 7),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.MISS,
        my_orders=Orders.parse('SONAR 9|MOVE E TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(11, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE W'))),
    turn(TurnSituation(
        my_position=Position(10, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE W'))),
    turn(TurnSituation(
        my_position=Position(9, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(9, 7),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE W'))),
    turn(TurnSituation(
        my_position=Position(9, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.MISS,
        my_orders=Orders.parse('SONAR 7|MOVE S TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('SILENCE|MOVE S'))),
    turn(TurnSituation(
        my_position=Position(9, 9),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(8, 9),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(8, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(7, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(6, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.MISS,
        my_orders=Orders.parse('SONAR 6|MOVE W TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(5, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(5, 9),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('SILENCE|MOVE E'))),
    turn(TurnSituation(
        my_position=Position(5, 10),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(5, 11),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SONAR'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(5, 12),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.HIT,
        my_orders=Orders.parse('SONAR 2|MOVE S SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(4, 12),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(4, 11),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(3, 11),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(2, 11),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('SILENCE|MOVE N'))),
    turn(TurnSituation(
        my_position=Position(1, 11),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(1, 10),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(2, 10),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE E SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(2, 9),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(2, 8),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(2, 7),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(2, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('SILENCE|MOVE S'))),
    turn(TurnSituation(
        my_position=Position(3, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE E SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(3, 5),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE W'))),
    turn(TurnSituation(
        my_position=Position(3, 5),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('SILENCE N 0'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(2, 5),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE W SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(2, 4),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE N SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE N'))),
    turn(TurnSituation(
        my_position=Position(3, 4),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE E SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE W'))),
    turn(TurnSituation(
        my_position=Position(4, 4),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE E TORPEDO'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('SILENCE|MOVE S'))),
    turn(TurnSituation(
        my_position=Position(4, 5),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(4, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(5, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE E SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(5, 6),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('SILENCE N 0'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE E'))),
    turn(TurnSituation(
        my_position=Position(5, 7),
        my_life=LifeStatus(6, 6),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('MOVE S SILENCE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('MOVE S'))),
    turn(TurnSituation(
        my_position=Position(5, 7),
        my_life=LifeStatus(6, 5),
        my_sonar_echo=SonarResult.NOT_APPLICABLE,
        my_orders=Orders.parse('SURFACE'),
        opponent_life=LifeStatus(6, 6),
        intelligence=IntelligenceReport.parse('SILENCE|MOVE S'))),
)
