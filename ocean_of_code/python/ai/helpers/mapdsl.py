# -*- coding: utf-8 -*-

from world_map import WorldMap


def lines_of(definition):
    return definition.splitlines()


def remove_horizontal_lines(lines):
    return (line for line in lines if '━' not in line)


def remove_vertical_lines(lines):
    return (line.replace("┃", "") for line in lines)


def remove_empty_lines(lines):
    return (line for line in lines if line != "")


def remove_whitespace(lines):
    return (line.replace(" ", "") for line in lines)


PROCESSORS = (
    remove_horizontal_lines,
    remove_vertical_lines,
    remove_whitespace,
    remove_empty_lines
)


def cleanup(definition):
    lines = lines_of(definition)
    for processor in PROCESSORS:
        lines = processor(lines)
    return list(lines)


def guess_size(data):
    height = len(data)
    width = len(data[0])
    return (width, height)


def parse(definition):
    data = cleanup(definition)
    width, height = guess_size(data)
    worldmap = WorldMap(width, height)
    specials = {}
    for position in worldmap.walk():
        symbol = data[position.y][position.x]
        if symbol == "▲":
            worldmap.set_island(position)
        elif symbol != "•":
            specials.setdefault(symbol, []).append(position)
    return worldmap, specials
