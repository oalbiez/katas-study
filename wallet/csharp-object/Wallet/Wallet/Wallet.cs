﻿using System.Linq;

namespace Wallet
{
    public class Wallet
    {
        private readonly Money[] _values;

        public Wallet(Money[] values)
        {
            _values = values;
        }

        public Money Value(Currency currency, IRateProvider provider)
        {
            return _values.Aggregate(currency.Amount(0), (total, money) => total + money.Change(currency, provider));
        }
    }
}
