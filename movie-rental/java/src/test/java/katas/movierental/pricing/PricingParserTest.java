package katas.movierental.pricing;

import org.junit.Test;

import static katas.movierental.Duration.days;
import static katas.movierental.Price.euro;
import static katas.movierental.pricing.PricingParser.pricing;
import static org.junit.Assert.assertEquals;

public class PricingParserTest {
    @Test
    public void pricing_should_price_constant_price() {
        assertEquals(euro(2), pricing("costs 2€").chargeFor(days(3)));
        assertEquals(euro(4), pricing("costs 4€").chargeFor(days(3)));
        assertEquals(euro(14), pricing("costs 14.0€").chargeFor(days(3)));
    }

    @Test
    public void pricing_should_price_per_day_price() {
        assertEquals(euro(6), pricing("costs 2€ per day").chargeFor(days(3)));
        assertEquals(euro(15), pricing("costs 5€ per day").chargeFor(days(3)));
    }

    @Test
    public void pricing_should_price_constant_price_with_limited_duration() {
        assertEquals(euro(2), pricing("costs 2.0€ for 3 days").chargeFor(days(1)));
        assertEquals(euro(2), pricing("costs 2.0€ for 3 days").chargeFor(days(3)));
        assertEquals(euro(2), pricing("costs 2.0€ for 1 day").chargeFor(days(1)));
    }

    @Test(expected = Overflow.class)
    public void pricing_should_price_constant_price_with_limited_duration_overflow() {
        pricing("costs 2.0€ for 3 days").chargeFor(days(5));
    }

    @Test
    public void pricing_should_price_per_day_price_with_limited_duration() {
        assertEquals(euro(2), pricing("costs 2€ per day for 3 days").chargeFor(days(1)));
        assertEquals(euro(4), pricing("costs 2€ per day for 3 days").chargeFor(days(2)));
        assertEquals(euro(6), pricing("costs 2€ per day for 3 days").chargeFor(days(3)));
    }

    @Test(expected = Overflow.class)
    public void pricing_should_price_per_day_price_with_limited_duration_overflow() {
        pricing("costs 2.0€ per day for 3 days").chargeFor(days(5));
    }

    @Test
    public void pricing_should_price_with_multiples_rules() {
        assertEquals(euro(8), pricing("costs 2€ for 3 days then costs 3€ per day").chargeFor(days(5)));
        assertEquals(euro(7), pricing("costs 2€ per day for 2 days then costs 3€").chargeFor(days(10)));
        assertEquals(euro(2), pricing("costs 2€ for 2 days then costs 3€").chargeFor(days(1)));
        assertEquals(euro(5), pricing("costs 2€ for 2 days then costs 3€").chargeFor(days(5)));
        assertEquals(euro(2 + 7 * 3 + 10 * 5), pricing("costs 2€ for 3 days then costs 3€ per day for 7 days then costs 5€ per days").chargeFor(days(20)));
    }
}
