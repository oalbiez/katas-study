package katas.marsrover;

import org.junit.Test;

import static katas.marsrover.Map.map;
import static katas.marsrover.Obstacles.obstacles;
import static katas.marsrover.Orientation.North;
import static katas.marsrover.Orientation.West;
import static katas.marsrover.Position.position;
import static katas.marsrover.Rover.rover;
import static org.junit.Assert.assertEquals;

public class PositionUnitTest {
    @Test
    public void a1() {
        assertEquals(position(1, 1), position(1, 1).wrap(5, 5));
        assertEquals(position(4, 1), position(-1, 1).wrap(5, 5));
    }

    @Test
    public void a2() {
        Map world = map(new MapSize(10, 10), obstacles());
        Rover rover = rover(position(2, 2), North);
        Rover next = rover.execute(Command.Left, world);
        assertEquals(position(2, 2), next.position());
        assertEquals(West, next.orientation());
    }
}
