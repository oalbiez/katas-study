﻿using System;
using static MovieRental.Duration;
using static MovieRental.PriceCodes;

namespace MovieRental
{
    public static class Program
    {
        public static void Main()
        {
            var customer = new Customer("jpartogi");
            customer.AddRental(new Rental(new Movie("Transformer", Regular()), Days(3)));
            Console.WriteLine(customer.Statement());
        }
    }
}