package katas.transactions.srp.ast;

import katas.transactions.Transaction;

public final class Doubles {
    public static ValueProvider<Double> add(final ValueProvider<Double> left, final ValueProvider<Double> right) {
        return new ValueProvider<Double>() {
            public Double get(final Transaction transaction) {
                return left.get(transaction) + right.get(transaction);
            }

            public String describe() {
                return left.describe() + " + " + right.describe();
            }
        };
    }

    public static ValueProvider<Boolean> gt(final ValueProvider<Double> left, final ValueProvider<Double> right) {
        return new ValueProvider<Boolean>() {
            public Boolean get(final Transaction transaction) {
                return left.get(transaction) > right.get(transaction);
            }

            public String describe() {
                return left.describe() + " > " + right.describe();
            }
        };
    }

}
