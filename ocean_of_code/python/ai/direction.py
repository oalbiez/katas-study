# -*- coding: utf-8 -*-
from __future__ import annotations

from enum import Enum
from typing import Text

from geometry import Delta, Position


class Direction(Enum):
    North = ("N", Delta(0, -1))
    South = ("S", Delta(0, +1))
    East = ("E", Delta(+1, 0))
    West = ("W", Delta(-1, 0))

    def __init__(self, code: Text, delta: Delta):
        self.code = code
        self.delta = delta

    def move(self, position: Position) -> Position:
        return position + self.delta

    def revert(self, position: Position) -> Position:
        return position - self.delta

    def scaled_move(self, position: Position, scale: int) -> Position:
        return position + (self.delta * scale)

    def __repr__(self) -> Text:
        return f"Direction.{self.name}"

    @classmethod
    def parse(cls, value: Text) -> Direction:
        for direction in cls:
            if value == direction.code:
                return direction
        raise ValueError("unknown direction " + repr(value))
