# -*- coding: utf-8 -*-

from hypothesis import given

from ai import Position, Trace
from helpers.strategies import positions


@given(positions())
def test_trace_should_contains_initial_position(position):
    assert position in Trace.on(position)


def test_trace_should_contains_positions():
    trace = trace_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert Position(1, 0) in trace
    assert Position(2, 0) in trace
    assert Position(3, 0) not in trace


def test_trace_should_keep_history_of_position():
    trace = trace_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert Position(0, 0) in trace
    assert Position(1, 0) in trace
    assert Position(2, 0) in trace


def test_trace_should_update_last_position():
    trace = trace_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert trace.last() == Position(2, 0)


def test_trace_should_clean_all_but_last_position():
    trace = trace_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert str(trace.clear()) == "Trace((2, 0))"


def test_trace_should_clamp_old_positions():
    trace = trace_with(Position(0, 0), Position(1, 0), Position(2, 0))
    assert str(trace.clamp(2)) == "Trace((1, 0), (2, 0))"


def trace_with(initial, *items):
    trace = Trace.on(initial)
    for position in items:
        trace = trace.add(position)
    return trace
