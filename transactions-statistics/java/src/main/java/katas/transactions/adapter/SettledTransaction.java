package katas.transactions.adapter;

import java.time.LocalDate;

public class SettledTransaction {
    public String client;
    public long amount;
    public LocalDate settlementdate;
}
