module CustomArbitraries where


import           OceanOfCode
import           Test.QuickCheck


instance Arbitrary Delta where
    arbitrary = δ <$> arbitrary <*> arbitrary
    shrink (Delta dx dy) = δ <$> shrink dx <*> shrink dy


instance Arbitrary Point where
    arbitrary = point <$> arbitrary <*> arbitrary
    shrink (Point x y) = point <$> shrink x <*> shrink y


instance Arbitrary Rect where
    arbitrary = rect <$> arbitrary <*> arbitrary
    shrink (Rect tl br) = rect <$> shrink tl <*> shrink br

