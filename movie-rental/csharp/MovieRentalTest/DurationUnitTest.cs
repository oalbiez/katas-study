﻿using NUnit.Framework;
using static MovieRental.Duration;
using static NUnit.Framework.Assert;

namespace MovieRentalTest
{
    [TestFixture]
    public class DurationUnitTest
    {
        [Test]
        public void duration_should_render()
        {
            AreEqual("Duration: 0", Null().ToString());
            AreEqual("Duration: 5", Days(5).ToString());
            AreEqual("Duration: infinite", Infinite().ToString());
        }

        [Test]
        public void duration_should_equal()
        {
            AreEqual(Null(), Null());
            AreEqual(Infinite(), Infinite());
            AreEqual(Days(5), Days(5));
        }

        [Test]
        public void duration_should_compare()
        {
            True(Null() < Days(1));
            True(Days(5) < Days(10));
            True(Days(5) < Infinite());
        }

        [Test]
        public void duration_should_add()
        {
            AreEqual(Days(7), Days(3) + Days(4));
            AreEqual(Days(7), Null() + Days(7));
            AreEqual(Infinite(), Days(3) + Infinite());
            AreEqual(Infinite(), Infinite() + Days(3));
        }

        [Test]
        public void duration_should_sub()
        {
            AreEqual(Days(3), Days(7) - Days(4));
            AreEqual(Null(), Days(4) - Days(7));
            AreEqual(Null(), Days(4) - Infinite());
            AreEqual(Null(), Infinite() - Infinite());
            AreEqual(Infinite(), Infinite() - Days(4));
        }

        [Test]
        public void duration_should_check_null()
        {
            True(Null().IsNull());
            True(Days(4).IsNotNull());
            True(Infinite().IsNotNull());
        }

        [Test]
        public void duration_should_check_infinite()
        {
            True(Null().IsNotInfinite());
            True(Days(4).IsNotInfinite());
            True(Infinite().IsInfinite());
        }

        [Test]
        public void duration_should_max()
        {
            AreEqual(Days(7), Max(Days(7), Days(4)));
            AreEqual(Days(7), Max(Days(7), Null()));
            AreEqual(Infinite(), Max(Infinite(), Days(5)));
            AreEqual(Infinite(), Max(Days(7), Infinite()));
            AreEqual(Infinite(), Max(Infinite(), Infinite()));
        }

        [Test]
        public void duration_should_min()
        {
            AreEqual(Days(4), Min(Days(7), Days(4)));
            AreEqual(Null(), Min(Days(7), Null()));
            AreEqual(Days(5), Min(Infinite(), Days(5)));
            AreEqual(Days(7), Min(Days(7), Infinite()));
            AreEqual(Infinite(), Min(Infinite(), Infinite()));
        }
    }
}