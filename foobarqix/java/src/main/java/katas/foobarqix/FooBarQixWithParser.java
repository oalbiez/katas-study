package katas.foobarqix;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.util.List;

import static java.lang.String.valueOf;

/**
 * Using Parser
 */
public class FooBarQixWithParser {

    interface Rule {
        boolean apply(int input, StringBuilder accumulator);
    }

    static FooBarQixWithParser rules(@SuppressWarnings("SameParameterValue") final String definition) {
        return new FooBarQixWithParser(parse(definition));
    }

    private final List<Rule> rules;

    private FooBarQixWithParser(List<Rule> rules) {
        this.rules = rules;
    }

    public String compute(final int input) {
        StringBuilder accumulator = new StringBuilder();
        boolean ruleMatched = false;
        for (Rule rule : rules) {
            ruleMatched |= rule.apply(input, accumulator);
        }
        return ruleMatched ? accumulator.toString() : valueOf(input);
    }

    private static Rule rule(final int divisor, final String label) {
        return (value, accumulator) -> {
            if (value % divisor == 0) {
                accumulator.append(label);
                return true;
            }
            return false;
        };
    }

    private static List<Rule> parse(final String definitions) {
        List<Rule> rules = Lists.newArrayList();
        for (String line : Splitter.on(';').trimResults().split(definitions)) {
            if (!line.startsWith("#") && !line.isEmpty()) {
                String[] parts = line.split("=>");
                rules.add(rule(Integer.parseInt(parts[0].trim()), parts[1].trim()));
            }
        }
        return rules;
    }
}
