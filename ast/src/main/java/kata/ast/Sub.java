package kata.ast;

public class Sub implements AST {
    private final AST left;
    private final AST right;

    public Sub(AST left, AST right) {
        this.left = left;
        this.right = right;
    }

    public AST left() {
        return left;
    }

    public AST right() {
        return right;
    }

    public <T> T accept(ASTVisitor<T> visitor) {
        return visitor.visitSub(this);
    }
}
