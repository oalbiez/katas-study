module Math exposing (Source(..), combineLcm, gcd, lcm, som, sumOfOneToN, sumOfmultiple)

import List.Extra


gcd : Int -> Int -> Int
gcd left right =
    if right == 0 then
        left

    else
        gcd right (modBy right left)


lcm : Int -> Int -> Int
lcm left right =
    (left // gcd left right) * right


sumOfOneToN : Int -> Int
sumOfOneToN n =
    n * (n + 1) // 2


type Source
    = Simple Int
    | Composition Source Source


combineLcm : Source -> Source -> Source
combineLcm left right =
    case ( left, right ) of
        ( Simple a, Simple b ) ->
            lcm a b |> Simple

        ( Simple _, Composition a b ) ->
            Composition (combineLcm left a) (combineLcm left b)

        ( Composition a b, Simple _ ) ->
            Composition (combineLcm right a) (combineLcm right b)

        ( Composition a b, Composition c d ) ->
            Composition
                (Composition (combineLcm a c) (combineLcm a d))
                (Composition (combineLcm b c) (combineLcm b d))


sumOfmultiple : Int -> Source -> Int
sumOfmultiple limit divisors =
    case divisors of
        Simple v ->
            v * sumOfOneToN ((limit - 1) // v)

        Composition left right ->
            let
                inner =
                    sumOfmultiple limit
            in
            inner left + inner right - inner (combineLcm left right)


som : Int -> List Int -> Int
som limit source =
    source
        |> List.map Simple
        |> List.Extra.foldl1 Composition
        |> Maybe.map (sumOfmultiple limit)
        |> Maybe.withDefault 0
