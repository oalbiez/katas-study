package katas.bottle;

import com.google.common.collect.AbstractIterator;

import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;

import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Sets.newHashSet;

public final class Solver {
    private final Queue<Solution> queue = newLinkedList();
    private final Set<String> found = newHashSet();
    private Predicate<Bottle> condition;

    public static Solver solver(final Solution initial, final Predicate<Bottle> condition) {
        return new Solver(initial, condition);
    }

    private Solver(final Solution initial, final Predicate<Bottle> condition) {
        this.condition = condition;
        consider(initial);
    }

    public Iterable<Solution> findAllSolutions() {
        return () -> new AbstractIterator<>() {
            @Override
            protected Solution computeNext() {
                while (!queue.isEmpty()) {
                    Solution current = queue.remove();
                    if (current.match(condition)) {
                        return current;
                    }
                    generateNextSolutions(current);
                }
                return endOfData();
            }
        };
    }

    private void consider(Solution solution) {
        String hash = solution.hash();
        if (!found.contains(hash)) {
            queue.add(solution);
            found.add(hash);
        }
    }

    private void generateNextSolutions(Solution solution) {
        for (int from : solution.fromIndexes()) {
            for (int to : solution.toIndexes()) {
                consider(solution.transfer(from, to));
            }
        }
    }
}
