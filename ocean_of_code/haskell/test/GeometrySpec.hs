{-# LANGUAGE OverloadedStrings #-}
module GeometrySpec (spec) where


import           CustomArbitraries ()
import           OceanOfCode
import           Test.Hspec
import           Test.QuickCheck


spec :: Spec
spec = do


    describe "Delta" $ do

        it "∆+∆ has neutral element" $ property $
            \d -> d ∆+∆ δzero == d && δzero ∆+∆ d == d

        it "∆+∆ is assosiative" $ property $
            \(a, b, c) -> (a ∆+∆ b) ∆+∆ c == a ∆+∆ (b ∆+∆ c)

        it "∆+∆ and ∆-∆ are opposite" $ property $
            \(a, b) -> (a ∆+∆ b) ∆-∆ b == a

        it "(∆*)" $ forAll (choose (0, 1000)) $
            \f -> property $
                \d -> d ∆* f == foldl (∆+∆) δzero (replicate f d)

        it "δlength is always positive" $ property $
            \d -> δlength d >= 0

        it "δlength" $ δlength (δ 3 4)  `shouldBe` 5


    describe "Point" $ do

        it "•+∆ and •-∆ are opposite" $ property $
            \(p, d) -> (p •+∆ d) •-∆ d == p

        it "•-• is a delta" $ property $
            \(a, b) -> a •+∆ (a •-• b) == b

        it "distance" $ property $
            \(a, b) -> distance a b == δlength (a •-• b)

        it "distance²" $
            distance² (point 0 0) (point 0 5) == 25

        it "minp" $
            minp (point 3 6) (point 5 2) `shouldBe` point 3 2

        it "maxp" $
            maxp (point 3 6) (point 5 2) `shouldBe` point 5 6


    describe "Rect" $ do

        it "rectangle should contains top left point" $
            origin •∈◻ rect origin (point 5 5) `shouldBe` True

        it "rectangle should not contains bottom righ point" $
            point 5 5 •∉◻ rect origin (point 5 5) `shouldBe` True

        it "•∈◻ should return true for internal points" $ do
            point 1 1 •∈◻ rectTo (point 5 5) `shouldBe` True
            point 0 4 •∈◻ rectTo (point 5 5) `shouldBe` True
            point 4 0 •∈◻ rectTo (point 5 5) `shouldBe` True
            point 4 4 •∈◻ rectTo (point 5 5) `shouldBe` True

        it "•∉◻ should return true for external points" $ do
            point (-1) 0 •∉◻ rectTo (point 5 5) `shouldBe` True
            point 0 (-1) •∉◻ rectTo (point 5 5) `shouldBe` True
            point 6 0 •∉◻ rectTo (point 5 5) `shouldBe` True
            point 0 6 •∉◻ rectTo (point 5 5) `shouldBe` True

        it "◻∪◻" $
            rectTo (point 3 4) ◻∪◻ rectTo (point 4 3) `shouldBe` rectTo (point 4 4)

        it "walkRect" $ do
            walkRect (rectTo (point 2 1)) `shouldBe` [origin, point 1 0]
            length (walkRect (rectTo (point 5 5))) `shouldBe` 25

        it "width" $
            width (rectTo (point 5 4)) `shouldBe` 5

        it "height" $
            height (rectTo (point 5 4)) `shouldBe` 4

        it "center" $
            center (rectTo (point 5 5)) `shouldBe` point 2 2

        it "area" $
            area (rectTo (point 5 5)) `shouldBe` 25

        it "split with outside point" $
            splitRect (point 10 2) (rectTo (point 5 5)) `shouldBe` [
                rectTo (point 5 5)]

        it "split with inside point" $ do
            splitRect (point 2 2) (rectTo (point 5 5)) `shouldMatchList` [
                rect origin (point 2 5),
                rect (point 3 0) (point 5 5),
                rect origin (point 5 2),
                rect (point 0 3) (point 5 5)]

            splitRect (point 4 4) (rectTo (point 5 5)) `shouldMatchList` [
                rect origin (point 4 5),
                rect origin (point 5 4)]

        it "boundingRect" $ do
            boundingRect [point 2 2, point 2 3, point 5 5]
                `shouldBe` rect (point 2 2) (point 6 6)

            boundingRect []
                `shouldBe` emptyRect
