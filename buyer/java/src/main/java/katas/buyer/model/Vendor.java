package katas.buyer.model;

public final class Vendor {
    private final String value;

    public static Vendor of(final String value) {
        return new Vendor(value);
    }

    private Vendor(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
