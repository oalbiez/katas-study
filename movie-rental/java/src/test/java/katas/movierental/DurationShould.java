package katas.movierental;

import org.junit.Test;

import static katas.movierental.Duration.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DurationShould {

    @Test
    public void render_to_string() {
        assertEquals("Duration: 0", zero().toString());
        assertEquals("Duration: 5", days(5).toString());
        assertEquals("Duration: infinite", infinite().toString());
    }

    @Test
    public void be_equal() {
        assertEquals(zero(), zero());
        assertEquals(infinite(), infinite());
        assertEquals(days(5), days(5));
    }

    @Test
    public void be_comparable() {
        assertEquals(-1, zero().compareTo(days(1)));
        assertEquals(-1, days(5).compareTo(days(10)));
        assertEquals(-1, days(5).compareTo(infinite()));
    }

    @Test
    public void be_addable() {
        assertEquals(days(7), add(days(3), days(4)));
        assertEquals(days(7), add(zero(), days(7)));
        assertEquals(infinite(), add(days(3), infinite()));
        assertEquals(infinite(), add(infinite(), days(3)));
    }

    @Test
    public void be_substractable() {
        assertEquals(days(3), sub(days(7), days(4)));
        assertEquals(zero(), sub(days(4), days(7)));
        assertEquals(zero(), sub(days(4), infinite()));
        assertEquals(zero(), sub(infinite(), infinite()));
        assertEquals(infinite(), sub(infinite(), days(4)));
    }

    @Test
    public void check_to_zero() {
        assertTrue(zero().isZero());
        assertTrue(days(4).isNotZero());
        assertTrue(infinite().isNotZero());
    }

    @Test
    public void duration_should_check_infinite() {
        assertTrue(zero().IsNotInfinite());
        assertTrue(days(4).IsNotInfinite());
        assertTrue(infinite().isInfinite());
    }

    @Test
    public void have_max() {
        assertEquals(days(7), max(days(7), days(4)));
        assertEquals(days(7), max(days(7), zero()));
        assertEquals(infinite(), max(infinite(), days(5)));
        assertEquals(infinite(), max(days(7), infinite()));
        assertEquals(infinite(), max(infinite(), infinite()));
    }

    @Test
    public void have_min() {
        assertEquals(days(4), min(days(7), days(4)));
        assertEquals(zero(), min(days(7), zero()));
        assertEquals(days(5), min(infinite(), days(5)));
        assertEquals(days(7), min(days(7), infinite()));
        assertEquals(infinite(), min(infinite(), infinite()));
    }
}
