package katas.transactions.kiss.statistics;

import katas.transactions.Transaction;

public final class Count implements Statistic {
    private long value = 0;

    public void updateWith(final Transaction transaction) {
        value += 1;
    }

    public long value() {
        return value;
    }
}
