﻿namespace MovieRental
{
    public class Rental
    {
        public Rental(Movie movie, Duration duration)
        {
            Movie = movie;
            Duration = duration;
        }

        public Movie Movie { get; }
        public Duration Duration { get; }
        public string Title => Movie.Title;

        public Price Charge() => Movie.ChargeFor(Duration);

        public int FrequentRenterPoints() => Movie.FrequentRenterPointsFor(Duration);
    }
}