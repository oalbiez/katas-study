package katas.birthdaygreetings.infrastructure;

import katas.birthdaygreetings.business.DateProvider;

import java.time.LocalDate;

public class SystemDateProvider implements DateProvider {
    public LocalDate today() {
        return LocalDate.now();
    }
}
