package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.value.Value;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static java.util.Arrays.stream;
import static katas.transactions.value.Values.value;
import static org.junit.Assert.assertEquals;

public class CountTest {
    @Test
    public void should_get_count_of_transactions() {
        assertEquals(value(1), count(transaction()));
        assertEquals(value(2), count(transaction(), transaction()));
    }

    private Transaction transaction() {
        return Transaction.transaction("client", "A", 100, LocalDate.of(2017, Month.APRIL, 17).atStartOfDay());
    }

    private Value count(final Transaction... transactions) {
        final Count stat = Statistics.count();
        stream(transactions).forEach(stat::updateWith);
        return stat.value();
    }
}
