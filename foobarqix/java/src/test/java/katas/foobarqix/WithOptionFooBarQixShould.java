package katas.foobarqix;

public class WithOptionFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return WithOption.fooBarQix(input);
    }
}
