# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Iterator, Optional, Set, Text, Tuple, Union

from direction import Direction
from geometry import Position


@dataclass(frozen=True)
class Nothing:
    def render(self) -> Text:
        # pylint: disable=no-self-use
        return "NA"


@dataclass(frozen=True)
class MineDroped:
    def render(self) -> Text:
        # pylint: disable=no-self-use
        return "MINE"


@dataclass(frozen=True)
class MineTrigged:
    target: Position

    def render(self) -> Text:
        return f"TRIGGER {self.target.x} {self.target.y}"


@dataclass(frozen=True)
class Movement:
    direction: Direction

    def render(self) -> Text:
        return f"MOVE {self.direction.code}"


@dataclass(frozen=True)
class Silence:
    def render(self) -> Text:
        # pylint: disable=no-self-use
        return "SILENCE"


@dataclass(frozen=True)
class Surface:
    sector: int

    def render(self) -> Text:
        return f"SURFACE {self.sector}"


@dataclass(frozen=True)
class Sonar:
    sector: int

    def render(self) -> Text:
        return f"SONAR {self.sector}"


@dataclass(frozen=True)
class Torpedo:
    target: Position

    def render(self) -> Text:
        return f"TORPEDO {self.target.x} {self.target.y}"


IntelligenceData = Union[
    Nothing,
    MineDroped,
    MineTrigged,
    Movement,
    Silence,
    Sonar,
    Surface,
    Torpedo,
    ]


@dataclass(frozen=True)
class IntelligenceReport:
    orders: Tuple[IntelligenceData, ...]

    @staticmethod
    def parse(report: Text) -> IntelligenceReport:
        def parse_order(item: Text) -> IntelligenceData:
            if item.startswith("MINE"):
                return MineDroped()
            if item.startswith("MOVE"):
                return Movement(Direction.parse(item[5:]))
            if item.startswith("SILENCE"):
                return Silence()
            if item.startswith("SONAR"):
                return Sonar(int(item[6:]))
            if item.startswith("SURFACE"):
                return Surface(int(item[8:]))
            if item.startswith("TORPEDO"):
                return Torpedo(Position.parse(item[7:]))
            if item.startswith("TRIGGER"):
                return MineTrigged(Position.parse(item[7:]))
            return Nothing()

        return IntelligenceReport.of(*(parse_order(i.strip()) for i in report.split("|")))

    @staticmethod
    def of(*orders: IntelligenceData) -> IntelligenceReport:
        return IntelligenceReport(orders)

    def __iter__(self) -> Iterator[IntelligenceData]:
        return iter(self.orders)

    def explosion_positions(self) -> Set[Position]:
        positions = set()
        for i in self.orders:
            if isinstance(i, Torpedo):
                positions.add(i.target)
            if isinstance(i, MineTrigged):
                positions.add(i.target)
        return positions

    def surface(self) -> Optional[Surface]:
        for i in self.orders:
            if isinstance(i, Surface):
                return i
        return None

    def sonar(self) -> Optional[Sonar]:
        for i in self.orders:
            if isinstance(i, Sonar):
                return i
        return None

    def render(self) -> Text:
        return "|".join(o.render() for o in self.orders)

    def __repr__(self):
        return f"IntelligenceReport.parse({repr(self.render())})"
