package katas.birthdaygreetings.business;

import java.util.List;

public interface PersonProvider {
    List<Person> getPersons();
}
