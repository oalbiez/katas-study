package katas.birthdaygreetings.business;

import com.google.common.base.Objects;

import java.time.LocalDate;

public final class Person {
    private final Name name;
    private final EmailAddress emailAddress;
    private final Birthdate birthdate;

    public static Person person(final Name name, final EmailAddress email, final Birthdate birthdate) {
        return new Person(name, email, birthdate);
    }

    private Person(final Name name, final EmailAddress emailAddress, final Birthdate birthdate) {
        this.name = name;
        this.emailAddress = emailAddress;
        this.birthdate = birthdate;
    }

    public boolean isBirthday(final LocalDate today) {
        return birthdate.isBirthday(today);
    }

    public EmailAddress email() {
        return emailAddress;
    }

    public Name name() {
        return name;
    }

    public Person with(final Birthdate birthdate) {
        return person(name, emailAddress, birthdate);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Person person = (Person) o;
        return Objects.equal(name, person.name) &&
                Objects.equal(emailAddress, person.emailAddress) &&
                Objects.equal(birthdate, person.birthdate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, emailAddress, birthdate);
    }
}
