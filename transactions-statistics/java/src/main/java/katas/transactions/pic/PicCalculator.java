package katas.transactions.pic;

import katas.transactions.Transaction;

import java.time.LocalDateTime;

public class PicCalculator {
    private long sold = 0;
    private long maxSold = Long.MIN_VALUE;
    private LocalDateTime instant = LocalDateTime.MIN;
    private final TransactionAgregate ta = new TransactionAgregate();


    public PicCalculator() {
    }

    public PicCalculator update(final Transaction transaction) {
        ta.update(transaction);
        return this;
    }

    private void updatePic(final TransactionAgregate.Agregate tx) {
        sold += tx.montant;
        if (sold > maxSold) {
            maxSold = sold;
            instant = tx.instant;
        }
    }

    public Pic pic() {
        for (final TransactionAgregate.Agregate agregate : ta.agregates) {
            updatePic(agregate);
        }
        return new Pic(maxSold, instant);
    }
}
