package katas.foobarqix;

public class AlgebraicFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return Algebraic.fooBarQix(input);
    }
}
