package katas.marsrover;

public enum Command {
    Backward() {
        public Orientation newOrientation(Orientation orientation) {
            return orientation;
        }

        public Distance distanceFor(Orientation orientation) {
            return orientation.distance().reverse();
        }
    },
    Forward {
        public Orientation newOrientation(Orientation orientation) {
            return orientation;
        }

        public Distance distanceFor(Orientation orientation) {
            return orientation.distance();
        }
    },
    Left {
        public Orientation newOrientation(Orientation orientation) {
            return orientation.left();
        }

        public Distance distanceFor(Orientation orientation) {
            return Distance.distance(0, 0);
        }
    },
    Right {
        public Orientation newOrientation(Orientation orientation) {
            return orientation.right();
        }

        public Distance distanceFor(Orientation orientation) {
            return Distance.distance(0, 0);
        }
    };

    public abstract Orientation newOrientation(final Orientation orientation);

    public abstract Distance distanceFor(final Orientation orientation);
}
