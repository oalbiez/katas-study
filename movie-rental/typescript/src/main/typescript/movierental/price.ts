
export class Price {
    public static euro(value: number): Price {
        return new Price(value);
    }

    private value: number;

    private constructor(value: number) {
        this.value = value;
    }

    public amount(): number {
        return this.value;
    }

    public render(): string {
        return this.value.toFixed(1);
    }
}

export function add(left: Price, right: Price): Price {
    return new Price(left.amount() + right.amount());
}

export function mul(left: Price, right: number): Price {
    return new Price(left.amount() * right);
}
