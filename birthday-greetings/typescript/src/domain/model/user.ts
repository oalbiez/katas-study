import { DateTime } from "luxon";

import { Birthdate } from "./birthdate";
import { EmailAddress } from "./email_address";
import { FirstName } from "./first_name";
import { LastName } from "./last_name";

export class User {

    public static create(
        firstName: FirstName,
        lastName: LastName,
        birthday: Birthdate,
        email: EmailAddress,
    ): User {
        return new User(firstName, lastName, birthday, email);
    }

    private constructor(
        public readonly firstName: FirstName,
        public readonly lastName: LastName,
        public readonly birthday: Birthdate,
        public readonly email: EmailAddress,
    ) { }

    public hasBirthday(today: DateTime): boolean {
        return this.birthday.isBirthday(today);
    }

    public render(): string {
        return `${this.firstName.value} ${this.lastName.value} <${this.email.value}>: ${this.birthday.render()}`;
    }
}
