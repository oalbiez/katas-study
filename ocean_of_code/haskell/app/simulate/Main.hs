module Main where


import qualified Data.HashSet as HashSet
import           OceanOfCode
import           WorldMapDSL


type Action = SituationAwarness -> IO SituationAwarness

renderPositions :: WorldMap -> Tracking -> String
renderPositions wm tracking =
    let solutions = HashSet.toList $ solutionsOf tracking
    in render (cricleFor solutions) wm

renderMines :: WorldMap -> Tracking -> String
renderMines wm tracking =
    let mines = HashSet.toList $ minesOf tracking
    in render (deathFor mines) wm

with :: (SituationAwarness -> SituationAwarness) -> Action
with action situation = do
    let nextSituation = action situation
    putStrLn "Opponent tracking"
    putStrLn (renderPositions (theWorldmap nextSituation) (opponentTracking nextSituation))
    putStrLn (trackingStatistics (theWorldmap nextSituation) (opponentTracking nextSituation))
    putStrLn "Opponent mines"
    putStrLn (renderMines (theWorldmap nextSituation) (opponentTracking nextSituation))
    return nextSituation

turn :: TurnData -> Action
turn turn = with $ updateTurn turn

orders :: String -> Action
orders orders = with $ updateMyOrders (parseOrders orders)

scenario :: String -> [Action] -> IO ()
scenario definition actions =
    let
        (wm, _) = parse definition
        process [] situation     = return situation
        process (a:xa) situation = a situation >>= process xa
    in do
        _ <- process actions (initialSituation wm)
        return ()


main :: IO ()
main = scenario "\
    \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
    \┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • • • • • ┃\n\
    \┃ • • • • ▲ ┃ ▲ • ▲ ▲ • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
    \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
    \┃ • • • • • ┃ • ▲ ▲ ▲ • ┃ • • • • ▲ ┃\n\
    \┃ • • • • • ┃ • ▲ ▲ ▲ • ┃ • • • • ▲ ┃\n\
    \┃ • • • ▲ ▲ ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
    \┃ • • • ▲ ▲ ┃ • • • • • ┃ • • • • • ┃\n\
    \┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
    \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
    \┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
    \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
    \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
    [ turn TurnData
        { turnMyPosition   = point 11 8
        , turnMyLife       = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 3 4 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "NA"
        }
    , orders "MOVE S SONAR"
    , turn TurnData
        { turnMyPosition   = point 11 9
        , turnMyLife       = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 3 3 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE N"
        }
    , orders "MOVE W SONAR"
    , turn TurnData
        { turnMyPosition   = point 10 9
        , turnMyLife       = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 3 2 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE E"
        }
    , orders "MOVE N SONAR"
    , turn TurnData
        { turnMyPosition   = point 10 8
        , turnMyLife       = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 3 1 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE E"
        }
    , orders "MOVE N SONAR"
    , turn TurnData
        { turnMyPosition   = point 10 7
        , turnMyLife       = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 3 0 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE E"
        }
    , orders "SONAR 9|MOVE E TORPEDO"
    , turn TurnData
        { turnMyPosition   = point 11 7
        , turnMyLife       = 6
        , turnMySonarEcho  = Miss
        , turnMySubsystems = Subsystem 2 4 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE S"
        }
    , orders "MOVE N SONAR"
    , turn TurnData
        { turnMyPosition   = point 11  6
        , turnMyLife = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 2 3 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE W"
        }
    , orders "MOVE W SONAR"
    , turn TurnData
        { turnMyPosition   = point 10  6
        , turnMyLife = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 2 2 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE W"
        }
    , orders "MOVE W SONAR"
    , turn TurnData
        { turnMyPosition   = point 9  6
        , turnMyLife = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 2 1 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE S"
        }
    , orders "MOVE S SONAR"
    , turn TurnData
        { turnMyPosition   = point 9  7
        , turnMyLife = 6
        , turnMySonarEcho  = NA
        , turnMySubsystems = Subsystem 2 0 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "MOVE W"
        }
    , orders "SONAR 7|MOVE S TORPEDO"
    , turn TurnData
        { turnMyPosition   = point 9  8
        , turnMyLife = 6
        , turnMySonarEcho  = Miss
        , turnMySubsystems = Subsystem 1 4 6 3
        , turnOpponentLife = 6
        , turnIntelligence = parseReport "SILENCE|MOVE S"
        }
    ]

