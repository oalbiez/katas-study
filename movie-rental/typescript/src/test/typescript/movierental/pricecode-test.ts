import { Duration } from "../../../main/typescript/movierental/duration";
import { add, mul, Price } from "../../../main/typescript/movierental/price";
import { Chidlrens, NewRelease, PriceCode, Regular } from "../../../main/typescript/movierental/pricecode";

describe("PriceCode", () => {

    describe("for regulars", () => {
        const REGULAR = new Regular();

        it("should charge flat when less than 3 days", () => {
            expect(REGULAR.chargeFor(Duration.days(1))).toEqual(Price.euro(2.0));
            expect(REGULAR.chargeFor(Duration.days(2))).toEqual(Price.euro(2.0));
        });

        it("should charge proportionaly when more than 3 days", () => {
            expect(REGULAR.chargeFor(Duration.days(3))).toEqual(Price.euro(3.5));
            expect(REGULAR.chargeFor(Duration.days(4))).toEqual(Price.euro(5.0));
        });

        it("should give 1 frequent renter point regardless of days", () => {
            expect(REGULAR.frequentRenterPointsFor(Duration.days(1))).toEqual(1);
            expect(REGULAR.frequentRenterPointsFor(Duration.days(2))).toEqual(1);
            expect(REGULAR.frequentRenterPointsFor(Duration.days(3))).toEqual(1);
            expect(REGULAR.frequentRenterPointsFor(Duration.days(4))).toEqual(1);
            expect(REGULAR.frequentRenterPointsFor(Duration.days(40))).toEqual(1);
        });
    };

    describe("for childrens", () => {
        const CHILDRENS = new Chidlrens();

        it("should charge flat when less than 4 days", () => {
            expect(CHILDRENS.chargeFor(Duration.days(1))).toEqual(Price.euro(1.5));
            expect(CHILDRENS.chargeFor(Duration.days(2))).toEqual(Price.euro(1.5));
            expect(CHILDRENS.chargeFor(Duration.days(3))).toEqual(Price.euro(1.5));
        });

        it("should charge proportionaly when more than 4 days", () => {
            expect(CHILDRENS.chargeFor(Duration.days(4))).toEqual(Price.euro(3.0));
            expect(CHILDRENS.chargeFor(Duration.days(5))).toEqual(Price.euro(4.5));
        });

        it("should give 1 frequent renter point regardless of days", () => {
            expect(CHILDRENS.frequentRenterPointsFor(Duration.days(1))).toEqual(1);
            expect(CHILDRENS.frequentRenterPointsFor(Duration.days(2))).toEqual(1);
            expect(CHILDRENS.frequentRenterPointsFor(Duration.days(3))).toEqual(1);
            expect(CHILDRENS.frequentRenterPointsFor(Duration.days(4))).toEqual(1);
            expect(CHILDRENS.frequentRenterPointsFor(Duration.days(40))).toEqual(1);
        });
    };

    describe("for new releases", () => {
        const NEW_RELEASE = new NewRelease();

        it("should charge proportionaly", () => {
            expect(NEW_RELEASE.chargeFor(Duration.days(1))).toEqual(Price.euro(3.0));
            expect(NEW_RELEASE.chargeFor(Duration.days(2))).toEqual(Price.euro(6.0));
            expect(NEW_RELEASE.chargeFor(Duration.days(3))).toEqual(Price.euro(9.0));
        });

        it("should give 1 frequent renter point for 1 day", () => {
            expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(1))).toEqual(1);
        });

        it("should give 1 frequent renter point when more than 1 day", () => {
            expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(2))).toEqual(2);
            expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(3))).toEqual(2);
            expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(6))).toEqual(2);
            expect(NEW_RELEASE.frequentRenterPointsFor(Duration.days(40))).toEqual(2);
        });
    };
});
