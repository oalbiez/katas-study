{-# LANGUAGE OverloadedStrings #-}
module TrackingSpec (spec) where


import qualified Data.HashSet as HashSet
import qualified Data.List    as List
import qualified Data.Map     as Map
import           OceanOfCode
import           Test.Hspec
import           WorldMapDSL


type Action = WorldMap -> Tracking -> IO Tracking

constraint :: [Constraint] -> Action
constraint constraints wm tracking = return $ updateTracking wm constraints tracking

expectSolutions :: String -> Action
expectSolutions definition _ tracking =
    let (_, symbols) = parse definition
        expectedSolutions = List.sort $ Map.findWithDefault [] '◎' symbols
        solutions = List.sort $ HashSet.toList $ solutionsOf tracking
        -- display p = if p `elem` solutions then Just "◎" else Nothing
    in do
        -- putStrLn (render display wm)
        solutions `shouldMatchList` expectedSolutions
        return tracking

expectMines :: String -> Action
expectMines definition _ tracking =
    let (_, symbols) = parse definition
        expectedMines = List.sort $ Map.findWithDefault [] '☠' symbols
        mines = List.sort $ HashSet.toList $ minesOf tracking
        -- display p = if p `elem` mines then Just "◎" else Nothing
    in do
        -- putStrLn (render display wm)
        mines `shouldMatchList` expectedMines
        return tracking

process :: WorldMap -> [Action] -> Tracking -> IO Tracking
process _ [] tracking      = return tracking
process wm (a:xa) tracking = a wm tracking >>= process wm xa

scenario :: String -> [Action] -> IO ()
scenario definition actions =
    let (wm, _) = parse definition
    in do
        _ <- process wm actions (mkTracking wm)
        return ()


spec :: Spec
spec = do
    it "tracking should consider all positions without information" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with single move constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [canMoveTo North],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ • • ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with multiples moves constraints" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [canMoveTo North],
                constraint [canMoveTo West],
                constraint [canMoveTo West],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃\n\
            \┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ • • ┃\n\
            \┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃\n\
            \┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃\n\
            \┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with silence constraints" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [atPositions [point 4 2]],
                constraint [canMoveWithSilence],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • ◎ ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • ◎ ┃ • • • • • ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with silence constraints after moves" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveWithSilence],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with silence constraints when blocked" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [atPositions [point 1 0]],
                constraint [canMoveTo West],
                constraint [canMoveWithSilence],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with silence constraints after surface" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [atPositions [point 1 0]],
                constraint [canMoveTo West],
                constraint [reset],
                constraint [canMoveWithSilence],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with in sector constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [inSector (Sector 4)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ • • • • • ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with not in sector constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [notInSector (Sector 4)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ • • • • • ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ • • • • • ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with at single position constraints" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [atPositions [point 4 2]],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • ◎ ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with not at single position constraints" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [notAtPositions [point 4 2]],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with in explosion range constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [inExplosion (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃\n\
            \┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with not in explosion range constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [notInExplosion (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with in blast explosion range constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [inBlastExplosion (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • ◎ • ┃ • • • • • ┃\n\
            \┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with not in blast explosion range constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [notInBlastExplosion (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ ◎ • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with in torpedo range constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [inTorpedoRange (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ • • • • ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • ◎ • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with in torpedo range constraint with islands" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • ▲ ▲ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [inTorpedoRange (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ • • • • ┃\n\
            \┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ • • ▲ ▲ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with not in torpedo range constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [notInTorpedoRange (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ • • • ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ • • • ┃ • • ◎ ◎ ◎ ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ • • • ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with not in torpedo range constraint with islands" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [notInTorpedoRange (point 2 2)],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ ◎ ◎ ▲ ▲ ◎ ┃\n\
            \┃ ▲ ▲ • • • ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┃ ▲ ▲ • • • ┃ • • ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ◎ ▲ ┃ ▲ ▲ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ ◎ ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]


    it "tracking with mine dropped constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [inExplosion (point 2 2)],
                constraint [mineDropped],
                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ ◎ ◎ • ┃ • • • • • ┃\n\
            \┃ • ◎ ◎ ◎ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n",
                expectMines "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • ☠ ☠ • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ ☠ ☠ ☠ ┃ • • • • • ┃\n\
            \┃ ▲ ▲ ☠ ☠ ☠ ┃ • • • • • ┃\n\
            \┃ ☠ ☠ ☠ ☠ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • ☠ ☠ ☠ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
        ]


    it "tracking with mine trigged constraint" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"
            [
                constraint [atPositions [point 2 2, point 3 2]],
                constraint [mineDropped],
                constraint [canMoveTo South],
                constraint [canMoveTo South],
                constraint [canMoveTo South],
                constraint [mineDropped],
                constraint [mineTrigged (point 2 1)],
                expectMines "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • ☠ • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • ☠ • ☠ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • ☠ • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
        ]


    it "tracking long scenario" $
        scenario "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            [
                constraint [inSector (Sector 7), reset],
                constraint [canMoveTo North],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [notInSector (Sector 8)],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [canMoveTo North],
                constraint [canMoveTo West],
                constraint [canMoveTo North],
                constraint [canMoveWithSilence],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [canMoveTo North],
                constraint [canMoveTo East],
                constraint [inSector (Sector 5)],
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveTo West],
                constraint [canMoveTo West],
                constraint [canMoveTo West],
                constraint [inSector (Sector 4)],
                constraint [canMoveTo North],
                constraint [canMoveWithSilence],
                constraint [inTorpedoRange (point 3 1)],
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveTo West],
                constraint [canMoveTo South],
                constraint [canMoveTo South],
                constraint [canMoveTo West],
                constraint [canMoveTo West],
                constraint [canMoveTo North],
                constraint [inTorpedoRange (point 1 1)],
                constraint [canMoveTo East],
                constraint [canMoveWithSilence],
                constraint [canMoveTo North],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [inTorpedoRange (point 4 4)],
                constraint [canMoveTo South],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [canMoveTo North],
                constraint [canMoveTo East],
                constraint [canMoveTo North],
                constraint [canMoveTo North],
                constraint [canMoveTo West],
                constraint [canMoveWithSilence],
                constraint [canMoveTo West],
                constraint [canMoveTo North],
                constraint [notInSector (Sector 1)],
                constraint [canMoveTo West],
                constraint [canMoveTo West],
                constraint [canMoveTo West],
                constraint [inSector (Sector 1), reset],
                constraint [inTorpedoRange (point 0 0)],
                constraint [canMoveTo West],
                constraint [canMoveWithSilence],
                constraint [canMoveTo South],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [canMoveTo East],
                constraint [inSector (Sector 1)],
                constraint [canMoveTo East],
                constraint [canMoveTo North],
                constraint [canMoveWithSilence],
                constraint [canMoveTo East],
                constraint [canMoveTo South],
                constraint [canMoveTo South],
                constraint [canMoveTo South],
                constraint [inSector (Sector 2)],
                constraint [canMoveTo West],
                constraint [canMoveTo South],
                constraint [canMoveTo West],
                constraint [canMoveTo North],
                constraint [canMoveTo West],
                constraint [canMoveWithSilence],

                expectSolutions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
            \┃ • • • • • ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ • • ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ ▲ ▲ • ◎ ◎ ┃ ◎ ◎ • • • ┃ • ▲ ▲ ▲ • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃ • ▲ ▲ ▲ • ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • ▲ ▲ • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]
