import { add, mul, Price } from "../../../main/typescript/movierental/price";

describe("Price should", () => {
    it("be comparable", () => {
        expect(Price.euro(2.0)).toEqual(Price.euro(2.0));
    });

    it("be addable", () => {
        expect(add(Price.euro(2.0), Price.euro(3.0))).toEqual(Price.euro(5.0));
    });

    it("be scalable", () => {
        expect(mul(Price.euro(2.0), 4)).toEqual(Price.euro(8.0));
    });

    it("be renderable", () => {
        expect(Price.euro(2).render()).toEqual("2.0");
        expect(Price.euro(2.3).render()).toEqual("2.3");
        expect(Price.euro(2.43).render()).toEqual("2.4");
    });
});
