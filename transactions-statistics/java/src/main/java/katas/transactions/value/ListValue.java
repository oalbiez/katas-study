package katas.transactions.value;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public final class ListValue implements Value {
    private final List<Value> values;

    ListValue(final List<Value> values) {
        this.values = values;
    }

    public List<Value> values() {
        return unmodifiableList(values);
    }

    public <T> T accept(final ValueVisitor<T> visitor) {
        return visitor.visitList(this);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        final ListValue value = (ListValue) other;
        return Objects.equal(values, value.values);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(values);
    }

    @Override
    public String toString() {
        return Joiner.on(", ").join(values);
    }
}
