package katas.movierental;

import org.junit.Test;

import static katas.movierental.CustomerName.name;
import static katas.movierental.PriceCode.*;
import static org.junit.Assert.assertEquals;

public class CustomerTest {
    @Test
    public void testChildren() {
        Customer customer = customer("John Doe", rental(movie("Bambi", CHILDREN), 5));
        assertEquals("Rental Record for John Doe\n" +
                "\tBambi\t4.5\n" +
                "Amount owed is 4.5\n" +
                "You earned 1 frequent renter points", statementFor(customer));
    }

    @Test
    public void testRegular() {
        Customer customer = customer("John Doe", rental(movie("Inception", REGULAR), 5));
        assertEquals("Rental Record for John Doe\n" +
                "\tInception\t6.5\n" +
                "Amount owed is 6.5\n" +
                "You earned 1 frequent renter points", statementFor(customer));
    }

    @Test
    public void testNewRelease() {
        Customer customer = customer("John Doe", rental(movie("New", NEW_RELEASE), 5));
        assertEquals("Rental Record for John Doe\n" +
                "\tNew\t15.0\n" +
                "Amount owed is 15.0\n" +
                "You earned 2 frequent renter points", statementFor(customer));
    }

    @Test
    public void testMultiplesMovies() {
        Customer customer = customer("John Doe", rental(movie("One", NEW_RELEASE), 5), rental(movie("Two", REGULAR), 5));
        assertEquals("Rental Record for John Doe\n" +
                "\tOne\t15.0\n" +
                "\tTwo\t6.5\n" +
                "Amount owed is 21.5\n" +
                "You earned 3 frequent renter points", statementFor(customer));
    }

    private String statementFor(Customer customer) {
        return customer.statement();
    }

    private Rental rental(Movie movie, int daysRented) {
        return new Rental(movie, daysRented);
    }

    private Movie movie(String one, PriceCode newRelease) {
        return new Movie(one, newRelease);
    }

    private Customer customer(String name, Rental... rentals) {
        Customer customer = new Customer(name(name));
        for (Rental rental : rentals) {
            customer.addRental(rental);
        }
        return customer;
    }
}
