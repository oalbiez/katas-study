package katas.foobarqix;

public class FooBarQixWithEventBusShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return new FooBarQixWithEventBus().fooBarQix(input);
    }
}
