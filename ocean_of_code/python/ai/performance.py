# -*- coding: utf-8 -*-
from __future__ import annotations

from time import perf_counter_ns
from typing import Text


class Performance:
    # pylint: disable=too-few-public-methods
    def __init__(self):
        self.start = perf_counter_ns()

    def elapsed(self) -> Text:
        return f"{(perf_counter_ns() - self.start) / 1000000} ms"
