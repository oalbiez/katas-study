package katas.foobarqix;

import static java.lang.String.valueOf;

/**
 */
public class FooBarQixWithDecoratorCompact {

    private interface Rule {
        String apply(int value);
    }

    private static Rule none() {
        return value -> "";
    }

    private static Rule number(final Rule inner) {
        return value -> {
            final String result = inner.apply(value);
            return result.isEmpty() ? valueOf(value) : result;
        };
    }

    private static Rule divisorRule(final int divisor, final String label, final Rule inner) {
        return value -> {
            if (isDivisibleBy(value, divisor)) {
                return label + inner.apply(value);
            }
            return inner.apply(value);
        };
    }

    private static Rule foo(final Rule inner) {
        return divisorRule(3, "Foo", inner);
    }

    private static Rule bar(final Rule inner) {
        return divisorRule(5, "Bar", inner);
    }

    private static Rule qix(final Rule inner) {
        return divisorRule(7, "Qix", inner);
    }

    public static String fooBarQix(final int input) {
        return number(foo(bar(qix(none())))).apply(input);
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
