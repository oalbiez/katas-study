package katas.foobarqix;

public class FooBarQixWithDecoratorCompactShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return FooBarQixWithDecoratorCompact.fooBarQix(input);
    }
}
