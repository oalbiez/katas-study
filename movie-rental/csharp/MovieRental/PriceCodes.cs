﻿using static MovieRental.Duration;
using static MovieRental.Price;
using static MovieRental.Rules;

namespace MovieRental
{
    public static class PriceCodes
    {
        public static IPriceCode Regular() => new Regular();
        public static IPriceCode Children() => new Children();
        public static IPriceCode NewRelease() => new NewRelease();
    }

    public interface IPriceCode
    {
        Price Chargefor(Duration duration);
        int FrequentRenterPointsFor(Duration duration);
        string Describe();
    }

    internal sealed class Regular : IPriceCode
    {
        private readonly Pricing _pricing = Pricing.Build(Cost(Euro(2)).For(Days(2))).Then(Cost(Euro(1.5)).PerDay());
        public Price Chargefor(Duration duration) => _pricing.ChargeFor(duration);
        public int FrequentRenterPointsFor(Duration duration) => 1;
        public string Describe() => _pricing.Describe();
    }

    internal sealed class Children : IPriceCode
    {
        private readonly Pricing _pricing = Pricing.Build(Cost(Euro(1.5)).For(Days(3))).Then(Cost(Euro(1.5)).PerDay());
        public Price Chargefor(Duration duration) => _pricing.ChargeFor(duration);
        public int FrequentRenterPointsFor(Duration duration) => 1;
        public string Describe() => _pricing.Describe();
    }

    internal sealed class NewRelease : IPriceCode
    {
        private readonly Pricing _pricing = Pricing.Build(Cost(Euro(3)).PerDay());
        public Price Chargefor(Duration duration) => _pricing.ChargeFor(duration);
        public int FrequentRenterPointsFor(Duration duration) => duration > Days(1) ? 2 : 1;
        public string Describe() => _pricing.Describe();
    }
}