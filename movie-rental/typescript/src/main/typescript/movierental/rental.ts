import { Duration } from "./duration";
import { Movie } from "./movie";
import { Price } from "./price";

export class Rental {
    private movie: Movie;
    private duration: Duration;

    public constructor(movie: Movie, daysRented: number) {
        this.movie = movie;
        this.duration = Duration.days(daysRented);
    }

    public getTitle(): string {
        return this.movie.getTitle();
    }

    public charge(): Price {
        return this.movie.chargeFor(this.duration);
    }

    public frequentRenterPoints(): number {
        return this.movie.frequentRenterPointsFor(this.duration);
    }
}
