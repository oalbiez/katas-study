package katas.transactions.pic;

import java.time.LocalDateTime;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

public class Pic {
    final long amount;
    final LocalDateTime instant;

    public Pic(long amount, LocalDateTime instant) {
        this.amount = amount;
        this.instant = instant;
    }

    @Override
    public String toString() {
        return "Pic{" + "amount=" + amount + ", instant=" + instant.format(ISO_DATE_TIME) + '}';
    }

    public LocalDateTime instant() {
        return instant;
    }

    public long amount() {
        return amount;
    }
}
