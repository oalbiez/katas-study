package katas.foobarqix;

/**
 */
public class FooBarQixWithDecorator {

    interface Rule {
        String apply(int value);
    }

    static class Number implements Rule {
        private final Rule inner;

        Number(Rule inner) {
            this.inner = inner;
        }

        public String apply(int value) {
            final String result = inner.apply(value);
            return result.isEmpty() ? String.valueOf(value) : result;
        }
    }

    static class Foo implements Rule {
        private final Rule inner;

        Foo(Rule inner) {
            this.inner = inner;
        }

        public String apply(int value) {
            final String result = inner.apply(value);
            if (isDivisibleBy(value, 3)) {
                return "Foo" + result;
            }
            return result;
        }
    }

    static class Bar implements Rule {
        private final Rule inner;

        Bar(Rule inner) {
            this.inner = inner;
        }

        public String apply(int value) {
            final String result = inner.apply(value);
            if (isDivisibleBy(value, 5)) {
                return "Bar" + result;
            }
            return result;
        }
    }

    static class Qix implements Rule {
        private final Rule inner;

        Qix(Rule inner) {
            this.inner = inner;
        }

        public String apply(int value) {
            final String result = inner.apply(value);
            if (isDivisibleBy(value, 7)) {
                return "Qix" + result;
            }
            return result;
        }
    }

    static class None implements Rule {
        public String apply(int value) {
            return "";
        }
    }

    public static String fooBarQix(final int input) {
        return new Number(new Foo(new Bar(new Qix(new None())))).apply(input);
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
