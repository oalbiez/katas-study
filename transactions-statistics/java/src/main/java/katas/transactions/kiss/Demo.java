package katas.transactions.kiss;

import com.google.common.io.Files;
import katas.transactions.Transaction;
import katas.transactions.kiss.render.Render;
import katas.transactions.kiss.statistics.AllStatistics;
import katas.transactions.kiss.statistics.Balance;
import katas.transactions.kiss.statistics.ByClientBalance;
import katas.transactions.kiss.statistics.Periods;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Month;
import java.util.Map;

import static katas.transactions.TransactionProviders.random;
import static katas.transactions.kiss.html.Render.render;
import static katas.transactions.kiss.poi.WorkbookBuilder.build;

public class Demo {
    private static final LocalDate TODAY = LocalDate.of(2018, Month.APRIL, 17);
    private static final LocalDate OLDEST = TODAY.minusDays(400);

    public static void main(final String[] args) {
        final AllStatistics statistics = new AllStatistics(TODAY);
        transactions().forEach(statistics::updateWith);

        System.out.println("Client summary");
        Render.renderByClientSummary(statistics).forEach(System.out::println);
        System.out.println();

        for (final Map.Entry<Periods, ByClientBalance> entry : statistics.byPeriods().entrySet()) {
            System.out.println("### " + entry.getKey());
            for (final Map.Entry<String, Balance> e : entry.getValue().getStatistics().entrySet()) {
                System.out.println("\t" + e.getKey() + ": " + e.getValue().count());
            }
        }

        build(statistics, TODAY).save("result.xlsx");
        try {
            Files.write(render(statistics), new File("result.xls"), StandardCharsets.UTF_8);
            Files.write(render(statistics), new File("result.html"), StandardCharsets.UTF_8);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static Iterable<Transaction> transactions() {
        return random(TODAY).transactionsSince(OLDEST);
    }
}
