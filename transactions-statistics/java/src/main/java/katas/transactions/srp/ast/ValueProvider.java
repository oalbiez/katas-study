package katas.transactions.srp.ast;

import katas.transactions.Transaction;

public interface ValueProvider<T> {
    T get(Transaction transaction);

    String describe();
}
