{-# LANGUAGE OverloadedStrings #-}
module DirectionSpec (spec) where


import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec = do


    it "Direction should have an opposite direction" $ do
        inverseDirection North `shouldBe` South
        inverseDirection South `shouldBe` North
        inverseDirection East `shouldBe` West
        inverseDirection West `shouldBe` East


    it "Direction should move a point" $ do
        moveInDirection North (point 4 4) `shouldBe` point 4 3
        moveInDirection South (point 4 4) `shouldBe` point 4 5
        moveInDirection East (point 4 4) `shouldBe` point 5 4
        moveInDirection West (point 4 4) `shouldBe` point 3 4

