package katas.foobarqix;

import java.util.List;
import java.util.function.Predicate;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;

/**
 * Using DSL
 */
public class FooBarQixWithDsl {

    interface Rule {
        boolean apply(int input, StringBuilder accumulator);
    }

    interface RuleBuilder {
        Rule then(final String label);
    }

    static Predicate<Integer> isDivisibleBy(int divisor) {
        return value -> value % divisor == 0;
    }

    static RuleBuilder when(final Predicate<Integer> predicate) {
        return label -> (input, accumulator) -> {
            if (predicate.test(input)) {
                accumulator.append(label);
                return true;
            }
            return false;
        };
    }

    static FooBarQixWithDsl rules(Rule... rules) {
        return new FooBarQixWithDsl(asList(rules));
    }

    private final List<Rule> rules;

    private FooBarQixWithDsl(List<Rule> rules) {
        this.rules = rules;
    }

    public String compute(final int input) {
        StringBuilder accumulator = new StringBuilder();
        boolean ruleMatched = false;
        for (Rule rule : rules) {
            ruleMatched |= rule.apply(input, accumulator);
        }
        return ruleMatched ? accumulator.toString() : valueOf(input);
    }
}
