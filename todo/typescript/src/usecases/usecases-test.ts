import { expect } from "chai";
import { TodoListIdentifierGenerator } from "../models/todo_list_identifier_generator";
import { TodoListStore } from "../models/todo_list_store";
import { FakeIdentifierGenerator } from "../testhelpers/fake_identifier_generator";
import { renderStore, storeWith } from "../testhelpers/store_dsl";
import { unindent } from "../testhelpers/unindent";
import { ItemUsecases } from "./item_usecases";
import { ListUsecases } from "./list_usecases";

describe("usecases", () => {

  describe("list managment", () => {

    it("should create a todo list", () => {
      scenario()
        .given_empty_store()
        .and_next_identifiers_are("2a")
        .when_I_create_a_new_list_named("my list")
        .then_store_should_contains(`
          |[2a] my list:
          |  <empty>
          `);
    });

    it("should list all todo lists", () => {
      scenario()
        .given_store(`
          |[01] first todo list:
          |  - [x] first
          |
          |[02] second todo list:
          |  - [x] first
        `)
        .then_the_list_all_todo_lists_should_be("first todo list, second todo list");
    });

    it("should rename a todo list", () => {
      scenario()
        .given_store(`
          |[2a] list:
          |  <empty>
        `)
        .when_I_rename_the_list("2a", "new name")
        .then_store_should_contains(`
          |[2a] new name:
          |  <empty>
          `);
    });

    it("should delete a todo list", () => {
      scenario()
        .given_store(`
          |[2a] list:
          |  <empty>
        `)
        .when_I_delete_list_identified_by("2a")
        .then_store_should_be_empty();
    });

  });

  describe("item managment", () => {

    it("create should add an new item on existing list", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  <empty>
        `)
        .when_I_create_item_on_list("2a", "first")
        .then_store_should_contains(`
          |[2a] my list:
          |  - [ ] first
          `);
    });

    it("create should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_create_item_on_list("2a", "first")
        .then_store_should_be_empty();
    });

    it("check should change the status of an unchecked item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [ ] first
        `)
        .when_I_check_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [x] first
          `);
    });

    it("check should do nothing on already checked item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_check_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [x] first
          `);
    });

    it("check should do nothing on unknown item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [ ] first
        `)
        .when_I_check_item("2a", 5)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [ ] first
          `);
    });

    it("check should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_check_item("2a", 0)
        .then_store_should_be_empty();
    });

    it("toggle should check an uncheked item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [ ] first
        `)
        .when_I_toggle_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [x] first
          `);
    });

    it("toggle should uncheck a checked item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_toggle_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [ ] first
          `);
    });

    it("toggle should do nothing on unknown item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_toggle_item("2a", 5)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [x] first
          `);
    });

    it("toggle should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_toggle_item("2a", 0)
        .then_store_should_be_empty();
    });

    it("uncheck should change the status of a checked item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_uncheck_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [ ] first
          `);
    });

    it("uncheck should do nothing on unchecked item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [ ] first
        `)
        .when_I_uncheck_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [ ] first
          `);
    });

    it("uncheck should do nothing on unknown item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_uncheck_item("2a", 5)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [x] first
          `);
    });

    it("uncheck should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_uncheck_item("2a", 0)
        .then_store_should_be_empty();
    });

    it("delete should remove an existing item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_delete_item("2a", 0)
        .then_store_should_contains(`
          |[2a] my list:
          |  <empty>
          `);
    });

    it("delete should do nothing on unknwon item", () => {
      scenario()
        .given_store(`
          |[2a] my list:
          |  - [x] first
        `)
        .when_I_delete_item("2a", 5)
        .then_store_should_contains(`
          |[2a] my list:
          |  - [x] first
          `);
    });

    it("delete should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_delete_item("2a", 0)
        .then_store_should_be_empty();
    });

  });

});

class Scenario {
  private store: TodoListStore = storeWith("");
  private generator: TodoListIdentifierGenerator = new FakeIdentifierGenerator();

  public given_store(content: string): Scenario {
    this.store = storeWith(unindent(content));
    return this;
  }

  public given_empty_store(): Scenario {
    this.store = storeWith("");
    return this;
  }

  public and_next_identifiers_are(...values: string[]): Scenario {
    this.generator = new FakeIdentifierGenerator(...values);
    return this;
  }

  public when_I_create_a_new_list_named(name: string): Scenario {
    new ListUsecases(this.store, this.generator).createList(name);
    return this;
  }

  public then_the_list_all_todo_lists_should_be(expected: string): Scenario {
    expect(new ListUsecases(this.store, this.generator).all()
      .map((list) => list.name.value)
      .join(", "))
      .to.equal(expected);
    return this;
  }

  public when_I_rename_the_list(identifier: string, newName: string): Scenario {
    new ListUsecases(this.store, this.generator).renameList(identifier, newName);
    return this;
  }

  public when_I_delete_list_identified_by(identifier: string): Scenario {
    new ListUsecases(this.store, this.generator).deleteList(identifier);
    return this;
  }

  public when_I_create_item_on_list(identifier: string, label: string): Scenario {
    new ItemUsecases(this.store).createItem(identifier, label);
    return this;
  }

  public when_I_check_item(identifier: string, index: number): Scenario {
    new ItemUsecases(this.store).checkItem(identifier, index);
    return this;
  }

  public when_I_toggle_item(identifier: string, index: number): Scenario {
    new ItemUsecases(this.store).toggleItem(identifier, index);
    return this;
  }

  public when_I_uncheck_item(identifier: string, index: number): Scenario {
    new ItemUsecases(this.store).uncheckItem(identifier, index);
    return this;
  }

  public when_I_delete_item(identifier: string, index: number): Scenario {
    new ItemUsecases(this.store).deleteItem(identifier, index);
    return this;
  }

  public then_store_should_be_empty(): Scenario {
    expect(this.store.all().size).to.equal(0);
    return this;
  }

  public then_store_should_contains(content: string): Scenario {
    expect(renderStore(this.store)).to.equal(unindent(content));
    return this;
  }

}

function scenario(): Scenario {
  return new Scenario();
}
