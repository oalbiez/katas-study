package katas.marsrover;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;

public class Obstacles {
    private final List<Obstacle> obstacles;

    public static Obstacles obstacles(List<Obstacle> obstacles) {
        return new Obstacles(obstacles);
    }

    public static Obstacles obstacles(final Obstacle... obstacles) {
        return new Obstacles(asList(obstacles));
    }

    private Obstacles(List<Obstacle> obstacles) {
        this.obstacles = obstacles;
    }

    public Optional<Obstacle> at(final Position position) {
        for (Obstacle obstacle : obstacles) {
            if (obstacle.isLocatedAt(position)) {
                return Optional.of(obstacle);
            }
        }
        return Optional.empty();
    }
}
