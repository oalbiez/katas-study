﻿namespace Wallet
{
    public interface IRateProvider
    {
        decimal GetRate(Currency from, Currency to);
    }
}