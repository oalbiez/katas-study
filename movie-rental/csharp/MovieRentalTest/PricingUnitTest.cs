﻿using MovieRental;
using NUnit.Framework;
using static MovieRental.Duration;
using static MovieRental.Price;
using static MovieRental.Rules;
using static NUnit.Framework.Assert;

namespace MovieRentalTest
{
    [TestFixture]
    public class PricingUnitTest
    {
        [Test]
        public void pricing_should_price_constant_price()
        {
            AreEqual(Euro(2), Pricing.Build(Cost(Euro(2))).ChargeFor(Days(3)));
        }

        [Test]
        public void pricing_should_price_constant_price_with_limited_duration()
        {
            AreEqual(Euro(2), Pricing.Build(Cost(Euro(2)).For(Days(3))).ChargeFor(Days(1)));
            AreEqual(Euro(2), Pricing.Build(Cost(Euro(2)).For(Days(3))).ChargeFor(Days(2)));
            AreEqual(Euro(2), Pricing.Build(Cost(Euro(2)).For(Days(3))).ChargeFor(Days(3)));
        }

        [Test]
        public void pricing_should_price_per_day_price()
        {
            AreEqual(Euro(6), Pricing.Build(Cost(Euro(2)).PerDay()).ChargeFor(Days(3)));
        }

        [Test]
        public void pricing_should_price_per_day_price_with_limited_duration()
        {
            AreEqual(Euro(2), Pricing.Build(Cost(Euro(2)).PerDay().For(Days(3))).ChargeFor(Days(1)));
            AreEqual(Euro(4), Pricing.Build(Cost(Euro(2)).PerDay().For(Days(3))).ChargeFor(Days(2)));
            AreEqual(Euro(6), Pricing.Build(Cost(Euro(2)).PerDay().For(Days(3))).ChargeFor(Days(3)));
        }

        [Test]
        public void pricing_should_price_with_multiples_rules()
        {
            AreEqual(Euro(8),
                Pricing.Build(Cost(Euro(2)).For(Days(3))).Then(Cost(Euro(3)).PerDay())
                    .ChargeFor(Days(5)));
            AreEqual(Euro(7),
                Pricing.Build(Cost(Euro(2)).PerDay().For(Days(2))).Then(Cost(Euro(3)))
                    .ChargeFor(Days(10)));
            AreEqual(Euro(2),
                Pricing.Build(Cost(Euro(2)).For(Days(2))).Then(Cost(Euro(3)))
                    .ChargeFor(Days(1)));
        }

        [Test]
        public void pricing_should_manage_overflow()
        {
            Catch(typeof(Overflow), () => Pricing.Build(Cost(Euro(2)).For(Days(3))).ChargeFor(Days(5)));
        }

        [Test]
        public void pricing_should_error_when_more_than_one_per_day_applyed_on_rule()
        {
            Catch(typeof(MalformedPricing), () => Cost(Euro(2)).PerDay().PerDay());
        }

        [Test]
        public void pricing_should_error_when_more_than_one_for_applyed_on_rule()
        {
            Catch(typeof(MalformedPricing), () => Cost(Euro(2)).For(Days(3)).For(Days(7)));
        }

        [Test]
        public void pricing_should_error_when_par_day_applyed_after_for()
        {
            Catch(typeof(MalformedPricing), () => Cost(Euro(2)).For(Days(3)).PerDay());
        }

        [Test]
        public void pricing_should_self_describe()
        {
            AreEqual("cost 2€", Pricing.Build(Cost(Euro(2))).Describe());
            AreEqual("cost 2€ per day", Pricing.Build(Cost(Euro(2)).PerDay()).Describe());
            AreEqual("cost 2€ for 5 day(s)", Pricing.Build(Cost(Euro(2)).For(Days(5))).Describe());
            AreEqual("cost 2€ for 5 day(s) then cost 3€ per day",
                Pricing.Build(Cost(Euro(2)).For(Days(5))).Then(Cost(Euro(3)).PerDay()).Describe());
        }

        [Test]
        public void pricing_should_explain()
        {
            AreEqual(
                "rule [cost 2€]: price=2€, total=2€, remaining=0",
                Pricing.Build(Cost(Euro(2))).Explain(Days(4)));
            AreEqual(
                "rule [cost 2€ for 4 day(s)]: price=2€, total=2€, remaining=6; " +
                "rule [cost 3€ per day]: price=18€, total=20€, remaining=0",
                Pricing.Build(Cost(Euro(2)).For(Days(4))).Then(Cost(Euro(3)).PerDay()).Explain(Days(10)));
            AreEqual(
                "rule [cost 2€ for 4 day(s)]: price=2€, total=2€, remaining=6; " +
                "overflow of 6",
                Pricing.Build(Cost(Euro(2)).For(Days(4))).Explain(Days(10)));
        }
    }
}