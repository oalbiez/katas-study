package katas.transactions.kiss.statistics;

import katas.transactions.Transaction;

import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableMap;

public final class ByClient<T extends Statistic> implements Statistic {
    private final Supplier<T> supplier;
    private final Map<String, T> statistics = newHashMap();

    ByClient(final Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public void updateWith(final Transaction transaction) {
        statisticFor(transaction.client()).updateWith(transaction);
    }

    public T statisticFor(final String client) {
        if (statistics.containsKey(client)) {
            return statistics.get(client);
        }
        final T value = supplier.get();
        statistics.put(client, value);
        return value;
    }

    public Map<String, T> getStatistics() {
        return unmodifiableMap(statistics);
    }

    public Collection<String> clients() {
        return unmodifiableCollection(statistics.keySet());
    }
}
