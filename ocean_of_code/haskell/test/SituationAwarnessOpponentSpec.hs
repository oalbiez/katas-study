{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE OverloadedLabels      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE ViewPatterns          #-}
module SituationAwarnessOpponentSpec (spec) where


import           Named
import           OceanOfCode
import           Test.Hspec


constraints ::
    "orders" :? String ->
    "report" :? String ->
    "echo" :? SonarEcho ->
    "life" :? LifeStatus
    -> [Constraint]
constraints
    (argDef #orders "" -> orders)
    (argDef #report "" -> report)
    (argDef #echo NA   -> echo)
    (argDef #life (LifeStatus 6 6)   -> life)
    = opponentConstraints life (parseOrders orders) (parseReport report) echo


spec :: Spec
spec = do

    it "should add no contraint with no report" $
        constraints ! #report "NA"
                    ! defaults
            `shouldMatchList` []


    it "should add sector constraint with sonar order and echo" $ do
        constraints ! #orders "SONAR 4"
                    ! #echo Hit
                    ! defaults
            `shouldMatchList` [inSector (Sector 4)]

        constraints ! #orders "SONAR 2"
                    ! #echo Miss
                    ! defaults
            `shouldMatchList` [notInSector (Sector 2)]


    it "should add movement contraint with move" $ do
        constraints ! #report "MOVE N"
                    ! defaults
            `shouldMatchList` [canMoveTo North]

        constraints ! #report "MOVE S"
                    ! defaults
            `shouldMatchList` [canMoveTo South]

        constraints ! #report "MOVE E"
                    ! defaults
            `shouldMatchList` [canMoveTo East]

        constraints ! #report "MOVE W"
                    ! defaults
            `shouldMatchList` [canMoveTo West]


    it "should add movement contraint with silence" $
        constraints ! #report "SILENCE"
                    ! defaults
            `shouldMatchList` [canMoveWithSilence]


    it "should add sector contraint with surface" $
        constraints ! #report "SURFACE 4"
                    ! defaults
            `shouldMatchList` [inSector (Sector 4), Reset]


    it "should add mine dropped contraint with mine drop" $
        constraints ! #report "MINE"
                    ! defaults
            `shouldMatchList` [mineDropped]


    describe "should add in mine trigged contraint with single mine trigger" $ do

        it "without life lost" $
            constraints ! #report "TRIGGER 4 4"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [mineTrigged (point 4 4), notInExplosion (point 4 4)]

        it "with one life lost" $
            constraints ! #report "TRIGGER 4 4"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [mineTrigged (point 4 4), inBlastExplosion (point 4 4)]

        it "with two lifes lost" $
            constraints ! #report "TRIGGER 4 4"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [mineTrigged (point 4 4), inPosition (point 4 4)]


    describe "should add in torpedo contraint with single torpedo" $ do

        it "without life lost" $
            constraints ! #report "TORPEDO 4 4"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 4 4),
                    notInExplosion (point 4 4)]

        it "with one life lost" $
            constraints ! #report "TORPEDO 4 4"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 4 4),
                    inBlastExplosion (point 4 4)]

        it "with two lifes lost" $
            constraints ! #report "TORPEDO 4 4"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 4 4),
                    inPosition (point 4 4)]


    describe "should add constraints regarding explosions and life lost" $ do

        it "our single explosion without life lost" $ do
            constraints ! #orders "TORPEDO 4 4"
                        ! #report "NA"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [notInExplosion (point 4 4)]

            constraints ! #orders "TRIGGER 4 4"
                        ! #report "NA"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [notInExplosion (point 4 4)]

        it "our single explosion with one life lost" $ do
            constraints ! #orders "TORPEDO 4 4"
                        ! #report "NA"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [inBlastExplosion (point 4 4)]

            constraints ! #orders "TRIGGER 4 4"
                        ! #report "NA"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [inBlastExplosion (point 4 4)]

        it "our single explosion with two lifes lost" $ do
            constraints ! #orders "TORPEDO 4 4"
                        ! #report "NA"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [inPosition (point 4 4)]

            constraints ! #orders "TRIGGER 4 4"
                        ! #report "NA"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [inPosition (point 4 4)]

        it "lots of explosions without life lost" $
            constraints ! #orders "TORPEDO 4 4 | TRIGGER 8 7"
                        ! #report "TORPEDO 3 7 | TRIGGER 8 4"
                        ! #life (LifeStatus 6 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 3 7),
                    mineTrigged (point 8 4),
                    notInExplosion (point 4 4),
                    notInExplosion (point 8 7),
                    notInExplosion (point 3 7),
                    notInExplosion (point 8 4)]

        it "lots of explosions with one life lost" $
            constraints ! #orders "TORPEDO 4 4 | TRIGGER 8 7"
                        ! #report "TORPEDO 3 7 | TRIGGER 8 4"
                        ! #life (LifeStatus 5 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 3 7),
                    mineTrigged (point 8 4),
                    notInPosition (point 4 4),
                    notInPosition (point 8 7),
                    notInPosition (point 3 7),
                    notInPosition (point 8 4)]

        it "lots of explosions with two lifes lost" $
            constraints ! #orders "TORPEDO 4 4 | TRIGGER 8 7"
                        ! #report "TORPEDO 3 7 | TRIGGER 8 4"
                        ! #life (LifeStatus 4 6)
                        ! defaults
                `shouldMatchList` [
                    inTorpedoRange (point 3 7),
                    mineTrigged (point 8 4)]
