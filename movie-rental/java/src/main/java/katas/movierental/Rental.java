package katas.movierental;

import static katas.movierental.Duration.days;

/**
 * The rental class represents a customer renting a movie.
 */
public class Rental {
    private Movie movie;
    private Duration duration;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.duration = days(daysRented);
    }

    Price chargeFor() {
        return movie.chargeFor(duration);
    }

    FrequentRenterPoint frequentRenterPointsFor() {
        return movie.frequentRenterPointFor(duration);

    }

    String getTitle() {
        return movie.getTitle();
    }
}
