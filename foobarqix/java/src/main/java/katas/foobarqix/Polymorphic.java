package katas.foobarqix;

/**
 * Configurable with constructors rules
 */
public class Polymorphic {

    interface Word {
        String say(int value);
    }

    private static Word numeric() {
        return String::valueOf;
    }

    private static Word literal(final String label) {
        return value -> label;
    }

    private static Word composite(final Word left, final Word right) {
        return value -> left.say(value) + right.say(value);
    }

    public static String fooBarQix(final int input) {
        return build(input).say(input);
    }

    private static Word build(final int input) {
        Word word = literal("");
        boolean ruleMatched = false;
        if (isDivisibleBy(input, 3)) {
            word = composite(word, literal("Foo"));
            ruleMatched = true;
        }
        if (isDivisibleBy(input, 5)) {
            word = composite(word, literal("Bar"));
            ruleMatched = true;
        }
        if (isDivisibleBy(input, 7)) {
            word = composite(word, literal("Qix"));
            ruleMatched = true;
        }
        return ruleMatched ? word : numeric();
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
