from getpass import getpass
from hangman.errors import WordTooSmall, WordWithInvalidLetter
from hangman.object import Hangman, WordToGuess


def input_word_to_guess():
    while True:
        try:
            return WordToGuess(getpass("Entrez le mot à deviner:"))
        except WordTooSmall:
            print("Le mot à deviner doit contenir au moins 4 lettres")
        except WordWithInvalidLetter:
            print("Le mot à deviner ne doit contenir que des lettres et sans accents")


def display_status(turn):
    print('\nMot à trouver: %(puzzle)s, vous avez fait  %(errors)i erreure(s)' % {
        'errors': turn.errors,
        'puzzle': turn.puzzle})


def display_final(turn):
    if turn.won:
        print('\nGagné avec %(errors)i erreure(s). Le mot était %(puzzle)s.' % {
            'errors': turn.errors,
            'puzzle': turn.puzzle})
    if turn.lost:
        print('\nPerdu. Le mot était %(puzzle)s.' % {
            'puzzle': turn.puzzle})


def user_guess(hangman):
    value = input("Entrez une lettre ou un mot: ")
    if len(value) == 1:
        hangman.test_a_letter(value)
    else:
        hangman.test_a_word(value)
    return hangman.turn()


def run(max_errors):
    hangman = Hangman(input_word_to_guess(), max_errors)
    turn = hangman.turn()
    while not (turn.won or turn.lost):
        display_status(turn)
        turn = user_guess(hangman)
    display_final(turn)


__all__ = ["run"]
