# -*- coding: utf-8 -*-
from __future__ import annotations

from enum import Enum
from typing import Text


class SonarResult(Enum):
    HIT = "Y"
    MISS = "N"
    NOT_APPLICABLE = "NA"

    @classmethod
    def parse(cls, value: Text) -> SonarResult:
        for result in cls:
            if value == result.value:
                return result
        return cls.NOT_APPLICABLE

    def __repr__(self) -> Text:
        return f"SonarResult.{self.name}"
