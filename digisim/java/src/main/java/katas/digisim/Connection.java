package katas.digisim;

public interface Connection {
    boolean value();
}
