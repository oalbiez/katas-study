﻿using System;
using System.Diagnostics.Contracts;

namespace Wallet
{
    public struct Money : IEquatable<Money>
    {
        private readonly decimal _value;
        private decimal Value => Currency.Normalize(_value);
        private Currency Currency { get; }

        public Money(decimal value, Currency currency)
        {
            Contract.Requires(currency != null);
            _value = value;
            Currency = currency;
        }

        [Pure]
        public Money Change(Currency to, IRateProvider provider)
        {
            Contract.Requires(to != null);
            Contract.Requires(provider != null);
            return Currency == to ? this : to.Amount(_value * provider.GetRate(Currency, to));
        }

        public static Money operator +(Money left, Money right)
        {
            if (left.Currency == right.Currency)
            {
                return new Money(left._value + right._value, left.Currency);
            }

            throw new MismatchCurrency();
        }

        public static bool operator ==(Money left, Money right) => Equals(left, right);

        public static bool operator !=(Money left, Money right) => !Equals(left, right);

        public override string ToString() => $"{Currency} {Value}";

        public bool Equals(Money other) => Value == other.Value && Currency.Equals(other.Currency);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Money && Equals((Money) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Value.GetHashCode() * 397) ^ Currency.GetHashCode();
            }
        }
    }
}