package kata.ast;

public interface AST {
    <T> T accept(ASTVisitor<T> visitor);
}
