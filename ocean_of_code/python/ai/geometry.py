# -*- coding: utf-8 -*-
from __future__ import annotations

import math

from dataclasses import dataclass
from typing import Iterator, List, Text


@dataclass(frozen=True, order=True)
class Delta:
    # pylint: disable=invalid-name
    dx: int
    dy: int

    def __add__(self, other: Delta) -> Delta:
        return Delta(self.dx + other.dx, self.dy + other.dy)

    def __sub__(self, other: Delta) -> Delta:
        return Delta(self.dx - other.dx, self.dy - other.dy)

    def __mul__(self, factor: int) -> Delta:
        return Delta(self.dx * factor, self.dy * factor)

    def length(self) -> float:
        return math.sqrt(self.dx * self.dx + self.dy * self.dy)

    @staticmethod
    def zero() -> Delta:
        return Delta(0, 0)


@dataclass(frozen=True, order=True)
class Position:
    x: int
    y: int

    def __add__(self, delta: Delta) -> Position:
        return Position(self.x + delta.dx, self.y + delta.dy)

    def __sub__(self, delta: Delta) -> Position:
        return Position(self.x - delta.dx, self.y - delta.dy)

    def __str__(self) -> Text:
        return f"({self.x}, {self.y})"

    def __repr__(self) -> Text:
        return f"Position({self.x}, {self.y})"

    @classmethod
    def parse(cls, value) -> Position:
        x, y = value.split()
        return cls(int(x), int(y))

    @staticmethod
    def zero() -> Position:
        return Position(0, 0)

    @staticmethod
    def delta(left: Position, right: Position) -> Delta:
        return Delta(left.x - right.x, left.y - right.y)

    @staticmethod
    def min(left: Position, right: Position) -> Position:
        return Position(min(left.x, right.x), min(left.y, right.y))

    @staticmethod
    def max(left: Position, right: Position) -> Position:
        return Position(max(left.x, right.x), max(left.y, right.y))

    @staticmethod
    def distance(left: Position, right: Position) -> float:
        return Position.delta(left, right).length()


@dataclass(frozen=True)
class Rect:
    topleft: Position
    bottomright: Position

    @staticmethod
    def to(bottomright: Position) -> Rect:
        return Rect.between(Position.zero(), bottomright)

    @staticmethod
    def between(first: Position, second: Position) -> Rect:
        return Rect(Position.min(first, second), Position.max(first, second))

    @staticmethod
    def containing(positions: List[Position]) -> Rect:
        return Rect(
            Position(
                min(p.x for p in positions),
                min(p.y for p in positions)
            ),
            Position(
                max(p.x for p in positions) + 1,
                max(p.y for p in positions) + 1
            )
        )

    def width(self) -> int:
        return self.bottomright.x - self.topleft.x

    def height(self) -> int:
        return self.bottomright.y - self.topleft.y

    def __contains__(self, position: Position) -> bool:
        if not self.topleft.x <= position.x < self.bottomright.x:
            return False
        if not self.topleft.y <= position.y < self.bottomright.y:
            return False
        return True

    def __iter__(self) -> Iterator[Position]:
        for x in range(self.topleft.x, self.bottomright.x):
            for y in range(self.topleft.y, self.bottomright.y):
                yield Position(x, y)

    def center(self) -> Position:
        return Position(
            (self.topleft.x + self.bottomright.x) // 2,
            (self.topleft.y + self.bottomright.y) // 2)

    def split(self, point: Position) -> List[Rect]:
        if point not in self:
            return [self]
        splitted = [
            Rect(self.topleft, Position(point.x, self.bottomright.y)),
            Rect(Position(point.x + 1, self.topleft.y), self.bottomright),
            Rect(self.topleft, Position(self.bottomright.x, point.y)),
            Rect(Position(self.topleft.x, point.y + 1), self.bottomright),
        ]
        return [rect for rect in splitted if rect.area() > 0]

    def area(self) -> int:
        return (self.bottomright.x - self.topleft.x) * (self.bottomright.y - self.topleft.y)
