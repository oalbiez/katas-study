package katas.foobarqix;

import static java.lang.String.valueOf;
import static katas.foobarqix.WithEnum.Rules.processRules;

/**
 * Using enum to externalize rules.
 * Pro: fooBarQix is open/close
 * Cons: more code
 */
public class WithEnum {
    @SuppressWarnings("unused")
    enum Rules {
        FOO(3, "Foo"),
        BAR(5, "Bar"),
        QIX(7, "Qix");

        private final int divisor;
        private final String result;

        Rules(int divisor, String result) {
            this.divisor = divisor;
            this.result = result;
        }

        boolean apply(int value, StringBuilder accumulator) {
            if (value % divisor == 0) {
                accumulator.append(result);
                return true;
            }
            return false;
        }

        static boolean processRules(final int input, StringBuilder accumulator) {
            boolean ruleMatched = false;
            for (Rules rule : Rules.values()) {
                ruleMatched |= rule.apply(input, accumulator);
            }
            return ruleMatched;
        }
    }

    public static String fooBarQix(final int input) {
        final StringBuilder accumulator = new StringBuilder();
        return processRules(input, accumulator) ? accumulator.toString() : valueOf(input);
    }
}
