package katas.transactions;

import java.time.LocalDate;

public interface TransactionProvider {
    Iterable<Transaction> transactionsSince(LocalDate day);
}
