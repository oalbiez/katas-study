package katas.movierental.pricing;

public interface ValueDriver {
    Value zero();

    Value add(Value left, Value right);

    Value mul(Value left, double right);
}
