# -*- coding: utf-8 -*-
from functools import partial


class MemoizeMethod:
    def __init__(self, function):
        self._function = function

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self._function
        return partial(self, obj)

    def __call__(self, *args, **kw):
        # pylint: disable=protected-access
        obj = args[0]
        try:
            cache = obj.__cache
        except AttributeError:
            cache = obj.__cache = {}
        key = (self._function, args[1:], frozenset(kw.items()))
        try:
            res = cache[key]
        except KeyError:
            res = cache[key] = self._function(*args, **kw)
        return res


def memoize_method(function):
    return MemoizeMethod(function)
