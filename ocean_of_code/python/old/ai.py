# -*- coding: utf-8 -*-
from __future__ import annotations

import heapq
import logging
import math
import random
import sys

from abc import ABC, abstractmethod
from collections import Counter
from dataclasses import dataclass
from enum import Enum
from functools import partial
from itertools import chain
from time import perf_counter_ns
from typing import Callable, Dict, FrozenSet, Iterator, List, Optional, Set, Text, Tuple
from textwrap import dedent


# pylint: disable=too-many-lines


# == Debug and Performance


logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


def debug(*args):
    print(*args, file=sys.stderr, flush=True)


class Performance:
    # pylint: disable=too-few-public-methods
    def __init__(self):
        self.start = perf_counter_ns()

    def elapsed(self):
        return f"{(perf_counter_ns() - self.start) / 1000000} ms"


# == Tools


class MemoizeMethod:
    def __init__(self, function):
        self._function = function

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self._function
        return partial(self, obj)

    def __call__(self, *args, **kw):
        # pylint: disable=protected-access
        obj = args[0]
        try:
            cache = obj.__cache
        except AttributeError:
            cache = obj.__cache = {}
        key = (self._function, args[1:], frozenset(kw.items()))
        try:
            res = cache[key]
        except KeyError:
            res = cache[key] = self._function(*args, **kw)
        return res


def memoize_method(function):
    return MemoizeMethod(function)


# == Algorithms


def search_distance(start, distance, neighbors):
    """ Performs a distance search on the graph.

    @param start the start location of the graph
    @param distance the goal as distance
    @param neighbors a functor position -> []
    """

    frontier = []
    _put(frontier, start, 0)
    cost_map = {start: 0}

    while not _empty(frontier):
        current = _pop(frontier)
        for neighbor in neighbors(current):
            new_cost = cost_map[current] + 1
            if new_cost > distance:
                continue
            if neighbor not in cost_map or new_cost < cost_map[neighbor]:
                cost_map[neighbor] = new_cost
                priority = new_cost
                _put(frontier, neighbor, priority)

    return cost_map


def astar_search(start, goal, neighbors, heuristic):
    """ Performs a A* search on the graph.

    @param start the start location of the graph
    @param goal the goal as graph location
    @param neighbors a functor position -> []
    @param heuristic a functor (from, to) -> cost estimate
    """

    frontier = []
    _put(frontier, start, 0)
    path = {start: None}
    costs = {start: 0}

    while not _empty(frontier):
        current = _pop(frontier)

        if current == goal:
            return True, _path(path, goal), costs[goal]

        for neighbor in neighbors(current):
            new_cost = costs[current] + 1
            if neighbor not in costs or new_cost < costs[neighbor]:
                costs[neighbor] = new_cost
                priority = new_cost + heuristic(goal, neighbor)
                _put(frontier, neighbor, priority)
                path[neighbor] = current
    return False, [], 0


def _empty(queue):
    return len(queue) == 0


def _put(queue, item, priority):
    heapq.heappush(queue, (priority, item))


def _pop(queue):
    return heapq.heappop(queue)[1]


def _path(path, goal):
    result = []
    index = goal
    while index is not None:
        result.append(index)
        index = path[index]
    return list(reversed(result))


# == Maths


@dataclass(frozen=True, order=True)
class Delta:
    # pylint: disable=invalid-name
    dx: int
    dy: int

    def __add__(self, other: Delta) -> Delta:
        return Delta(self.dx + other.dx, self.dy + other.dy)

    def __sub__(self, other: Delta) -> Delta:
        return Delta(self.dx - other.dx, self.dy - other.dy)

    def __mul__(self, factor: int) -> Delta:
        return Delta(self.dx * factor, self.dy * factor)

    def length(self) -> float:
        return math.sqrt(self.dx * self.dx + self.dy * self.dy)

    @staticmethod
    def zero() -> Delta:
        return Delta(0, 0)


@dataclass(frozen=True, order=True)
class Position:
    x: int
    y: int

    def __add__(self, delta: Delta) -> Position:
        return Position(self.x + delta.dx, self.y + delta.dy)

    def __sub__(self, delta: Delta) -> Position:
        return Position(self.x - delta.dx, self.y - delta.dy)

    def __str__(self) -> Text:
        return "(" + str(self.x) + ", " + str(self.y) + ")"

    @classmethod
    def parse(cls, value) -> Position:
        x, y = value.split()
        return cls(int(x), int(y))

    @staticmethod
    def zero() -> Position:
        return Position(0, 0)

    @staticmethod
    def delta(left: Position, right: Position) -> Delta:
        return Delta(left.x - right.x, left.y - right.y)

    @staticmethod
    def min(left: Position, right: Position) -> Position:
        return Position(min(left.x, right.x), min(left.y, right.y))

    @staticmethod
    def max(left: Position, right: Position) -> Position:
        return Position(max(left.x, right.x), max(left.y, right.y))

    @staticmethod
    def distance(left: Position, right: Position) -> float:
        return Position.delta(left, right).length()


@dataclass(frozen=True)
class Rect:
    topleft: Position
    bottomright: Position

    @staticmethod
    def to(bottomright: Position) -> Rect:
        return Rect.between(Position.zero(), bottomright)

    @staticmethod
    def between(first: Position, second: Position) -> Rect:
        return Rect(Position.min(first, second), Position.max(first, second))

    @staticmethod
    def containing(positions: List[Position]) -> Rect:
        return Rect(
            Position(
                min(p.x for p in positions),
                min(p.y for p in positions)
            ),
            Position(
                max(p.x for p in positions) + 1,
                max(p.y for p in positions) + 1
            )
        )

    def width(self) -> int:
        return self.bottomright.x - self.topleft.x

    def height(self) -> int:
        return self.bottomright.y - self.topleft.y

    def __contains__(self, position: Position) -> bool:
        if not self.topleft.x <= position.x < self.bottomright.x:
            return False
        if not self.topleft.y <= position.y < self.bottomright.y:
            return False
        return True

    def __iter__(self) -> Iterator[Position]:
        for x in range(self.topleft.x, self.bottomright.x):
            for y in range(self.topleft.y, self.bottomright.y):
                yield Position(x, y)

    def center(self) -> Position:
        return Position(
            (self.topleft.x + self.bottomright.x) // 2,
            (self.topleft.y + self.bottomright.y) // 2)

    def split(self, point: Position) -> List[Rect]:
        if point not in self:
            return [self]
        splitted = [
            Rect(self.topleft, Position(point.x, self.bottomright.y)),
            Rect(Position(point.x + 1, self.topleft.y), self.bottomright),
            Rect(self.topleft, Position(self.bottomright.x, point.y)),
            Rect(Position(self.topleft.x, point.y + 1), self.bottomright),
        ]
        return [rect for rect in splitted if rect.area() > 0]

    def area(self) -> int:
        return (self.bottomright.x - self.topleft.x) * (self.bottomright.y - self.topleft.y)


class Direction(Enum):
    North = ("N", Delta(0, -1))
    South = ("S", Delta(0, +1))
    East = ("E", Delta(+1, 0))
    West = ("W", Delta(-1, 0))

    def __init__(self, code: Text, delta: Delta):
        self.code = code
        self.delta = delta

    def move(self, position: Position) -> Position:
        return position + self.delta

    def revert(self, position: Position) -> Position:
        return position - self.delta

    def scaled_move(self, position: Position, scale: int) -> Position:
        return position + (self.delta * scale)

    def __repr__(self) -> Text:
        return f"Direction.{self.name}"

    @classmethod
    def parse(cls, value: Text) -> Direction:
        for direction in cls:
            if value == direction.code:
                return direction
        raise ValueError("unknown direction " + repr(value))


@dataclass(frozen=True)
class Targets:
    safe: FrozenSet[Position]
    danger: FrozenSet[Position]

    def all(self):
        return self.safe | self.danger


class WorldMap:
    def __init__(self, width: int, height: int):
        self.area: Rect = Rect.to(Position(width, height))
        self.islands: Set[Position] = set()
        self.water: Set[Position] = set(self.area)

    def walk(self) -> Iterator[Position]:
        return iter(self.area)

    def set_island(self, position: Position) -> None:
        self.islands.add(position)
        self.water.remove(position)

    def to_sector(self, position: Position) -> int:
        return (position.x // 5 + (position.y // 5) * (self.area.width() // 5)) + 1

    def is_water(self, position: Position) -> bool:
        return position in self.water

    @memoize_method
    def all_water(self) -> Set[Position]:
        return self.water

    @memoize_method
    def neighbors(self, position: Position) -> List[Position]:
        return [p for p in [d.move(position) for d in Direction] if self.is_water(p)]

    @memoize_method
    def find_biggest_water_area(self) -> Rect:
        result = [self.area]
        for island in self.islands:
            result = list(chain.from_iterable(r.split(island) for r in result))
        return sorted(result, key=lambda r: r.area(), reverse=True)[0]

    @memoize_method
    def distance(self, start: Position, goal: Position) -> int:
        found, _, distance = astar_search(start, goal, self.neighbors, Position.distance)
        if found:
            return distance
        return -1

    @memoize_method
    def possible_targets(self, position: Position) -> Targets:
        def is_in_torpedo_radius(torpedo: Position, position: Position) -> bool:
            delta = Position.delta(torpedo, position)
            return -1 <= delta.dx <= 1 and -1 <= delta.dy <= 1

        costs = search_distance(position, 4, self.neighbors)
        safe = set()
        danger = set()
        for target, distance in costs.items():
            if distance == 1:
                danger.add(target)
            elif distance == 2 and is_in_torpedo_radius(target, position):
                danger.add(target)
            elif distance != 0:
                safe.add(target)
        return Targets(frozenset(safe), frozenset(danger))

    @memoize_method
    def explosion_range(self, position: Position) -> Set[Position]:
        result = set()
        # pylint: disable=invalid-name
        for dx in (-1, 0, 1):
            for dy in (-1, 0, 1):
                result.add(position + Delta(dx, dy))
        return result & self.all_water()

    def render(self, symbols: Callable[[Position], Optional[Text]] = None) -> Text:
        def display(position: Position) -> Text:
            if symbols:
                return (symbols(position) or "•") + " "
            return "• "

        topline = "┏" + "┳".join(["━━━━━━━━━━━"] * (self.area.width() // 5)) + "┓\n"
        middleline = "┣" + "╋".join(["━━━━━━━━━━━"] * (self.area.width() // 5)) + "┫\n"
        bottomline = "┗" + "┻".join(["━━━━━━━━━━━"] * (self.area.width() // 5)) + "┛\n"

        result = topline
        for y in range(self.area.height()):
            if y % 5 == 0 and y != 0:
                result += middleline
            for x in range(self.area.width()):
                position = Position(x, y)
                if x % 5 == 0:
                    result += "┃ "
                if not self.is_water(position):
                    result += "▲ "
                else:
                    result += display(position)
            result += "┃\n"
        return result + bottomline


class Navigation:
    def __init__(self, worldmap):
        self.worldmap = worldmap
        self.avoid = set()
        deadends = set(p for p in self.all_navigable() if len(self.neighbors(p)) <= 1)
        while deadends:
            self.avoid.update(deadends)
            deadends = set(p for p in self.all_navigable() if len(self.neighbors(p)) <= 1)

    def all_navigable(self):
        return self.worldmap.all_water() - self.avoid

    def is_navigable(self, position):
        if position in self.avoid:
            return False
        return self.worldmap.is_water(position)

    def neighbors(self, position):
        return [p for p in self.worldmap.neighbors(position) if p not in self.avoid]

    @memoize_method
    def path(self, trace: Trace, goal: Position) -> List[Position]:
        def neighbors(position):
            return [p for p in self.neighbors(position) if p not in trace]

        found, path, _ = astar_search(trace.last(), goal, neighbors, Position.distance)
        if found:
            return path
        return []

    def render(self):
        def display(position: Position) -> Optional[Text]:
            if position in self.avoid:
                return "x"
            return None

        return self.worldmap.render(display)


class SonarResult(Enum):
    HIT = "Y"
    MISS = "N"
    NOT_APPLICABLE = "NA"

    @classmethod
    def parse(cls, value: Text) -> SonarResult:
        for result in cls:
            if value == result.value:
                return result
        return cls.NOT_APPLICABLE

    def __repr__(self) -> Text:
        return f"SonarResult.{self.name}"


class TrackVisitor(ABC):
    @abstractmethod
    def visit_move(self, direction: Direction) -> None:
        pass

    @abstractmethod
    def visit_silence(self) -> None:
        pass

    @abstractmethod
    def visit_surface(self, sector: int) -> None:
        pass

    @abstractmethod
    def visit_sonar(self, sector: int, result: SonarResult) -> None:
        pass

    @abstractmethod
    def visit_torpedo(self, target: Position) -> None:
        pass

    @abstractmethod
    def visit_mine_droped(self) -> None:
        pass

    @abstractmethod
    def visit_mine_trigged(self, position: Position) -> None:
        pass

    @abstractmethod
    def visit_not_in(self, targets: FrozenSet[Position]) -> None:
        pass


@dataclass
class TrackingInformations:
    informations: List[TrackBase]

    @staticmethod
    def parse(orders: Text) -> TrackingInformations:
        def parse_order(order: Text) -> TrackBase:
            # pylint: disable=too-many-return-statements
            if order.startswith("SURFACE"):
                return TrackSurface(int(order[8:]))
            if order.startswith("MOVE"):
                return TrackMove(Direction.parse(order[5:]))
            if order.startswith("SILENCE"):
                return TrackSilence()
            if order.startswith("TORPEDO"):
                return TrackTorpedo(Position.parse(order[7:]))
            if order.startswith("MINE"):
                return TrackMineDroped()
            if order.startswith("TRIGGER"):
                return TrackMineTrigged(Position.parse(order[7:]))
            if order.startswith("SONAR"):
                return TrackSonar(int(order[6:]), SonarResult.NOT_APPLICABLE)
            return TrackNothing()

        return TrackingInformations([parse_order(o.strip()) for o in orders.split("|")])

    def __iter__(self) -> Iterator[TrackBase]:
        return iter(self.informations)

    def add(self, information: TrackBase) -> None:
        self.informations.append(information)

    def explosion_positions(self) -> Set[Position]:
        positions = set()
        for i in self.informations:
            if isinstance(i, TrackTorpedo):
                positions.add(i.target)
            if isinstance(i, TrackMineTrigged):
                positions.add(i.target)
        return positions

    def surface(self) -> Optional[TrackSurface]:
        for i in self.informations:
            if isinstance(i, TrackSurface):
                return i
        return None

    def pop_sonar(self) -> Optional[TrackSonar]:
        sonars = [i for i in self.informations if isinstance(i, TrackSonar)]
        if sonars:
            self.informations.remove(sonars[0])
            return sonars[0]
        return None

    def __repr__(self):
        return f"track({repr(self.informations)})"


class TrackBase(ABC):
    # pylint: disable=too-few-public-methods

    @abstractmethod
    def accept(self, visitor: TrackVisitor) -> None:
        pass


@dataclass(frozen=True)
class TrackNothing(TrackBase):
    def accept(self, visitor: TrackVisitor) -> None:
        pass


@dataclass(frozen=True)
class TrackMove(TrackBase):
    direction: Direction

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_move(self.direction)

    def __repr__(self):
        return f"TrackMove({self.direction})"


@dataclass(frozen=True)
class TrackSilence(TrackBase):
    # pylint: disable=no-self-use

    def accept(self, visitor: TrackVisitor):
        visitor.visit_silence()

    def __repr__(self):
        return "TrackSilence()"


@dataclass(frozen=True)
class TrackSurface(TrackBase):
    sector: int

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_surface(self.sector)

    def __repr__(self):
        return f"TrackSurface({self.sector})"


@dataclass(frozen=True)
class TrackSonar(TrackBase):
    sector: int
    result: SonarResult

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_sonar(self.sector, self.result)

    def __repr__(self):
        return f"TrackSonar({self.sector}, {self.result})"


@dataclass(frozen=True)
class TrackTorpedo(TrackBase):
    target: Position

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_torpedo(self.target)

    def __repr__(self):
        return f"TrackTorpedo({self.target})"


@dataclass(frozen=True)
class TrackMineDroped(TrackBase):
    # pylint: disable=no-self-use

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_mine_droped()

    def __repr__(self):
        return "TrackMineDroped()"


@dataclass(frozen=True)
class TrackMineTrigged(TrackBase):
    target: Position

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_mine_trigged(self.target)

    def __repr__(self):
        return f"TrackMineTrigged({self.target})"


@dataclass(frozen=True)
class TrackNotIn(TrackBase):
    targets: FrozenSet[Position]

    def accept(self, visitor: TrackVisitor) -> None:
        visitor.visit_not_in(self.targets)


@dataclass(frozen=True)
class Trace:
    current: Position
    positions: Tuple[Position, ...]

    @staticmethod
    def on(initial: Position) -> Trace:
        return Trace(initial, (initial,))

    def __contains__(self, position: Position) -> bool:
        return position in self.positions

    def last(self) -> Position:
        return self.current

    def move(self, direction: Direction) -> Trace:
        return self.add(direction.move(self.current))

    def add(self, position: Position) -> Trace:
        return Trace(position, (*self.positions, position))

    def clear(self) -> Trace:
        return Trace.on(self.current)

    def clamp(self, size: int) -> Trace:
        return Trace(self.current, self.positions[-size:])

    def __str__(self) -> Text:
        return "Trace(" + ", ".join(str(p) for p in self.positions) + ")"

    def __repr__(self) -> Text:
        return "Trace(" + ", ".join(str(p) for p in self.positions) + ")"


@dataclass(frozen=True)
class PositionEstimation:
    position: Position
    estimation: float

    def __str__(self):
        return f"{self.position}, {self.estimation * 100:.2f}%"


class TrackingByTrace(TrackVisitor):
    worldmap: WorldMap
    traces: Set[Trace]

    def __init__(self, worldmap: WorldMap):
        self.worldmap = worldmap
        self.__set_traces(Trace.on(p) for p in worldmap.all_water())

    def statistics(self):
        return dedent(f"""
            - solutions: {self.count()}
            - traces: {len(self.traces)}
            - area: {self.area().area()}
            - best estimation: {str(self.best_solution())}
            """)

    def track(self, informations: TrackingInformations) -> None:
        for event in informations:
            event.accept(self)

    def area(self):
        return Rect.containing(list(self.solutions()))

    def best_target(self, positions: Set[Position]) -> Optional[PositionEstimation]:
        if not self.solutions() & positions:
            return None
        estimations = [p for p in self.estimate_positions() if p.position in positions]
        best = max(p.estimation for p in estimations)
        for estimation in estimations:
            if estimation.estimation == best:
                return estimation
        return None

    def best_solution(self) -> Optional[PositionEstimation]:
        counters = Counter(trace.last() for trace in self.traces)
        solutions = counters.most_common(1)
        if solutions:
            total = sum(counters.values())
            (position, count) = solutions[0]
            return PositionEstimation(position, count / total)
        return None

    def estimate_positions(self) -> List[PositionEstimation]:
        counters = Counter(trace.last() for trace in self.traces)
        total = sum(counters.values())
        return [PositionEstimation(position, count / total) for position, count in counters.items()]

    def solutions(self) -> Set[Position]:
        return set(trace.last() for trace in self.traces)

    def count(self) -> int:
        return len(self.solutions())

    def render(self) -> Text:
        solutions = self.solutions()

        def display(position: Position) -> Optional[Text]:
            if position in solutions:
                return "◎"
            return None
        return self.worldmap.render(display)

    def visit_move(self, direction: Direction) -> None:
        new_traces = []
        for trace in self.traces:
            new_position = direction.move(trace.last())
            if new_position not in trace and self.worldmap.is_water(new_position):
                new_traces.append(trace.move(direction))
        self.__set_traces(iter(new_traces))

    def visit_silence(self) -> None:
        def expand(direction: Direction, trace: Trace) -> Iterator[Trace]:
            for _ in range(4):
                new_position = direction.move(trace.last())
                if new_position not in trace and self.worldmap.is_water(new_position):
                    trace = trace.add(new_position).clamp(4)
                    yield trace
                else:
                    break

        new_traces = []
        for trace in self.traces:
            new_traces.append(trace)
            new_traces.extend(expand(Direction.North, trace))
            new_traces.extend(expand(Direction.South, trace))
            new_traces.extend(expand(Direction.East, trace))
            new_traces.extend(expand(Direction.West, trace))
        self.__set_traces(iter(new_traces))

    def visit_surface(self, sector: int) -> None:
        self.__set_traces(
            Trace.on(position)
            for position in self.solutions()
            if self.worldmap.to_sector(position) == sector)

    def visit_torpedo(self, target: Position) -> None:
        result = self.worldmap.possible_targets(target)
        targets = set([target]) | result.safe | result.danger
        self.__set_traces(
            trace
            for trace in self.traces
            if trace.last() in targets)

    def visit_sonar(self, sector: int, result: SonarResult) -> None:
        if result == SonarResult.HIT:
            self.__set_traces(
                trace
                for trace in self.traces
                if self.__is_in_sector(sector, trace))
        elif result == SonarResult.MISS:
            self.__set_traces(
                trace
                for trace in self.traces
                if not self.__is_in_sector(sector, trace))
        else:
            pass

    def visit_mine_droped(self):
        pass

    def visit_mine_trigged(self, position: Position):
        pass

    def visit_not_in(self, targets: FrozenSet[Position]):
        pass

    def __is_in_sector(self, sector: int, trace: Trace) -> bool:
        return self.worldmap.to_sector(trace.last()) == sector

    def __set_traces(self, traces: Iterator[Trace]) -> None:
        self.traces = set(traces)
        if not self.traces:
            self.traces = set(Trace.on(p) for p in self.worldmap.all_water())


@dataclass(frozen=True)
class SubSystem:
    torpedo: int
    sonar: int
    silence: int
    mine: int

    def available(self) -> List[Powers]:
        result = []
        if self.torpedo >= 0:
            result.append(Powers.Torpedo)
        if self.sonar >= 0:
            result.append(Powers.Sonar)
        if self.silence >= 0:
            result.append(Powers.Silence)
        if self.mine >= 0:
            result.append(Powers.Mine)
        return result

    def is_torpedo_ready(self) -> bool:
        return self.torpedo == 0

    def is_sonar_ready(self) -> bool:
        return self.sonar == 0

    def is_silence_ready(self) -> bool:
        return self.silence == 0


class OpponentSubmarine:
    # pylint: disable=too-few-public-methods

    def __init__(self):
        self.lost = 0
        self.life = 6

    def update_life(self, life: int) -> OpponentSubmarine:
        self.lost = self.life - life
        self.life = life
        return self


class Submarine:
    def __init__(self, initial: Position):
        self.trace: Trace = Trace.on(initial)
        self.subsystem = SubSystem(-1, -1, -1, -1)
        self.life = 6
        self.orders: Orders = Orders.empty()

    def position(self) -> Position:
        return self.trace.last()

    def update_position(self, position: Position) -> Submarine:
        self.trace = self.trace.add(position)
        return self

    def surface(self) -> None:
        self.trace = self.trace.clear()

    def can_move(self, position: Position) -> bool:
        return position not in self.trace

    def update_subsystem(self, subsystem: SubSystem) -> Submarine:
        self.subsystem = subsystem
        return self

    def update_life(self, life: int) -> Submarine:
        self.life = life
        return self

    def update_orders(self, orders: Orders) -> Submarine:
        self.orders = orders
        return self

    def possible_subsystem(self) -> List[Powers]:
        return self.subsystem.available()

    def is_torpedo_ready(self) -> bool:
        return self.subsystem.is_torpedo_ready()

    def is_sonar_ready(self) -> bool:
        return self.subsystem.is_sonar_ready()

    def is_silence_ready(self) -> bool:
        return self.subsystem.is_silence_ready()


class Powers(Enum):
    Torpedo = "TORPEDO"
    Sonar = "SONAR"
    Silence = "SILENCE"
    Mine = "MINE"

    def render(self) -> Text:
        return self.value

    def __repr__(self) -> Text:
        return f"Powers.{self.name}"


@dataclass(frozen=True)
class Orders:
    commands: List[Command]

    @staticmethod
    def empty() -> Orders:
        return Orders([])

    def __iter__(self) -> Iterator[Command]:
        return iter(self.commands)

    def add(self, command: Command) -> Orders:
        return Orders(self.commands + [command])

    def sonar(self) -> Optional[Sonar]:
        for command in self.commands:
            if isinstance(command, Sonar):
                return command
        return None

    def explosion_positions(self) -> Set[Position]:
        positions = set()
        for command in self.commands:
            if isinstance(command, Torpedo):
                positions.add(command.target)
            if isinstance(command, TriggerMine):
                positions.add(command.target)
        return positions

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackingInformations:
        return TrackingInformations(
            [command.to_track(worldmap, position) for command in self.commands])

    def __repr__(self):
        return f"orders({self.commands})"


class Command(ABC):
    # pylint: disable=too-few-public-methods

    @abstractmethod
    def render(self) -> Text:
        pass

    @abstractmethod
    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        pass


@dataclass(frozen=True)
class Surface(Command):
    def render(self) -> Text:
        # pylint: disable=no-self-use
        return "SURFACE"

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackSurface(worldmap.to_sector(position))

    def __repr__(self):
        return "Surface()"


@dataclass(frozen=True)
class Move(Command):
    direction: Direction
    power: Powers

    def render(self) -> Text:
        return "MOVE " + self.direction.code + " " + self.power.render()

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackMove(self.direction)

    def __repr__(self):
        return f"Move({self.direction}, {self.power})"


@dataclass(frozen=True)
class Silence(Command):
    direction: Direction
    distance: int

    def render(self) -> Text:
        return "SILENCE " + self.direction.code + " " + str(self.distance)

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackSilence()

    def __repr__(self):
        return f"Silence({self.direction}, {self.distance})"


@dataclass(frozen=True)
class Torpedo(Command):
    target: Position

    def render(self) -> Text:
        return "TORPEDO " + str(self.target.x) + " " + str(self.target.y)

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackTorpedo(self.target)

    def __repr__(self):
        return f"Torpedo({repr(self.target)})"


@dataclass(frozen=True)
class Sonar(Command):
    sector: int

    def render(self) -> Text:
        return "SONAR " + str(self.sector)

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackSonar(1, SonarResult.NOT_APPLICABLE)  # a corriger

    def __repr__(self):
        return f"Sonar({self.sector})"


@dataclass(frozen=True)
class DropMine(Command):
    direction: Direction

    def render(self) -> Text:
        return "MINE " + self.direction.code

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackMineDroped()

    def __repr__(self):
        return f"DropMine({self.direction})"


@dataclass(frozen=True)
class TriggerMine(Command):
    target: Position

    def render(self) -> Text:
        return "TRIGGER " + str(self.target.x) + " " + str(self.target.y)

    def to_track(self, worldmap: WorldMap, position: Position) -> TrackBase:
        return TrackMineTrigged(self.target)

    def __repr__(self):
        return f"TriggerMine({repr(self.target)})"


def select_starting_point(worldmap: WorldMap) -> Position:
    return worldmap.find_biggest_water_area().center()


@dataclass(frozen=True)
class TurnInformations:
    position: Position
    my_life: int
    opponent_life: int
    subsystem: SubSystem
    sonar: SonarResult
    track: TrackingInformations

    def __repr__(self):
        return dedent(f"""
        TurnInformations(
            position={repr(self.position)},
            my_life={repr(self.my_life)},
            opponent_life={repr(self.opponent_life)},
            subsystem={repr(self.subsystem)},
            sonar={repr(self.sonar)},
            track={repr(self.track)}
        )
            """)


class SituationAwarness:
    # pylint: disable=too-few-public-methods

    def __init__(self, worldmap: WorldMap):
        self.worldmap = worldmap
        self.opponent_tracking = TrackingByTrace(self.worldmap)
        self.my_tracking = TrackingByTrace(self.worldmap)
        self.my_life = 6
        self.opponent_life = 6

    def update_tracking(self, orders: Orders, turn: TurnInformations):
        opponent_track = turn.track
        opponent_sonar = opponent_track.pop_sonar()

        my_sonar = orders.sonar()
        if my_sonar:
            opponent_track.add(TrackSonar(my_sonar.sector, turn.sonar))

        delta_life = self.opponent_life - turn.opponent_life
        if delta_life == 0:
            explosions = opponent_track.explosion_positions() | orders.explosion_positions()
            for explosion in explosions:
                opponent_track.add(self.__not_in_explosion_range(explosion))

        if delta_life == 1:
            if not opponent_track.surface():
                explosions = opponent_track.explosion_positions() | orders.explosion_positions()
                for explosion in explosions:
                    opponent_track.add(TrackNotIn(frozenset([explosion])))

        if delta_life == 2:
            if opponent_track.surface():
                explosions = opponent_track.explosion_positions() | orders.explosion_positions()
                for explosion in explosions:
                    opponent_track.add(TrackNotIn(frozenset([explosion])))

        self.opponent_tracking.track(opponent_track)

        my_track = orders.to_track(self.worldmap, turn.position)
        if opponent_sonar:
            my_track.add(TrackSonar(
                opponent_sonar.sector,
                self.__sonar_result_for(opponent_sonar.sector, turn.position)))
        self.my_tracking.track(my_track)

        self.my_life = turn.my_life
        self.opponent_life = turn.opponent_life

    def position_count(self):
        return self.opponent_tracking.count()

    def best_opponent_position(self):
        best_solution = self.opponent_tracking.best_solution()
        if best_solution and best_solution.estimation > 0.4:
            return best_solution.position
        return None

    def select_torpedo_target(self, my_position: Position) -> Optional[Position]:
        ranged_targets = self.worldmap.possible_targets(my_position)
        best_solution = self.opponent_tracking.best_target(ranged_targets.safe)
        debug("best target", best_solution)
        if best_solution and best_solution.estimation > 0.4:
            return best_solution.position
        return None

    def select_sonar_sector(self) -> Optional[int]:
        sectors: Dict[int, int] = {}
        for trace in self.opponent_tracking.traces:
            sector = self.worldmap.to_sector(trace.last())
            if sector in sectors:
                sectors[sector] += 1
            else:
                sectors[sector] = 1
        upper = max(sectors.values())
        for sector, count in sectors.items():
            if count == upper:
                return sector
        return None

    def statistics(self):
        return "opponent: " + self.opponent_tracking.statistics() + "\nme: " + self.my_tracking.statistics()

    def __sonar_result_for(self, sector: int, position: Position) -> SonarResult:
        if self.worldmap.to_sector(position) == sector:
            return SonarResult.HIT
        return SonarResult.MISS

    def __not_in_explosion_range(self, target):
        return TrackNotIn(frozenset(self.worldmap.explosion_range(target)))


class Game:
    def __init__(self, interaction: Interaction):
        self.interaction = interaction
        self.worldmap: WorldMap = interaction.parse_map()
        self.navigation = Navigation(self.worldmap)
        self.my_ship: Submarine = Submarine(select_starting_point(self.worldmap))
        self.opponent = OpponentSubmarine()
        self.situation = SituationAwarness(self.worldmap)
        debug("# Initial state")
        debug(self.navigation.render())

    def choose_starting_position(self) -> None:
        self.interaction.send_starting_position(self.my_ship.position())

    def turn(self) -> None:
        debug("## Waiting")
        turn = self.interaction.parse_turn()
        debug("turn:", repr(turn))
        perf = Performance()

        debug("## Update status")
        self.my_ship.update_position(turn.position).update_life(turn.my_life).update_subsystem(turn.subsystem)
        self.opponent.update_life(turn.opponent_life)
        debug("duration:", perf.elapsed())

        debug("## Spying")
        self.situation.update_tracking(self.my_ship.orders, turn)
        debug(self.situation.statistics())
        debug("duration:", perf.elapsed())

        debug("## Thinking")
        orders = self.think(turn)
        debug("orders:", repr(orders))
        self.my_ship.update_orders(orders)
        self.interaction.send_orders(self.my_ship.orders)
        debug("duration:", perf.elapsed())
        debug("")

    def think(self, turn) -> Orders:
        best_position = self.situation.best_opponent_position()
        if best_position:
            debug("ATTACKING", best_position)
            return self.attacking(turn, best_position)
        else:
            debug("HUNTING")
            return self.hunting(turn)

    def attacking(self, turn, position):
        def goto(target):
            path = self.navigation.path(self.my_ship.trace, target)
            if len(path) > 3:
                next_step = path[1]
                for direction in Direction:
                    if direction.move(self.my_ship.position()) == next_step:
                        return direction
            return None

        def move_command():
            move = goto(position)
            if not move:
                move = self.possible_moves()
            if not move:
                self.my_ship.surface()
                return Surface()
            return Move(move, Powers.Torpedo)

        commands = Orders.empty()
        if self.my_ship.is_torpedo_ready():
            targets = self.worldmap.possible_targets(self.my_ship.position())
            if position in targets.safe:
                commands = commands.add(Torpedo(position))
            elif position in targets.danger and turn.my_life > 3:
                commands = commands.add(Torpedo(position))
        return commands.add(move_command())

    def hunting(self, turn):
        def move_command():
            move = self.possible_moves()
            if not move:
                self.my_ship.surface()
                return Surface()
            return Move(move, power())

        def power():
            if Powers.Sonar in self.my_ship.possible_subsystem():
                if self.situation.position_count() > 10 and not self.my_ship.is_sonar_ready():
                    return Powers.Sonar
            if not self.my_ship.is_torpedo_ready():
                return Powers.Torpedo
            return Powers.Silence

        commands = Orders.empty()
        if self.my_ship.is_sonar_ready():
            sector = self.situation.select_sonar_sector()
            if sector:
                commands = commands.add(Sonar(sector))
        if self.my_ship.is_silence_ready():
            commands = commands.add(Silence(Direction.North, 0))
        else:
            commands = commands.add(move_command())
        return commands

    def possible_moves(self) -> Optional[Direction]:
        def is_valid(position):
            return self.my_ship.can_move(position) and self.navigation.is_navigable(position)
        moves = []
        for direction in Direction:
            next_position = direction.move(self.my_ship.position())
            if is_valid(next_position):
                close_index = len([p for p in self.navigation.neighbors(next_position) if not self.my_ship.can_move(p)])
                moves.append((close_index, direction))
        if moves:
            best = max(move[0] for move in moves)
            return random.choice([move[1] for move in moves if move[0] == best])
        return None


class Interaction:
    # pylint: disable=no-self-use

    def __init__(self):
        pass

    def parse_map(self):
        # pylint: disable=unused-variable
        width, height, my_id = self.__parse_integer()
        data = [input() for i in range(height)]
        worldmap = WorldMap(width, height)
        for position in worldmap.walk():
            if data[position.y][position.x] == "x":
                worldmap.set_island(position)
        return worldmap

    def send_starting_position(self, position):
        print(position.x, position.y)

    def send_orders(self, orders: Orders):
        print("|".join([c.render() for c in orders]))

    def parse_turn(self) -> TurnInformations:
        (
            x,
            y,
            my_life,
            opp_life,
            torpedo_cooldown,
            sonar_cooldown,
            silence_cooldown,
            mine_cooldown) = self.__parse_integer()
        sonar = SonarResult.parse(input())
        track = TrackingInformations.parse(input())
        return TurnInformations(
            Position(x, y),
            my_life,
            opp_life,
            SubSystem(torpedo_cooldown, sonar_cooldown, silence_cooldown, mine_cooldown),
            sonar,
            track)

    def __parse_integer(self):
        return [int(i) for i in input().split()]


if __name__ == "__main__":
    GAME = Game(Interaction())
    GAME.choose_starting_position()
    while True:
        GAME.turn()
