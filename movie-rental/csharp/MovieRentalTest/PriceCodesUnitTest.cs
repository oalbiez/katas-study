﻿using NUnit.Framework;
using static MovieRental.Duration;
using static MovieRental.Price;
using static MovieRental.PriceCodes;
using static NUnit.Framework.Assert;

namespace MovieRentalTest
{
    [TestFixture]
    public class PriceCodesUnitTest
    {
        [Test]
        public void children_should_describe_pricing()
        {
            AreEqual("cost 1,5€ for 3 day(s) then cost 1,5€ per day", Children().Describe());
        }

        [Test]
        public void children_should_charge()
        {
            AreEqual(Euro(1.5), Children().Chargefor(Days(1)));
            AreEqual(Euro(1.5), Children().Chargefor(Days(2)));
            AreEqual(Euro(1.5), Children().Chargefor(Days(3)));
            AreEqual(Euro(3), Children().Chargefor(Days(4)));
            AreEqual(Euro(12), Children().Chargefor(Days(10)));
        }

        [Test]
        public void children_should_frequent_renter_point()
        {
            AreEqual(1, Children().FrequentRenterPointsFor(Days(1)));
            AreEqual(1, Children().FrequentRenterPointsFor(Days(2)));
            AreEqual(1, Children().FrequentRenterPointsFor(Days(3)));
            AreEqual(1, Children().FrequentRenterPointsFor(Days(4)));
            AreEqual(1, Children().FrequentRenterPointsFor(Days(10)));
        }

        [Test]
        public void regular_should_describe_pricing()
        {
            AreEqual("cost 2€ for 2 day(s) then cost 1,5€ per day", Regular().Describe());
        }

        [Test]
        public void regular_should_charge()
        {
            AreEqual(Euro(2), Regular().Chargefor(Days(1)));
            AreEqual(Euro(2), Regular().Chargefor(Days(2)));
            AreEqual(Euro(3.5), Regular().Chargefor(Days(3)));
            AreEqual(Euro(5), Regular().Chargefor(Days(4)));
            AreEqual(Euro(14), Regular().Chargefor(Days(10)));
        }

        [Test]
        public void regular_should_frequent_renter_point()
        {
            AreEqual(1, Regular().FrequentRenterPointsFor(Days(1)));
            AreEqual(1, Regular().FrequentRenterPointsFor(Days(2)));
            AreEqual(1, Regular().FrequentRenterPointsFor(Days(3)));
            AreEqual(1, Regular().FrequentRenterPointsFor(Days(4)));
            AreEqual(1, Regular().FrequentRenterPointsFor(Days(10)));
        }

        [Test]
        public void new_release_should_describe_pricing()
        {
            AreEqual("cost 3€ per day", NewRelease().Describe());
        }

        [Test]
        public void new_release_should_charge()
        {
            AreEqual(Euro(3), NewRelease().Chargefor(Days(1)));
            AreEqual(Euro(6), NewRelease().Chargefor(Days(2)));
            AreEqual(Euro(9), NewRelease().Chargefor(Days(3)));
            AreEqual(Euro(12), NewRelease().Chargefor(Days(4)));
            AreEqual(Euro(30), NewRelease().Chargefor(Days(10)));
        }

        [Test]
        public void new_release_should_frequent_renter_point()
        {
            AreEqual(1, NewRelease().FrequentRenterPointsFor(Days(1)));
            AreEqual(2, NewRelease().FrequentRenterPointsFor(Days(2)));
            AreEqual(2, NewRelease().FrequentRenterPointsFor(Days(3)));
            AreEqual(2, NewRelease().FrequentRenterPointsFor(Days(4)));
            AreEqual(2, NewRelease().FrequentRenterPointsFor(Days(10)));
        }
    }
}