package katas.bottle;

import static com.google.common.collect.Iterables.limit;
import static katas.bottle.Bottle.bottle;
import static katas.bottle.Solution.solution;
import static katas.bottle.Solver.solver;

public class Main {
    public static void main(String[] args) {
        Solver solver = solver(
                solution(bottle(3, 3), bottle(5, 5), bottle(8, 0)),
                bottle -> bottle.quantity() == 4);
        for (Solution solution : limit(solver.findAllSolutions(), 10)) {
            System.out.println(solution.history());
        }
    }
}
