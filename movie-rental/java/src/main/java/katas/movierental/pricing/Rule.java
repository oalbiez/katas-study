package katas.movierental.pricing;

import katas.movierental.Duration;

public interface Rule {
    Rule perDay();

    Rule for_(Duration duration);

    Value priceFor(Duration duration, ValueDriver driver);

    Duration consume(Duration remaining);

    String describe();
}
