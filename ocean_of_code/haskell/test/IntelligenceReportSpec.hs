{-# LANGUAGE OverloadedStrings #-}
module IntelligenceReportSpec (spec) where


import           Data.Function ((&))
import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec = do


    it "report should be able to render" $
        let
            render items
                = items
                & reportWith
                & renderReport
        in do
            render [ReportNothing] `shouldBe` "NA"

            render [ReportMove North] `shouldBe` "MOVE N"
            render [ReportMove East] `shouldBe` "MOVE E"
            render [ReportMove South] `shouldBe` "MOVE S"
            render [ReportMove West] `shouldBe` "MOVE W"

            render [ReportSilence] `shouldBe` "SILENCE"
            render [ReportTorpedo (point 4 7)] `shouldBe` "TORPEDO 4 7"
            render [ReportSonar (Sector 4)] `shouldBe` "SONAR 4"
            render [ReportSurface (Sector 5)] `shouldBe` "SURFACE 5"
            render [ReportMineDropped] `shouldBe` "MINE"
            render [ReportMineTrigged (point 3 8)] `shouldBe` "TRIGGER 3 8"
            render [ReportSonar (Sector 4), ReportSurface (Sector 5)]
                `shouldBe` "SONAR 4 | SURFACE 5"


    it "report should be parsed" $ do
        parseReport "MOVE W | SURFACE 4" `shouldBe` reportWith [
            ReportMove West, ReportSurface (Sector 4)]

        parseReport "SILENCE | MINE" `shouldBe` reportWith [
            ReportSilence, ReportMineDropped]

        parseReport "SILENCE | unknown" `shouldBe` emptyReport


    it "report render/parser should be opposite operation" $
        let text = "NA | MOVE W | SURFACE 3 | SILENCE | TORPEDO 3 10 | MINE | TRIGGER 3 10 | SONAR 2"
        in (text & parseReport & renderReport) `shouldBe` text


    it "explosionsInReport should find all explosions in report" $
        let
            check_explosion_positions items expected
                = explosionsInReport (reportWith items) `shouldMatchList` expected
        in do

            check_explosion_positions
                [ReportSurface (Sector 4), ReportTorpedo (point 5 12), ReportMineTrigged (point 3 8)]
                [point 5 12, point 3 8]

            check_explosion_positions
                [ReportSonar (Sector 4), ReportSilence]
                []


    it "sonarInReport should find sonar in report" $
        let
            check_sonar items expected
                = sonarInReport (reportWith items) `shouldBe` expected
        in do

            check_sonar
                [ReportSurface (Sector 4), ReportTorpedo (point 5 12), ReportMineTrigged (point 3 8)]
                Nothing

            check_sonar
                [ReportSonar (Sector 4), ReportSilence]
                (Just (Sector 4))


    it "surfaceInReport should find surface in report" $
        let
            check_surface items expected
                = surfaceInReport (reportWith items) `shouldBe` expected
        in do

            check_surface
                [ReportSurface (Sector 4), ReportTorpedo (point 5 12), ReportMineTrigged (point 3 8)]
                True

            check_surface
                [ReportSonar (Sector 4), ReportSilence]
                False
