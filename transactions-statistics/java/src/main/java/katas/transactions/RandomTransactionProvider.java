package katas.transactions;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

import static java.util.stream.Collectors.toList;
import static katas.transactions.Transaction.transaction;

final class RandomTransactionProvider implements TransactionProvider {
    private final static String[] categories = {"A", "B", "C", "D", "E", "F"};
    private final static String[] clients = {"SG", "SG", "SG", "SG", "BNP", "BNP", "BNP", "LBP", "CA", "CM", "CE", "CO"};
    private final LocalDate today;

    RandomTransactionProvider(final LocalDate today) {
        this.today = today;
    }

    private Transaction generate(final int seed, final long days) {
        return transaction(clients[seed % clients.length], categories[seed % categories.length], (seed % 200) - 100, today.minusDays(seed % days).atStartOfDay());
    }

    public Iterable<Transaction> transactionsSince(final LocalDate day) {
        final Duration duration = Duration.between(day.atStartOfDay(), today.atStartOfDay());
        return new Random().ints(200000, 100, 1000000).mapToObj((int seed) -> generate(seed, duration.toDays())).collect(toList());
    }

    public static void main(String[] args) {
        for (Transaction transaction : new RandomTransactionProvider(LocalDate.of(2018, 8, 6)).transactionsSince(LocalDate.of(2018, 7, 6))) {
            System.out.println(render(transaction));
        }
    }

    private static String render(Transaction transaction) {
        LocalDateTime instant = transaction.creation();
        return "transaction(\""+transaction.client() + "\", \"" + transaction.category() + "\", " + transaction.amount() + ", LocalDateTime.of("+ instant.getYear() +")";
    }


}
