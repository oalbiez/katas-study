{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TupleSections     #-}
module WorldMapSpec (spec) where


import           Control.Monad     (forM_)
import           Data.Function     ((&))
import qualified Data.HashSet      as HashSet
import qualified Data.Map          as Map
import           OceanOfCode
import           Test.Hspec
import           Text.RawString.QQ
import           WorldMapDSL


spec :: Spec
spec = do


    it "worldmap without island" $
        let (wm, _) = parse [r|
            ┏━━━━━━━━━━━┓
            ┃ • • • • • ┃
            ┃ • • • • • ┃
            ┃ • • • • • ┃
            ┃ • • • • • ┃
            ┃ • • • • • ┃
            ┗━━━━━━━━━━━┛|]
        in HashSet.size (water wm) `shouldBe` 25


    it "worldmap without water" $
        let (wm, _) = parse "\
            \┏━━━━━━━━━━━┓\n\
            \┃ ▲ ▲ ▲ ▲ ▲ ┃\n\
            \┃ ▲ ▲ ▲ ▲ ▲ ┃\n\
            \┃ ▲ ▲ ▲ ▲ ▲ ┃\n\
            \┃ ▲ ▲ ▲ ▲ ▲ ┃\n\
            \┃ ▲ ▲ ▲ ▲ ▲ ┃\n\
            \┗━━━━━━━━━━━┛"
        in water wm `shouldBe` HashSet.empty


    it "isWater should be true for all point which are not islands" $
        let (wm, _) = parse "\
            \┏━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃\n\
            \┃ • • • ▲ • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ ▲ • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┛"
        in do
            isWater wm (point 0 0) `shouldBe` True
            isWater wm (point 3 1) `shouldBe` False
            isWater wm (point 0 3) `shouldBe` False
            isWater wm (point 4 4) `shouldBe` True


    it "isWater should be false for point outside the map" $
        let (wm, _) = parse "\
            \┏━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┛"
        in do
            isWater wm (point (-1) 0) `shouldBe` False
            isWater wm (point 5 0) `shouldBe` False
            isWater wm (point 0 (-1)) `shouldBe` False
            isWater wm (point 0 5) `shouldBe` False


    it "toSector should returns the sector of the position" $
        let (wm, symbols) = parse "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ 1 • • • • ┃ 2 • • • • ┃ • ▲ ▲ • ▲ ┃\n\
            \┃ • • • • 1 ┃ • • • • • ┃ • • • ▲ ▲ ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃\n\
            \┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • 3 ▲ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ 4 • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • 4 ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ 7 • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ 9 • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • 7 ┃ • • • • • ┃ • • • • 9 ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛"
            flattenPoints sector = map (Sector (read [sector]),)
            points = Map.toList symbols
                    & concatMap (uncurry flattenPoints)
        in forM_ points
            (\(sector, p) -> toSector wm p `shouldBe` sector)


    it "neighborsOf should give neighbors water cells" $ do
        checkNeighbors "\
            \┏━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃\n\
            \┃ • • ◇ • • ┃\n\
            \┃ ▲ ▲ ◉ ◇ • ┃\n\
            \┃ ▲ ▲ ◇ • ▲ ┃\n\
            \┃ • • • • ▲ ┃\n\
            \┗━━━━━━━━━━━┛"

        checkNeighbors "\
            \┏━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃\n\
            \┃ • • • ◇ • ┃\n\
            \┃ ▲ ▲ ◇ ◉ ◇ ┃\n\
            \┃ ▲ ▲ • ◇ ▲ ┃\n\
            \┃ • • • • ▲ ┃\n\
            \┗━━━━━━━━━━━┛"

        checkNeighbors "\
            \┏━━━━━━━━━━━┓\n\
            \┃ • • • ◇ ◉ ┃\n\
            \┃ • • • • ◇ ┃\n\
            \┃ ▲ ▲ • • • ┃\n\
            \┃ ▲ ▲ • • ▲ ┃\n\
            \┃ • • • • ▲ ┃\n\
            \┗━━━━━━━━━━━┛"


    it "move should update a ShipTrail if possible" $
        let (wm, _) = parse "\
            \┏━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┛"
            trail = mkTrail (point 0 0)
        in do
            moveTrail wm trail (Path [North]) `shouldBe` Nothing
            moveTrail wm trail (Path [West]) `shouldBe` Nothing
            moveTrail wm trail (Path [South, South]) `shouldNotBe` Nothing


    it "targetsFrom" $ do
        checkTargets "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ S S S S S ┃ • • • • • ┃\n\
            \┃ S D D D S ┃ S • • • • ┃\n\
            \┃ ▲ ▲ x D S ┃ S S • • • ┃\n\
            \┃ ▲ ▲ D D ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ S S S S ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • S S S ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • S • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ ▲ ▲ ▲ • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"

        checkTargets "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ S S S S S ┃ • • • • • ┃\n\
            \┃ S D D D S ┃ S • • • • ┃\n\
            \┃ ▲ ▲ x D S ┃ S S • • • ┃\n\
            \┃ ▲ ▲ ▲ ▲ ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ ▲ ▲ ▲ • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"

        checkTargets "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • ▲ ▲ ▲ ▲ ┃ ▲ ▲ ▲ • ▲ ┃\n\
            \┃ • • • ▲ ▲ ┃ ▲ ▲ ▲ • • ┃\n\
            \┃ • • • ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • S ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • • ┃ • • • S S ┃\n\
            \┃ • • • • ▲ ┃ ▲ • S S S ┃\n\
            \┃ • • • • ▲ ┃ ▲ S S D D ┃\n\
            \┃ • • • • ▲ ┃ ▲ S S D x ┃\n\
            \┃ • • • • • ┃ • S S D D ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┛\n"


    it "worldmap rendering" $
        let (wm, _) = parse "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • • • • ┃ • ▲ ▲ • ▲ ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃\n\
            \┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛"
        in render none wm `shouldBe` "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • • • • • ┃ • • • • • ┃ • ▲ ▲ • ▲ ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃\n\
            \┃ ▲ ▲ • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃\n\
            \┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"


    it "biggestWaterZone" $
        let (wm, symbols) = parse [r|
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ ▲ • • • • ┃
            ┃ • • • • • ┃ ▲ • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ o o o o ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ o o o o ▲ ┃ ▲ ▲ • • • ┃
            ┃ o o o o • ┃ • • • • • ┃
            ┃ o o o o • ┃ ▲ ▲ ▲ • • ┃
            ┃ o o o o ▲ ┃ ▲ ▲ ▲ • • ┃
            ┃ o o o o ▲ ┃ ▲ ▲ ▲ ▲ • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛|]
            biggestZone = biggestWaterZone wm
            expectedZone = Map.findWithDefault [] 'o' symbols
        in
            walkRect biggestZone `shouldMatchList` expectedZone


checkTargets :: String -> IO ()
checkTargets definition =
    let (wm, symbols) = parse definition
        position = head (Map.findWithDefault [] 'x' symbols)
        expected_safe_targets = Map.findWithDefault [] 'S' symbols
        expected_unsafe_targets = position : Map.findWithDefault [] 'D' symbols
        targets = targetsAt wm position
    in do
        safePositions targets `shouldMatchList` expected_safe_targets
        unsafePositions targets `shouldMatchList` expected_unsafe_targets

checkNeighbors :: String -> IO ()
checkNeighbors definition =
    let (wm, symbols) = parse definition
        position = head (Map.findWithDefault [] '◉' symbols)
        expected_neighbors = Map.findWithDefault [] '◇' symbols
    in neighborsOf wm position `shouldMatchList` expected_neighbors

