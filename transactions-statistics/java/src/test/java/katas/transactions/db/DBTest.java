package katas.transactions.db;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.*;
import java.util.List;

public class DBTest {

    class Transaction {
        String client;
        long amount;

        public Transaction(String client, long amount) {
            this.client = client;
            this.amount = amount;
        }
    }


    interface TransactionRepository {
        List<Transaction> findAllForClient(String client);
        List<Transaction> all(String client);
    }

    /*
    class SqlTransactionRepository implements TransactionRepository {
        final Connection connection = DriverManager.getConnection("jdbc:hsqldb:mem:aname", "SA", "");
        final Statement statement = connection.createStatement();

        @Override
        public List<Transaction> findAllForClient(String client) {
            return null;
        }

        @Override
        public List<Transaction> all(String client) {
            final ResultSet q = connection.createStatement().executeQuery("SELECT client, amount FROM Transaction");
            q.next();
            Transaction t = new Transaction(q.getString("db_client"), q.getLong("db_amount"));
            return null;
        }
    }
*/

//    @Test
    public void testDB() throws SQLException {
        final Connection connection = DriverManager.getConnection("jdbc:hsqldb:mem:aname", "SA", "");
        final Statement statement = connection.createStatement();
        statement.execute("CREATE TABLE Transaction  (db_client varchar(255), db_amount int)");
        statement.execute("INSERT INTO Transaction (db_client, db_amount) VALUES ('toto', 42)");


        final ResultSet q = connection.createStatement().executeQuery("SELECT client, amount FROM Transaction");
        q.next();
        Transaction t = new Transaction(q.getString("db_client"), q.getLong("db_amount"));
        //Assert.assertEquals("toto", q.getString("client"));
    }
}
