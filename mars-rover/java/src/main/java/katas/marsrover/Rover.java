package katas.marsrover;

public class Rover {
    private final Position position;
    private final Orientation orientation;

    public static Rover rover(final Position position, final Orientation orientation) {
        return new Rover(position, orientation);
    }

    private Rover(final Position position, final Orientation orientation) {
        this.position = position;
        this.orientation = orientation;
    }

    public Rover execute(final Command command, final Map map) {
        Move move = map.move(position, command.distanceFor(orientation));
        return rover(move.finalPosition(), command.newOrientation(orientation));
    }

    public Position position() {
        return position;
    }

    public Orientation orientation() {
        return orientation;
    }
}
