package katas.transactions.pic;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import katas.transactions.Transaction;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TransactionAgregateTest {

    @Test
    public void compute_pic_should___1() {
        assertEquals("{amount=400, instant=08:00:00}",
                agregateFor(
                        transaction(400, LocalTime.of(8, 0, 0))
                ));
    }

    @Test
    public void compute_pic_should___2() {
        assertEquals("{amount=400, instant=08:00:00}{amount=400, instant=08:15:00}",
                agregateFor(
                        transaction(400, LocalTime.of(8, 0, 0)),
                        transaction(400, LocalTime.of(8, 15, 0))
                ));
    }

    @Test
    public void compute_pic_should___3() {
        assertEquals("{amount=400, instant=08:00:00}{amount=800, instant=08:15:00}",
                agregateFor(
                        transaction(400, LocalTime.of(8, 0, 0)),
                        transaction(400, LocalTime.of(8, 15, 0)),
                        transaction(400, LocalTime.of(8, 15, 0))
                ));
    }

    @Test
    public void compute_pic_should___4() {
        assertEquals("{amount=400, instant=08:00:00}{amount=800, instant=08:15:00}{amount=400, instant=08:35:00}",
                agregateFor(
                        transaction(400, LocalTime.of(8, 0, 0)),
                        transaction(400, LocalTime.of(8, 15, 0)),
                        transaction(400, LocalTime.of(8, 15, 0)),
                        transaction(400, LocalTime.of(8, 35, 0))
                ));
    }

    private String agregateFor(final Transaction... transactions) {
        final TransactionAgregate ta = new TransactionAgregate();
        for (final Transaction transaction : transactions) {
            ta.update(transaction);
        }
        return ta.agregates.stream().map(renderAgregate()).collect(Collectors.joining());
    }

    private Function<TransactionAgregate.Agregate, String> renderAgregate() {
        return agregate -> "{amount="+ agregate.montant + ", instant=" + agregate.instant.toLocalTime().format(DateTimeFormatter.ISO_TIME) +"}";
    }


    private static Transaction transaction(final int amount, final LocalTime time) {
        return Transaction.transaction("PAREL", "12", amount, LocalDate.of(2018, 7, 25).atTime(time));
    }


}
