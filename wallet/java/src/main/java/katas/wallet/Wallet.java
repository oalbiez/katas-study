package katas.wallet;

import java.math.BigDecimal;
import java.util.List;

import static katas.wallet.Price.price;

public final class Wallet {
    private final List<Price> prices;

    public static Wallet wallet(final List<Price> prices) {
        return new Wallet(prices);
    }

    private Wallet(List<Price> prices) {
        this.prices = prices;
    }

    public Price valuateIn(Currency currency, RateProvider rateProvider) {
        return prices.stream()
                .map(price -> price.changeTo(currency, rateProvider))
                .reduce(price(currency, 0), Price::add);
    }
}
