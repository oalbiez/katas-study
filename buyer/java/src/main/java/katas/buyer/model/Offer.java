package katas.buyer.model;

public final class Offer {
    private final Vendor vendor;
    private final Quantity quantity;
    private final Price price;
    private final Delay delay;

    public static Offer of(final Vendor vendor, final Quantity quantity, final Price price) {
        return new Offer(vendor, quantity, price, Delay.none());
    }

    public static Offer of(final Vendor vendor, final Quantity quantity, final Price price, final Delay delay) {
        return new Offer(vendor, quantity, price, delay);
    }

    private Offer(final Vendor vendor, final Quantity quantity, final Price price, final Delay delay) {
        this.vendor = vendor;
        this.quantity = quantity;
        this.price = price;
        this.delay = delay;
    }

    public Vendor vendor() {
        return vendor;
    }

    public Quantity quantity() {
        return quantity;
    }

    public Price price() {
        return price;
    }

    public Price evaluateFor(final Quantity quantity, final TimedPenalty timedPenalty) {
        return price.mul(this.quantity).add(timedPenalty.evaluate(this.delay)).div(quantity);
    }

    @Override
    public String toString() {
        return String.format("%s items for %s in %s days by %s", quantity, price, delay, vendor);
    }
}
