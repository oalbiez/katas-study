package katas.movierental;

import org.junit.Test;

import static katas.movierental.Duration.days;
import static katas.movierental.Price.euro;
import static katas.movierental.PriceCode.*;
import static org.junit.Assert.assertEquals;

public class PriceCodeTest {
    @Test
    public void regular_movie_should_charge_flat_when_less_than_3_days() {
        assertEquals(euro(2.0), REGULAR.chargeFor(days(1)));
        assertEquals(euro(2.0), REGULAR.chargeFor(days(2)));
    }

    @Test
    public void regular_movie_should_charge_proportional_when_more_than_2_days() {
        assertEquals(euro(3.5), REGULAR.chargeFor(days(3)));
        assertEquals(euro(5.0), REGULAR.chargeFor(days(4)));
    }

    @Test
    public void regular_movie_should_give_one_frequent_renter_point() {
        assertEquals(FrequentRenterPoint.of(1), REGULAR.frequentRenterPointFor(days(1)));
        assertEquals(FrequentRenterPoint.of(1), REGULAR.frequentRenterPointFor(days(2)));
        assertEquals(FrequentRenterPoint.of(1), REGULAR.frequentRenterPointFor(days(3)));
        assertEquals(FrequentRenterPoint.of(1), REGULAR.frequentRenterPointFor(days(10)));
    }

    @Test
    public void children_movie_should_charge_flat_when_less_than_4_days() {
        assertEquals(euro(1.5), CHILDREN.chargeFor(days(1)));
        assertEquals(euro(1.5), CHILDREN.chargeFor(days(2)));
        assertEquals(euro(1.5), CHILDREN.chargeFor(days(3)));
    }

    @Test
    public void children_movie_should_charge_proportional_when_more_than_3_days() {
        assertEquals(euro(3.0), CHILDREN.chargeFor(days(4)));
        assertEquals(euro(4.5), CHILDREN.chargeFor(days(5)));
    }

    @Test
    public void children_movie_should_give_one_frequent_renter_point() {
        assertEquals(FrequentRenterPoint.of(1), CHILDREN.frequentRenterPointFor(days(1)));
        assertEquals(FrequentRenterPoint.of(1), CHILDREN.frequentRenterPointFor(days(2)));
        assertEquals(FrequentRenterPoint.of(1), CHILDREN.frequentRenterPointFor(days(3)));
        assertEquals(FrequentRenterPoint.of(1), CHILDREN.frequentRenterPointFor(days(10)));
    }

    @Test
    public void new_release_movie_should_charge_proportional() {
        assertEquals(euro(3.0), NEW_RELEASE.chargeFor(days(1)));
        assertEquals(euro(6.0), NEW_RELEASE.chargeFor(days(2)));
        assertEquals(euro(9.0), NEW_RELEASE.chargeFor(days(3)));
    }

    @Test
    public void new_release_movie_should_give_one_frequent_renter_point_when_less_than_2_days() {
        assertEquals(FrequentRenterPoint.of(1), NEW_RELEASE.frequentRenterPointFor(days(1)));
    }

    @Test
    public void new_release_movie_should_give_two_frequent_renter_points_when_more_than_1_days() {
        assertEquals(FrequentRenterPoint.of(2), NEW_RELEASE.frequentRenterPointFor(days(2)));
        assertEquals(FrequentRenterPoint.of(2), NEW_RELEASE.frequentRenterPointFor(days(10)));
    }
}