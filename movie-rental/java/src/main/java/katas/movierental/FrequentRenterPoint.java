package katas.movierental;

import com.google.common.base.Objects;

public final class FrequentRenterPoint {
    private final long value;

    public static FrequentRenterPoint of(long value) {
        return new FrequentRenterPoint(value);
    }

    private FrequentRenterPoint(long value) {
        this.value = value;
    }

    public FrequentRenterPoint add(FrequentRenterPoint right) {
        return of(value + right.value);
    }

    public String render() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FrequentRenterPoint that = (FrequentRenterPoint) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }
}
