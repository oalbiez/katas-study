# -*- coding: utf-8 -*-

from ai import TrackingInformations, TrackingByTrace, Position, Direction
from ai import TrackMove, TrackSurface, TrackTorpedo, TrackSonar, TrackSilence, SonarResult
from ai import TurnInformations, SituationAwarness
from helpers.mapdsl import parse


class Simulation:
    def __init__(self, definition):
        self.worldmap, _ = parse(definition)
        self.situation = SituationAwarness(self.worldmap)

    def turn(self, orders, turn):
        self.situation.update_tracking(orders, turn)
        print(self.situation.statistics())
        print("")
        return self

    def expected_positions(self, expected, symbols="◎"):
        _, positions = parse(expected)

        expected_positions = set()
        for symbol in symbols:
            expected_positions.update(positions[symbol] or [])

        print(self.situation.render())
        assert self.situation.position_count() == len(expected_positions)
        assert self.situation.opponent_tracking.solutions() == expected_positions
        return self
