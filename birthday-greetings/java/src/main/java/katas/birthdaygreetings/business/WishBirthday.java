package katas.birthdaygreetings.business;

public interface WishBirthday {
    void wish(final Person person);
}
