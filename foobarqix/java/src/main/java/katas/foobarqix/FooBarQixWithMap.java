package katas.foobarqix;

import java.util.Map;

import static java.lang.String.valueOf;

/**
 */
public class FooBarQixWithMap {
    private final static Map<Boolean, String> FOO = Map.of(true, "Foo", false, "");
    private final static Map<Boolean, String> BAR = Map.of(true, "Bar", false, "");
    private final static Map<Boolean, String> QIX = Map.of(true, "Qix", false, "");

    public static String fooBarQix(final int input) {
        final String accumulator = FOO.get(isDivisibleBy(input, 3)) + BAR.get(isDivisibleBy(input, 5)) + QIX.get(isDivisibleBy(input, 7));
        return Map.of(true, valueOf(input), false, accumulator).get(accumulator.isEmpty());
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
