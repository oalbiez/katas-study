# -*- coding: utf-8 -*-

from hypothesis import given, assume
from hypothesis.strategies import integers

from life import LifeStatus


def life_values():
    return integers(min_value=0, max_value=100)


@given(life_values(), life_values())
def test_representation_of_life_status_should_be_python(previous, current):
    assume(current <= previous)
    life = LifeStatus(previous, current)
    # pylint: disable=eval-used
    assert eval(repr(life)) == life


@given(life_values(), life_values())
def test_life_status_should_give_amount_lost(previous, current):
    assume(current <= previous)
    life = LifeStatus(previous, current)
    assert life.amount_lost() == previous - current


def test_life_status_should_be_updatable():
    life = LifeStatus(6, 4)
    assert life.update(4) == LifeStatus(4, 4)
