package katas.movierental.pricing;

import com.google.common.base.Joiner;
import katas.movierental.Duration;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

final class PriceCalculator {
    private final List<String> audit = newArrayList();
    private boolean overflow = false;
    private Value total;

    PriceCalculator(List<Rule> rules, Duration duration, ValueDriver driver) {
        Duration remaining = duration;
        total = driver.zero();
        for (Rule rule : rules) {
            Value price = rule.priceFor(remaining, driver);
            total = driver.add(total, price);
            remaining = rule.consume(remaining);
            audit.add("rule [" + rule.describe() + "]: price=" + price + ", total=" + total + ", remaining=" + remaining.value());
            if (remaining.isZero()) {
                return;
            }
        }

        if (remaining.isNotZero()) {
            audit.add("overflow of " + remaining.value());
            overflow = true;
        }
    }

    String audit() {
        return Joiner.on("; ").join(audit);
    }

    Value total() {
        if (overflow) {
            throw new Overflow();
        }
        return total;
    }
}
