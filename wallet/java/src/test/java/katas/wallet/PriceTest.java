package katas.wallet;

import org.junit.Test;

import static katas.wallet.Currency.EUR;
import static katas.wallet.Currency.USD;
import static katas.wallet.FixedRateProvider.rates;
import static katas.wallet.Price.price;
import static org.junit.Assert.assertEquals;

public class PriceTest {
    @Test
    public void money_should_equal() {
        assertEquals(price(EUR, 0), price(EUR, 0));
    }

    @Test
    public void money_should_add_when_same_currency() {
        assertEquals(price(EUR, 100), price(EUR, 60).add(price(EUR, 40)));
    }

    @Test
    public void money_should_change() {
        assertEquals(price(EUR, 20), price(EUR, 20).changeTo(EUR, rates()));
        assertEquals(price(EUR, 20), price(USD, 10).changeTo(EUR, rates("USD/EUR 2.0")));
    }

    @Test
    public void money_should_take_currency_precision() {
        assertEquals("EUR 20.23", price(EUR, 20.234).toString());
    }
}