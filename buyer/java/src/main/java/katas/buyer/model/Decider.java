package katas.buyer.model;

import java.util.Comparator;
import java.util.List;

public final class Decider {
    private final Penalty penalty;

    public static Decider decider() {
        return decider(Penalty.of(Price.of(10)));
    }

    public static Decider decider(final Penalty penalty) {
        return new Decider(penalty);
    }

    private Decider(final Penalty penalty) {
        this.penalty = penalty;
    }

    public Decision decisionFor(final PurchaseRequest request, final List<Offer> offers) {
        return Decision.of(offers.stream().min(Comparator.comparing(o -> o.evaluateFor(request.quantity(), penalty.after(request.delay())))).get());
    }
}
