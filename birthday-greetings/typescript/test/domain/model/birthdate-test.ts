import { expect } from "chai";
import * as fc from "fast-check";

import { Birthdate } from "domain/model/birthdate";
import { DateTime } from "luxon";

describe("Birthdate", () => {

    describe("isBirthday should", () => {

        it("false when today is before birthday", () => {
            expect(hasBirthday("2018-01-01", "2000-01-01")).to.be.false;
        });

        it("true when same month and day of month and today", () => {
            expect(hasBirthday("2000-01-01", "2018-01-01")).to.be.true;

            fc.property(
                fc.integer({ min: 1, max: 28 }),
                fc.integer({ min: 1, max: 12 }),
                fc.integer({ min: 0, max: 3000 }),
                fc.integer({ min: 0, max: 3000 }),
                (day: number, month: number, year: number, delta: number) => {
                    expect(hasBirthday(`${year}-${month}-${day}`, `${year + 10}-${month}-${day}`)).to.be.true;
                });
        });

        it("true when born on 29 february and today is 28 february on non leap years", () => {
            expect(hasBirthday("2004-02-29", "2015-02-28")).to.be.true;
        });

        it("false otherwise", () => {
            expect(hasBirthday("2000-01-01", "2018-07-01")).to.be.false;
            expect(hasBirthday("2004-02-29", "2016-02-28")).to.be.false;
        });

        function hasBirthday(birthdate: string, today: string): boolean {
            return Birthdate.fromString(birthdate).isBirthday(DateTime.fromISO(today));
        }

    });
});
