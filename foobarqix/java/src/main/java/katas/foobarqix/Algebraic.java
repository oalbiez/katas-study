package katas.foobarqix;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.valueOf;
import static katas.foobarqix.Algebraic.Literal.*;
import static katas.foobarqix.Algebraic.Numeric.numeric;

/**
 * Using algebraic
 */
public class Algebraic {

    interface ValueVisitor<T> {
        T visitNumeric(final Numeric value);

        T visitLiteral(final Literal value);
    }

    interface Value {
        Value compose(Value another);

        <T> T accept(final ValueVisitor<T> visitor);

        String render();
    }

    static class Numeric implements Value {
        private final int value;

        static Value numeric(final int value) {
            return new Numeric(value);
        }

        Numeric(int value) {
            this.value = value;
        }

        public Value compose(final Value another) {
            return another.accept(new ValueVisitor<Value>() {
                public Value visitNumeric(Numeric another) {
                    return Numeric.this;
                }

                public Value visitLiteral(Literal another) {
                    return another;
                }
            });
        }

        public <T> T accept(ValueVisitor<T> visitor) {
            return visitor.visitNumeric(this);
        }

        public String render() {
            return valueOf(value);
        }
    }

    static class Literal implements Value {
        private final String value;

        static Value foo() {
            return new Literal("Foo");
        }

        static Value bar() {
            return new Literal("Bar");
        }

        static Value qix() {
            return new Literal("Qix");
        }

        private Literal(final String value) {
            this.value = value;
        }

        public Value compose(Value another) {
            return another.accept(new ValueVisitor<Value>() {
                public Value visitNumeric(Numeric another) {
                    return Literal.this;
                }

                public Value visitLiteral(Literal another) {
                    return new Literal(value + another.value);
                }
            });
        }

        public <T> T accept(ValueVisitor<T> visitor) {
            return visitor.visitLiteral(this);
        }

        public String render() {
            return value;
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static String fooBarQix(final int input) {
        return newArrayList(
                isDivisibleBy(input, 3) ? foo() : numeric(input),
                isDivisibleBy(input, 5) ? bar() : numeric(input),
                isDivisibleBy(input, 7) ? qix() : numeric(input)).
                stream().reduce(Value::compose).map(Value::render).get();
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
