package katas.transactions.kiss.statistics;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.iterate;

public class Days {
    public static List<LocalDate> enumerateLastWeekDays(final LocalDate today, final int size) {
        return iterate(today, date -> date.minusDays(1))
                .filter(d -> isWeekDay(d.getDayOfWeek()))
                .limit(size)
                .collect(toList());
    }

    private static boolean isWeekDay(final DayOfWeek day) {
        return day != DayOfWeek.SATURDAY && day != DayOfWeek.SUNDAY;
    }

}
