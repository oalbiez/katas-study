package katas.transactions.kiss.poi;

import katas.transactions.kiss.statistics.AllStatistics;
import katas.transactions.kiss.statistics.Balance;
import katas.transactions.kiss.statistics.ByClientBalance;
import katas.transactions.kiss.statistics.Periods;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;
import java.util.function.IntSupplier;

import static java.util.Arrays.asList;
import static katas.transactions.kiss.poi.FontBuilder.font;
import static katas.transactions.kiss.poi.StyleBuilder.style;
import static katas.transactions.kiss.statistics.Statistics.ratio;
import static org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER;

public class WorkbookBuilder {
    private final Workbook workbook = new XSSFWorkbook();
    private final Font bold = font(workbook).bold().get();
    private final CellStyle headerStyle = style(workbook).align(CENTER).font(bold).get();
    private final CellStyle moneyStyle = style(workbook).format("#,##0.00 k€").get();
    private final CellStyle numberStyle = style(workbook).format("#,##0").get();
    private final CellStyle ratioStyle = style(workbook).format("#,##0.000").get();
    private final CellStyle noStyle = style(workbook).get();

    public static WorkbookBuilder build(final AllStatistics statistics, final LocalDate today) {
        final WorkbookBuilder builder = new WorkbookBuilder();
        builder.populateWithSummary(statistics, today);
        builder.populateWithClientBalance(statistics);
        builder.populateWithPeriods(statistics);
        return builder;
    }

    public void save(final String filename) {
        try {
            workbook.write(new FileOutputStream(filename));
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void populateWithSummary(final AllStatistics statistics, final LocalDate today) {
        final Sheet sheet = workbook.createSheet("Summary");
        final IntSupplier rowCounter = counter();

        sheet.setColumnWidth(0, 30 * 256);
        sheet.setColumnWidth(1, 20 * 256);
        sheet.setColumnWidth(2, 20 * 256);
        sheet.setColumnWidth(3, 20 * 256);
        sheet.setColumnWidth(4, 20 * 256);

        final Row date = sheet.createRow(rowCounter.getAsInt());
        createCell(date, 0, "Date: " + today.toString(), headerStyle);

        createCells(
                sheet.createRow(rowCounter.getAsInt()),
                0,
                asList("Nombre de transactions", "Credit", "Debit", "Volume", "Sold"),
                headerStyle);

        final Row row = sheet.createRow(rowCounter.getAsInt());
        final IntSupplier column = counter();
        createCell(row, column.getAsInt(), statistics.balance().count(), numberStyle);
        createCell(row, column.getAsInt(), statistics.balance().credit(), moneyStyle);
        createCell(row, column.getAsInt(), statistics.balance().debt(), moneyStyle);
        createCell(row, column.getAsInt(), statistics.balance().volume(), moneyStyle);
        createCell(row, column.getAsInt(), statistics.balance().sold(), moneyStyle);
    }

    private void populateWithClientBalance(final AllStatistics statistics) {
        final Sheet sheet = workbook.createSheet("Client balance");
        final IntSupplier rowCounter = counter();

        createCells(
                sheet.createRow(rowCounter.getAsInt()),
                0,
                asList("Client", "Count", "Credit", "Debit", "Sold", "Volume", "Ratio"),
                headerStyle);
        sheet.setColumnWidth(0, 20 * 256);
        sheet.setColumnWidth(1, 8 * 256);
        sheet.setColumnWidth(2, 20 * 256);
        sheet.setColumnWidth(3, 20 * 256);
        sheet.setColumnWidth(4, 20 * 256);
        sheet.setColumnWidth(5, 20 * 256);
        sheet.setColumnWidth(6, 8 * 256);

        for (final String client : statistics.clients()) {
            final Balance balance = statistics.byClient().statisticFor(client);
            final Row row = sheet.createRow(rowCounter.getAsInt());
            final IntSupplier column = counter();
            createCell(row, column.getAsInt(), client, noStyle);
            createCell(row, column.getAsInt(), balance.count(), numberStyle);
            createCell(row, column.getAsInt(), balance.credit(), moneyStyle);
            createCell(row, column.getAsInt(), balance.debt(), moneyStyle);
            createCell(row, column.getAsInt(), balance.sold(), moneyStyle);
            createCell(row, column.getAsInt(), balance.volume(), moneyStyle);
            createCell(row, column.getAsInt(), ratio(balance.volume(), statistics.balance().volume()), ratioStyle);
        }
    }

    private void populateWithPeriods(final AllStatistics statistics) {
        final Sheet sheet = workbook.createSheet("Periodes");
        final IntSupplier rowCounter = counter();
        sheet.setColumnWidth(0, 20 * 256);

        final Row period = sheet.createRow(rowCounter.getAsInt());
        final Row header = sheet.createRow(rowCounter.getAsInt());
        int periodColumn = 1;
        for (final Periods p : Periods.values()) {
            createCell(period, periodColumn, p.describe(), headerStyle);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, periodColumn, periodColumn + 1));
            createCell(header, periodColumn, "Number", headerStyle);
            createCell(header, periodColumn + 1, "Volume", headerStyle);
            sheet.setColumnWidth(periodColumn, 20 * 256);
            sheet.setColumnWidth(periodColumn + 1, 20 * 256);
            periodColumn += 2;
        }

        for (final String client : statistics.clients()) {
            final Row row = sheet.createRow(rowCounter.getAsInt());
            createCell(row, 0, client, noStyle);
            final IntSupplier column = counter(1);

            for (final Map.Entry<Periods, ByClientBalance> entry : statistics.byPeriods().entrySet()) {
                final Balance balance = entry.getValue().statisticFor(client);
                createCell(row, column.getAsInt(), balance.count(), numberStyle);
                createCell(row, column.getAsInt(), balance.volume(), moneyStyle);
            }
        }
    }

    private void createCell(final Row row, final int column, final long value, final CellStyle style) {
        final Cell cell = row.createCell(column);
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    private void createCell(final Row row, final int column, final double value, final CellStyle style) {
        final Cell cell = row.createCell(column);
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    private void createCell(final Row row, final int column, final String value, final CellStyle style) {
        final Cell cell = row.createCell(column);
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    private void createCells(final Row row, final int start, final Iterable<String> values, final CellStyle style) {
        final IntSupplier column = counter(start);
        for (final String value : values) {
            createCell(row, column.getAsInt(), value, style);
        }
    }

    private static IntSupplier counter() {
        return counter(0);
    }

    private static IntSupplier counter(final int start) {
        return new IntSupplier() {
            int i = start;

            public int getAsInt() {
                return i++;
            }
        };
    }
}
