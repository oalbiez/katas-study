import { expect } from "chai";
import * as fc from "fast-check";

import { EmailAddress } from "domain/model/email_address";

describe("EmailAddress", () => {

    describe("fromString should", () => {

        it("succed with RFC 1123 and RFC 5322 email", () => {
            fc.property(
                fc.emailAddress(),
                (email: string) => {
                    expect(validateMail(email)).to.be.true;
                });
        });

        it("succed with minimal address", () => {
            expect(validateMail("toto@nowhere.null")).to.be.true;
        });

        it("succed with special chaaracters in the name", () => {
            expect(validateMail("toto+-%_.bla@nowhere.null")).to.be.true;
        });

        it("succed with domains", () => {
            expect(validateMail("toto@a.b.c.d.null")).to.be.true;
        });

        it("fail when no @", () => {
            expect(validateMail("toto")).to.be.false;
        });

        function validateMail(value: string): boolean {
            try {
                EmailAddress.fromString(value);
                return true;
            } catch (error) {
                if (error instanceof TypeError) { return false; }
                throw error;
            }
        }

    });
});
