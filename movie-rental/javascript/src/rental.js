'use strict';


import { MovieCode } from 'movie-code';


export function statementText(customer, movies) {
  const data = renderData(customer, movies);
  let result = `Rental Record for ${data.name}\n`;
  for (const rental of data.rentals) {
    result += `\t${rental.title}\t${rental.amount}\n`;
  }
  result += `Amount owed is ${data.totalAmount}\n`;
  result += `You earned ${data.totalFrequentRenterPoint} frequent renter points\n`;
  return result;
}


export function statementHtml(customer, movies) {
  const data = renderData(customer, movies);
  let result = `<h1>Rental Record for <em>${data.name}</em></h1>\n`;
  result += '<table>\n';
  for (const rental of data.rentals) {
    result += `<tr><td>${rental.title}</td><td>${rental.amount}</td></tr>\n`;
  }
  result += '</table>\n';
  result += `<p>Amount owed is <em>${data.totalAmount}</em></p>\n`;
  result += `<p>You earned <em>${data.totalFrequentRenterPoint}</em> frequent renter points</p>\n`;
  return result;
}


function renderData(customer, movies) {
  const data = Object.assign({}, customer);
  data.rentals = customer.rentals.map((r) => rental(r));
  data.totalAmount = data.rentals.map((r) => r.amount).reduce(SUM, 0);
  data.totalFrequentRenterPoint = totalFrequentRenterPoint();
  return data;

  function rental(rental) {
    const data = Object.assign({}, rental);
    data.title = movieFor(rental).title;
    data.amount = MovieCode[movieFor(rental).code].charge(rental.days);
    return data;
  }

  function totalFrequentRenterPoint() {
    return customer.rentals.map((r) => MovieCode[movieFor(r).code].frequentRenterPoints(r.days)).reduce(SUM, 0);
  }

  function movieFor(rental) {
    return movies[rental.movieID];
  }
}


const SUM = (a, b) => a + b;
