package katas.transactions.kiss.statistics;

import katas.transactions.Transaction;

public interface Statistic {
    void updateWith(Transaction transaction);
}
