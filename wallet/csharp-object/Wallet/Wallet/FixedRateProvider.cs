﻿using System.Collections.Generic;

namespace Wallet
{
    public class FixedRateProvider : IRateProvider
    {
        private readonly Dictionary<string, decimal> _rates = new Dictionary<string, decimal>();

        public static FixedRateProvider Fixed()
        {
            return new FixedRateProvider();
        }

        public FixedRateProvider Rate(Currency from, Currency to, decimal rate)
        {
            _rates[Key(from, to)] = rate;
            return this;
        }

        public decimal GetRate(Currency from, Currency to)
        {
            return _rates[Key(from, to)];
        }

        private static string Key(Currency from, Currency to)
        {
            return from + "-" + to;
        }
    }
}