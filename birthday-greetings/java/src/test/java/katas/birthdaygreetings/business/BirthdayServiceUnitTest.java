package katas.birthdaygreetings.business;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.time.Month.*;
import static katas.birthdaygreetings.business.Birthdate.birthdate;
import static katas.birthdaygreetings.business.EmailAddress.emailAddress;
import static katas.birthdaygreetings.business.Name.name;
import static org.junit.Assert.assertEquals;

public class BirthdayServiceUnitTest {
    private LocalDate today;
    private final List<Person> people = newArrayList();

    @Test
    public void we_should_wish_birthday_for_right_person() {
        given_today_is(2018, MARCH, 1);
        and(person("Alexa").with(birthdate(2000, MARCH, 1)));
        and(person("Alice").with(birthdate(2004, AUGUST, 12)));
        and(person("Isabelle").with(birthdate(2004, MARCH, 1)));
        the_people_we_wish_for_their_birthday_should_be("Alexa, Isabelle");
    }

    @Test
    public void sometime_there_is_no_birthday() {
        given_today_is(2018, DECEMBER, 5);
        and(person("Alexa").with(birthdate(2000, MARCH, 1)));
        and(person("Alice").with(birthdate(2004, AUGUST, 12)));
        and(person("Isabelle").with(birthdate(2004, MARCH, 1)));
        then_we_should_not_wish_the_birthday_to_anyone();
    }

    @Test
    public void special_case_for_people_born_on_29_february() {
        given_today_is(2015, FEBRUARY, 28);
        and(person("Peter").with(birthdate(2000, JANUARY, 1)));
        and(person("Yann").with(birthdate(2004, FEBRUARY, 29)));
        and(person("Arthur").with(birthdate(2004, FEBRUARY, 28)));
        the_people_we_wish_for_their_birthday_should_be("Yann, Arthur");
    }


    private void given_today_is(final int year, final Month month, final int day) {
        this.today = LocalDate.of(year, month, day);
    }

    private void and(final Person person) {
        people.add(person);
    }

    private void the_people_we_wish_for_their_birthday_should_be(final String expected) {
        assertEquals(expected, personsToWishBirthday());
    }

    private void then_we_should_not_wish_the_birthday_to_anyone() {
        assertEquals("", personsToWishBirthday());
    }

    private String personsToWishBirthday() {
        final List<Person> selected = newArrayList();
        new BirthdayService(() -> people, selected::add, () -> today).process();
        return selected.stream().map(Person::name).map(Name::value).collect(Collectors.joining(", "));
    }

    private static Person person(final String name) {
        return Person.person(name(name), emailAddress(name + "+toto@mail.it"), birthdate(0, JANUARY, 1));
    }
}
