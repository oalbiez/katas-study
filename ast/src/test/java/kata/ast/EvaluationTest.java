package kata.ast;

import org.junit.Assert;
import org.junit.Test;

public class EvaluationTest {
    @Test
    public void test_evaluation() {
        Add add = new Add(new Number(2), new Sub(new Number(3), new Number((1))));
        Assert.assertEquals(4, Evaluator.evaluate(add));
    }


    @Test
    public void test_render_rpn_extern() {
        Add add = new Add(new Number(5), new Number(3));
        Assert.assertEquals("5 3 +", Renderer.renderRpn(add));
    }

}
