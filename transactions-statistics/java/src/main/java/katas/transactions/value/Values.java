package katas.transactions.value;

import java.util.List;
import java.util.Map;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;

public final class Values {
    public static SingleValue value(final String content) {
        return new SingleValue(content);
    }

    public static SingleValue value(final int content) {
        return value(valueOf(content));
    }

    public static SingleValue value(final long content) {
        return value(valueOf(content));
    }

    public static SingleValue value(final float content) {
        return value(valueOf(content));
    }

    public static SingleValue value(final double content) {
        return value(valueOf(content));
    }

    public static ListValue list(final Value... values) {
        return list(asList(values));
    }

    public static ListValue list(final List<Value> values) {
        return new ListValue(values);
    }

    public static MapValue map(final Map<String, Value> values) {
        return new MapValue(values);
    }
}
