package katas.transactions.kiss.statistics;

import katas.transactions.Transaction;

public final class Balance implements Statistic {
    private long count = 0;
    private long credit = 0;
    private long debt = 0;

    public void updateWith(final Transaction transaction) {
        count += 1;
        if (transaction.amount() > 0) {
            credit += transaction.amount();
        } else {
            debt += transaction.amount();
        }
    }

    public long count() {
        return count;
    }

    public long credit() {
        return credit;
    }

    public long debt() {
        return debt;
    }

    public long sold() {
        return credit + debt;
    }

    public long volume() {
        return credit - debt;
    }
}
