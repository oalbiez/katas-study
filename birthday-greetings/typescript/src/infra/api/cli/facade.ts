import { User } from "domain/model/user";
import { GreetBirthday } from "domain/usecases/greet_birthday";
import { UserProvider } from "domain/usecases/user_provider";
import { DateTime } from "luxon";


export class Facade {
    public constructor(private readonly users: UserProvider) {
    }

    public listUsers(): string {
        return this.users.all().map((u) => u.render()).join("\n");
    }

    public wishBirthday(date: string): string {
        const today = parse_date(date);
        const collector: User[] = [];
        new GreetBirthday(this.users, {
            wishFor: (user: User): void => {
                collector.push(user);
            }
        }).processBirthday(today);

        return `today: ${today.toISODate()}\n` + collector.map((u) => `- Happy birthday ${u.firstName.value} ${u.lastName.value} : ${u.birthday.render()}`).join("\n");
    }
}

function parse_date(value: string): DateTime {
    if (value === "now") {
        return DateTime.now();
    } else {
        return DateTime.fromISO(value);
    }
}
