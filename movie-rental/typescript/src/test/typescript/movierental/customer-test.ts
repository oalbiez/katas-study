import { Customer } from "../../../main/typescript/movierental/customer";
import { Movie } from "../../../main/typescript/movierental/movie";
import { Rental } from "../../../main/typescript/movierental/rental";

describe("Customer", () => {

    const Madagascar = childrensMovie("Madagascar");
    const Inception = regularMovie("Inception");
    const Tides = newReleaseMovie("Tides");

    it("Statement for regular movie", () => {
        expect(statementFor(customer("Sallie", rental(Inception, 3))))
            .toBe("Rental Record for Sallie\n" +
                "\tInception\t3.5\n" +
                "Amount owed is 3.5\n" +
                "You earned 1 frequent renter points");
    });

    it("Statement for new release movie", () => {
        expect(statementFor(customer("Sallie", rental(Tides, 3))))
            .toBe("Rental Record for Sallie\n" +
                "\tTides\t9.0\n" +
                "Amount owed is 9.0\n" +
                "You earned 2 frequent renter points");
    });

    it("Statement for childrens movie", () => {
        expect(statementFor(customer("Sallie", rental(Madagascar, 3))))
            .toBe("Rental Record for Sallie\n" +
                "\tMadagascar\t1.5\n" +
                "Amount owed is 1.5\n" +
                "You earned 1 frequent renter points");
    });

    it("Statement for many movies", () => {
        expect(statementFor(customer("David",
            rental(Madagascar, 6),
            rental(Tides, 2),
            rental(Inception, 8))))
            .toBe("Rental Record for David\n" +
                "\tMadagascar\t6.0\n" +
                "\tTides\t6.0\n" +
                "\tInception\t11.0\n" +
                "Amount owed is 23.0\n" +
                "You earned 4 frequent renter points");
    });

    function rental(movie: Movie, daysRented: number): Rental {
        return new Rental(movie, daysRented);
    }

    function childrensMovie(title: string): Movie {
        return new Movie(title, Movie.CHILDRENS);
    }

    function regularMovie(title: string): Movie {
        return new Movie(title, Movie.REGULAR);
    }

    function newReleaseMovie(title: string): Movie {
        return new Movie(title, Movie.NEW_RELEASE);
    }

    function customer(name: string, ...rentals: Rental[]): Customer {
        const result = new Customer(name);
        rentals.forEach((e) => result.addRental(e));
        return result;
    }

    function statementFor(c: Customer): string {
        return c.statement();
    }
});
