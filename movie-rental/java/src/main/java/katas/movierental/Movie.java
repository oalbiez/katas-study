package katas.movierental;

public class Movie {
    private String title;
    private PriceCode priceCode;

    public Movie(String title, PriceCode priceCode) {
        this.title = title;
        this.priceCode = priceCode;
    }

    public String getTitle() {
        return title;
    }

    Price chargeFor(Duration duration) {
        return priceCode.chargeFor(duration);
    }

    FrequentRenterPoint frequentRenterPointFor(Duration duration) {
        return priceCode.frequentRenterPointFor(duration);
    }
}
