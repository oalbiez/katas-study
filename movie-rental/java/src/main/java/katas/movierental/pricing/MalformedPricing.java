package katas.movierental.pricing;


public class MalformedPricing extends RuntimeException {
    MalformedPricing(String message) {
        super(message);
    }
}
