package katas.buyer.model;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

public final class Delay {
    private final Duration value;

    public static Delay of(final String definition) {
        if (Strings.isNullOrEmpty(definition)) {
            return none();
        }

        final String[] tokens = definition.split(" ");
        return new Delay(Duration.of(Long.valueOf(tokens[0]), ChronoUnit.DAYS));
    }

    public static Delay none() {
        return new Delay(Duration.ZERO);
    }

    private Delay(final Duration delay) {
        this.value = delay;
    }

    public Delay sub(final Delay delay) {
        return new Delay(this.value.minus(delay.value));
    }

    public Delay clampToZero() {
        if (value.isNegative()) {
            return new Delay(Duration.ZERO);
        }
        return this;
    }

    public BigDecimal inDays() {
        return BigDecimal.valueOf(value.toDays());
    }

    @Override
    public String toString() {
        return inDays() + " day(s)";
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        return Objects.equal(value, ((Delay) other).value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }
}
