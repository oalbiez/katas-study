package katas.marsrover;

public class MoveAborted implements Move {
    private final Obstacle obstacle;
    private final Position position;

    public MoveAborted(Obstacle obstacle, Position position) {
        this.obstacle = obstacle;
        this.position = position;
    }

    public Position finalPosition() {
        return position;
    }

    public Move move(Command command) {
        return null;
    }
}
