package katas.movierental.pricing;

import katas.movierental.Duration;
import katas.movierental.Price;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.util.regex.Pattern.MULTILINE;
import static java.util.regex.Pattern.compile;
import static katas.movierental.Price.euro;

public class PricingParser {
    private static final Pattern AMOUNT = compile("^[0-9]*\\.?[0-9]+", MULTILINE);
    private static final Pattern INT = compile("^[0-9]*", MULTILINE);

    private final String content;
    private int position = 0;

    public static Pricing pricing(String content) {
        return new PricingParser(content).parse();
    }

    private PricingParser(String content) {
        this.content = content;
    }

    private Pricing parse() {
        Pricing pricing = Pricing.pricing(rule());
        while (check_token("then")) {
            pricing = pricing.then(rule());
        }
        return pricing;
    }

    private Rule rule() {
        token("costs");
        Rule cost = Pricing.cost(amount());
        if (check_token("per")) {
            token("day");
            cost = cost.perDay();
        }
        if (check_token("for")) {
            int value = integer();
            token("day");
            check_token("s");
            cost = cost.for_(Duration.days(value));
        }
        return cost;
    }

    private void spaces() {
        while (!eos() && current() == ' ') {
            advance(1);
        }
    }

    private Price amount() {
        final Matcher matcher = AMOUNT.matcher(tail());
        if (matcher.find(0) && matcher.start() == 0) {
            advance(matcher.end());
            token("€");
            spaces();
            return euro(parseDouble(matcher.group()));
        }
        throw parseError("amount");
    }

    private int integer() {
        final Matcher matcher = INT.matcher(tail());
        if (matcher.find(0) && matcher.start() == 0) {
            advance(matcher.end());
            spaces();
            return Integer.parseInt(matcher.group());
        }
        throw parseError("int");
    }


    private boolean eos() {
        return position >= content.length();
    }

    private char current() {
        return content.charAt(position);
    }

    private String tail() {
        return content.substring(position);
    }

    private void advance(final int count) {
        position += count;
    }

    private void token(final String token) {
        if (tail().startsWith(token)) {
            advance(token.length());
        } else {
            throw parseError(token);
        }
        spaces();
    }

    private boolean check_token(final String token) {
        if (tail().startsWith(token)) {
            advance(token.length());
            spaces();
            return true;
        } else {
            return false;
        }
    }

    private RuntimeException parseError(final String token) {
        return new IllegalArgumentException("Expected " + token + " at #" + position + " got '" + tail() + '\'');
    }
}
