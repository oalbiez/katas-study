package katas.transactions.value;

import com.google.common.base.Objects;

import java.util.Collections;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public final class MapValue implements Value {
    private final Map<String, Value> values;

    MapValue(final Map<String, Value> values) {
        this.values = values;
    }

    public Map<String, Value> values() {
        return Collections.unmodifiableMap(values);
    }

    public <T> T accept(final ValueVisitor<T> visitor) {
        return visitor.visitMap(this);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        final MapValue value = (MapValue) other;
        return Objects.equal(values, value.values);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(values);
    }

    @Override
    public String toString() {
        return values.entrySet().stream().map(e -> "(" + e.getKey() + ": " + e.getValue() + ")").collect(joining(", "));
    }
}
