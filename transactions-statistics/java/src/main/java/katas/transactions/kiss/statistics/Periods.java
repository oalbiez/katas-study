package katas.transactions.kiss.statistics;

public enum Periods {
    GT_YEAR("more than 1 year"),
    GT_SIX_MONTHS("between 6 months and 1 year"),
    GT_THREE_MONTHS("between 3 months and 6 months"),
    GT_ONE_MONTH("between 1 month and 3 months"),
    GT_D5("between D - 5 and 1 month"),
    EQ_D4("D - 4"),
    EQ_D3("D - 3"),
    EQ_D2("D - 2"),
    EQ_D1("D - 1"),
    TODAY("D");

    private final String description;

    Periods(final String description) {
        this.description = description;
    }

    public String describe() {
        return description;
    }

    public static Periods findPeriod(final long age) {
        if (age > 360) return GT_YEAR;
        if (age > 180) return GT_SIX_MONTHS;
        if (age > 90) return GT_THREE_MONTHS;
        if (age > 30) return GT_ONE_MONTH;
        if (age > 4) return GT_D5;
        if (age == 4) return EQ_D4;
        if (age == 3) return EQ_D3;
        if (age == 2) return EQ_D2;
        if (age == 1) return EQ_D1;
        return TODAY;
    }
}
