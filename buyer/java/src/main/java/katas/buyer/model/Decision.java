package katas.buyer.model;

public final class Decision {
    private final Offer offer;

    public static Decision of(final Offer offer) {
        return new Decision(offer);
    }

    private Decision(final Offer offer) {
        this.offer = offer;
    }

    public Vendor vendor() {
        return offer.vendor();
    }

    public Quantity quantity() {
        return offer.quantity();
    }

    public Price price() {
        return offer.price();
    }
}
