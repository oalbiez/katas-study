package katas.birthdaygreetings.business;

import java.time.LocalDate;

public interface DateProvider {
    LocalDate today();
}
