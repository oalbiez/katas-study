package katas.foobarqix;

public class FooBarQixWithDecoratorShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return FooBarQixWithDecorator.fooBarQix(input);
    }
}
