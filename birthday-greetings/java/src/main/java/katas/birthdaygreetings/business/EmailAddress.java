package katas.birthdaygreetings.business;

import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

public final class EmailAddress {
    private final static Pattern VALIDATION = compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}$", CASE_INSENSITIVE);
    private final String value;

    public static EmailAddress emailAddress(final String value) {
        if (VALIDATION.matcher(value).matches()) {
            return new EmailAddress(value);
        }
        throw new IllegalArgumentException(value + " is not a valid email");
    }

    private EmailAddress(final String value) {
        this.value = value;
    }
}
