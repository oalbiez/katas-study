
export class Duration {
    public static days(value: number): Duration {
        return new Duration(value);
    }

    public static min(left: Duration, right: Duration): Duration {
        return new Duration(Math.min(left.days(), right.days()));
    }

    public static sub(left: Duration, right: Duration): Duration {
        return new Duration(Math.max(left.days() - right.days(), 0));
    }

    private value: number;

    private constructor(value: number) {
        this.value = value;
    }

    public days(): number {
        return this.value;
    }

    public whenGretherThan(days: number, callback: (remaining: number) => void): void {
        if (this.value > days) {
            callback(this.value - days);
        }
    }
}
