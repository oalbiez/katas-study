package katas.transactions.srp.ast;

import katas.transactions.Transaction;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public final class Strings {
    public static ValueProvider<String> literal(final String value) {
        return new ValueProvider<String>() {
            public String get(final Transaction transaction) {
                return value;
            }

            public String describe() {
                return value;
            }
        };
    }

    @SafeVarargs
    public static ValueProvider<String> concat(final ValueProvider<String>... item) {
        return new ValueProvider<String>() {
            public String get(final Transaction transaction) {
                return stream(item).map(i -> i.get(transaction)).collect(joining());
            }

            public String describe() {
                return stream(item).map(ValueProvider::describe).collect(joining());
            }
        };
    }
}
