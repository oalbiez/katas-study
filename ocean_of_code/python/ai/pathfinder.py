# -*- coding: utf-8 -*-
from __future__ import annotations

import heapq


def search_distance(start, distance, neighbors):
    """ Performs a distance search on the graph.

    @param start the start location of the graph
    @param distance the goal as distance
    @param neighbors a functor position -> []
    """

    frontier = []
    _put(frontier, start, 0)
    costs = {start: 0}
    while not _empty(frontier):
        current = _pop(frontier)
        current_cost = costs[current]
        for neighbor in neighbors(current):
            neighbor_cost = current_cost + 1
            if neighbor_cost <= distance and neighbor not in costs:
                costs[neighbor] = neighbor_cost
                _put(frontier, neighbor, neighbor_cost)
    return costs


def astar_search(start, goal, neighbors, heuristic):
    """ Performs a A* search on the graph.

    @param start the start location of the graph
    @param goal the goal as graph location
    @param neighbors a functor position -> []
    @param heuristic a functor (from, to) -> cost estimate
    """

    frontier = []
    _put(frontier, start, 0)
    path = {start: None}
    costs = {start: 0}

    while not _empty(frontier):
        current = _pop(frontier)

        if current == goal:
            return True, _path(path, goal), costs[goal]

        for neighbor in neighbors(current):
            new_cost = costs[current] + 1
            if neighbor not in costs or new_cost < costs[neighbor]:
                costs[neighbor] = new_cost
                priority = new_cost + heuristic(goal, neighbor)
                _put(frontier, neighbor, priority)
                path[neighbor] = current
    return False, [], 0


def _empty(queue):
    return len(queue) == 0


def _put(queue, item, priority):
    heapq.heappush(queue, (priority, item))


def _pop(queue):
    return heapq.heappop(queue)[1]


def _path(path, goal):
    result = []
    index = goal
    while index is not None:
        result.append(index)
        index = path[index]
    return list(reversed(result))
