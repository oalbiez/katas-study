package katas.wallet;

import java.math.BigDecimal;

public final class Rate {
    private final BigDecimal value;

    public static Rate of(double value) {
        return new Rate(BigDecimal.valueOf(value));
    }

    private Rate(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal value() {
        return value;
    }

    @Override
    public String toString() {
        return "rate: " + value;
    }
}
