import { User } from "domain/model/user";

export interface UserProvider {
    all(): User[];
}
