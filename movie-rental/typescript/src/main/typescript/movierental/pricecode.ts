import { Duration } from "./duration";
import { add, mul, Price } from "./price";

// tslint:disable:max-classes-per-file
export interface PriceCode {
    chargeFor(duration: Duration): Price;
    frequentRenterPointsFor(duration: Duration): number;
}

export class Chidlrens implements PriceCode {
    public chargeFor(duration: Duration): Price {
        let thisAmount = Price.euro(1.5);
        duration.whenGretherThan(3, (remaining) => {
            thisAmount = add(thisAmount, mul(Price.euro(1.5), remaining));
        });
        return thisAmount;
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return 1;
    }
}

export class NewRelease implements PriceCode {
    public chargeFor(duration: Duration): Price {
        return mul(Price.euro(3), duration.days());
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return duration.days() > 1 ? 2 : 1;
    }
}

export class Regular implements PriceCode {
    public chargeFor(duration: Duration): Price {
        let thisAmount = Price.euro(2);
        duration.whenGretherThan(2, (remaining) => {
            thisAmount = add(thisAmount, mul(Price.euro(1.5), remaining));
        });
        return thisAmount;
    }

    public frequentRenterPointsFor(duration: Duration): number {
        return 1;
    }
}
