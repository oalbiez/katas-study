package katas.birthdaygreetings.business;

import java.time.LocalDate;

public final class BirthdayService {
    private final PersonProvider provider;
    private final WishBirthday notifier;
    private final DateProvider dateProvider;

    public BirthdayService(final PersonProvider provider, final WishBirthday notifier, final DateProvider dateProvider) {
        this.provider = provider;
        this.notifier = notifier;
        this.dateProvider = dateProvider;
    }

    public void process() {
        processFor(dateProvider.today());
    }

    private void processFor(final LocalDate today) {
        provider.getPersons().stream().filter(person -> person.isBirthday(today)).forEach(notifier::wish);
    }
}
