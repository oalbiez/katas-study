package katas.transactions.srp.ast;

import katas.transactions.Transaction;

public final class Booleans {
    public static ValueProvider<Boolean> and(final ValueProvider<Boolean> left, final ValueProvider<Boolean> right) {
        return new ValueProvider<Boolean>() {
            public Boolean get(final Transaction transaction) {
                return left.get(transaction) && right.get(transaction);
            }

            public String describe() {
                return left.describe() + " and " + right.describe();
            }
        };
    }
}
