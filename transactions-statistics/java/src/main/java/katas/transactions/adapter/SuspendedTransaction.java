package katas.transactions.adapter;

import java.time.LocalDate;

public class SuspendedTransaction {
    public String counterpart;
    public long amount;
    public LocalDate suspended;
}
