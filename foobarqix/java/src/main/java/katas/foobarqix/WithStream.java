package katas.foobarqix;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static java.lang.String.valueOf;
import static java.util.stream.Collectors.joining;

/**
 * Using stream.
 */
public class WithStream {
    private final static List<Integer> DIVISORS = List.of(3, 5, 7);
    private final static Map<Integer, String> VALUES = Map.of(3, "Foo", 5, "Bar", 7, "Qix");

    public static String fooBarQix(final int input) {
        if (DIVISORS.stream().anyMatch(isDivisorOf(input))) {
            return DIVISORS.stream().filter(isDivisorOf(input)).map(VALUES::get).collect(joining());
        }
        return valueOf(input);
    }

    private static Predicate<Integer> isDivisorOf(int input) {
        return divisor -> input % divisor == 0;
    }
}
