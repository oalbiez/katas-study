package katas.transactions.kiss.render;

import com.google.common.base.Strings;

public final class ByClientSummary {
    private final String client;
    private final long transactionCount;
    private final long credit;
    private final long debt;
    private final long sold;
    private final long volume;
    private final double volumeRatio;

    ByClientSummary(final String client, final long transactionCount, final long credit, final long debt, final long sold, final long volume, final double volumeRatio) {
        this.client = client;
        this.transactionCount = transactionCount;
        this.credit = credit;
        this.debt = debt;
        this.sold = sold;
        this.volume = volume;
        this.volumeRatio = volumeRatio;
    }

    @Override
    public String toString() {
        return "| client: " + Strings.padEnd(client, 10, ' ') +
                " | transaction count: " + String.format("%8s", transactionCount) +
                " | credit: " + String.format("%12s", credit) +
                " | debit: " + String.format("%12s", debt) +
                " | volume: " + String.format("%12s", volume) +
                " | solde: " + String.format("%12s", sold) +
                " | ratio: " + String.format("%2.3f", volumeRatio) +
                " |";
    }
}
