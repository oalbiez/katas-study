package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.value.Value;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static java.util.Arrays.stream;
import static katas.transactions.srp.ast.Transactions.amount;
import static katas.transactions.value.Values.value;
import static org.junit.Assert.assertEquals;

public class SumOfTest {
    @Test
    public void should_get_sum_of_transactions() {
        assertEquals(value(100), sum(transaction(100)));
        assertEquals(value(250), sum(transaction(100), transaction(150)));
    }

    private Transaction transaction(final long amount) {
        return Transaction.transaction("client", "A", amount, LocalDate.of(2017, Month.APRIL, 17).atStartOfDay());
    }

    private Value sum(final Transaction... transactions) {
        final SumOf stat = Statistics.sum(amount());
        stream(transactions).forEach(stat::updateWith);
        return stat.value();
    }
}
