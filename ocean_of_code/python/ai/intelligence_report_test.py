# -*- coding: utf-8 -*-

from geometry import Position
from intelligence_report import IntelligenceReport
from intelligence_report import Nothing, Movement, Surface, Sonar, Silence, Torpedo, MineDroped, MineTrigged
from direction import Direction


def report(expression):
    return IntelligenceReport.parse(expression)


def test_report_should_be_parsable_from_text():
    def parse(expression, *expected):
        assert report(expression) == IntelligenceReport(expected)

    parse("NA", Nothing())
    parse("MOVE N", Movement(Direction.North))
    parse("MOVE S", Movement(Direction.South))
    parse("MOVE E", Movement(Direction.East))
    parse("MOVE W", Movement(Direction.West))
    parse("SURFACE 1", Surface(1))
    parse("SURFACE 3", Surface(3))
    parse("SURFACE 5", Surface(5))
    parse("SONAR 5", Sonar(5))
    parse("SILENCE", Silence())
    parse("TORPEDO 3 10", Torpedo(Position(3, 10)))
    parse("MINE", MineDroped())
    parse("TRIGGER 3 10", MineTrigged(Position(3, 10)))
    parse("TORPEDO 7 0| MOVE E", Torpedo(Position(7, 0)), Movement(Direction.East))


def test_report_should_find_all_explosion_positions():
    def check_explosion_positions(expression, expected):
        assert report(expression).explosion_positions() == set(expected)

    check_explosion_positions(
        "TORPEDO 7 0 | TRIGGER 5 9", (Position(7, 0), Position(5, 9)))

    check_explosion_positions("MOVE E | SONAR 5 | SURFACE 7", ())


def test_report_should_find_surface():
    assert report("TORPEDO 7 0 | SURFACE 4").surface() == Surface(4)
    assert report("TORPEDO 7 0 | MOVE E").surface() is None


def test_report_should_find_sonar():
    assert report("TORPEDO 7 0 | SONAR 4").sonar() == Sonar(4)
    assert report("TORPEDO 7 0 | MOVE E").sonar() is None


def test_representation_of_report_should_be_python():
    # pylint: disable=eval-used
    value = report("NA | MOVE W | SURFACE 3 | SILENCE | TORPEDO 3 10 | MINE | TRIGGER 3 10 | SONAR 2")
    assert eval(repr(value)) == value


def test_report_should_be_iterable():
    assert len(list(report("TORPEDO 7 0 | SONAR 4"))) == 2
