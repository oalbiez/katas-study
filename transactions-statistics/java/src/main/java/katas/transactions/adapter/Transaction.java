package katas.transactions.adapter;

import java.time.LocalDate;

public interface Transaction {
    String client();

    long amount();

    LocalDate date();

    boolean isSuspended();
}
