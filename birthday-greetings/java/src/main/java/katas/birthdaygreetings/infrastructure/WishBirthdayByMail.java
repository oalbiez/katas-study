package katas.birthdaygreetings.infrastructure;

import katas.birthdaygreetings.business.Person;
import katas.birthdaygreetings.business.WishBirthday;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

import static com.google.common.base.Throwables.propagate;

public class WishBirthdayByMail implements WishBirthday {
    private final Session session;

    public WishBirthdayByMail(final Properties properties) {
        this.session = Session.getDefaultInstance(properties);
    }

    public void wish(final Person person) {
        try {
            final MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from@mail.it"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(person.email().toString()));
            message.setSubject("Happy Birthday!");
            message.setText("Happy birthday, dear " + person.name().value() + "!");
            Transport.send(message);
        } catch (final MessagingException e) {
            throw propagate(e);
        }
    }
}