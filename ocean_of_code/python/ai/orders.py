# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Iterator, List, Optional, Set, Text, Tuple, Union

from direction import Direction
from geometry import Position
from powers import Powers


@dataclass(frozen=True)
class DropMine:
    direction: Direction

    def render(self) -> Text:
        return f"MINE {self.direction.code}"


@dataclass(frozen=True)
class Message:
    content: Text

    def render(self) -> Text:
        return f"MSG {self.content}"


@dataclass(frozen=True)
class Move:
    direction: Direction
    power: Powers

    def render(self) -> Text:
        return f"MOVE {self.direction.code} {self.power.render()}"


@dataclass(frozen=True)
class Silence:
    direction: Direction
    distance: int

    def render(self) -> Text:
        return f"SILENCE {self.direction.code} {self.distance}"


@dataclass(frozen=True)
class Sonar:
    sector: int

    def render(self) -> Text:
        return f"SONAR {self.sector}"


@dataclass(frozen=True)
class Surface:
    def render(self) -> Text:
        # pylint: disable=no-self-use
        return "SURFACE"


@dataclass(frozen=True)
class Torpedo:
    target: Position

    def render(self) -> Text:
        return f"TORPEDO {self.target.x} {self.target.y}"


@dataclass(frozen=True)
class TriggerMine:
    target: Position

    def render(self) -> Text:
        return f"TRIGGER {self.target.x} {self.target.y}"


Command = Union[DropMine, Message, Move, Silence, Sonar, Surface, Torpedo, TriggerMine]


@dataclass(frozen=True)
class Orders:
    commands: Tuple[Command, ...]

    @staticmethod
    def empty() -> Orders:
        return Orders(())

    @staticmethod
    def parse(orders: Text) -> Orders:
        def to_command(item: List[Text]) -> Command:
            if item[0] == "MINE":
                return DropMine(Direction.parse(item[1]))
            if item[0] == "MOVE":
                return Move(Direction.parse(item[1]), Powers.parse(item[2]))
            if item[0] == "SILENCE":
                return Silence(Direction.parse(item[1]), int(item[2]))
            if item[0] == "SONAR":
                return Sonar(int(item[1]))
            if item[0] == "SURFACE":
                return Surface()
            if item[0] == "TORPEDO":
                return Torpedo(Position(int(item[1]), int(item[2])))
            if item[0] == "TRIGGER":
                return TriggerMine(Position(int(item[1]), int(item[2])))
            if item[0] == "MSG":
                return Message(" ".join(item[1:]))
            return Message(f"unknown command {item}")

        return Orders(tuple(to_command(i.strip().split(" ")) for i in orders.split("|")))

    @staticmethod
    def of(*commands: Command) -> Orders:
        return Orders(commands)

    def __iter__(self) -> Iterator[Command]:
        return iter(self.commands)

    def add(self, command: Command) -> Orders:
        return Orders((*self.commands, command))

    def sonar(self) -> Optional[Sonar]:
        for command in self.commands:
            if isinstance(command, Sonar):
                return command
        return None

    def surface(self) -> Optional[Surface]:
        for command in self.commands:
            if isinstance(command, Surface):
                return command
        return None

    def explosion_positions(self) -> Set[Position]:
        positions = set()
        for command in self.commands:
            if isinstance(command, Torpedo):
                positions.add(command.target)
            if isinstance(command, TriggerMine):
                positions.add(command.target)
        return positions

    def render(self):
        return "|".join([command.render() for command in self.commands])

    def __repr__(self):
        return f"Orders.parse({repr(self.render())})"
