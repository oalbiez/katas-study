package katas.transactions.srp.ast;

import katas.transactions.Transaction;

public final class Transactions {
    public static ValueProvider<Long> transaction() {
        return new ValueProvider<Long>() {
            public Long get(final Transaction transaction) {
                return 1L;
            }

            public String describe() {
                return "transaction";
            }
        };
    }


    public static ValueProvider<Long> amount() {
        return new ValueProvider<Long>() {
            public Long get(final Transaction transaction) {
                return transaction.amount();
            }

            public String describe() {
                return "amount";
            }
        };
    }

    public static ValueProvider<String> byClient() {
        return new ValueProvider<String>() {
            public String get(final Transaction transaction) {
                return transaction.client();
            }

            public String describe() {
                return "client";
            }
        };
    }

    public static ValueProvider<String> byCategory() {
        return new ValueProvider<String>() {
            public String get(final Transaction transaction) {
                return transaction.category();
            }

            public String describe() {
                return "category";
            }
        };
    }

}
