package katas.bottle;

import org.junit.Test;

import static katas.bottle.Bottle.bottle;
import static katas.bottle.Bottle.delta;
import static org.junit.Assert.assertEquals;

public class BottleTest {

    @Test
    public void bottle_should_have_remaining_quantity() {
        assertEquals(4, bottle(4, 0).remaining());
        assertEquals(1, bottle(4, 3).remaining());
    }

    @Test
    public void bottle_should_update_with_delta() {
        assertEquals(bottle(4, 2), bottle(4, 0).updateWith(2));
        assertEquals(bottle(4, 1), bottle(4, 3).updateWith(-2));
    }

    @Test
    public void delta_should_give_the_quantity_we_can_transfert_from_two_bottles() {
        assertEquals(3, delta(bottle(5, 3), bottle(8, 2)));
        assertEquals(0, delta(bottle(5, 3), bottle(8, 8)));
        assertEquals(2, delta(bottle(5, 5), bottle(8, 6)));
    }
}
