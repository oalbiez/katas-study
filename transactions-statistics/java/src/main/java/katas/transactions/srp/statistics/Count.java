package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.value.Value;
import katas.transactions.value.Values;

public final class Count implements Statistic {
    private long statistic = 0;

    public void updateWith(final Transaction transaction) {
        statistic += 1;
    }

    public void accept(final StatisticVisitor visitor) {
        visitor.visitCount(this);
    }

    public Statistic fork() {
        return new Count();
    }

    public String describe() {
        return "transaction count";
    }

    public Value value() {
        return Values.value(statistic);
    }
}
