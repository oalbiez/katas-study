# -*- coding: utf-8 -*-

from ai import Position, Targets
from helpers.mapdsl import parse


def test_without_islands():
    worldmap, _ = parse("""
        ┏━━━━━━━━━━━┓
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┗━━━━━━━━━━━┛
    """)
    assert len(set(worldmap.all_water())) == 25
    for position in worldmap.walk():
        assert worldmap.is_water(position) is True


def test_without_water():
    worldmap, _ = parse("""
        ┏━━━━━━━━━━━┓
        ┃ ▲ ▲ ▲ ▲ ▲ ┃
        ┃ ▲ ▲ ▲ ▲ ▲ ┃
        ┃ ▲ ▲ ▲ ▲ ▲ ┃
        ┃ ▲ ▲ ▲ ▲ ▲ ┃
        ┃ ▲ ▲ ▲ ▲ ▲ ┃
        ┗━━━━━━━━━━━┛
    """)
    assert not set(worldmap.all_water())
    for position in worldmap.walk():
        assert worldmap.is_water(position) is False


def test_is_water():
    worldmap, _ = parse("""
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • • • • • ┃ • • • • • ┃ • ▲ ▲ • ▲ ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)
    assert worldmap.is_water(Position(12, 0)) is False
    assert worldmap.is_water(Position(13, 0)) is True


def test_outside_map_is_not_water():
    worldmap, _ = parse("""
        ┏━━━━━━━━━━━┓
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┃ • • • • • ┃
        ┗━━━━━━━━━━━┛
    """)
    assert worldmap.is_water(Position(-1, 0)) is False
    assert worldmap.is_water(Position(5, 0)) is False
    assert worldmap.is_water(Position(0, -1)) is False
    assert worldmap.is_water(Position(0, 5)) is False
    assert worldmap.is_water(Position(5, 5)) is False


def test_to_sector():
    worldmap, points = parse("""
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ 1 • • • • ┃ 2 • • • • ┃ • ▲ ▲ • ▲ ┃
        ┃ • • • • 1 ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • 3 ▲ ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ 4 • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃
        ┃ • • • 4 ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ 7 • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ 9 • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • 7 ┃ • • • • • ┃ • • • • 9 ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)

    for key, positions in points.items():
        for position in positions:
            assert worldmap.to_sector(position) == int(key)


def test_neighbors():
    def check_neighbors_of(definition):
        worldmap, symbols = parse(definition)
        position = symbols["◉"][0]
        neighbors = set(symbols["◇"])
        assert set(worldmap.neighbors(position)) == neighbors

    check_neighbors_of("""
        ┏━━━━━━━━━━━┓
        ┃ • • • • • ┃
        ┃ • • ◇ • • ┃
        ┃ ▲ ▲ ◉ ◇ • ┃
        ┃ ▲ ▲ ◇ • ▲ ┃
        ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┛
    """)

    check_neighbors_of("""
        ┏━━━━━━━━━━━┓
        ┃ • • • • • ┃
        ┃ • • • ◇ • ┃
        ┃ ▲ ▲ ◇ ◉ ◇ ┃
        ┃ ▲ ▲ • ◇ ▲ ┃
        ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┛
    """)

    check_neighbors_of("""
        ┏━━━━━━━━━━━┓
        ┃ • • • ◇ ◉ ┃
        ┃ • • • • ◇ ┃
        ┃ ▲ ▲ • • • ┃
        ┃ ▲ ▲ • • ▲ ┃
        ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┛
    """)


def test_distance():
    def check_distance_of(distance, definition):
        worldmap, symbols = parse(definition)
        start = symbols["◉"][0]
        goal = symbols["○"][0]
        assert worldmap.distance(start, goal) == distance

    check_distance_of(3, """
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • ◉ • • ○ ┃ • ▲ ▲ • ▲ ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)

    check_distance_of(-1, """
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • • • • ○ ┃ • ▲ ▲ ◉ ▲ ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)

    check_distance_of(8, """
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • • • • • ┃ • ▲ ▲ • ▲ ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ◉ • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ ▲ ▲ • ┃ • • • • ▲ ┃
        ┃ ▲ ▲ ○ • • ┃ • • • • ▲ ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)


def test_possible_targets():
    def targets(definition):
        worldmap, symbols = parse(definition)
        start = symbols["x"][0]
        expected = Targets(set(symbols["S"]), set(symbols["D"]))
        assert worldmap.possible_targets(start) == expected

    targets("""
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ S S S S S ┃ • • • • • ┃
        ┃ S D D D S ┃ S • • • • ┃
        ┃ ▲ ▲ x D S ┃ S S • • • ┃
        ┃ ▲ ▲ D D ▲ ┃ ▲ ▲ • • • ┃
        ┃ S S S S ▲ ┃ ▲ ▲ • • • ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • S S S ▲ ┃ ▲ ▲ • • • ┃
        ┃ • • S • • ┃ • • • • • ┃
        ┃ • • • • • ┃ ▲ ▲ ▲ • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)


def test_possible_targets_bug():
    def targets(definition):
        worldmap, symbols = parse(definition)
        start = symbols["x"][0]
        expected = Targets(set(symbols["S"]), set(symbols["D"]))
        targets = worldmap.possible_targets(start)

        def display(position):
            if position == start:
                return "x"
            if position in targets.safe:
                return "S"
            if position in targets.danger:
                return "D"
            return None

        print(worldmap.render(display))
        assert targets == expected

    targets("""
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • • • ▲ ▲ ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ • • • • • ┃ • • • • • ┃ • ▲ ▲ • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • ▲ ▲ • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • ▲ ▲ ▲ ▲ ┃ ▲ • • • ▲ ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • • • • • ┃ • ▲ ▲ ▲ ▲ ┃ ▲ ▲ ▲ • ▲ ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃ ▲ ▲ ▲ • • ┃
        ┃ • • • • • ┃ • • • ▲ ▲ ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • S ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • • • • • ┃ • • • • • ┃ • • • S S ┃
        ┃ • • • • • ┃ • • • • ▲ ┃ ▲ • S S S ┃
        ┃ • • • • • ┃ • • • • ▲ ┃ ▲ S S D D ┃
        ┃ • • • • • ┃ • • • • ▲ ┃ ▲ S S D x ┃
        ┃ • • • • • ┃ • • • • • ┃ • S S D D ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)


def test_render_worldmap():
    definition = """
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • • • • • ┃ • • • • • ┃ • ▲ ▲ • ▲ ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • • ┃ • • • • • ┃ • • • ▲ ▲ ┃
        ┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • ▲ ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ ▲ ▲ ▲ • • ┃ • • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ • • ┃ • A • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ • • • • • ┃
        ┃ • • • • ▲ ┃ ▲ ▲ ▲ ▲ • ┃ 1 • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┃ • • • • • ┃ • • • • • ┃ • • • • • ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛
    """
    worldmap, symbols = parse(definition)

    def hook(position):
        for key, positions in symbols.items():
            if position in positions:
                return key
        return None

    assert "A" in worldmap.render(hook)
    assert "1" in worldmap.render(hook)
    assert "B" not in worldmap.render(hook)
    assert worldmap.render(hook).count("▲") == 44
    assert worldmap.render(hook).count("•") == 15 * 15 - 44 - 2
    assert worldmap.render().count("•") == 15 * 15 - 44


def test_find_biggest_water_area():
    worldmap, symbols = parse("""
        ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
        ┃ • • • • • ┃ ▲ • • • • ┃
        ┃ • • • • • ┃ ▲ • • • • ┃
        ┃ ▲ ▲ • • • ┃ • • • • • ┃
        ┃ ▲ ▲ • • ▲ ┃ ▲ ▲ • • • ┃
        ┃ o o o o ▲ ┃ ▲ ▲ • • • ┃
        ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
        ┃ o o o o ▲ ┃ ▲ ▲ • • • ┃
        ┃ o o o o • ┃ • • • • • ┃
        ┃ o o o o • ┃ ▲ ▲ ▲ • • ┃
        ┃ o o o o ▲ ┃ ▲ ▲ ▲ • • ┃
        ┃ o o o o ▲ ┃ ▲ ▲ ▲ ▲ • ┃
        ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
    """)

    assert worldmap.find_biggest_water_area().area() == len(symbols['o'])
