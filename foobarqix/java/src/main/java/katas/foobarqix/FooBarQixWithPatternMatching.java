package katas.foobarqix;

import io.vavr.Tuple;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Tuple3;
import static java.lang.String.valueOf;

/**
 */
public class FooBarQixWithPatternMatching {

    public static String fooBarQix(final int input) {
        return Match(Tuple.of(input % 3, input % 5, input % 7)).of(
                Case($Tuple3($(0), $(0), $(0)), "FooBarQix"),
                Case($Tuple3($(0), $(0), $()), "FooBar"),
                Case($Tuple3($(0), $(), $(0)), "FooQix"),
                Case($Tuple3($(), $(0), $(0)), "BarQix"),
                Case($Tuple3($(0), $(), $()), "Foo"),
                Case($Tuple3($(), $(0), $()), "Bar"),
                Case($Tuple3($(), $(), $(0)), "Qix"),
                Case($(), valueOf(input))
        );
    }
}
