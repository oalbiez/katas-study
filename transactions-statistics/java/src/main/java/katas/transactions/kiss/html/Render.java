package katas.transactions.kiss.html;

import katas.transactions.kiss.statistics.AllStatistics;
import katas.transactions.kiss.statistics.Balance;

import static katas.transactions.kiss.statistics.Statistics.ratio;

public class Render {
    public static String render(final AllStatistics statistics) {
        final StringBuilder builder = new StringBuilder();
        builder.append("<head>\n");
        builder.append("</head>\n");
        builder.append("<body>\n");
        /*builder.append("<style type=\"text/css\">\n");
        builder.append("table, th { border: 1px solid #333; }\n");
        builder.append("tr, td { padding: 3px; }\n");
        builder.append("tr { border-top-style: solid #333; }\n");
        builder.append("th { background-color: #aaa; }\n");
        builder.append("</style>\n");*/
        builder.append("<h2>Resume par clients</h2>\n");
        builder.append("<table rules=\"rows\" cellpadding=\"4\">\n");
        builder.append("<thead bgcolor=\"silver\">\n");
        builder.append("<tr>");
        builder.append("<th>").append("Client").append("</th>");
        builder.append("<th>").append("Number").append("</th>");
        builder.append("<th>").append("Credit").append("</th>");
        builder.append("<th>").append("Debit").append("</th>");
        builder.append("<th>").append("Solde").append("</th>");
        builder.append("<th>").append("Volume").append("</th>");
        builder.append("<th>").append("Ratio").append("</th>");
        builder.append("</tr>\n");
        builder.append("</thead>\n");

        builder.append("<tbody>\n");
        for (final String client : statistics.byClient().clients()) {
            final Balance balance = statistics.byClient().statisticFor(client);
            builder.append("<tr>");
            builder.append("<td>").append(client).append("</td>");
            builder.append("<td>").append(balance.count()).append("</td>");
            builder.append("<td>").append(balance.credit()).append("</td>");
            builder.append("<td>").append(balance.debt()).append("</td>");
            builder.append("<td>").append(balance.sold()).append("</td>");
            builder.append("<td>").append(balance.volume()).append("</td>");
            builder.append("<td>").append(ratio(balance.volume(), statistics.balance().volume())).append("</td>");
            builder.append("</tr>\n");
        }
        builder.append("</tbody>\n");

        builder.append("</table>\n");
        builder.append("</body>\n");
        return builder.toString();
    }
}
