﻿using System;
using System.Diagnostics.Contracts;

namespace Wallet
{
    public struct Currency : IEquatable<Currency>
    {
        private string Code { get; }
        public int Precision { get; }
        private string Name { get; }

        Currency(string code, int precision, string name)
        {
            Contract.Requires(code != null);
            Contract.Requires(precision >= 0);
            Contract.Requires(name != null);
            Code = code;
            Precision = precision;
            Name = name;
        }

        [Pure]
        public Money Amount(decimal value) => new Money(value, this);

        decimal Normalize(decimal value) => decimal.Round(value, Precision);

        public static bool operator ==(Currency left, Currency right) => Equals(left, right);

        public static bool operator !=(Currency left, Currency right) => !Equals(left, right);

        public override string ToString() => $"{Code}";

        public bool Equals(Currency other) => string.Equals(Code, other.Code) && Precision == other.Precision &&
                                              string.Equals(Name, other.Name);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Currency && Equals((Currency) obj);
        }

        public override int GetHashCode()
        {
            var hashCode = Code.GetHashCode();
            hashCode = (hashCode * 397) ^ Precision;
            hashCode = (hashCode * 397) ^ Name.GetHashCode();
            return hashCode;
        }

        // ReSharper disable InconsistentNaming, UnusedMember.Global
        public static readonly Currency AED = new Currency("AED", 2, "United Arab Emirates dirham");
        public static readonly Currency AFN = new Currency("AFN", 2, "Afghan afghani");
        public static readonly Currency ALL = new Currency("ALL", 2, "Albanian lek");
        public static readonly Currency AMD = new Currency("AMD", 2, "Armenian dram");
        public static readonly Currency ANG = new Currency("ANG", 2, "Netherlands Antillean guilder");
        public static readonly Currency AOA = new Currency("AOA", 2, "Angolan kwanza");
        public static readonly Currency ARS = new Currency("ARS", 2, "Argentine peso");
        public static readonly Currency AUD = new Currency("AUD", 2, "Australian dollar");
        public static readonly Currency AWG = new Currency("AWG", 2, "Aruban florin");
        public static readonly Currency AZN = new Currency("AZN", 2, "Azerbaijani manat");
        public static readonly Currency BAM = new Currency("BAM", 2, "Bosnia and Herzegovina convertible mark");
        public static readonly Currency BBD = new Currency("BBD", 2, "Barbados dollar");
        public static readonly Currency BDT = new Currency("BDT", 2, "Bangladeshi taka");
        public static readonly Currency BGN = new Currency("BGN", 2, "Bulgarian lev");
        public static readonly Currency BHD = new Currency("BHD", 3, "Bahraini dinar");
        public static readonly Currency BIF = new Currency("BIF", 0, "Burundian franc");
        public static readonly Currency BMD = new Currency("BMD", 2, "Bermudian dollar");
        public static readonly Currency BND = new Currency("BND", 2, "Brunei dollar");
        public static readonly Currency BOB = new Currency("BOB", 2, "Boliviano");
        public static readonly Currency BOV = new Currency("BOV", 2, "Bolivian Mvdol");
        public static readonly Currency BRL = new Currency("BRL", 2, "Brazilian real");
        public static readonly Currency BSD = new Currency("BSD", 2, "Bahamian dollar");
        public static readonly Currency BTN = new Currency("BTN", 2, "Bhutanese ngultrum");
        public static readonly Currency BWP = new Currency("BWP", 2, "Botswana pula");
        public static readonly Currency BYN = new Currency("BYN", 2, "Belarusian ruble");
        public static readonly Currency BZD = new Currency("BZD", 2, "Belize dollar");
        public static readonly Currency CAD = new Currency("CAD", 2, "Canadian dollar");
        public static readonly Currency CDF = new Currency("CDF", 2, "Congolese franc");
        public static readonly Currency CHE = new Currency("CHE", 2, "WIR Euro (complementary currency)");
        public static readonly Currency CHF = new Currency("CHF", 2, "Swiss franc");
        public static readonly Currency CHW = new Currency("CHW", 2, "WIR Franc (complementary currency)");
        public static readonly Currency CLF = new Currency("CLF", 4, "Unidad de Fomento");
        public static readonly Currency CLP = new Currency("CLP", 0, "Chilean peso");
        public static readonly Currency CNY = new Currency("CNY", 2, "Chinese yuan");
        public static readonly Currency COP = new Currency("COP", 2, "Colombian peso");
        public static readonly Currency COU = new Currency("COU", 2, "Unidad de Valor Real (UVR)");
        public static readonly Currency CRC = new Currency("CRC", 2, "Costa Rican colon");
        public static readonly Currency CUC = new Currency("CUC", 2, "Cuban convertible peso");
        public static readonly Currency CUP = new Currency("CUP", 2, "Cuban peso");
        public static readonly Currency CVE = new Currency("CVE", 0, "Cape Verde escudo");
        public static readonly Currency CZK = new Currency("CZK", 2, "Czech koruna");
        public static readonly Currency DASH = new Currency("DASH", 8, "Dash");
        public static readonly Currency DJF = new Currency("DJF", 0, "Djiboutian franc");
        public static readonly Currency DKK = new Currency("DKK", 2, "Danish krone");
        public static readonly Currency DOP = new Currency("DOP", 2, "Dominican peso");
        public static readonly Currency DZD = new Currency("DZD", 2, "Algerian dinar");
        public static readonly Currency EGP = new Currency("EGP", 2, "Egyptian pound");
        public static readonly Currency ERN = new Currency("ERN", 2, "Eritrean nakfa");
        public static readonly Currency ETB = new Currency("ETB", 2, "Ethiopian birr");
        public static readonly Currency ETH = new Currency("ETH", 2, "Ether");
        public static readonly Currency EUR = new Currency("EUR", 2, "Euro");
        public static readonly Currency FJD = new Currency("FJD", 2, "Fiji dollar");
        public static readonly Currency FKP = new Currency("FKP", 2, "Falkland Islands pound");
        public static readonly Currency GBP = new Currency("GBP", 2, "Pound sterling");
        public static readonly Currency GEL = new Currency("GEL", 2, "Georgian lari");
        public static readonly Currency GHS = new Currency("GHS", 2, "Ghanaian cedi");
        public static readonly Currency GIP = new Currency("GIP", 2, "Gibraltar pound");
        public static readonly Currency GMD = new Currency("GMD", 2, "Gambian dalasi");
        public static readonly Currency GNF = new Currency("GNF", 0, "Guinean franc");
        public static readonly Currency GTQ = new Currency("GTQ", 2, "Guatemalan quetzal");
        public static readonly Currency GYD = new Currency("GYD", 2, "Guyanese dollar");
        public static readonly Currency HKD = new Currency("HKD", 2, "Hong Kong dollar");
        public static readonly Currency HNL = new Currency("HNL", 2, "Honduran lempira");
        public static readonly Currency HRK = new Currency("HRK", 2, "Croatian kuna");
        public static readonly Currency HTG = new Currency("HTG", 2, "Haitian gourde");
        public static readonly Currency HUF = new Currency("HUF", 2, "Hungarian forint");
        public static readonly Currency IDR = new Currency("IDR", 2, "Indonesian rupiah");
        public static readonly Currency ILS = new Currency("ILS", 2, "Israeli new shekel");
        public static readonly Currency INR = new Currency("INR", 2, "Indian rupee");
        public static readonly Currency IOT = new Currency("IOT", 0, "IOTA");
        public static readonly Currency IQD = new Currency("IQD", 3, "Iraqi dinar");
        public static readonly Currency IRR = new Currency("IRR", 2, "Iranian rial");
        public static readonly Currency ISK = new Currency("ISK", 0, "Icelandic króna");
        public static readonly Currency JMD = new Currency("JMD", 2, "Jamaican dollar");
        public static readonly Currency JOD = new Currency("JOD", 3, "Jordanian dinar");
        public static readonly Currency JPY = new Currency("JPY", 0, "Japanese yen");
        public static readonly Currency KES = new Currency("KES", 2, "Kenyan shilling");
        public static readonly Currency KGS = new Currency("KGS", 2, "Kyrgyzstani som");
        public static readonly Currency KHR = new Currency("KHR", 2, "Cambodian riel");
        public static readonly Currency KMF = new Currency("KMF", 0, "Comoro franc");
        public static readonly Currency KPW = new Currency("KPW", 2, "North Korean won");
        public static readonly Currency KRW = new Currency("KRW", 2, "South Korean won");
        public static readonly Currency KWD = new Currency("KWD", 3, "Kuwaiti dinar");
        public static readonly Currency KYD = new Currency("KYD", 2, "Cayman Islands dollar");
        public static readonly Currency KZT = new Currency("KZT", 2, "Kazakhstani tenge");
        public static readonly Currency LAK = new Currency("LAK", 2, "Lao kip");
        public static readonly Currency LBP = new Currency("LBP", 2, "Lebanese pound");
        public static readonly Currency LKR = new Currency("LKR", 2, "Sri Lankan rupee");
        public static readonly Currency LRD = new Currency("LRD", 2, "Liberian dollar");
        public static readonly Currency LSL = new Currency("LSL", 2, "Lesotho loti");
        public static readonly Currency LYD = new Currency("LYD", 3, "Libyan dinar");
        public static readonly Currency MAD = new Currency("MAD", 2, "Moroccan dirham");
        public static readonly Currency MDL = new Currency("MDL", 2, "Moldovan leu");
        public static readonly Currency MGA = new Currency("MGA", 1, "Malagasy ariary");
        public static readonly Currency MKD = new Currency("MKD", 2, "Macedonian denar");
        public static readonly Currency MMK = new Currency("MMK", 2, "Myanmar kyat");
        public static readonly Currency MNT = new Currency("MNT", 2, "Mongolian tögrög");
        public static readonly Currency MOP = new Currency("MOP", 2, "Macanese pataca");
        public static readonly Currency MRO = new Currency("MRO", 1, "Mauritanian ouguiya");
        public static readonly Currency MUR = new Currency("MUR", 2, "Mauritian rupee");
        public static readonly Currency MVR = new Currency("MVR", 2, "Maldivian rufiyaa");
        public static readonly Currency MWK = new Currency("MWK", 2, "Malawian kwacha");
        public static readonly Currency MXN = new Currency("MXN", 2, "Mexican peso");
        public static readonly Currency MXV = new Currency("MXV", 2, "Mexican Unidad de Inversion (UDI)");
        public static readonly Currency MYR = new Currency("MYR", 2, "Malaysian ringgit");
        public static readonly Currency MZN = new Currency("MZN", 2, "Mozambican metical");
        public static readonly Currency NAD = new Currency("NAD", 2, "Namibian dollar");
        public static readonly Currency NGN = new Currency("NGN", 2, "Nigerian naira");
        public static readonly Currency NIO = new Currency("NIO", 2, "Nicaraguan córdoba");
        public static readonly Currency NOK = new Currency("NOK", 2, "Norwegian krone");
        public static readonly Currency NPR = new Currency("NPR", 2, "Nepalese rupee");
        public static readonly Currency NZD = new Currency("NZD", 2, "New Zealand dollar");
        public static readonly Currency OMR = new Currency("OMR", 3, "Omani rial");
        public static readonly Currency PAB = new Currency("PAB", 2, "Panamanian balboa");
        public static readonly Currency PEN = new Currency("PEN", 2, "Peruvian Sol");
        public static readonly Currency PGK = new Currency("PGK", 2, "Papua New Guinean kina");
        public static readonly Currency PHP = new Currency("PHP", 2, "Philippine peso");
        public static readonly Currency PKR = new Currency("PKR", 2, "Pakistani rupee");
        public static readonly Currency PLN = new Currency("PLN", 2, "Polish złoty");
        public static readonly Currency PYG = new Currency("PYG", 0, "Paraguayan guaraní");
        public static readonly Currency QAR = new Currency("QAR", 2, "Qatari riyal");
        public static readonly Currency RON = new Currency("RON", 2, "Romanian leu");
        public static readonly Currency RSD = new Currency("RSD", 2, "Serbian dinar");
        public static readonly Currency RUB = new Currency("RUB", 2, "Russian ruble");
        public static readonly Currency RWF = new Currency("RWF", 0, "Rwandan franc");
        public static readonly Currency SAR = new Currency("SAR", 2, "Saudi riyal");
        public static readonly Currency SBD = new Currency("SBD", 2, "Solomon Islands dollar");
        public static readonly Currency SCR = new Currency("SCR", 2, "Seychelles rupee");
        public static readonly Currency SDG = new Currency("SDG", 2, "Sudanese pound");
        public static readonly Currency SEK = new Currency("SEK", 2, "Swedish krona/kronor");
        public static readonly Currency SGD = new Currency("SGD", 2, "Singapore dollar");
        public static readonly Currency SHP = new Currency("SHP", 2, "Saint Helena pound");
        public static readonly Currency SLL = new Currency("SLL", 2, "Sierra Leonean leone");
        public static readonly Currency SOS = new Currency("SOS", 2, "Somali shilling");
        public static readonly Currency SRD = new Currency("SRD", 2, "Surinamese dollar");
        public static readonly Currency SSP = new Currency("SSP", 2, "South Sudanese pound");
        public static readonly Currency STD = new Currency("STD", 2, "São Tomé and Príncipe dobra");
        public static readonly Currency SVC = new Currency("SVC", 2, "Salvadoran colón");
        public static readonly Currency SYP = new Currency("SYP", 2, "Syrian pound");
        public static readonly Currency SZL = new Currency("SZL", 2, "Swazi lilangeni");
        public static readonly Currency THB = new Currency("THB", 2, "Thai baht");
        public static readonly Currency TJS = new Currency("TJS", 2, "Tajikistani somoni");
        public static readonly Currency TMT = new Currency("TMT", 2, "Turkmenistan manat");
        public static readonly Currency TND = new Currency("TND", 3, "Tunisian dinar");
        public static readonly Currency TOP = new Currency("TOP", 2, "Tongan paʻanga");
        public static readonly Currency TRY = new Currency("TRY", 2, "Turkish lira");
        public static readonly Currency TTD = new Currency("TTD", 2, "Trinidad and Tobago dollar");
        public static readonly Currency TWD = new Currency("TWD", 2, "New Taiwan dollar");
        public static readonly Currency TZS = new Currency("TZS", 2, "Tanzanian shilling");
        public static readonly Currency UAH = new Currency("UAH", 2, "Ukrainian hryvnia");
        public static readonly Currency UGX = new Currency("UGX", 0, "Ugandan shilling");
        public static readonly Currency USD = new Currency("USD", 3, "United States dollar");
        public static readonly Currency USN = new Currency("USN", 2, "United States dollar (next day)");
        public static readonly Currency UYI = new Currency("UYI", 0, "Uruguay Peso en Unidades Indexadas (URUIURUI)");
        public static readonly Currency UYU = new Currency("UYU", 2, "Uruguayan peso");
        public static readonly Currency UZS = new Currency("UZS", 2, "Uzbekistan som");
        public static readonly Currency VEF = new Currency("VEF", 2, "Venezuelan bolívar");
        public static readonly Currency VND = new Currency("VND", 0, "Vietnamese đồng");
        public static readonly Currency VUV = new Currency("VUV", 0, "Vanuatu vatu");
        public static readonly Currency WST = new Currency("WST", 2, "Samoan tala");
        public static readonly Currency XAF = new Currency("XAF", 0, "CFA franc BEAC");
        public static readonly Currency XAG = new Currency("XAG", 8, "Silver (one troy ounce)");
        public static readonly Currency XAU = new Currency("XAU", 8, "Gold (one troy ounce)");
        public static readonly Currency XBC = new Currency("XBC", 8, "Bitcoin Cash");
        public static readonly Currency XBT = new Currency("XBT", 8, "Bitcoin Cash");
        public static readonly Currency XCD = new Currency("XCD", 2, "East Caribbean dollar");
        public static readonly Currency XLM = new Currency("XLM", 8, "Stellar Lumen");
        public static readonly Currency XMR = new Currency("XMR", 8, "Monero");
        public static readonly Currency XOF = new Currency("XOF", 0, "CFA franc BCEAO");
        public static readonly Currency XPD = new Currency("XPD", 8, "Palladium (one troy ounce)");
        public static readonly Currency XPF = new Currency("XPF", 0, "CFP franc (franc Pacifique)");
        public static readonly Currency XPT = new Currency("XPT", 8, "Platinum (one troy ounce)");
        public static readonly Currency XRP = new Currency("XRP", 8, "Ripple");
        public static readonly Currency XSU = new Currency("XSU", 8, "SUCRE");
        public static readonly Currency XZC = new Currency("XZC", 8, "Zcoin");
        public static readonly Currency YER = new Currency("YER", 2, "Yemeni rial");
        public static readonly Currency ZAR = new Currency("ZAR", 2, "South African rand");
        public static readonly Currency ZEC = new Currency("ZEC", 8, "Zcash");
        public static readonly Currency ZMW = new Currency("ZMW", 2, "Zambian kwacha");

        public static readonly Currency ZWL = new Currency("ZWL", 2, "Zimbabwean dollar A/10");
        // ReSharper restore InconsistentNaming, UnusedMember.Global
    }
}