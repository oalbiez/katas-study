{-# LANGUAGE OverloadedStrings #-}
module SituationAwarnessSpec (spec) where


import qualified Control.Monad as Monad
import qualified Data.HashSet  as HashSet
import qualified Data.List     as List
import qualified Data.Map      as Map
import           OceanOfCode
import           Test.Hspec
import           WorldMapDSL


data TrackingDebug
    = ON | OFF
    deriving (Eq)

type Action = TrackingDebug -> SituationAwarness -> IO SituationAwarness

with :: (SituationAwarness -> SituationAwarness) -> Action
with action _ situation = return $ action situation

turn :: TurnData -> Action
turn turn = with $ updateTurn turn

orders :: String -> Action
orders orders = with $ updateMyOrders (parseOrders orders)

checkPositions :: String -> Tracking -> String -> Action
checkPositions context tracking definition debug situation =
    let (_, symbols) = parse definition
        expectedSolutions = List.sort $ Map.findWithDefault [] '◎' symbols
        solutions = List.sort $ HashSet.toList $ solutionsOf tracking
        display = cricleFor solutions
    in do
        Monad.when (debug == ON) $
            do
                putStrLn context
                putStrLn (render display (theWorldmap situation))

        solutions `shouldMatchList` expectedSolutions
        return situation

opponentPositions :: String -> Action
opponentPositions definition debug situation =
    checkPositions "Opponent positions" (opponentTracking situation) definition debug situation

myPositions :: String -> Action
myPositions definition debug situation =
    checkPositions "My positions" (myTracking situation) definition debug situation

checkMines :: String -> Tracking -> String -> Action
checkMines context tracking definition debug situation =
    let (_, symbols) = parse definition
        expectedMines = List.sort $ Map.findWithDefault [] '☠' symbols
        mines = List.sort $ HashSet.toList $ minesOf tracking
        display = deathFor mines
    in do
        Monad.when (debug == ON) $
            do
                putStrLn context
                putStrLn (render display (theWorldmap situation))

        mines `shouldMatchList` expectedMines
        return situation

opponentMines :: String -> Action
opponentMines definition debug situation =
    checkMines "Opponent mines" (opponentTracking situation) definition debug situation

myMines :: String -> Action
myMines definition debug situation =
    checkMines "My mines" (myTracking situation) definition debug situation

scenario :: TrackingDebug -> String -> [Action] -> IO ()
scenario debug definition actions =
    let
        (wm, _) = parse definition
        process [] situation     = return situation
        process (a:xa) situation = a debug situation >>= process xa
    in do
        _ <- process actions (initialSituation wm)
        return ()

minimap :: String
minimap = "\
    \┏━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
    \┃ • • • • • ┃ • • ▲ ▲ • ┃\n\
    \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
    \┃ ▲ ▲ • • • ┃ • • • • • ┃\n\
    \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
    \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
    \┣━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
    \┃ • • • • ▲ ┃ ▲ ▲ • • • ┃\n\
    \┃ • • • • • ┃ • • • • • ┃\n\
    \┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
    \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
    \┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃\n\
    \┗━━━━━━━━━━━┻━━━━━━━━━━━┛"


spec :: Spec
spec =


    it "situation awarness with sample scenario" $
        scenario OFF "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ • ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • • • ┃ • ▲ ▲ ▲ • ┃ • • • • ▲ ┃\n\
            \┃ • • • • • ┃ • ▲ ▲ ▲ • ┃ • • • • ▲ ┃\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • ▲ ▲ ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            [ turn TurnData
                { turnMyPosition   = point 11 8
                , turnMyLife       = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 3 4 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "NA"
                }
            , orders "MOVE S SONAR"
            , turn TurnData
                { turnMyPosition   = point 11 9
                , turnMyLife       = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 3 3 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE N"
                }
            , orders "MOVE W SONAR"
            , turn TurnData
                { turnMyPosition   = point 10 9
                , turnMyLife       = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 3 2 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE E"
                }
            , orders "MOVE N SONAR"
            , turn TurnData
                { turnMyPosition   = point 10 8
                , turnMyLife       = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 3 1 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE E"
                }
            , orders "MOVE N SONAR"
            , turn TurnData
                { turnMyPosition   = point 10 7
                , turnMyLife       = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 3 0 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE E"
                }
            , orders "SONAR 9|MOVE E TORPEDO"
            , turn TurnData
                { turnMyPosition   = point 11 7
                , turnMyLife       = 6
                , turnMySonarEcho  = Miss
                , turnMySubsystems = Subsystem 2 4 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE S"
                }
            , opponentPositions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ • ▲ ▲ • ┃ • • • ◎ ◎ ┃\n\
            \┃ • • • ◎ • ┃ • • • • • ┃ • • ◎ ◎ ◎ ┃\n\
            \┃ • • • ◎ ◎ ┃ ◎ ▲ ▲ ◎ • ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┃ • • • ◎ ◎ ┃ ◎ ▲ ▲ • • ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • ◎ ◎ ┃ ◎ ▲ ▲ ▲ • ┃ • • ◎ ◎ ▲ ┃\n\
            \┃ • • • ◎ ◎ ┃ ◎ ▲ ▲ ▲ • ┃ • • ◎ ◎ ▲ ┃\n\
            \┃ • • • ▲ ▲ ┃ ◎ ▲ ▲ ▲ • ┃ • • ◎ ◎ • ┃\n\
            \┃ • • • ▲ ▲ ┃ • • • • • ┃ • • ◎ ◎ ◎ ┃\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ ◎ • ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ • • ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • • • • ┃\n\
            \┃ • • • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • • • • ┃\n\
            \┃ • • • ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            , myPositions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ◎ ▲ ┃ ▲ • ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ◎ ◎ ┃ ◎ • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ◎ ◎ ┃ ◎ ▲ ▲ • • ┃ ◎ ◎ ◎ ◎ • ┃\n\
            \┃ • ◎ ◎ ◎ ◎ ┃ ◎ ▲ ▲ • • ┃ ◎ ◎ ◎ ◎ • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • ◎ ◎ • • ┃ • ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ▲ ┃\n\
            \┃ • ◎ ◎ • • ┃ • ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ▲ ┃\n\
            \┃ • ◎ ◎ ▲ ▲ ┃ • ▲ ▲ ▲ • ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ▲ ▲ ┃ • • • • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ▲ ▲ ┃ • ▲ ▲ • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • ◎ ◎ ▲ ▲ ┃ • ▲ ▲ • ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"

            , orders "MOVE N SONAR"
            , turn TurnData
                { turnMyPosition   = point 11  6
                , turnMyLife = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 2 3 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE W"
                }
            , orders "MOVE W SONAR"
            , turn TurnData
                { turnMyPosition   = point 10  6
                , turnMyLife = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 2 2 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE W"
                }
            , orders "MOVE W SONAR"
            , turn TurnData
                { turnMyPosition   = point 9  6
                , turnMyLife = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 2 1 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE S"
                }
            , orders "MOVE S SONAR"
            , turn TurnData
                { turnMyPosition   = point 9  7
                , turnMyLife = 6
                , turnMySonarEcho  = NA
                , turnMySubsystems = Subsystem 2 0 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "MOVE W"
                }
            , opponentPositions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ • ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ • • • • ┃ • ▲ ▲ • ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ • • ┃ • ▲ ▲ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ • • ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ • • ▲ ┃\n\
            \┃ ◎ ◎ ◎ • • ┃ • ▲ ▲ ▲ ◎ ┃ ◎ • • • ▲ ┃\n\
            \┃ ◎ ◎ • ▲ ▲ ┃ • ▲ ▲ ▲ ◎ ┃ ◎ • • • • ┃\n\
            \┃ • • • ▲ ▲ ┃ • • • • ◎ ┃ ◎ • • • • ┃\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ • ◎ ┃ ◎ ◎ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ • • • ▲ ▲ ┃ • ▲ ▲ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ • • • • • ┃ • • • ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃ • • • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"

            , orders "SONAR 7|MOVE S TORPEDO"
            , turn TurnData
                { turnMyPosition   = point 9  8
                , turnMyLife = 6
                , turnMySonarEcho  = Miss
                , turnMySubsystems = Subsystem 1 4 6 3
                , turnOpponentLife = 6
                , turnIntelligence = parseReport "SILENCE|MOVE S"
                }
            , opponentPositions "\
            \┏━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓\n\
            \┃ • ▲ ▲ • ▲ ┃ ▲ • ▲ ▲ ▲ ┃ • • • • • ┃\n\
            \┃ • • • • ▲ ┃ ▲ • ▲ ▲ • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ • • • • • ┃ • • • • • ┃\n\
            \┃ • • • • • ┃ ◎ ▲ ▲ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ • • • • ┃ • ▲ ▲ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ • • ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ • • ▲ ┃\n\
            \┃ ◎ ◎ ◎ • • ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ • • ▲ ┃\n\
            \┃ ◎ ◎ ◎ ▲ ▲ ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ ▲ ▲ ┃ • • • • ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ ▲ ▲ ┃ ◎ ▲ ▲ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━━┫\n\
            \┃ ◎ ◎ ◎ ▲ ▲ ┃ • ▲ ▲ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ • • ┃ • • • ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ • • • ┃ • • • ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┃ ◎ ◎ ◎ ◎ ◎ ┃ ◎ ◎ • ◎ ◎ ┃ ◎ ◎ • • • ┃\n\
            \┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛\n"
            ]

