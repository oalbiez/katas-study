﻿using System;
using System.Collections.Generic;

namespace MovieRental
{
    public class Pricing
    {
        private readonly List<IRule> _rules = new List<IRule>();

        private Pricing(IRule rule)
        {
            _rules.Add(rule);
        }

        public static Pricing Build(IRule rule) => new Pricing(rule);


        public Pricing Then(IRule rule)
        {
            _rules.Add(rule);
            return this;
        }

        public Price ChargeFor(Duration duration) => new PriceCalculator(_rules, duration).Total;

        public string Describe()
        {
            return string.Join(" then ", _rules.ConvertAll(rule => rule.Describe()));
        }

        public string Explain(Duration duration) => new PriceCalculator(_rules, duration).Audit;
    }

    public static class Rules
    {
        public static IRule Cost(Price price) => new FlatRule(price);
    }

    public class Overflow : Exception
    {
    }

    public class MalformedPricing : Exception
    {
        public MalformedPricing(string message) : base(message)
        {
        }
    }

    public interface IRule
    {
        IRule PerDay();
        IRule For(Duration duration);
        Price PriceFor(Duration duration);
        Duration Consume(Duration remaining);
        string Describe();
    }

    internal sealed class FlatRule : IRule
    {
        private readonly Price _price;

        public FlatRule(Price price)
        {
            _price = price;
        }

        public IRule PerDay() => new ProportionalRule(_price);

        public IRule For(Duration duration) => new LimitedDurationRule(duration, this);

        public Price PriceFor(Duration duration) => _price;
        public Duration Consume(Duration remaining) => Duration.Null();
        public string Describe() => $"cost {_price}";
    }

    internal sealed class ProportionalRule : IRule
    {
        private readonly Price _price;

        public ProportionalRule(Price price)
        {
            _price = price;
        }

        public IRule PerDay()
        {
            throw new MalformedPricing("cannont apply PerDay more than one on a rule");
        }

        public IRule For(Duration duration) => new LimitedDurationRule(duration, this);

        public Price PriceFor(Duration duration) => _price * duration.Value;
        public Duration Consume(Duration remaining) => Duration.Null();
        public string Describe() => $"cost {_price} per day";
    }

    internal sealed class LimitedDurationRule : IRule
    {
        private readonly Duration _duration;
        private readonly IRule _inner;

        public LimitedDurationRule(Duration duration, IRule inner)
        {
            _duration = duration;
            _inner = inner;
        }

        public IRule PerDay()
        {
            throw new MalformedPricing("cannont apply PerDay after a For rule");
        }

        public IRule For(Duration duration)
        {
            throw new MalformedPricing("cannont apply For more than one on a rule");
        }

        public Price PriceFor(Duration duration) => _inner.PriceFor(Duration.Min(duration, _duration));
        public Duration Consume(Duration remaining) => remaining - _duration;
        public string Describe() => $"{_inner.Describe()} for {_duration.Value} day(s)";
    }

    internal sealed class PriceCalculator
    {
        private readonly List<string> _audit = new List<string>();
        private readonly bool _overflow;
        private readonly Price _total = Price.Euro(0);

        public PriceCalculator(IEnumerable<IRule> rules, Duration duration)
        {
            var remaining = duration;
            foreach (var rule in rules)
            {
                var price = rule.PriceFor(remaining);
                _total += price;
                remaining = rule.Consume(remaining);
                _audit.Add($"rule [{rule.Describe()}]: price={price}, total={_total}, remaining={remaining.Value}");
                if (remaining.IsNull())
                {
                    return;
                }
            }

            if (remaining.IsNotNull())
            {
                _audit.Add($"overflow of {remaining.Value}");
                _overflow = true;
            }
        }

        public string Audit => string.Join("; ", _audit);

        public Price Total
        {
            get
            {
                if (_overflow)
                {
                    throw new Overflow();
                }

                return _total;
            }
        }
    }
}