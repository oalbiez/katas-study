package katas.buyer.model.ast;

public interface ExpressionVisitor<T> {

    T acceptAdd(Expression left, Expression right);

    T acceptSub(Expression left, Expression right);

    T acceptMul(Expression left, Expression right);

    T acceptDiv(Expression left, Expression right);

    T acceptOfferPrice();

    T acceptOfferQuantity();

    T acceptRequestedQuantity();
}
