module MathTests exposing (all)

import Expect
import Fuzz exposing (Fuzzer, float)
import Math exposing (Source(..), combineLcm, gcd, lcm, som, sumOfOneToN, sumOfmultiple)
import Random
import Shrink
import Test exposing (Test, describe, fuzz, fuzz2, test)


all : Test
all =
    describe "Math" <|
        [ describe "greatest common divisor"
            [ test "two same values should return the value" <|
                \_ ->
                    gcd 1 1
                        |> Expect.equal 1
            , test "general case" <|
                \_ ->
                    gcd 45 30
                        |> Expect.equal 15
            , test "commutative" <|
                \_ ->
                    gcd 30 45
                        |> Expect.equal 15
            , test "bug" <|
                \_ ->
                    gcd 210 45
                        |> Expect.equal 15
            ]
        , describe "least common multiple"
            [ test "general case" <|
                \_ ->
                    lcm 210 45
                        |> Expect.equal 630
            ]
        , describe "sum of n first integer should"
            [ test "1" <|
                \_ ->
                    sumOfOneToN 0
                        |> Expect.equal 0
            , test "2" <|
                \_ ->
                    sumOfOneToN 50
                        |> Expect.equal 1275
            ]
        , describe "combineLcm"
            [ test "combineLcm of two simple values" <|
                \_ ->
                    combineLcm (Simple 3) (Simple 5)
                        |> Expect.equal (Simple 15)
            , test "combineLcm of a simple value and a binary value" <|
                \_ ->
                    combineLcm (Simple 3) (Composition (Simple 5) (Simple 7))
                        |> Expect.equal (Composition (Simple 15) (Simple 21))
            , test "combineLcm is comutative" <|
                \_ ->
                    combineLcm (Composition (Simple 5) (Simple 7)) (Simple 3)
                        |> Expect.equal (Composition (Simple 15) (Simple 21))
            , test "combineLcm of two composition" <|
                \_ ->
                    combineLcm
                        (Composition (Simple 3) (Simple 5))
                        (Composition (Simple 7) (Simple 11))
                        |> Expect.equal
                            (Composition
                                (Composition (Simple 21) (Simple 33))
                                (Composition (Simple 35) (Simple 55))
                            )
            ]
        , describe "sum of multiples"
            [ test "with single value" <|
                \_ ->
                    sumOfmultiple 20 (Simple 3)
                        |> Expect.equal (3 + 6 + 9 + 12 + 15 + 18)
            , test "with single value 2" <|
                \_ ->
                    sumOfmultiple 20 (Simple 5)
                        |> Expect.equal (5 + 10 + 15)
            , test "with two values" <|
                \_ ->
                    sumOfmultiple 20 (Composition (Simple 3) (Simple 5))
                        |> Expect.equal (3 + 5 + 6 + 9 + 10 + 12 + 15 + 18)
            , test "with multiples values" <|
                \_ ->
                    sumOfmultiple 20 (Composition (Simple 3) (Composition (Simple 5) (Simple 7)))
                        |> Expect.equal (3 + 5 + 6 + 7 + 9 + 10 + 12 + 14 + 15 + 18)
            , test "som" <|
                \_ ->
                    som 20 [ 3, 5, 7 ]
                        |> Expect.equal (3 + 5 + 6 + 7 + 9 + 10 + 12 + 14 + 15 + 18)
            ]
        ]
