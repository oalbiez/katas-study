package katas.foobarqix;


import static katas.foobarqix.FooBarQixWithDsl.*;

public class FooBarQixWithDslShould extends FooBarQixShould {
    private final static FooBarQixWithDsl RULES = rules(
            when(isDivisibleBy(3)).then("Foo"),
            when(isDivisibleBy(5)).then("Bar"),
            when(isDivisibleBy(7)).then("Qix"));

    String fooBarQix(final int input) {
        return RULES.compute(input);
    }
}
