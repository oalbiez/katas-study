package katas.foobarqix;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.Function;

/**
 */
public class Bitwise {

    private static List<Function<Integer, String>> values = buildValues();

    @SuppressWarnings("unchecked")
    public static String fooBarQix(final int input) {
        int divisibleBy3 = input % 3 == 0 ? 1 : 0;
        int divisibleBy5 = input % 5 == 0 ? 1 : 0;
        int divisibleBy7 = input % 7 == 0 ? 1 : 0;
        int index = divisibleBy7 << 2 | divisibleBy5 << 1 | divisibleBy3;
        return values.get(index).apply(input);
    }

    private static List<Function<Integer, String>> buildValues() {
        return Lists.newArrayList(
                String::valueOf, // 0: 0b000
                i -> "Foo",      // 1: 0b001
                i -> "Bar",      // 2: 0b010
                i -> "FooBar",   // 3: 0b011
                i -> "Qix",      // 4: 0b100
                i -> "FooQix",   // 5: 0b101
                i -> "BarQix",   // 6: 0b110
                i -> "FooBarQix" // 7: 0b111
        );
    }
}
