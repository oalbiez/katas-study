package katas.foobarqix;

import static java.lang.String.valueOf;

/**
 */
public class FooBarQixWithTernary {
    public static String fooBarQix(final int input) {
        String accumulator = (isDivisibleBy(input, 3) ? "Foo" : "") +
                (isDivisibleBy(input, 5) ? "Bar" : "") +
                (isDivisibleBy(input, 7) ? "Qix" : "");
        return accumulator.isEmpty() ? valueOf(input) : accumulator;
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
