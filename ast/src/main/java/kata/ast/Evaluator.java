package kata.ast;

public class Evaluator {

    public static int evaluate(AST ast) {
        return ast.accept(new ASTVisitor<Integer>() {
            public Integer visitNumber(Number number) {
                return number.value();
            }

            public Integer visitAdd(Add add) {
                return (add.left().accept(this) + add.right().accept(this));
            }

            public Integer visitSub(Sub sub) {
                return sub.left().accept(this) - sub.right().accept(this);
            }
        });
    }

}
