package katas.marsrover;

public class MoveCompleted implements Move {
    private final Position position;

    public MoveCompleted(Position position) {
        this.position = position;
    }

    public Position finalPosition() {
        return position;
    }

    public Move move(Command command) {
        return null;
    }
}
