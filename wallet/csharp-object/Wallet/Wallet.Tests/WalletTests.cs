﻿using NUnit.Framework;
using static NUnit.Framework.Assert;
using static Wallet.Currency;
using static Wallet.FixedRateProvider;

namespace Wallet.Tests
{
    [TestFixture]
    public class WalletTests
    {
        [Test]
        public void empty_wallet_should_value_zero_in_euro()
        {
            AreEqual(EUR.Amount(0), Wallet().Value(EUR, Fixed()));
        }

        [Test]
        public void empty_wallet_should_value_zero_in_dollar()
        {
            AreEqual(USD.Amount(0), Wallet().Value(USD, Fixed()));
        }

        [Test]
        public void wallet_with_stock_in_euros_should_sum_of_values()
        {
            AreEqual(EUR.Amount(15), Wallet(EUR.Amount(5), EUR.Amount(10)).Value(EUR, Fixed()));
        }

        [Test]
        public void wallet_with_stock_in_different_currency_should_use_change()
        {
            AreEqual(EUR.Amount(5), Wallet(USD.Amount(10)).Value(EUR, Fixed().Rate(USD, EUR, 0.5m)));
        }

        [Test]
        public void wallet_with_stock_in_different_currencies_should_use_change()
        {
            AreEqual(EUR.Amount(10.79m),
                Wallet(EUR.Amount(10), DZD.Amount(50), KRW.Amount(200), XBT.Amount(0.00002354m))
                    .Value(EUR, Fixed().Rate(DZD, EUR, 0.01m).Rate(KRW, EUR, 0.001m).Rate(XBT, EUR, 4000)));
        }


        [Test]
        public void wallet_should_round_the_whole_sum()
        {
            AreEqual(EUR.Amount(0.01m),
                Wallet(KRW.Amount(1), KRW.Amount(1), KRW.Amount(1), KRW.Amount(1), KRW.Amount(1), KRW.Amount(1),
                        KRW.Amount(1), KRW.Amount(1), KRW.Amount(1), KRW.Amount(1))
                    .Value(EUR, Fixed().Rate(KRW, EUR, 0.001m)));
        }

        private static Wallet Wallet(params Money[] values)
        {
            return new Wallet(values);
        }
    }
}