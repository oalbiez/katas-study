package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.srp.ast.ValueProvider;
import katas.transactions.value.Value;
import katas.transactions.value.Values;

public final class SumOf implements Statistic {
    private final ValueProvider<Long> provider;
    private long statistic = 0;
    private long count = 0;

    SumOf(final ValueProvider<Long> provider) {
        this.provider = provider;
    }

    public void updateWith(final Transaction transaction) {
        statistic += provider.get(transaction);
        count += 1;
    }

    public void accept(final StatisticVisitor visitor) {
        visitor.visitSum(this);
    }

    public Statistic fork() {
        return new SumOf(provider);
    }

    public String describe() {
        return "sum of " + provider.describe();
    }

    public Value value() {
        return Values.value(statistic);
    }

    public Value mean() {
        return Values.value(statistic / (double) count);
    }
}
