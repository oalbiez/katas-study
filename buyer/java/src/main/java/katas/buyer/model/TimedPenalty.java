package katas.buyer.model;

public final class TimedPenalty {
    private final Price penalty;
    private final Delay after;

    public TimedPenalty(final Price penalty, final Delay after) {
        this.penalty = penalty;
        this.after = after;
    }

    public Price evaluate(final Delay real) {
        return penalty.mul(real.sub(after).clampToZero());
    }
}
