# Todo List

This project is for training purpose to learn some architectural principles.

1. Hexagonal Architecture, with models and usecases.
2. Immutability whenever possible
3. Use Maybe instead of null
4. Encapsulate all primitives types in value objects


What next ?
- implements différent store (SQL, noSQL, s3, ...)
- implement differents delivery method (rest, command line, html, ...)
- play with different framework (knex, express, ...)
