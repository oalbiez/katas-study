from collections import namedtuple


Product = namedtuple('CartItem', 'reference label price')
Reference = namedtuple('Reference', 'value')


class Catalog(object):
    def __init__(self, products):
        self.__stock = dict((product.reference, product) for product in products)

    def find_product(self, reference):
        return self.__stock[reference]

    def price_for(self, reference):
        return self.find_product(reference).price
