{-# LANGUAGE OverloadedStrings #-}
module ParserSpec (spec) where


import qualified Data.Either   as Either
import           Data.Function ((&))
import           OceanOfCode
import           Test.Hspec
import qualified Text.Parsec   as Parsec


spec :: Spec
spec = do

    it "reportItemP should parse NA" $
        parseReportItem "NA" `shouldBe` ReportNothing

    it "reportItemP should parse mine dropped" $
        parseReportItem "MINE" `shouldBe` ReportMineDropped

    it "reportItemP should parse mine trigged" $
        parseReportItem "TRIGGER 4 5" `shouldBe` ReportMineTrigged (point 4 5)

    it "reportItemP should parse movement" $ do
        parseReportItem "MOVE N" `shouldBe` ReportMove North
        parseReportItem "MOVE S" `shouldBe` ReportMove South
        parseReportItem "MOVE E" `shouldBe` ReportMove East
        parseReportItem "MOVE W" `shouldBe` ReportMove West

    it "reportItemP should parse mine trigged" $
        parseReportItem "SILENCE" `shouldBe` ReportSilence

    it "reportItemP should parse mine trigged" $
        parseReportItem "SURFACE 4" `shouldBe` ReportSurface (Sector 4)

    it "reportItemP should parse mine trigged" $
        parseReportItem "SONAR 4" `shouldBe` ReportSonar (Sector 4)

    it "reportItemP should parse torpedo" $
        parseReportItem "TORPEDO 5 4" `shouldBe` ReportTorpedo (point 5 4)

    it "parseReport report" $ let
            parse text = parseReport text & reportItemsOf
        in do
            parse "MOVE N|TORPEDO 5 4" `shouldBe` [
                ReportMove North,
                ReportTorpedo (point 5 4)]

            parse "MOVE N|SURFACE 4" `shouldBe` [
                ReportMove North,
                ReportSurface (Sector 4)]

    it "orderItemP should parse message" $
        parseOrderItem "MSG Hunting" `shouldBe` OrderMessage "Hunting"

    it "orderItemP should parse mine dropped" $
        parseOrderItem "MINE N" `shouldBe` OrderDropMine North

    it "orderItemP should parse move" $
        parseOrderItem "MOVE S MINE" `shouldBe` OrderMove South Mine


parseReportItem :: String -> IntelligenceReportItem
parseReportItem text = Parsec.parse reportItemP "<string>" text
                     & Either.fromRight ReportNothing

parseOrderItem :: String -> OrderItem
parseOrderItem text = Parsec.parse orderItemP "<string>" text
                     & Either.fromRight (OrderMessage "Failed")
