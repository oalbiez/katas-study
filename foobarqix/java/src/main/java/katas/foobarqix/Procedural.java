package katas.foobarqix;

import static java.lang.String.valueOf;

/**
 * Procedural implementation.
 * Pro: straight forward
 * Cons: violate open/close principle
 */
public class Procedural {
    public static String fooBarQix(final int input) {
        String accumulator = "";
        boolean ruleMatched = false;
        if (isDivisibleBy(input, 3)) {
            accumulator += "Foo";
            ruleMatched = true;
        }
        if (isDivisibleBy(input, 5)) {
            accumulator += "Bar";
            ruleMatched = true;
        }
        if (isDivisibleBy(input, 7)) {
            accumulator += "Qix";
            ruleMatched = true;
        }
        return ruleMatched ? accumulator : valueOf(input);
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
