package katas.buyer.model.ast;

public class Expressions {

    public static Expression add(final Expression left, final Expression right) {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptAdd(left, right);
            }
        };
    }

    public static Expression sub(final Expression left, final Expression right) {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptSub(left, right);
            }
        };
    }

    public static Expression mul(final Expression left, final Expression right) {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptMul(left, right);
            }
        };
    }

    public static Expression div(final Expression left, final Expression right) {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptDiv(left, right);
            }
        };
    }

    public static Expression offerPrice() {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptOfferPrice();
            }
        };
    }

    public static Expression offerQuantity() {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptOfferQuantity();
            }
        };
    }

    public static Expression requestedQuantity() {
        return new Expression() {
            public <T> T accept(final ExpressionVisitor<T> visitor) {
                return visitor.acceptRequestedQuantity();
            }
        };
    }

}
