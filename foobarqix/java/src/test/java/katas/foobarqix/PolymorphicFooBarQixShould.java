package katas.foobarqix;

public class PolymorphicFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return Polymorphic.fooBarQix(input);
    }
}
