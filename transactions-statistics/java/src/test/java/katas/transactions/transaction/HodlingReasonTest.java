package katas.transactions.transaction;

import org.junit.Test;

import static katas.transactions.transaction.HoldingReason.PTYH;
import static org.junit.Assert.assertEquals;

public class HodlingReasonTest {
    @Test
    public void should_be_able_to_render_for_human() {
        assertEquals("Party Hold", PTYH.describe());
    }
}
