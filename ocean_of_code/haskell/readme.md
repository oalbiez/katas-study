# Ocean of Code - Python


## TODO

- comment n'afficher du debug dans les tests que en cas d'erreur
- améliorer le rendu des chaines dans les tests
- faire un quasiquoter pour le parsing des cartes


## Sample

    it "polop2" $
        forM_ [position (-1) 0, position (-2) 0] $
            \p -> p •∉◻ rectTo (position 5 5) `shouldBe` True




dijkstra :: Point -> Int -> (Point -> [Point]) -> [(Point, Int)]
dijkstra start limit neighbors = []


dijkstra' :: Int -> (Point -> [Point]) -> [(Point, Int)] -> [(Point, Int)] -> [(Point, Int)]
dijkstra' limit neighbors [] found = found
dijkstra' limit neighbors ((current, cost)::frontier) found =
  dijkstra' limit neighbors new_frontier new_found
  where
    new_frontier = []
    new_found = []
    -- frontier = []
    -- _put(frontier, start, 0)
    -- costs = {start: 0}
    -- while not _empty(frontier):
    --     current = _pop(frontier)
    --     current_cost = costs[current]
    --     for neighbor in neighbors(current):
    --         neighbor_cost = current_cost + 1
    --         if neighbor_cost <= distance and neighbor not in costs:
    --             costs[neighbor] = neighbor_cost
    --             _put(frontier, neighbor, neighbor_cost)
    -- return costs
