package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.value.Value;

public final class All implements Statistic {
    private final Statistic inner;

    All(final Statistic inner) {
        this.inner = inner;
    }

    public void updateWith(final Transaction transaction) {
        inner.updateWith(transaction);
    }

    public void accept(final StatisticVisitor visitor) {
        visitor.visitAll(this);
    }

    public Statistic fork() {
        return new All(inner.fork());
    }

    public String describe() {
        return inner.describe() + " of all transactions";
    }

    public Value value() {
        return inner.value();
    }

    public Statistic inner() {
        return inner;
    }
}
