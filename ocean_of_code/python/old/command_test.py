# -*- coding: utf-8 -*-

from ai import Move, Direction, Powers


def test_move():
    assert Move(Direction.North, Powers.Torpedo).render() == "MOVE N TORPEDO"
