# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import FrozenSet

from geometry import Delta


def _build_torpedo_deltas() -> FrozenSet[Delta]:
    deltas = set([Delta(0, 0)])
    # pylint: disable=invalid-name
    for dx in range(0, 5):
        for dy in range(0, 5):
            if dx + dy <= 4:
                deltas.add(Delta(dx, dy))
                deltas.add(Delta(-dx, dy))
                deltas.add(Delta(dx, -dy))
                deltas.add(Delta(-dx, -dy))
    return frozenset(deltas)


TorpedoDeltas = _build_torpedo_deltas()


ExplosionDelta = frozenset([Delta(dx, dy) for dx in (-1, 0, 1) for dy in (-1, 0, 1)])


ExplosionBlastDelta = ExplosionDelta - frozenset([Delta(0, 0)])
