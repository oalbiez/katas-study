{-# LANGUAGE OverloadedStrings #-}
module PowerSpec (spec) where


import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec =

    it "Direction should be able to render" $ do
        renderPower Mine `shouldBe` "MINE"
        renderPower Silence `shouldBe` "SILENCE"
        renderPower Sonar `shouldBe` "SONAR"
        renderPower Torpedo `shouldBe` "TORPEDO"

