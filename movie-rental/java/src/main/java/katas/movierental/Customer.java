package katas.movierental;

import katas.movierental.statement.Line;
import katas.movierental.statement.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Customer {

    private CustomerName name;
    private List<Rental> rentals = new ArrayList<Rental>();

    public Customer(CustomerName name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public CustomerName getName() {
        return name;
    }

    public String statement() {
        return build().renderText();
    }

    private FrequentRenterPoint getFrequentRenterPoints() {
        return rentals.stream().map(Rental::frequentRenterPointsFor).reduce(FrequentRenterPoint.of(0), FrequentRenterPoint::add);
    }

    private Price getTotalAmount() {
        return rentals.stream().map(Rental::chargeFor).reduce(Price.euro(0), Price::add);
    }

    private Statement build() {
        return new Statement(
                name,
                getTotalAmount(),
                getFrequentRenterPoints(),
                rentals.stream().map(rental -> new Line(rental.getTitle(), rental.chargeFor())).collect(Collectors.toList()));
    }
}
