package katas.wallet;

import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static katas.wallet.Currency.EUR;
import static katas.wallet.FixedRateProvider.fixed;
import static katas.wallet.FixedRateProvider.rates;
import static org.junit.Assert.assertEquals;


public class WalletTest {
    @Test
    public void empty_wallet_should_value_zero_in_euro() {
        assertEquals("EUR 0.00",
                amountInEuro(wallet()));
    }

    @Test
    public void wallet_with_stock_in_euros_should_sum_of_values_in_euro() {
        assertEquals("EUR 300.00",
                amountInEuro(wallet("EUR 100", "EUR 200")));
    }

    @Test
    public void wallet_with_stock_in_different_currency_should_use_change() {
        assertEquals("EUR 100.00",
                amountInEuro(wallet("USD 200"), rates("USD/EUR 0.5")));
    }

    @Test
    public void wallet_with_stock_in_different_currencies_should_use_change() {
        assertEquals("EUR 10.79",
                amountInEuro(
                        wallet("EUR 10", "DZD 50", "KRW 200", "XBT 0.00002354"),
                        rates("DZD/EUR 0.01", "KRW/EUR 0.001", "XBT/EUR 4000")));
    }

    @Test
    public void wallet_should_round_the_whole_sum() {
        assertEquals("EUR 0.01",
                amountInEuro(
                        wallet("KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1", "KRW 1"),
                        rates("KRW/EUR 0.001")));
    }

    private static Wallet wallet(final String... stocks) {
        return Wallet.wallet(Stream.of(stocks).map(Price::price).collect(Collectors.toList()));
    }

    private static String amountInEuro(final Wallet wallet) {
        return amountInEuro(wallet, fixed());
    }

    private static String amountInEuro(final Wallet wallet, final RateProvider rateProvider) {
        return wallet.valuateIn(EUR, rateProvider).toString();
    }
}
