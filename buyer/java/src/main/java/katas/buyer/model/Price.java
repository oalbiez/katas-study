package katas.buyer.model;

import com.google.common.base.Objects;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;

public final class Price implements Comparable<Price> {
    private final BigDecimal value;

    public static Price of(final double value) {
        return of(BigDecimal.valueOf(value));
    }

    public static Price of(final String value) {
        return of(new BigDecimal(value));
    }

    public static Price of(final BigDecimal value) {
        return new Price(value);
    }

    private Price(final BigDecimal value) {
        this.value = value.setScale(2, HALF_UP);
    }

    public int compareTo(final Price other) {
        return value.compareTo(other.value);
    }

    public Price add(final Price price) {
        return of(value.add(price.value));
    }

    public Price mul(final Quantity quantity) {
        return of(value.multiply(quantity.value()));
    }

    public Price mul(final Delay delay) {
        return of(value.multiply(delay.inDays()));
    }

    public Price div(final Quantity quantity) {
        return of(value.divide(quantity.value(), HALF_UP));
    }

    @Override
    public String toString() {
        return "EUR " + value;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        return Objects.equal(value, ((Price) other).value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    public BigDecimal value() {
        return value;
    }
}
