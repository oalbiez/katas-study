package katas.foobarqix;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.runner.RunWith;

import static java.lang.String.valueOf;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeThat;

@RunWith(JUnitQuickcheck.class)
public abstract class FooBarQixShould {

    @Property
    public void foo_when_input_is_divisible_by_3(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, allOf(not(isDivisibleBy(5)), not(isDivisibleBy(7))));
        assertEquals("Foo", fooBarQix(3 * value));
    }

    @Property
    public void bar_when_input_is_divisible_by_5(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, allOf(not(isDivisibleBy(3)), not(isDivisibleBy(7))));
        assertEquals("Bar", fooBarQix(5 * value));
    }

    @Property
    public void bar_when_input_is_divisible_by_7(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, allOf(not(isDivisibleBy(3)), not(isDivisibleBy(5))));
        assertEquals("Qix", fooBarQix(7 * value));
    }

    @Property
    public void foobar_when_input_is_divisible_by_3_and_5(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, not(isDivisibleBy(7)));
        assertEquals("FooBar", fooBarQix(3 * 5 * value));
    }

    @Property
    public void fooqix_when_input_is_divisible_by_3_and_7(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, not(isDivisibleBy(5)));
        assertEquals("FooQix", fooBarQix(3 * 7 * value));
    }

    @Property
    public void barqix_when_input_is_divisible_by_5_and_7(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, not(isDivisibleBy(3)));
        assertEquals("BarQix", fooBarQix(5 * 7 * value));
    }

    @Property
    public void foobarqix_when_input_is_divisible_by_3_5_and_7(@InRange(min = "0", max = "1000000") int value) {
        assertEquals("FooBarQix", fooBarQix(3 * 5 * 7 * value));
    }

    @Property
    public void return_input_otherwise(@InRange(min = "0", max = "1000000") int value) {
        assumeThat(value, allOf(not(isDivisibleBy(3)), not(isDivisibleBy(5)), not(isDivisibleBy(7))));
        assertEquals(valueOf(value), fooBarQix(value));
    }

    abstract String fooBarQix(final int input);

    private Matcher<Integer> isDivisibleBy(final int divisor) {
        return new TypeSafeMatcher<>() {
            @Override
            protected boolean matchesSafely(Integer item) {
                return item % divisor == 0;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is divisible by ").appendValue(divisor);
            }
        };
    }
}
