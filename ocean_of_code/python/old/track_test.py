# -*- coding: utf-8 -*-

from ai import TrackingInformations, Direction, Position
from ai import TrackMove, TrackNothing, TrackSilence, TrackSurface, TrackTorpedo, TrackMineDroped, TrackMineTrigged


def check(expression, *expected):
    assert TrackingInformations.parse(expression) == TrackingInformations(list(expected))


def test_parse_not_applicable():
    check("NA", TrackNothing())


def test_parse_move():
    check("MOVE N", TrackMove(Direction.North))
    check("MOVE S", TrackMove(Direction.South))
    check("MOVE E", TrackMove(Direction.East))
    check("MOVE W", TrackMove(Direction.West))


def test_parse_surface():
    check("SURFACE 1", TrackSurface(1))
    check("SURFACE 3", TrackSurface(3))
    check("SURFACE 5", TrackSurface(5))


def test_parse_silence():
    check("SILENCE", TrackSilence())


def test_parse_torpedo():
    check("TORPEDO 3 10", TrackTorpedo(Position(3, 10)))


def test_parse_mine_droped():
    check("MINE", TrackMineDroped())


def test_parse_mine_trigged():
    check("TRIGGER 3 10", TrackMineTrigged(Position(3, 10)))


def test_parse_orders():
    check("TORPEDO 7 0| MOVE E", TrackTorpedo(Position(7, 0)), TrackMove(Direction.East))
