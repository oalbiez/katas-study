﻿using MovieRental;
using NUnit.Framework;
using static MovieRental.Duration;
using static MovieRental.PriceCodes;

namespace MovieRentalTest
{
    [TestFixture]
    public class UnitTest
    {
        [Test]
        public void statement_with_regular_movie()
        {
            var customer = new Customer("jpartogi");
            customer.AddRental(new Rental(new Movie("Transformer", Regular()), Days(5)));
            Assert.AreEqual(
                "Rental Record for jpartogi\n" +
                "\tTransformer\t6,5\n" +
                "Amount owed is 6,5\n" +
                "You earned 1 frequent renter points",
                customer.Statement());
        }

        [Test]
        public void statement_with_children_movie()
        {
            var customer = new Customer("jpartogi");
            customer.AddRental(new Rental(new Movie("Bambi", Children()), Days(5)));
            Assert.AreEqual(
                "Rental Record for jpartogi\n" +
                "\tBambi\t4,5\n" +
                "Amount owed is 4,5\n" +
                "You earned 1 frequent renter points",
                customer.Statement());
        }

        [Test]
        public void statement_with_new_release_movie()
        {
            var customer = new Customer("jpartogi");
            customer.AddRental(new Rental(new Movie("New one", NewRelease()), Days(5)));
            Assert.AreEqual(
                "Rental Record for jpartogi\n" +
                "\tNew one\t15\n" +
                "Amount owed is 15\n" +
                "You earned 2 frequent renter points",
                customer.Statement());
        }

        [Test]
        public void statement_with_differents_movies()
        {
            var customer = new Customer("jpartogi");
            customer.AddRental(new Rental(new Movie("New one", NewRelease()), Days(5)));
            customer.AddRental(new Rental(new Movie("Transformer", Regular()), Days(5)));
            Assert.AreEqual(
                "Rental Record for jpartogi\n" +
                "\tNew one\t15\n" +
                "\tTransformer\t6,5\n" +
                "Amount owed is 21,5\n" +
                "You earned 3 frequent renter points",
                customer.Statement());
        }
    }
}