package katas.buyer.model;

public final class PurchaseRequest {
    private final Quantity quantity;
    private Delay delay;

    public static PurchaseRequest of(final Quantity quantity) {
        return new PurchaseRequest(quantity, Delay.none());
    }

    public static PurchaseRequest of(final Quantity quantity, final Delay delay) {
        return new PurchaseRequest(quantity, delay);
    }

    private PurchaseRequest(final Quantity quantity, final Delay delay) {
        this.quantity = quantity;
        this.delay = delay;
    }

    public Quantity quantity() {
        return quantity;
    }

    public Delay delay() {
        return delay;
    }
}
