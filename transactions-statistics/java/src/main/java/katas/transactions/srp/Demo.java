package katas.transactions.srp;

import katas.transactions.Transaction;
import katas.transactions.srp.statistics.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.Map;

import static katas.transactions.TransactionProviders.random;
import static katas.transactions.srp.ast.Longs.gt;
import static katas.transactions.srp.ast.Longs.literal;
import static katas.transactions.srp.ast.Transactions.*;
import static katas.transactions.srp.statistics.Statistics.*;

public class Demo {

    public static class ConsoleStatisticVisitor implements StatisticVisitor {
        public void visitAll(final All statistic) {
            describe(statistic);
            statistic.inner().accept(this);
            System.out.println();
        }

        public void visitCount(final Count statistic) {
            System.out.print(statistic.value());
        }

        public void visitSum(final SumOf statistic) {
            System.out.print(statistic.value());
        }

        public void visitGroup(final Group statistic) {
            describe(statistic);
            for (final Map.Entry<String, Statistic> entry : statistic.stats().entrySet()) {
                System.out.print("\t" + entry.getKey() + ": ");
                entry.getValue().accept(this);
                System.out.println();
            }
        }

        public void visitSelect(final Select statistic) {
            describe(statistic);
            statistic.statistic().accept(this);
            System.out.println();
        }

        public void visitComposite(final Composite statistic) {
            statistic.statistics().forEach(s -> s.accept(this));
        }

        private void describe(final Statistic statistic) {
            System.out.println(statistic.describe() + ": ");
        }
    }

    private static Iterable<Transaction> transactions() {
        return random(LocalDate.of(2018, Month.APRIL, 17)).transactionsSince(LocalDate.of(2018, Month.JANUARY, 1));
    }

    public static void main(final String[] args) {
        Demo.class.getResource("polop");
        final Statistic statistics = composite(
                all(sum(amount())),
                group(byClient(), sum(amount())),

                group(byClient(), count()),
                group(byClient(), sum(transaction())),

                group(byCategory(), sum(amount())),
                select(gt(amount(), literal(1000)), group(byClient(), count())));

        transactions().forEach(statistics::updateWith);
        statistics.accept(new ConsoleStatisticVisitor());
    }
}
