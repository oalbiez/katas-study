package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.value.Value;

import java.util.List;

import static com.google.common.collect.Lists.transform;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.joining;
import static katas.transactions.value.Values.list;

public final class Composite implements Statistic {
    private final List<Statistic> statistics;

    Composite(final List<Statistic> statistics) {
        this.statistics = statistics;
    }

    public void updateWith(final Transaction transaction) {
        statistics.forEach(statistic -> statistic.updateWith(transaction));
    }

    public void accept(final StatisticVisitor visitor) {
        visitor.visitComposite(this);
    }

    public Statistic fork() {
        return new Composite(transform(statistics, Statistic::fork));
    }

    public String describe() {
        return statistics.stream().map(Statistic::describe).collect(joining(" and "));
    }

    public Value value() {
        return list(transform(statistics, Statistic::value));
    }

    public List<Statistic> statistics() {
        return unmodifiableList(statistics);
    }
}
