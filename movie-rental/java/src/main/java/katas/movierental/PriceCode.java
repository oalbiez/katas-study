package katas.movierental;

import katas.movierental.pricing.Pricing;

import static katas.movierental.Duration.days;
import static katas.movierental.Price.euro;
import static katas.movierental.pricing.Pricing.cost;
import static katas.movierental.pricing.Pricing.pricing;

public enum PriceCode {
    REGULAR(pricing(cost(euro(2)).for_(days(2))).then(cost(euro(1.5)).perDay())) {
        public FrequentRenterPoint frequentRenterPointFor(Duration daysRented) {
            return FrequentRenterPoint.of(1);
        }
    },
    NEW_RELEASE(pricing(cost(euro(3)).perDay())) {
        public FrequentRenterPoint frequentRenterPointFor(Duration daysRented) {
            return FrequentRenterPoint.of(daysRented.value() > 1 ? 2 : 1);
        }
    },
    CHILDREN(pricing(cost(euro(1.5)).for_(days(3))).then(cost(euro(1.5)).perDay())) {
        public FrequentRenterPoint frequentRenterPointFor(Duration daysRented) {
            return FrequentRenterPoint.of(1);
        }
    };

    private final Pricing pricing;

    PriceCode(Pricing pricing) {
        this.pricing = pricing;
    }

    public Price chargeFor(Duration daysRented) {
        return pricing.chargeFor(daysRented);
    }

    public abstract FrequentRenterPoint frequentRenterPointFor(Duration daysRented);
}
