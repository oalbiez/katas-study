package katas.foobarqix;

import java.util.List;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;

/**
 * Configurable with constructors rules
 */
public class FooBarQixWithRules {

    interface Rule {
        boolean apply(int value, StringBuilder accumulator);
    }

    static Rule rule(final int divisor, final String label) {
        return (value, accumulator) -> {
            if (isDivisibleBy(value, divisor)) {
                accumulator.append(label);
                return true;
            }
            return false;
        };
    }

    static FooBarQixWithRules rules(final Rule... rules) {
        return new FooBarQixWithRules(rules);
    }

    private final List<Rule> rules;

    private FooBarQixWithRules(Rule... rules) {
        this.rules = asList(rules);
    }

    public String compute(final int input) {
        StringBuilder accumulator = new StringBuilder();
        boolean ruleMatched = false;
        for (Rule rule : rules) {
            ruleMatched |= rule.apply(input, accumulator);
        }
        return ruleMatched ? accumulator.toString() : valueOf(input);
    }

    private static boolean isDivisibleBy(int input, int divisor) {
        return input % divisor == 0;
    }
}
