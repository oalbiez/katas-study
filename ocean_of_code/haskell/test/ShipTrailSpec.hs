{-# LANGUAGE OverloadedStrings #-}
module ShipTrailSpec (spec) where


import           CustomArbitraries ()
import           Data.Function     ((&))
import qualified Data.HashSet      as HashSet
import           OceanOfCode
import           Test.Hspec
import           Test.QuickCheck


spec :: Spec
spec = do

    it "ShipTrail should contains starting position" $ property $
        \p -> hasPosition p (mkTrail p)

    it "ShipTrail should have current position on the starting position" $ property $
        \p -> trailPosition (mkTrail p) == p

    it "ShipTrail should have no mine at start" $ property $
        \p -> trailMines (mkTrail p) == HashSet.empty

    it "ShipTrail should move current position when adding a trail position" $
        let trail = mkTrail (point 4 3)
                    & addTrail (point 5 3)
        in trailPosition trail `shouldBe` point 5 3

    it "ShipTrail should contains all visited points" $
        let trail = mkTrail (point 4 3)
                    & addTrail (point 5 3)
                    & addTrail (point 6 3)
                    & addTrail (point 7 3)
        in do
            hasPosition (point 4 3) trail `shouldBe` True
            hasPosition (point 5 3) trail `shouldBe` True
            hasPosition (point 6 3) trail `shouldBe` True
            hasPosition (point 7 3) trail `shouldBe` True
            hasPosition (point 8 3) trail `shouldBe` False

    it "ShipTrail should forget all but last point on reset" $
        let trail = mkTrail (point 4 3)
                    & addTrail (point 5 3)
                    & addTrail (point 6 3)
                    & addTrail (point 7 3)
                    & resetTrail
        in do
            hasPosition (point 4 3) trail `shouldBe` False
            hasPosition (point 5 3) trail `shouldBe` False
            hasPosition (point 6 3) trail `shouldBe` False
            hasPosition (point 7 3) trail `shouldBe` True
            hasPosition (point 8 3) trail `shouldBe` False
