
import { MovieCode } from 'movie-code';
import expect from 'expect';


describe('Charge', () => {
  it('new movies should be charged 3 times the days rent', () => {
    const code = MovieCode['new'];
    expect(code.charge(1)).toEqual(3);
    expect(code.charge(4)).toEqual(12);
  });

  it('regular movies should be charged $2 when less than 3 days and $1.5 per supplementary days', () => {
    const code = MovieCode['regular'];
    expect(code.charge(1)).toEqual(2);
    expect(code.charge(2)).toEqual(2);
    expect(code.charge(3)).toEqual(2 + 1 * 1.5);
    expect(code.charge(10)).toEqual(2 + 8 * 1.5);
  });

  it('childrens movies should be charged $1.5 when less than 4 days and $1.5 per supplementary days', () => {
    const code = MovieCode['childrens'];
    expect(code.charge(1)).toEqual(1.5);
    expect(code.charge(2)).toEqual(1.5);
    expect(code.charge(3)).toEqual(1.5);
    expect(code.charge(4)).toEqual(1.5 + 1 * 1.5);
    expect(code.charge(10)).toEqual(1.5 + 7 * 1.5);
  });
});


describe('FrequentRenterPoints', () => {
  it('new movies should give 1 when rent for less than 3 days, 2 otherwise', () => {
    const code = MovieCode['new'];
    expect(code.frequentRenterPoints(1)).toEqual(1);
    expect(code.frequentRenterPoints(2)).toEqual(1);
    expect(code.frequentRenterPoints(3)).toEqual(2);
    expect(code.frequentRenterPoints(4)).toEqual(2);
    expect(code.frequentRenterPoints(10)).toEqual(2);
  });

  it('regular movies should give 1 frequent renter point regardless of the days rent', () => {
    const code = MovieCode['regular'];
    expect(code.frequentRenterPoints(1)).toEqual(1);
    expect(code.frequentRenterPoints(10)).toEqual(1);
  });

  it('childrens movies should give 1 frequent renter point regardless of the days rent', () => {
    const code = MovieCode['childrens'];
    expect(code.frequentRenterPoints(1)).toEqual(1);
    expect(code.frequentRenterPoints(10)).toEqual(1);
  });
});
