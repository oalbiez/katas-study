# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import List, Optional, Set, Text

from geometry import Position
from pathfinder import astar_search
from ship_trail import ShipTrail
from world_map import WorldMap


class Navigation:
    def __init__(self, worldmap: WorldMap):
        self.worldmap = worldmap
        self.navigable: Set[Position] = set(worldmap.all_water())
        self.avoid: Set[Position] = set()
        self.__fill_avoid()

    def __fill_avoid(self):
        deadends = set(p for p in self.all_navigable() if len(self.neighbors(p)) <= 1)
        while deadends:
            self.avoid.update(deadends)
            self.navigable = self.navigable - deadends
            deadends = set(p for p in self.all_navigable() if len(self.neighbors(p)) <= 1)

    def all_navigable(self) -> Set[Position]:
        return self.navigable

    #
    def is_navigable(self, position: Position) -> bool:
        return position in self.navigable

    #
    def neighbors(self, position: Position) -> List[Position]:
        return [p for p in self.worldmap.neighbors(position) if p in self.navigable]

    #
    def path(self, trail: ShipTrail, goal: Position) -> List[Position]:
        def neighbors(position):
            return [p for p in self.neighbors(position) if p not in trail]

        found, path, _ = astar_search(trail.last(), goal, neighbors, Position.distance)
        if found:
            return path
        return []

    def render(self) -> Text:
        def display(position: Position) -> Optional[Text]:
            if position in self.avoid:
                return "x"
            return None

        return self.worldmap.render(display)
