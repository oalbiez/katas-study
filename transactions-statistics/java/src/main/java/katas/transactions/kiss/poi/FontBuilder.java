package katas.transactions.kiss.poi;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;

public class FontBuilder {
    private final Font font;

    public static FontBuilder font(final Workbook workbook) {
        return new FontBuilder(workbook);
    }

    private FontBuilder(final Workbook workbook) {
        font = workbook.createFont();
    }

    public FontBuilder bold() {
        font.setBold(true);
        return this;
    }

    public Font get() {
        return font;
    }
}
