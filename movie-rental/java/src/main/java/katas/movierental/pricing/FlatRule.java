package katas.movierental.pricing;

import katas.movierental.Duration;

final class FlatRule implements Rule {
    private final Value price;

    FlatRule(Value price) {
        this.price = price;
    }

    public Rule perDay() {
        return new ProportionalRule(price);
    }

    public Rule for_(Duration duration) {
        return new LimitedDurationRule(duration, this);
    }

    public Value priceFor(Duration duration, ValueDriver driver) {
        return price;
    }

    public Duration consume(Duration remaining) {
        return Duration.zero();
    }

    public String describe() {
        return "cost " + price;
    }
}
