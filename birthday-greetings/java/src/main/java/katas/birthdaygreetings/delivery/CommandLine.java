package katas.birthdaygreetings.delivery;

import katas.birthdaygreetings.business.BirthdayService;
import katas.birthdaygreetings.infrastructure.CSVPersonProvider;
import katas.birthdaygreetings.infrastructure.SystemDateProvider;
import katas.birthdaygreetings.infrastructure.WishBirthdayByMail;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

public final class CommandLine {
    public static void main(final String[] args) throws IOException {
        new BirthdayService(new CSVPersonProvider(Paths.get("people.csv")), new WishBirthdayByMail(loadMailProperties()), new SystemDateProvider()).process();
    }

    private static Properties loadMailProperties() throws IOException {
        final Properties properties = new Properties();
        properties.load(new FileInputStream("./mail.properties"));
        return properties;
    }
}
