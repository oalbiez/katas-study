package katas.marsrover;

public enum Orientation {
    East() {
        public Distance distance() {
            return Distance.distance(1, 0);
        }

        public Orientation left() {
            return North;
        }

        public Orientation right() {
            return South;
        }
    },
    North() {
        public Distance distance() {
            return Distance.distance(0, 1);
        }

        public Orientation left() {
            return West;
        }

        public Orientation right() {
            return East;
        }
    },
    South() {
        public Distance distance() {
            return Distance.distance(0, -1);
        }

        public Orientation left() {
            return East;
        }

        public Orientation right() {
            return West;
        }
    },
    West() {
        public Distance distance() {
            return Distance.distance(-1, 0);
        }

        public Orientation left() {
            return South;
        }

        public Orientation right() {
            return North;
        }
    };

    public abstract Distance distance();

    public abstract Orientation left();

    public abstract Orientation right();
}
