package katas.foobarqix;

public class BitwiseShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return Bitwise.fooBarQix(input);
    }
}
