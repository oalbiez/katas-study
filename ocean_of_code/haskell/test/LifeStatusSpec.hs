{-# LANGUAGE OverloadedStrings #-}
module LifeStatusSpec (spec) where


import           Data.Function ((&))
import           OceanOfCode
import           Test.Hspec


spec :: Spec
spec = do

    it "LifeStatus should have an no life lost at creation" $
        lifeLost (mkLifeStatus 6) `shouldBe` 0

    it "LifeStatus should compute amount of life lost" $ do
        (mkLifeStatus 6 & updateLifeStatus 6 & lifeLost) `shouldBe` 0
        (mkLifeStatus 6 & updateLifeStatus 5 & lifeLost) `shouldBe` 1
        (mkLifeStatus 6 & updateLifeStatus 4 & lifeLost) `shouldBe` 2
        (mkLifeStatus 6 & updateLifeStatus 2 & lifeLost) `shouldBe` 4
