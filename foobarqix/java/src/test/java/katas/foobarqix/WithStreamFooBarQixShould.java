package katas.foobarqix;

public class WithStreamFooBarQixShould extends FooBarQixShould {
    String fooBarQix(final int input) {
        return WithStream.fooBarQix(input);
    }
}
