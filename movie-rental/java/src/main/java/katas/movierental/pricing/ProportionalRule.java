package katas.movierental.pricing;

import katas.movierental.Duration;

final class ProportionalRule implements Rule {
    private final Value price;

    ProportionalRule(Value price) {
        this.price = price;
    }

    public Rule perDay() {
        throw new MalformedPricing("cannot apply perDay more than one on a rule");
    }

    public Rule for_(Duration duration) {
        return new LimitedDurationRule(duration, this);
    }

    public Value priceFor(Duration duration, ValueDriver driver) {
        return driver.mul(price, duration.value());
    }

    public Duration consume(Duration remaining) {
        return Duration.zero();
    }

    public String describe() {
        return "cost " + price + " per day";
    }
}
