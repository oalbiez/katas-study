package katas.birthdaygreetings.delivery;

import katas.birthdaygreetings.business.BirthdayService;
import katas.birthdaygreetings.infrastructure.CSVPersonProvider;
import katas.birthdaygreetings.infrastructure.SystemDateProvider;
import katas.birthdaygreetings.infrastructure.WishBirthdayByMail;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

import static spark.Spark.get;

public final class RestApi {
    public static void main(final String[] args) throws IOException {
        new BirthdayController();
    }

    public static class BirthdayController {
        private final BirthdayService service;

        BirthdayController() throws IOException {
            service = new BirthdayService(new CSVPersonProvider(Paths.get("people.csv")), new WishBirthdayByMail(loadMailProperties()), new SystemDateProvider());
            get("/wishBirthday", (request, response) -> process());
        }

        private boolean process() {
            service.process();
            return true;
        }

        private static Properties loadMailProperties() throws IOException {
            final Properties properties = new Properties();
            properties.load(new FileInputStream("./mail.properties"));
            return properties;
        }
    }
}
