{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE ScopedTypeVariables #-}
module OceanOfCode where


import           Control.Monad    (void)
import qualified Data.Char        as Char
import qualified Data.Either      as Either
import           Data.Function    (on, (&))
import           Data.Hashable
import qualified Data.HashSet     as HashSet
import qualified Data.List        as List
import qualified Data.Map         as Map
import qualified Data.Maybe       as Maybe
import           GHC.Generics     (Generic)
import qualified Text.Parsec      as Parsec
import qualified Text.Parsec.Char as Parsec.Char
import qualified Text.Printf      as Print

{- | Parser
-}
type Parser = Parsec.Parsec String ()

spaceP :: Parser a -> Parser a
spaceP p = p <* Parsec.Char.spaces

symbolP :: String -> Parser ()
symbolP v = Parsec.try (void $ spaceP $ Parsec.Char.string v)

numberP :: Parser Int
numberP = read <$> spaceP (Parsec.many1 Parsec.Char.digit)

stringP :: Parser String
stringP = spaceP (Parsec.many (Parsec.Char.satisfy (not . Char.isSpace)))


{- | Algorithm dijkstra
-}
dijkstra :: (Eq a) => Int -> (a -> [a]) -> a -> [a]
dijkstra limit neighbors start  =
    let
        dijkstra' [] found = found
        dijkstra' ((cost, current):frontier) found =
            let
                new = [p | p <- neighbors current, p `notElem` found]
            in if cost + 1 > limit
                then dijkstra' frontier found
                else dijkstra' (frontier ++ [(cost + 1, p) | p <- new]) (found ++ new)
    in dijkstra' [(0, start)] [start]


{- | Delta is a difference between two points
-}
data Delta
    = Delta Int Int
    deriving (Show, Read, Eq, Ord, Generic)
instance Hashable Delta


δ :: Int -> Int -> Delta
δ = Delta

δzero :: Delta
δzero = Delta 0 0

δx :: Delta -> Int
δx (Delta dx _) = dx

δy :: Delta -> Int
δy (Delta _ dy) = dy

(∆+∆) :: Delta -> Delta -> Delta
(∆+∆) (Delta dx1 dy1) (Delta dx2 dy2) = Delta (dx1 + dx2) (dy1 + dy2)

(∆-∆) :: Delta -> Delta -> Delta
(∆-∆) (Delta dx1 dy1) (Delta dx2 dy2) = Delta (dx1 - dx2) (dy1 - dy2)

(∆*) :: Delta -> Int -> Delta
(∆*) (Delta dx dy) f = Delta (dx * f) (dy * f)

δlength² :: Delta -> Int
δlength² (Delta dx dy) = dx *dx + dy *dy

δlength :: Delta -> Float
δlength d = fromIntegral (δlength² d) & sqrt

explosionDelta :: [Delta]
explosionDelta = [δ dx  dy | dx <- [-1, 0, 1], dy <- [-1, 0, 1]]

explosionBlastDelta :: [Delta]
explosionBlastDelta = List.delete δzero explosionDelta


{- | Point
-}
data Point
    = Point Int Int
    deriving (Show, Read, Eq, Ord, Generic)
instance Hashable Point

point :: Int -> Int -> Point
point = Point

origin :: Point
origin = Point 0 0

xOf :: Point -> Int
xOf (Point x _) = x

yOf :: Point -> Int
yOf (Point _ y) = y

setX :: Point -> Int -> Point
setX (Point _ y) nx = Point nx y

setY :: Point -> Int -> Point
setY (Point x _) = Point x

(•+∆) :: Point -> Delta -> Point
(•+∆) (Point x y) (Delta dx dy) = Point (x + dx) (y + dy)

(•-∆) :: Point -> Delta -> Point
(•-∆) (Point x y) (Delta dx dy) = Point (x - dx) (y - dy)

(•-•) :: Point -> Point -> Delta
(•-•) (Point xa ya) (Point xb yb) = δ (xb - xa) (yb - ya)

minp :: Point -> Point -> Point
minp (Point x1 y1) (Point x2 y2) = Point (min x1 x2) (min y1 y2)

maxp :: Point -> Point -> Point
maxp (Point x1 y1) (Point x2 y2) = Point (max x1 x2) (max y1 y2)

distance² :: Point -> Point -> Int
distance² first second = δlength² (first •-• second)

distance :: Point -> Point -> Float
distance first second  = δlength (first •-• second)

renderPoint :: Point -> String
renderPoint (Point x y) = Print.printf "%d %d" x y

pointP :: Parser Point
pointP = point <$> numberP <*> numberP


{- | Rect is a rectangle defined by two points
-}
data Rect
    = Rect Point Point
    deriving (Show, Read, Eq, Ord, Generic)

emptyRect :: Rect
emptyRect = rectTo origin

rect :: Point -> Point -> Rect
rect left right = Rect (minp left right) (maxp left right)

rectTo :: Point -> Rect
rectTo = rect origin

boundingRect :: [Point] -> Rect
boundingRect [] = emptyRect
boundingRect xp = Rect (foldl1 minp xp) (foldl1 maxp xp •+∆ δ 1 1)

height :: Rect -> Int
height (Rect (Point _ ya) (Point _ yb)) = yb - ya

width :: Rect -> Int
width (Rect (Point xa _) (Point xb _)) = xb - xa

topleft :: Rect -> Point
topleft (Rect tl _) = tl

bottomright :: Rect -> Point
bottomright (Rect _ br) = br

center :: Rect -> Point
center (Rect (Point xa ya) (Point xb yb)) =
    point
        ((xa + xb) `div` 2)
        ((ya + yb) `div` 2)

area :: Rect -> Int
area r = height r * width r

aspectRatio :: Rect -> Float
aspectRatio r = fromIntegral (width r) / fromIntegral (height r)

(•∈◻) :: Point -> Rect -> Bool
(•∈◻) (Point x y) (Rect (Point xa ya) (Point xb yb)) =
    xa <= x && x < xb && ya <= y && y < yb

(•∉◻) :: Point -> Rect -> Bool
(•∉◻) p r = not $ (•∈◻) p r

(◻∪◻) :: Rect -> Rect -> Rect
(◻∪◻) left right =
    rect
        (minp (topleft left) (topleft right))
        (maxp (bottomright left) (bottomright right))

walkRect :: Rect -> [Point]
walkRect (Rect (Point xa ya) (Point xb yb)) =
    [ point x y | x <- [xa..xb-1], y <- [ya..yb-1]]

splitRect :: Point -> Rect -> [Rect]
splitRect p r
    | p •∉◻ r = [r]
    | otherwise =
        let
            tl = topleft r
            br = bottomright r
            x = xOf p
            y = yOf p
        in filter (\e -> area e > 0) [
            rect tl (setX br x),
            rect (setX tl (x + 1)) br,
            rect tl (setY br y),
            rect (setY tl (y + 1)) br]


{- | Direction
-}
data Direction
    = North
    | South
    | East
    | West
    deriving (Show, Read, Eq, Ord, Generic)

inverseDirection :: Direction -> Direction
inverseDirection North = South
inverseDirection South = North
inverseDirection East  = West
inverseDirection West  = East

deltaForDirection :: Direction -> Delta
deltaForDirection North = δ 0 (-1)
deltaForDirection South = δ 0 1
deltaForDirection East  = δ 1 0
deltaForDirection West  = δ (-1) 0

moveInDirection :: Direction -> Point -> Point
moveInDirection d p = p •+∆ deltaForDirection d

renderDirection :: Direction -> String
renderDirection North = "N"
renderDirection South = "S"
renderDirection East  = "E"
renderDirection West  = "W"

directionP :: Parser Direction
directionP = Parsec.choice [
    do { symbolP "N"; return North },
    do { symbolP "S"; return South },
    do { symbolP "E"; return East },
    do { symbolP "W"; return West }]


{- | Path
-}
newtype Path
    = Path [Direction]
    deriving (Show, Read, Eq, Ord, Generic)

emptyPath :: Path
emptyPath = Path []

pathOn :: Direction -> Path
pathOn direction = Path [direction]

pathWith :: [Direction] -> Path
pathWith = Path

silencePaths :: [Path]
silencePaths
    = emptyPath:concat [develop 5 d | d <- [North, South, East, West]]
    where
        develop count direction = [pathWith $ replicate c direction | c <- [1..count]]


{- | Sector
-}
newtype Sector
    = Sector Int
    deriving (Show, Read, Eq, Ord, Generic)

sectorValue :: Sector -> Int
sectorValue (Sector value) = value

renderSector :: Sector -> String
renderSector (Sector value) = show value

sectorP :: Parser Sector
sectorP = Sector <$> numberP


{- | Targets
-}
data Targets
    = Targets [Point] [Point]
    deriving (Show, Read, Eq, Ord, Generic)

safePositions :: Targets -> [Point]
safePositions (Targets safe _) = safe

unsafePositions :: Targets -> [Point]
unsafePositions (Targets _ unsafe) = unsafe

targetsContain :: Targets -> Point -> Bool
targetsContain (Targets safe unsafe) p = p `elem` safe || p `elem` unsafe


{- | WorldMap
-}
data WorldMap
    = WorldMap Rect (HashSet.HashSet Point)
    deriving (Show, Read, Eq)

worldmap :: Rect -> [Point] -> WorldMap
worldmap zone islands
    = WorldMap zone (HashSet.difference zone_set island_set)
    where
        zone_set = HashSet.fromList $ walkRect zone
        island_set = HashSet.fromList islands

water :: WorldMap -> HashSet.HashSet Point
water (WorldMap _ waters) = waters

isWater :: WorldMap -> Point -> Bool
isWater (WorldMap _ waters) p = HashSet.member p waters

neighborsOf :: WorldMap -> Point -> [Point]
neighborsOf worldmap p
    = [North, South, East, West]
    & map (`moveInDirection` p)
    & filter (isWater worldmap)

toSector :: WorldMap -> Point -> Sector
toSector (WorldMap zone _) p =
    1 + (xOf p `div` 5) + (yOf p `div` 5) * (width zone `div` 5) & Sector

moveTrail :: WorldMap -> ShipTrail -> Path -> Maybe ShipTrail
moveTrail _ trail (Path []) = Just trail
moveTrail wm trail (Path (d:xs)) =
    let new_position = moveInDirection d (trailPosition trail)
    in if isWater wm new_position && not (hasPosition new_position trail)
        then moveTrail wm (addTrail new_position trail) (Path xs)
        else Nothing

targetsAt :: WorldMap -> Point -> Targets
targetsAt wm position
    = Targets
        (List.filter (`notElem` blast) result)
        (List.filter (`elem` blast) result)
    where
        result = dijkstra 4 (neighborsOf wm) position
        blast = explosionDelta & map (position •+∆)


biggestWaterZone :: WorldMap -> Rect
biggestWaterZone (WorldMap zone waters) =
    let
        islands
            = waters
            & HashSet.difference (HashSet.fromList $ walkRect zone)
            & HashSet.toList
        split zones island = concatMap (splitRect island) zones
        zones = foldl split [zone] islands
    in [(area z, z) | z <- zones]
        & List.sort
        & List.reverse
        & head
        & snd


{- | ShipTrail
-}
data ShipTrail
    = ShipTrail [Point] (HashSet.HashSet Point)
    deriving (Show, Read, Eq, Generic)
instance Hashable ShipTrail

mkTrail :: Point -> ShipTrail
mkTrail p = ShipTrail [p] HashSet.empty

trailPosition :: ShipTrail -> Point
trailPosition (ShipTrail path _) = head path

trailMines :: ShipTrail -> HashSet.HashSet Point
trailMines (ShipTrail _ mines) = mines

addTrail :: Point -> ShipTrail -> ShipTrail
addTrail p (ShipTrail path mines) = ShipTrail (p:path) mines

resetTrail :: ShipTrail -> ShipTrail
resetTrail (ShipTrail path mines) = ShipTrail [head path] mines

hasPosition :: Point -> ShipTrail -> Bool
hasPosition p (ShipTrail path _) = p `elem` path

addMine :: Point -> ShipTrail -> ShipTrail
addMine p (ShipTrail path mines) = ShipTrail path (HashSet.insert p mines)

removeMine :: Point -> ShipTrail -> ShipTrail
removeMine p (ShipTrail path mines) = ShipTrail path (HashSet.delete p mines)

hasMine :: Point -> ShipTrail -> Bool
hasMine p (ShipTrail _ mines) = HashSet.member p mines


{- | Tracking
-}
newtype Tracking
    = Tracking (HashSet.HashSet ShipTrail)

mkTracking :: WorldMap -> Tracking
mkTracking wm = Tracking $ HashSet.map mkTrail (water wm)


data Constraint
    = CanMove [Path]
    | MineDropped
    | MineTrigged Point
    | AtPositions [Point]
    | NotAtPositions [Point]
    | InsideTorpedoRange Point
    | OutsideTorpedoRange Point
    | InSector Sector
    | NotInSector Sector
    | Reset
    deriving (Show, Read, Eq)


data PositionEstimation
    = PositionEstimation Point Float
    deriving (Show, Read, Eq)


trackingTrails :: Tracking -> HashSet.HashSet ShipTrail
trackingTrails (Tracking trails) = trails

trackingStatistics :: WorldMap -> Tracking -> String
trackingStatistics wm tracking =
    Print.printf "- trails: %d\n" (HashSet.size $ trackingTrails tracking)
    ++ Print.printf "- solutions: %d\n" (HashSet.size $ solutionsOf tracking)
    ++ Print.printf "- area: %d\n" (solutionsOf tracking & HashSet.toList & boundingRect & area )
    ++ Print.printf "- by_sectors: %s\n" (show $ bySectorCount wm tracking)
    ++ Print.printf "- best estimation: %s\n" (show $ bestPositionEstimation tracking)

updateTracking :: WorldMap -> [Constraint] -> Tracking -> Tracking
updateTracking wm constraints (Tracking trails)
    = constraints
    & foldl update (HashSet.toList trails)
    & HashSet.fromList
    & resetIfEmpty
    & Tracking
    where
        update trails constraint = concatMap (processTrailWithConstraint constraint wm) trails
        resetIfEmpty trails = if null trails
            then HashSet.map mkTrail (water wm)
            else trails

bestPositionEstimation :: Tracking -> PositionEstimation
bestPositionEstimation (Tracking trails) =
    let byPositions = trails
                    & HashSet.toList
                    & List.map trailPosition
                    & List.foldr (Map.alter $ Just . Maybe.maybe 1 (1 +)) Map.empty
                    :: Map.Map Point Int
        count k _ = explosionDelta
                       & List.map (k •+∆)
                       & Maybe.mapMaybe (`Map.lookup` byPositions)
                       & List.sum
        blur = byPositions
             & Map.mapWithKey count
        best = blur
             & Map.toList
             & List.maximumBy (compare `on` snd)
    in PositionEstimation
        (fst best)
        (fromIntegral (snd best) / fromIntegral (HashSet.size trails))

bySectorCount :: WorldMap -> Tracking -> Map.Map Sector Int
bySectorCount wm (Tracking trails)
    = trails
    & HashSet.toList
    & List.map trailPosition
    & List.map (toSector wm)
    & List.foldr (Map.alter $ Just . Maybe.maybe 1 (1 +)) Map.empty

solutionsOf :: Tracking -> HashSet.HashSet Point
solutionsOf (Tracking trails) = HashSet.map trailPosition trails

minesOf :: Tracking -> HashSet.HashSet Point
minesOf (Tracking trails)
    = HashSet.toList trails
    & map trailMines
    & HashSet.unions

processTrailWithConstraint :: Constraint -> WorldMap -> ShipTrail -> [ShipTrail]
processTrailWithConstraint constraint wm trail =
    case constraint of
        CanMove paths -> Maybe.mapMaybe (moveTrail wm trail) paths
        MineDropped -> [addMine target trail | target <- neighborsOf wm (trailPosition trail)]
        MineTrigged target -> [removeMine target trail | hasMine target trail]
        AtPositions positions -> [trail | trailPosition trail `elem` positions]
        NotAtPositions positions -> [trail | trailPosition trail `notElem` positions]
        InsideTorpedoRange target -> [trail | targetsContain targets current]
                                        where
                                            targets = targetsAt wm target
                                            current = trailPosition trail
        OutsideTorpedoRange target -> [trail | not $ targetsContain targets current]
                                        where
                                            targets = targetsAt wm target
                                            current = trailPosition trail
        InSector sector -> [trail | sector == toSector wm (trailPosition trail)]
        NotInSector sector -> [trail | sector /= toSector wm (trailPosition trail)]
        Reset        -> [resetTrail trail]


canMove :: [Path] -> Constraint
canMove = CanMove

canMoveTo :: Direction -> Constraint
canMoveTo direction = canMove [pathOn direction]

canMoveWithSilence :: Constraint
canMoveWithSilence = canMove silencePaths

mineDropped :: Constraint
mineDropped = MineDropped

mineTrigged :: Point -> Constraint
mineTrigged = MineTrigged

inPosition :: Point -> Constraint
inPosition position =  AtPositions [position]

atPositions :: [Point] -> Constraint
atPositions =  AtPositions

inExplosion :: Point -> Constraint
inExplosion target = explosionDelta & map (target •+∆) & atPositions

inBlastExplosion :: Point -> Constraint
inBlastExplosion target = explosionBlastDelta & map (target •+∆) & atPositions

inTorpedoRange :: Point -> Constraint
inTorpedoRange = InsideTorpedoRange

notInPosition :: Point -> Constraint
notInPosition position = NotAtPositions [position]

notAtPositions :: [Point] -> Constraint
notAtPositions = NotAtPositions

notInExplosion :: Point -> Constraint
notInExplosion target = explosionDelta & map (target •+∆) & notAtPositions

notInBlastExplosion :: Point -> Constraint
notInBlastExplosion target = explosionBlastDelta & map (target •+∆) & notAtPositions

notInTorpedoRange :: Point -> Constraint
notInTorpedoRange = OutsideTorpedoRange

inSector :: Sector -> Constraint
inSector = InSector

notInSector :: Sector -> Constraint
notInSector = NotInSector

reset :: Constraint
reset = Reset


{- | Power
-}
data Power
    = Mine
    | Silence
    | Sonar
    | Torpedo
    deriving (Show, Read, Eq)

renderPower :: Power -> String
renderPower Mine    = "MINE"
renderPower Silence = "SILENCE"
renderPower Sonar   = "SONAR"
renderPower Torpedo = "TORPEDO"

powerP :: Parser Power
powerP = Parsec.choice [
    do { symbolP "MINE"; return Mine },
    do { symbolP "SILENCE"; return Silence },
    do { symbolP "SONAR"; return Sonar },
    do { symbolP "TORPEDO"; return Torpedo }]


{- | OrderItem
-}
data OrderItem
    = OrderDropMine Direction
    | OrderMessage String
    | OrderMove Direction Power
    | OrderSilence Direction Int
    | OrderSonar Sector
    | OrderSurface
    | OrderTorpedo Point
    | OrderTriggerMine Point
    deriving (Show, Read, Eq)

renderOrderItem :: OrderItem -> String
renderOrderItem (OrderDropMine direction)
    = Print.printf "MINE %s" (renderDirection direction)
renderOrderItem (OrderMessage content)
    = Print.printf "MSG %s" content
renderOrderItem (OrderMove direction power)
    = Print.printf "MOVE %s %s" (renderDirection direction) (renderPower power)
renderOrderItem (OrderSilence direction distance)
    = Print.printf "SILENCE %s %d" (renderDirection direction) distance
renderOrderItem (OrderSonar sector)
    = Print.printf "SONAR %s" (renderSector sector)
renderOrderItem OrderSurface
    = "SURFACE"
renderOrderItem (OrderTorpedo target)
    = Print.printf "TORPEDO %s" (renderPoint target)
renderOrderItem (OrderTriggerMine target)
    = Print.printf "TRIGGER %s" (renderPoint target)

orderItemP :: Parser OrderItem
orderItemP = Parsec.choice [
    do { symbolP "MSG"; OrderMessage <$> stringP },
    do { symbolP "MINE"; OrderDropMine <$> directionP },
    do { symbolP "MOVE"; OrderMove <$> directionP <*> powerP },
    do { symbolP "SILENCE"; OrderSilence <$> directionP <*> numberP },
    do { symbolP "SONAR"; OrderSonar <$> sectorP },
    do { symbolP "SURFACE"; return OrderSurface },
    do { symbolP "TORPEDO"; OrderTorpedo <$> pointP },
    do { symbolP "TRIGGER"; OrderTriggerMine <$> pointP }]


{- | Orders
-}
newtype Orders
    = Orders [OrderItem]
    deriving (Show, Read, Eq)

emptyOrders :: Orders
emptyOrders = Orders []

ordersWith :: [OrderItem] -> Orders
ordersWith = Orders

orderItemsOf :: Orders -> [OrderItem]
orderItemsOf (Orders items) = items

addOrder :: OrderItem -> Orders -> Orders
addOrder order (Orders orders) = Orders (orders ++ [order])

explosionsInOrders :: Orders -> [Point]
explosionsInOrders (Orders items) =
    let
        explosions []                           = []
        explosions (OrderTorpedo target:xo)     = target: explosions xo
        explosions (OrderTriggerMine target:xo) = target: explosions xo
        explosions (_:xo)                       = explosions xo
    in explosions items

sonarInOrders :: Orders -> Maybe Sector
sonarInOrders (Orders items) =
    let
        sonar []                    = Nothing
        sonar (OrderSonar sector:_) = Just sector
        sonar (_:xo)                = sonar xo
    in sonar items

surfaceInOrders :: Orders -> Bool
surfaceInOrders (Orders items) = OrderSurface `elem` items

renderOrders :: Orders -> String
renderOrders (Orders items)
    = items
    & map renderOrderItem
    & List.intersperse " | "
    & List.concat

ordersP :: Parser Orders
ordersP = do
    r <- Parsec.sepBy orderItemP (symbolP "|")
    return $ ordersWith r

parseOrders :: String -> Orders
parseOrders text
    = Parsec.parse ordersP "<string>" text
    & Either.fromRight emptyOrders


{- | IntelligenceReportItem
-}
data IntelligenceReportItem
    = ReportNothing
    | ReportMineDropped
    | ReportMineTrigged Point
    | ReportMove Direction
    | ReportSilence
    | ReportSonar Sector
    | ReportSurface Sector
    | ReportTorpedo Point
    deriving (Show, Read, Eq)

renderReportItem :: IntelligenceReportItem -> String
renderReportItem ReportNothing
    = "NA"
renderReportItem ReportMineDropped
    = "MINE"
renderReportItem (ReportMineTrigged target)
    = Print.printf "TRIGGER %s" (renderPoint target)
renderReportItem (ReportMove direction)
    = Print.printf "MOVE %s" (renderDirection direction)
renderReportItem ReportSilence
    = "SILENCE"
renderReportItem (ReportSonar sector)
    = Print.printf "SONAR %s" (renderSector sector)
renderReportItem (ReportSurface sector)
    = Print.printf "SURFACE %s" (renderSector sector)
renderReportItem (ReportTorpedo target)
    = Print.printf "TORPEDO %s" (renderPoint target)

reportItemP :: Parser IntelligenceReportItem
reportItemP = Parsec.choice [
    do { symbolP "NA"; return ReportNothing },
    do { symbolP "MINE"; return ReportMineDropped },
    do { symbolP "MOVE"; ReportMove <$> directionP },
    do { symbolP "SILENCE"; return ReportSilence },
    do { symbolP "SONAR"; ReportSonar <$> sectorP },
    do { symbolP "SURFACE"; ReportSurface <$> sectorP },
    do { symbolP "TORPEDO"; ReportTorpedo <$> pointP },
    do { symbolP "TRIGGER"; ReportMineTrigged <$> pointP }]


{- | IntelligenceReport
-}
newtype IntelligenceReport
    = IntelligenceReport [IntelligenceReportItem]
    deriving (Show, Read, Eq)

emptyReport :: IntelligenceReport
emptyReport = IntelligenceReport []

reportWith :: [IntelligenceReportItem] -> IntelligenceReport
reportWith = IntelligenceReport

reportItemsOf :: IntelligenceReport -> [IntelligenceReportItem]
reportItemsOf (IntelligenceReport items) = items

explosionsInReport :: IntelligenceReport -> [Point]
explosionsInReport (IntelligenceReport items) =
    let
        explosions []                            = []
        explosions (ReportTorpedo target:xo)     = target: explosions xo
        explosions (ReportMineTrigged target:xo) = target: explosions xo
        explosions (_:xo)                        = explosions xo
    in explosions items

sonarInReport :: IntelligenceReport -> Maybe Sector
sonarInReport (IntelligenceReport items) =
    let
        sonar []                     = Nothing
        sonar (ReportSonar sector:_) = Just sector
        sonar (_:xo)                 = sonar xo
    in sonar items

surfaceInReport :: IntelligenceReport -> Bool
surfaceInReport (IntelligenceReport items) =
    let
        surface []                  = False
        surface (ReportSurface _:_) = True
        surface (_:xo)              = surface xo
    in surface items

renderReport :: IntelligenceReport -> String
renderReport (IntelligenceReport items)
    = items
    & map renderReportItem
    & List.intersperse " | "
    & List.concat

reportP :: Parser IntelligenceReport
reportP = do
    r <- Parsec.sepBy reportItemP (symbolP "|")
    return $ IntelligenceReport r

parseReport :: String -> IntelligenceReport
parseReport text
    = Parsec.parse reportP "<string>" text
    & Either.fromRight emptyReport


{- | SonarEcho
-}
data SonarEcho
    = NA
    | Hit
    | Miss
    deriving (Show, Read, Eq)

renderSonarEcho :: SonarEcho -> String
renderSonarEcho NA   = "NA"
renderSonarEcho Hit  = "Y"
renderSonarEcho Miss = "N"

sonarEchoP :: Parser SonarEcho
sonarEchoP = Parsec.choice [
    do { symbolP "NA"; return NA },
    do { symbolP "Y"; return Hit },
    do { symbolP "N"; return Miss }]

parseSonarEcho :: String -> SonarEcho
parseSonarEcho text
    = Parsec.parse sonarEchoP "<string>" text
    & Either.fromRight NA


{- | LifeStatus
-}
data LifeStatus
    = LifeStatus Int Int
    deriving (Show, Read, Eq)

mkLifeStatus :: Int -> LifeStatus
mkLifeStatus value = LifeStatus value value

updateLifeStatus :: Int -> LifeStatus -> LifeStatus
updateLifeStatus new (LifeStatus current _) = LifeStatus new current

lifeLost :: LifeStatus -> Int
lifeLost (LifeStatus current previous) = previous - current


{- | Subsystem
-}
data Subsystem
    = Subsystem Int Int Int Int
    deriving (Show, Read, Eq)

defaultSubsystem :: Subsystem
defaultSubsystem = Subsystem 4 (-1) (-1) (-1)

isReady :: Power -> Subsystem -> Bool
isReady Mine (Subsystem _ _ _ mine)       = mine == 0
isReady Silence (Subsystem _ _ silence _) = silence == 0
isReady Sonar (Subsystem _ sonar _ _)     = sonar == 0
isReady Torpedo (Subsystem torpedo _ _ _) = torpedo == 0

isAvailable :: Power -> Subsystem -> Bool
isAvailable Mine (Subsystem _ _ _ mine)       = mine >= 0
isAvailable Silence (Subsystem _ _ silence _) = silence >= 0
isAvailable Sonar (Subsystem _ sonar _ _)     = sonar >= 0
isAvailable Torpedo _                         = True


{- | TurnData
-}
data TurnData = TurnData
    { turnMyPosition   :: Point
    , turnMyLife       :: Int
    , turnMySonarEcho  :: SonarEcho
    , turnMySubsystems :: Subsystem
    , turnOpponentLife :: Int
    , turnIntelligence :: IntelligenceReport
    }

-- readTurnData :: IO TurnData
-- readTurnData = do
--     status_line <- getLine
--     sonar_line <- getLine
--     intelligence_line <- getLine

--     let input = words input_line
--     let x = read (input!!0) :: Int
--     let y = read (input!!1) :: Int
--     let mylife = read (input!!2) :: Int
--     let opplife = read (input!!3) :: Int
--     let torpedocooldown = read (input!!4) :: Int
--     let sonarcooldown = read (input!!5) :: Int
--     let silencecooldown = read (input!!6) :: Int
--     let minecooldown = read (input!!7) :: Int
--     input_line <- getLine
--     let sonarresult = input_line :: String
--     opponentorders <- getLine


{- | SituationAwarness
-}
data SituationAwarness = SituationAwarness
    { theWorldmap      :: WorldMap
    , myPosition       :: Point
    , myLife           :: LifeStatus
    , myOrders         :: Orders
    , mySubsystems     :: Subsystem
    , myEcho           :: SonarEcho
    , myTrail          :: ShipTrail
    , myTracking       :: Tracking
    , opponentLife     :: LifeStatus
    , opponentTracking :: Tracking
    , intelligence     :: IntelligenceReport
    }

initialSituation :: WorldMap -> SituationAwarness
initialSituation wm
    = SituationAwarness
    { theWorldmap = wm
    , myPosition = startingPosition
    , myLife = mkLifeStatus 6
    , myOrders = emptyOrders
    , mySubsystems = defaultSubsystem
    , myEcho = NA
    , myTrail = mkTrail startingPosition
    , myTracking = mkTracking wm
    , opponentLife = mkLifeStatus 6
    , opponentTracking = mkTracking wm
    , intelligence = emptyReport
    }
    where
        startingPosition = wm & biggestWaterZone & center

bestOpponentPosition :: SituationAwarness -> Maybe Point
bestOpponentPosition situation =
    let PositionEstimation position estimation = situation
                                               & opponentTracking
                                               & bestPositionEstimation
    in if estimation > 0.1 then Just position else Nothing

shouldSonar :: SituationAwarness -> Maybe Sector
shouldSonar situation =
    let bySector = situation
                 & opponentTracking
                 & bySectorCount (theWorldmap situation)
    in if Map.size bySector == 1
        then Nothing
        else bySector
             & Map.toList
             & List.maximumBy (compare `on` snd)
             & fst
             & Just

shouldTorpedo :: SituationAwarness -> Maybe Point
shouldTorpedo situation
    = case bestOpponentPosition situation of
        Nothing -> Nothing
        Just target ->
            let
                targets = targetsAt (theWorldmap situation) target
            in if target `elem` safePositions targets
                then Just target
                else Nothing


updateMyOrders :: Orders -> SituationAwarness -> SituationAwarness
updateMyOrders orders situation
    = situation { myOrders = orders }

updateTurn :: TurnData -> SituationAwarness -> SituationAwarness
updateTurn turn situation
    = situation
        { myPosition = turnMyPosition turn
        , myTrail = addTrail (turnMyPosition turn) (myTrail situation)
        , myLife = updateLifeStatus (turnMyLife turn) (myLife situation)
        , myEcho = turnMySonarEcho turn
        , mySubsystems = turnMySubsystems turn
        , opponentLife = updateLifeStatus (turnOpponentLife turn) (opponentLife situation)
        , intelligence = turnIntelligence turn
        }
    & processTracking

processTracking :: SituationAwarness -> SituationAwarness
processTracking situation =
    let
        wm = theWorldmap situation
        my = myConstraints
                (myLife situation)
                (myOrders situation)
                (intelligence situation)
                (toSector wm (myPosition situation))
        opponent = opponentConstraints
                (opponentLife situation)
                (myOrders situation)
                (intelligence situation)
                (myEcho situation)
    in
        situation
            { myTracking = myTracking situation & updateTracking wm my
            , opponentTracking = opponentTracking situation & updateTracking wm opponent
            }

explosionsConstraints ::
    Int -> Orders -> IntelligenceReport -> [Point] -> [Constraint]
explosionsConstraints lost orders report targets =
    let
        explosions = explosionsInOrders orders ++ explosionsInReport report
    in
        case (lost, length explosions) of
            (0, _) -> map notInExplosion targets
            (1, 1) -> map inBlastExplosion targets
            (1, _) -> map notInPosition targets
            (2, 1) -> map inPosition targets
            _      -> []

opponentConstraints ::
    LifeStatus -> Orders -> IntelligenceReport -> SonarEcho -> [Constraint]
opponentConstraints life orders report echo =
    let
        lifelost = lifeLost life - (if surfaceInReport report then 1 else 0)
        explosions = explosionsConstraints lifelost orders report

        sonarConstraints =
            case (sonarInOrders orders, echo) of
                (Just sector, Hit)  -> [inSector sector]
                (Just sector, Miss) -> [notInSector sector]
                _                   -> []

        itemConstraints item =
            case item of
                ReportMove direction     -> [canMoveTo direction]
                ReportSilence            -> [canMoveWithSilence]
                ReportSurface sector     -> [inSector sector, reset]
                ReportTorpedo target     -> inTorpedoRange target : explosions [target]
                ReportMineDropped        -> [mineDropped]
                ReportMineTrigged target -> mineTrigged target : explosions [target]
                _                        -> []

    in
        sonarConstraints
        ++ explosions (explosionsInOrders orders)
        ++ List.concatMap itemConstraints (reportItemsOf report)

myConstraints ::
    LifeStatus -> Orders -> IntelligenceReport -> Sector -> [Constraint]
myConstraints life orders report mySector =
    let
        lifelost = lifeLost life - (if surfaceInOrders orders then 1 else 0)
        explosions = explosionsConstraints lifelost orders report

        sonarConstraints
            = case sonarInReport report of
                Just s -> if s == mySector then [inSector s] else [notInSector s]
                _      -> []

        itemConstraints item =
            case item of
                OrderDropMine _         -> [mineDropped]
                OrderMove direction _   -> [canMoveTo direction]
                OrderSilence _ _        -> [canMoveWithSilence]
                OrderSurface            -> [inSector mySector, reset]
                OrderTorpedo target     -> inTorpedoRange target : explosions [target]
                OrderTriggerMine target -> mineTrigged target : explosions [target]
                _                       -> []

    in
        sonarConstraints
        ++ explosions (explosionsInReport report)
        ++ List.concatMap itemConstraints (orderItemsOf orders)
