# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import FrozenSet, List, Set, Text

from geometry import Position


@dataclass(frozen=True)
class Mine:
    positions: FrozenSet[Position]

    @staticmethod
    def on(position: Position) -> Mine:
        return Mine(frozenset([position]))

    @staticmethod
    def entanglement(positions: List[Position]) -> Mine:
        return Mine(frozenset(positions))

    def __contains__(self, position: Position) -> bool:
        return position in self.positions


@dataclass(frozen=True)
class ShipTrail:
    current: Position
    visited: FrozenSet[Position]
    mines: FrozenSet[Mine]

    @staticmethod
    def on(initial: Position) -> ShipTrail:
        return ShipTrail(initial, frozenset([initial]), frozenset())

    def __contains__(self, position: Position) -> bool:
        return position in self.visited

    def __len__(self) -> int:
        return len(self.visited)

    def last(self) -> Position:
        return self.current

    def move(self, position: Position) -> ShipTrail:
        return ShipTrail(position, self.visited | frozenset([position]), self.mines)

    def mine(self, mine: Mine) -> ShipTrail:
        return ShipTrail(
            self.current,
            self.visited,
            self.mines | frozenset([mine]))

    def remove_mine(self, target: Position) -> ShipTrail:
        return ShipTrail(
            self.current,
            self.visited,
            frozenset(mine for mine in self.mines if target not in mine))

    def clear(self) -> ShipTrail:
        return ShipTrail(self.current, frozenset([self.current]), self.mines)

    def has_mine(self, position: Position) -> bool:
        return any(position in mine for mine in self.mines)

    def all_mines(self) -> FrozenSet[Position]:
        result: Set[Position] = set()
        for mine in self.mines:
            result |= mine.positions
        return frozenset(result)

    def __str__(self) -> Text:
        return "ShipTrail(" + ", ".join(str(p) for p in self.visited) + ")"

    def __repr__(self) -> Text:
        return f"ShipTrail({repr(self.current)}, {repr(self.visited)})"
