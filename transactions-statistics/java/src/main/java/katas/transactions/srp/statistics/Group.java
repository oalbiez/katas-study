package katas.transactions.srp.statistics;

import katas.transactions.Transaction;
import katas.transactions.srp.ast.ValueProvider;
import katas.transactions.value.Value;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.transformValues;
import static java.util.Collections.unmodifiableMap;
import static katas.transactions.value.Values.map;

public final class Group implements Statistic {
    private final ValueProvider<String> key;
    private final Statistic exemplar;
    private final Map<String, Statistic> statistics = newHashMap();

    Group(final ValueProvider<String> key, final Statistic exemplar) {
        this.key = key;
        this.exemplar = exemplar;
    }

    public void updateWith(final Transaction transaction) {
        statisticFor(key.get(transaction)).updateWith(transaction);
    }

    public void accept(final StatisticVisitor visitor) {
        visitor.visitGroup(this);
    }

    public Statistic statisticFor(final String category) {
        if (statistics.containsKey(category)) {
            return statistics.get(category);
        }
        final Statistic value = exemplar.fork();
        statistics.put(category, value);
        return value;
    }

    public Map<String, Statistic> stats() {
        return unmodifiableMap(statistics);
    }

    public Statistic fork() {
        return new Group(key, exemplar.fork());
    }

    public String describe() {
        return exemplar.describe() + " by " + key.describe();
    }

    public Value value() {
        return map(transformValues(statistics, Statistic::value));
    }
}
