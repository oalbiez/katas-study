
import Prelude

divise :: Int -> Int -> Bool
divise value divisor = value `mod` divisor == 0


translate :: Int -> String
translate n | n == 3  = "Fizz"
            | n == 5  = "Buzz"


fizz :: Int -> String
fizz n | divise n 15 = "FizzBuzz"
       | divise n 3  = "Fizz"
       | divise n 5  = "Buzz"
       | otherwise   = show n

--fizz2 :: Int -> String
fizz2 n = show (concat [translate x | x <- [3, 5], (divise n x)])


main :: IO()
main = mapM_ putStrLn $ map fizz2 [1..100]
