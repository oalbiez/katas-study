package katas.marsrover;

public class Distance {
    private final long dx;
    private final long dy;

    public static Distance distance(long dx, long dy) {
        return new Distance(dx, dy);
    }

    private Distance(long dx, long dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public long dx() {
        return dx;
    }

    public long dy() {
        return dy;
    }

    public Distance reverse() {
        return distance(-dx, -dy);
    }
}
