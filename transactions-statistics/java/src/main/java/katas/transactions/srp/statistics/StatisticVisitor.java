package katas.transactions.srp.statistics;

public interface StatisticVisitor {
    void visitAll(All statistic);

    void visitCount(Count statistic);

    void visitSum(SumOf statistic);

    void visitGroup(Group statistic);

    void visitSelect(Select statistic);

    void visitComposite(Composite statistic);
}
