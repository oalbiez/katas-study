package katas.transactions.pic;

import katas.transactions.Transaction;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static java.time.format.DateTimeFormatter.ISO_TIME;
import static org.junit.Assert.assertEquals;

public class ComputePicTest {
    @Test
    public void compute_pic_should___() {
        assertEquals("Pic{amount=700, instant=08:00:00}", computePicFor(transaction(700, LocalTime.of(8, 0, 0))));
    }

    @Test
    public void compute_pic_should___2() {
        assertEquals("Pic{amount=500, instant=08:00:00}", computePicFor(transaction(500, LocalTime.of(8, 0, 0))));
    }

    @Test
    public void compute_pic_should___3() {
        assertEquals("Pic{amount=500, instant=08:03:00}", computePicFor(transaction(500, LocalTime.of(8, 3, 0))));
    }

    @Test
    public void compute_pic_should___4() {
        assertEquals("Pic{amount=1500, instant=09:15:00}", computePicFor(transaction(500, LocalTime.of(8, 3, 0)), transaction(1000, LocalTime.of(9, 15, 0))));
    }

    @Test
    public void compute_pic_should___5() {
        assertEquals("Pic{amount=500, instant=08:03:00}", computePicFor(transaction(500, LocalTime.of(8, 3, 0)), transaction(-100, LocalTime.of(9, 15, 0))));
    }

    @Test
    public void compute_pic_should___6() {
        assertEquals("Pic{amount=500, instant=09:25:00}", computePicFor(
                transaction(400, LocalTime.of(8, 0, 0)),
                transaction(-100, LocalTime.of(9, 15, 0)),
                transaction(200, LocalTime.of(9, 25, 0))

        ));
    }

    @Test
    public void compute_pic_should___7() {
        assertEquals("Pic{amount=-400, instant=08:00:00}", computePicFor(
                transaction(-400, LocalTime.of(8, 0, 0)),
                transaction(-100, LocalTime.of(9, 15, 0)),
                transaction(-200, LocalTime.of(9, 25, 0))

        ));
    }

    @Test
    public void compute_pic_should___71() {
        assertEquals("Pic{amount=-400, instant=08:00:00}", computePicFor(
                transaction(-400, LocalTime.of(8, 0, 0)),
                transaction(-100, LocalTime.of(9, 15, 0))
        ));
    }

    @Test
    public void compute_pic_should___8() {
        assertEquals("Pic{amount=-400, instant=08:00:00}", computePicFor(
                transaction(-400, LocalTime.of(8, 0, 0)),
                transaction(100, LocalTime.of(9, 15, 0)),
                transaction(-200, LocalTime.of(9, 15, 0))
        ));
    }


    private static Transaction transaction(final int amount, final LocalTime time) {
        return Transaction.transaction("PAREL", "12", amount, LocalDate.of(2018, 7, 25).atTime(time));
    }

    private static String computePicFor(final Transaction... transactions) {
        final PicCalculator calculator = new PicCalculator();
        for (final Transaction transaction : transactions) {
            calculator.update(transaction);
        }
        return renderPic(calculator.pic());
    }

    private static String renderPic(final Pic pic) {
        return "Pic{" + "amount=" + pic.amount() + ", instant=" + pic.instant().toLocalTime().format(ISO_TIME) + '}';
    }

}
