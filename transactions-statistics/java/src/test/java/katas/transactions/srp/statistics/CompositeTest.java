package katas.transactions.srp.statistics;

import com.google.common.base.Joiner;
import katas.transactions.Transaction;
import katas.transactions.value.Value;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static katas.transactions.Transaction.transaction;
import static katas.transactions.srp.statistics.Statistics.composite;
import static org.junit.Assert.assertEquals;

public class CompositeTest {
    @Test
    public void should_propagate_update_on_inner_statistics() {
        final RecorderStatistics recorder = new RecorderStatistics();
        composite(recorder).updateWith(transaction("SG", "A", 150, LocalDate.of(2017, Month.APRIL, 17).atStartOfDay()));
        assertEquals("transaction(SG, A, 150, 2017-04-17T00:00:00)", recorder.events());
    }

    class RecorderStatistics implements Statistic {
        List<String> events = newArrayList();

        public void updateWith(final Transaction transaction) {
            events.add(transaction.render());
        }

        public void accept(final StatisticVisitor visitor) {
        }

        public Statistic fork() {
            return new RecorderStatistics();
        }

        public String describe() {
            return null;
        }

        public Value value() {
            return null;
        }

        String events() {
            return Joiner.on(", ").join(events);
        }
    }
}