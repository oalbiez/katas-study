from collections import namedtuple
from hangman import word
from .errors import WordTooSmall, WordWithInvalidLetter


class LetterToGuess(object):
    def __init__(self, letter):
        self.__letter = letter

    @property
    def letter(self):
        return self.__letter

    def render(self, known_letters):
        if self.is_discovered(known_letters):
            return self.__letter
        return '_'

    def is_discovered(self, known_letters):
        return self.__letter in known_letters


VALID_LETTERS = "abcdefghijklmnopqrstuvwxyz"


class WordToGuess(object):
    def __init__(self, word_to_guess):
        self.__word = word_to_guess.lower()
        if len(self.__word) < 4:
            raise WordTooSmall("Word too small")
        invalid_letters = [repr(c) for c in self.__word if c not in VALID_LETTERS]
        if len(invalid_letters) > 0:
            raise WordWithInvalidLetter(invalid_letters)
        self.__initial = word.initial(self.__word)
        self.__middle = word.middle(self.__word)
        self.__final = word.final(self.__word)
        self.__characters = [LetterToGuess(c) for c in self.__middle]

    @property
    def word(self):
        return self.__word

    def render(self, letters):
        return self.__initial + "".join(c.render(letters) for c in self.__characters) + self.__final

    def is_discovered(self, words, letters):
        return self.__word in words or all(c.is_discovered(letters) for c in self.__characters)

    def errors(self, words, letters):
        word_errors = len(set(words) - set([self.__word]))
        letter_errors = len(set(letters) - set(self.__middle))
        return word_errors + letter_errors


HangmanTurn = namedtuple('HangmanTurn', 'puzzle errors won lost')


class Hangman(object):
    def __init__(self, word_to_guess, max_errors=3):
        self.__word_to_guess = word_to_guess
        self.__letters = set()
        self.__words = set()
        self.__max_errors = max_errors

    def test_a_letter(self, letter_to_test):
        self.__letters.add(letter_to_test)

    def test_a_word(self, word_to_test):
        self.__words.add(word_to_test)

    def errors(self):
        return self.__word_to_guess.errors(self.__words, self.__letters)

    def puzzle(self):
        return self.__word_to_guess.render(self.__letters)

    def is_won(self):
        return self.__word_to_guess.is_discovered(self.__words, self.__letters)

    def is_lost(self):
        return self.errors() >= self.__max_errors

    def turn(self):
        if self.is_lost():
            return HangmanTurn(self.__word_to_guess.word, self.errors(), self.is_won(), self.is_lost())
        return HangmanTurn(self.puzzle(), self.errors(), self.is_won(), self.is_lost())
