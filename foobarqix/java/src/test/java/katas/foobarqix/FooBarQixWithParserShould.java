package katas.foobarqix;

public class FooBarQixWithParserShould extends FooBarQixShould {
    private final static FooBarQixWithParser RULES = FooBarQixWithParser.rules("3 => Foo; 5 => Bar; 7 => Qix");

    String fooBarQix(final int input) {
        return RULES.compute(input);
    }
}
